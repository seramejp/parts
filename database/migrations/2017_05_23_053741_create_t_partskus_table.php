<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTPartskusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('t_partskus', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('partsku');
            $table->string('oldsku')->nullable();
            $table->text('relatedsku')->nullable();

            $table->string('supplier')->nullable();
            $table->string('parent')->nullable();
            
            $table->string('xsell')->nullable();
            $table->text('description')->nullable();
            $table->string('whlocation')->nullable();
            $table->string('bulklocation')->nullable();
            $table->integer('qtyinbulkloc')->default(0);
            $table->integer('partweight')->default(0);
            $table->integer('partlength')->default(0);
            $table->integer('partheight')->default(0);
            $table->integer('partwidth')->default(0);
            $table->string('shippingmethod')->nullable();
            $table->float('price', 15, 2)->default(0);
            $table->string('photo')->default('qmbyjpserame.png');
            $table->text('remarks')->nullable();
            $table->text('notes')->nullable();
            $table->string('barcode')->nullable();
            $table->string('wiseitemid')->nullable();
            

            $table->string('partsorpurchasing')->nullable();
            $table->string('dhlorcontainer')->nullable();

            $table->boolean('needsordering')->default(false);
            $table->integer('sys_addedby')->default(0);
            $table->integer('sys_lasttouch')->default(0);
            $table->boolean('sys_isactive')->default(false);
            $table->boolean('sys_deactivate')->default(false);
            $table->string('sys_status')->nullable();
            $table->boolean('sys_indb')->default(false);
            $table->boolean('processed')->default(false);


            //STOCKS 
            $table->integer('soh')->default(0);
            $table->integer('restockqty')->default(0);
            $table->integer('reorderqty')->default(0);
            $table->integer('salespast150d')->default(0);
            $table->integer('onorderqty')->default(0);


            $table->string('eta')->nullable();

            $table->integer('inmagento')->default(0);
            $table->integer('inneto')->default(0);
            $table->integer('inwise')->default(0);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //t_partskus
        Schema::dropIfExists('t_partskus');
    }
}
