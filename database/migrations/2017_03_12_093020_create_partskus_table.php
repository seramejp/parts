<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartskusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partskus', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('partsku');
            $table->string('oldsku')->nullable();
            $table->text('relatedsku')->nullable();

            $table->integer('supplier_id')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->default(0);
            /*$table->integer('supplierorder_id')->nullable();*/

            $table->string('suppcode')->nullable();
            $table->string('parentcode')->nullable();
            
            $table->string('xsell')->nullable();
            $table->text('descr')->nullable();
            $table->string('whloc')->nullable();
            $table->string('bulkloc')->nullable();
            $table->integer('qtyinbulkloc')->default(0);
            $table->float('partwt')->default(0);
            $table->float('partlen')->default(0);
            $table->float('partht')->default(0);
            $table->float('partwidth')->default(0);
            $table->string('shipmethod')->nullable();
            $table->float('price', 15, 2)->default(0);
            $table->string('photo')->default('qmbyjpserame.png');
            $table->text('remarks')->nullable();
            $table->text('notes')->nullable();
            $table->string('barcode')->nullable();
            $table->string('wiseitemid')->nullable();
            $table->string('spordernum')->nullable();

            /*$table->integer('soh')->default(0);
            $table->integer('restockqty')->default(0);
            $table->integer('reorderqty')->default(0);
            $table->integer('salespast150d')->default(0);
            $table->integer('qtyinbulkloc')->default(0);
            $table->integer('onorderqty')->default(0);*/

            $table->string('ispurchasing')->nullable();
            $table->string('isdhl')->nullable();

            $table->boolean('needsordering')->default(false);
            $table->integer('sys_addedby')->default(0);
            $table->integer('sys_lasttouch')->default(0);
            $table->boolean('sys_isactive')->default(false);
            /*$table->string('sys_status')->default("Pending");*/
            $table->boolean('sys_deactivate')->default(false);
            /*$table->string('sys_finalstatus')->default("Unresolved");*/

            $table->date('eta')->nullable();

            $table->integer('inmagento')->default(0);
            $table->integer('inneto')->default(0);
            $table->integer('inwise')->default(0);

            $table->boolean('haschdescr')->default(0);
            $table->boolean('haschwiseitemid')->default(0);
            $table->boolean('haschparent')->default(0);
            $table->boolean('haschsuppcode')->default(0);
            $table->boolean('hascholdsku')->default(0);
            $table->boolean('haschwhloc')->default(0);
            $table->boolean('haschqtyinbulkloc')->default(0);
            $table->boolean('haschbarcode')->default(0);
            $table->boolean('haschnotes')->default(0);
            $table->boolean('haschstatus')->default(0);
            $table->boolean('haschpartlen')->default(0);
            $table->boolean('haschpartwd')->default(0);
            $table->boolean('haschpartht')->default(0);
            $table->boolean('haschpartwt')->default(0);
            $table->boolean('haschshipmethod')->default(0);
            $table->boolean('haschpartprice')->default(0);
            $table->boolean('haschsoh')->default(0);
            $table->boolean('haschrestockqty')->default(0);
            $table->boolean('haschreorderqty')->default(0);
            $table->boolean('haschsaleslast150d')->default(0);
            $table->boolean('haschorderstatus')->default(0);
            $table->boolean('haschrelatedsku')->default(0);
            $table->boolean('haschbulkloc')->default(0);
            $table->boolean('haschisactive')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partskus');
    }
}
