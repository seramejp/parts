<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExistingordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('existingorders', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('partsku');
            
            $table->text('descr')->nullable();

            $table->integer('supplier_id')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->default(0);

            $table->string('suppcode')->nullable();
            $table->string('parentcode')->nullable();

            $table->string('spnum')->nullable();
            $table->date('eta')->nullable();
            $table->string('orderavenue')->nullable();            
            $table->string('notes')->nullable();            
            $table->integer('packlistreceived')->default(0);            
            $table->date('arrivedsevenhills')->nullable();            
            $table->date('unpackedsevenhills')->nullable();            

            $table->integer('reorderqty')->default(0);
            $table->integer('backorder')->default(0);
            $table->integer('openbox')->default(0);
            $table->integer('specialorder')->default(0);
            $table->integer('rap')->default(0);

            $table->integer('sys_total')->default(0);
            $table->boolean('sys_isactive')->default(false);
            $table->integer('sys_addedby')->default(0);
            $table->integer('sys_lasttouch')->default(0);
            $table->string('sys_finalstatus')->default("Unresolved");

            $table->string('photo')->default('qmbyjpserame.png');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('existingorders');
    }
}
