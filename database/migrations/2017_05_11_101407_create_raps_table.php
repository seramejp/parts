<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('raps', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('partsku');

            $table->text('descr')->nullable();
            $table->text('notes')->nullable();
            $table->string('rmanum')->nullable();
            $table->string('kayakoid')->nullable();
            $table->string('spid')->nullable();
            $table->string('whloc')->nullable();
            $table->string('outcome')->nullable();

            $table->integer('supplier_id')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->default(0);
            $table->string('suppcode')->nullable();
            $table->string('parentcode')->nullable();

            $table->integer('qty')->default(0);
            
            $table->string('sys_orderstatus')->default("New");
            $table->boolean('sys_isactive')->default(false);
            $table->integer('sys_addedby')->default(0);
            $table->integer('sys_lasttouch')->default(0);
            $table->string('sys_finalstatus')->default("Unresolved");

            $table->integer('supplierorder_id')->unsigned()->default(0);

            $table->string('photo')->default('qmbyjpserame.png');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('raps');
    }
}
