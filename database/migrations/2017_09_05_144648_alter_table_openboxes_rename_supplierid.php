<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOpenboxesRenameSupplierid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('openboxes', 'supplier_id')) {
            Schema::table('openboxes', function($table) {
                $table->renameColumn('supplier_id', 'is_archived');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('openboxes', 'is_archived')) {
            Schema::table('openboxes', function($table) {
                $table->renameColumn('is_archived', 'supplier_id');
            });
        }
    }
}
