<?php
/**
*   24.05.17 - changed function topending()
*
*
*/


namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Partsku;
use App\Parentsku;
use App\Supplier;
use App\Stock;
use App\Supplierorder;
use App\Specialorder;
use App\Existingorder;
use App\Openbox;
use App\Rap;
use App\Backorder;

use Auth;
use DB;

class ExistingordersController extends RootController
{
    /**
     * Controller for Parts
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Generate JSON File
     *
     * @return void
     */
    public function generate(){
        return $this->existingordersGenerateJson();
    }


    /**
     * Re-Generate ALL JSON File
     *
     * @return void
     */
    public function generateAllJson(){
        $this->partsGenerateJson();
        $this->supplierordersGenerateJson();
        $this->specialordersGenerateJson();
        $this->existingordersGenerateJson();
        $this->backordersGenerateJson();
        $this->openboxesordersGenerateJson();
        $this->rapsordersGenerateJson();

        return "All Data Generated.";
    }


    /**
     * Load View for Supplierorders Main List
     *
     * @return void
     */
    public function index (){
        return view("existingorders.index");
    }


    public function getOrderKabits (Existingorder $exOrd){
        $tempVal = "";
        if ($exOrd->supplierorders->count() > 0) {
            foreach ($exOrd->supplierorders as $spoTmp) {
                if ($spoTmp->specialorders->count() > 0) {
                    foreach ($spoTmp->specialorders as $spo) {
                        $tempVal .= "<tr>".
                            "<td><a href='/parts/public/specialorders/".$spo->id."/edit'>" . $spo->partsku . "</a></td>" .
                            "<td>" . $spo->descr . "</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td>". $spo->qty."</td>".
                            "</tr>";
                    }
                }

                if ($spoTmp->backorders->count() > 0) {
                    foreach ($spoTmp->backorders as $bko) {
                        $tempVal .= "<tr>".
                            "<td><a href='/parts/public/backorders/".$bko->id."/edit'>" . $bko->partsku . "</a></td>" .
                            "<td>" . $bko->descr . "</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td>". $bko->qty."</td>".
                            "<td> - </td>" .
                            "</tr>";
                    }
                }
                
                if ($spoTmp->raps->count() > 0) {
                    foreach ($spoTmp->raps as $rap) {
                        $tempVal .= "<tr>".
                            "<td><a href='/parts/public/raps/".$rap->id."/edit'>" . $rap->partsku . "</a></td>" .
                            "<td>" . $rap->descr . "</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td>". $rap->qty."</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "</tr>";
                    }
                }

                if ($spoTmp->openboxes->count() > 0) {
                    foreach ($spoTmp->openboxes as $obp) {
                        $tempVal .= "<tr>".
                            "<td><a href='/parts/public/openboxes/".$obp->id."/edit'>" . $obp->partsku . "</a></td>" .
                            "<td>" . $obp->descr . "</td>".
                            "<td> - </td>" .
                            "<td>". $obp->qty."</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "</tr>";
                    }
                }

                if ($spoTmp->stocks->count() > 0) {
                    
                    foreach ($spoTmp->stocks as $psku) {
                        
                        $tempVal .= "<tr>".
                            "<td><a href='/parts/public/partspms/".$psku->partsku_id."/edit'>" . $psku->partsku . "</a></td>" .
                            "<td>" . $psku->partskuIbelong->descr . "</td>".
                            "<td>". $psku->reorderqty."</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "</tr>";
                    }
                }

                
            }
        }
        return $tempVal;
    }


    public function edit(Existingorder $exOrd){

        $theSuppOrds = Supplierorder::where("existingorder_id", $exOrd->id)
            ->get();

        $consolidated = collect();


        foreach ($theSuppOrds as $so) {
            
            $spo = Specialorder::where("supplierorder_id", $so->id)
                ->where("sys_finalstatus", $exOrd->sys_finalstatus)
                ->get();

            if (count($spo) > 0) {
                foreach ($spo as $key => $value) {
                    $consolidated->push($value);
                }    
            }
            
            $bko = Backorder::where("supplierorder_id", $so->id)
                ->where("sys_finalstatus", $exOrd->sys_finalstatus)
                ->get();
            if (count($bko) > 0) {
                foreach ($bko as $key => $value) {
                    $consolidated->push($value);        
                }    
            }

            $opb = Openbox::where("supplierorder_id", $so->id)
                ->where("sys_finalstatus", $exOrd->sys_finalstatus)
                ->get();
            if (count($opb) > 0) {
                foreach ($opb as $key => $value) {
                    $consolidated->push($value);        
                }    
            }

            $rap = Rap::where("supplierorder_id", $so->id)
                ->where("sys_finalstatus", $exOrd->sys_finalstatus)
                ->get();
            if (count($rap) > 0) {
                foreach ($rap as $key => $value) {
                    $consolidated->push($value);        
                }    
            }

            $stk = Stock::where("supplierorder_id", $so->id)
                ->where("sys_finalstatus", $exOrd->sys_finalstatus)
                ->get();
            if (count($stk) > 0) {
                foreach ($stk as $key => $value) {
                    $consolidated->push($value);        
                }    
            }
        }

        /*foreach ($consolidated as $key => $value) {
            get_class($value);
        }*/


        return view('existingorders.edit', compact('exOrd', 'consolidated'));
    }

    /**
     * Load View for Special Orders Main List
     *
     * @return void
     */
    public function getMaxReferenceNumber (){
        $maxrefnum = Openbox::where("sys_isactive", 1)->max("referencenum");

        
        if (empty($maxrefnum)) {
            $maxrefnum = "OBP00001";
        }else{
            $arr = explode("OBP", $maxrefnum);
            $maxrefnum = "OBP" . str_pad(intval($arr[1]) + 1, 5, "0", STR_PAD_LEFT);
        }
        return $maxrefnum;
    }



    /**
     * Save modification by user
     * @param Request $request, Partsku $part the Partsku instance
     * @return void
     */
    public function saveUpdateMain(Request $request, Existingorder $exOrd){
        

        $theSuppOrds = Supplierorder::where("existingorder_id", $exOrd->id)->get();
        if (empty($theSuppOrds)) {
            abort(500, "Something is wrong. Please contact JP and tell him to check Existing Order ID: " . $exOrd->id . ". Thank you!");
        }else{
            foreach ($theSuppOrds as $currentSuppOrd) {
                $currentSuppOrd->sys_finalstatus = $request->sys_finalstatus;
                $currentSuppOrd->save();


                if (count($currentSuppOrd->openboxes) > 0) {
                    foreach ($currentSuppOrd->openboxes as $opb) {
                        $opb->sys_finalstatus = $request->sys_finalstatus;
                        $opb->sys_lasttouch = Auth::user()->id;
                        $opb->save();
                    }
                }

                if (count($currentSuppOrd->stocks) > 0) {
                    foreach ($currentSuppOrd->stocks as $stk) {
                        $stk->sys_finalstatus = $request->sys_finalstatus;
                        $stk->sys_lasttouch = Auth::user()->id;
                        $stk->save();
                    }
                }

                if (count($currentSuppOrd->backorders) > 0) {
                    foreach ($currentSuppOrd->backorders as $bko) {
                        $bko->sys_finalstatus = $request->sys_finalstatus;
                        $bko->sys_lasttouch = Auth::user()->id;
                        $bko->save();
                    }
                }

                if (count($currentSuppOrd->specialorders) > 0) {
                    foreach ($currentSuppOrd->specialorders as $spo) {
                        $spo->sys_finalstatus = $request->sys_finalstatus;
                        $spo->sys_lasttouch = Auth::user()->id;
                        $spo->save();
                    }
                }

                if (count($currentSuppOrd->raps) > 0) {
                    foreach ($currentSuppOrd->raps as $rap) {
                        $rap->sys_finalstatus = $request->sys_finalstatus;
                        $rap->sys_lasttouch = Auth::user()->id;
                        $rap->save();
                    }
                }
            }
        }

        $exOrd->update($request->except(
            'created_at', 'photo', 'packlistreceived'
            ));
        $exOrd->sys_lasttouch = Auth::user()->id;
        $exOrd->save();

        if (!empty($request->packlistreceived)) {
            $exOrd->packlistreceived = 1;
            $exOrd->save();
        }else{
            $exOrd->packlistreceived = 0;
            $exOrd->save();
        }

        $this->generateAllJson();
       
        return redirect('/existingorders');
    }





    /**
     * Load View for Creating a New Special Order
     *
     * @return list of Parents, list of Suppliers
     */
    public function additionalSOFrom(Existingorder $exOrd){
        $supplierorders = Supplierorder::where("suppcode", $exOrd->suppcode)
            ->where("sys_finalstatus", "Pending")
            ->with("specialordersdesc", "backordersdesc", "openboxesdesc", "rapsdesc", "stocksdesc")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();


        return view("existingorders.addso", compact('exOrd', 'supplierorders'));
    }






    public function additionalSPO(Existingorder $exOrd, Request $request){
        $newSpo = new Specialorder;

        $newSpo->ordernum = $request->ordernum;
        $newSpo->parentcode = $request->parentcode;
        $newSpo->suppcode = $request->suppcode;
        $newSpo->partsku = $request->partsku;

        $newSpo->descr = $request->descr;
        $newSpo->rma = $request->rma;
        $newSpo->fault = $request->fault;
        $newSpo->qty = $request->qty;

        $newSpo->sys_orderstatus = $request->sys_orderstatus;
        $newSpo->sys_finalstatus = $request->sys_finalstatus;
        $newSpo->sys_addedby = Auth::user()->id;
        $newSpo->sys_lasttouch = Auth::user()->id;        
        $newSpo->sys_isactive = 1; //Active always
    
        $newSpo->photo = $request->hasFile('photo') ? $request->photo->getClientOriginalName() : "qmbyjpserame.png";
        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/images'), $request->photo->getClientOriginalName());
        }

        $newSpo->save();

        $theSuppOrds = Supplierorder::where("existingorder_id", $exOrd->id)
            ->where("sys_finalstatus", $exOrd->sys_finalstatus)
            ->first();

        $newSpo->supplierorder_id = $theSuppOrds->id;
        $newSpo->save();

        return redirect('/existingorders/'.$exOrd->id.'/edit');
    }



    public function topending(Existingorder $exOrd, Request $request){
        
        $forPending = json_decode($request->to_pending);
        /*dd($forPending);*/
        /*$redirect = '/existingorders/'.$exOrd->id.'/edit';*/


        foreach ($forPending as $key => $value) {
            switch ($value->type) {
                case 'Special Order':
                    $spo = Specialorder::find($value->id);
                    $theSuppOrd = Supplierorder::find($spo->supplierorder_id);

                    if (intval($theSuppOrd->sys_total) == ($spo->qty)) { //this means that there is only one ordertype in SO
                        $theSuppOrd->sys_finalstatus = "Cancelled";
                        $theSuppOrd->save();

                        $spo->sys_finalstatus = "Pending";
                        $spo->save();

                        $this->fromEOforSupplierOrders($spo, "spo");
                    
                    }else{ 
                        //detach spo to SO, recompute SO
                        //detach spo to EO, recompute EO
                        //recompute for needs ordering the spo and save as Pending
                        $spo->supplierorder_id = "0";
                        $spo->sys_finalstatus = "Pending";
                        $spo->save();

                        $theSuppOrd->specialorder = intval($theSuppOrd->specialorder) - intval($spo->qty);
                        $theSuppOrd->save();

                        $this->fromEOforSupplierOrders($spo, "spo");
                    }

                    if (intval($exOrd->sys_total) == intval($spo->qty)) {
                        $exOrd->sys_finalstatus = "Pending";
                        $exOrd->sys_isactive = "0";
                        $exOrd->save();

                        $theSuppOrd->existingorder_id = "0";
                        $theSuppOrd->save();
                        
                    }else{
                        $exOrd->specialorder =  intval($exOrd->specialorder) - intval($spo->qty);
                        $exOrd->save();
                        $this->recomputeExistingOrderTotals($exOrd);    
                        
                    }

                    
                    $this->specialordersGenerateJson();
                    $this->supplierordersGenerateJson();
                    $this->existingordersGenerateJson();
                    
                    break;


                case 'Back Order':
                    //dd("Backorder");
                    $bko = Backorder::find($value->id);
                    $theSuppOrd = Supplierorder::find($bko->supplierorder_id);

                    if (intval($theSuppOrd->sys_total) == ($bko->qty)) { 
                        //this means that there is only one ordertype in SO
                        $theSuppOrd->sys_finalstatus = "Cancelled";
                        $theSuppOrd->save();

                        $bko->sys_finalstatus = "Pending";
                        $bko->save();

                        $this->fromEOforSupplierOrders($bko, "bko");
                    
                    }else{ 
                        //detach spo to SO, recompute SO
                        //detach spo to EO, recompute EO
                        //recompute for needs ordering the spo and save as Pending
                        $bko->supplierorder_id = "0";
                        $bko->sys_finalstatus = "Pending";
                        $bko->save();

                        $theSuppOrd->backorder = intval($theSuppOrd->backorder) - intval($bko->qty);
                        $theSuppOrd->save();

                        $this->fromEOforSupplierOrders($bko, "bko");
                    }


                    if (intval($exOrd->sys_total) == intval($bko->qty)) {
                        $exOrd->sys_finalstatus = "Pending";
                        $exOrd->sys_isactive = "0";
                        $exOrd->save();

                        $theSuppOrd->existingorder_id = "0";
                        $theSuppOrd->save();

                    }else{
                        $exOrd->backorder =  intval($exOrd->backorder) - intval($bko->qty);
                        $exOrd->save();
                        $this->recomputeExistingOrderTotals($exOrd);    
                    }
                    
                    $this->backordersGenerateJson();
                    $this->supplierordersGenerateJson();
                    $this->existingordersGenerateJson();
                    
                    break;

                case 'Rap':

                    $rap = Rap::find($value->id);
                    $theSuppOrd = Supplierorder::find($rap->supplierorder_id);

                    if (intval($theSuppOrd->sys_total) == ($rap->qty)) { //this means that there is only one ordertype in SO
                        $theSuppOrd->sys_finalstatus = "Cancelled";
                        $theSuppOrd->save();

                        $rap->sys_finalstatus = "Pending";
                        $rap->save();

                        $this->fromEOforSupplierOrders($rap, "rap");
                    
                    }else{ 
                        //detach spo to SO, recompute SO
                        //detach spo to EO, recompute EO
                        //recompute for needs ordering the spo and save as Pending
                        $rap->supplierorder_id = "0";
                        $rap->sys_finalstatus = "Pending";
                        $rap->save();

                        $theSuppOrd->rap = intval($theSuppOrd->rap) - intval($rap->qty);
                        $theSuppOrd->save();

                        $this->fromEOforSupplierOrders($rap, "rap");
                    }

                    if (intval($exOrd->sys_total) == intval($rap->qty)) {
                        $exOrd->sys_finalstatus = "Pending";
                        $exOrd->sys_isactive = "0";
                        $exOrd->save();

                        $theSuppOrd->existingorder_id = "0";
                        $theSuppOrd->save();
                        
                    }else{
                        $exOrd->rap =  intval($exOrd->rap) - intval($rap->qty);
                        $exOrd->save();
                        $this->recomputeExistingOrderTotals($exOrd);    
                        
                    }

                    
                    $this->rapsordersGenerateJson();
                    $this->supplierordersGenerateJson();
                    $this->existingordersGenerateJson();
                    
                    break;

                case 'Openbox':

                    $opb = Openbox::find($value->id);
                    $theSuppOrd = Supplierorder::find($opb->supplierorder_id);

                    if (intval($theSuppOrd->sys_total) == ($opb->qty)) { //this means that there is only one ordertype in SO
                        $theSuppOrd->sys_finalstatus = "Cancelled";
                        $theSuppOrd->save();

                        $opb->sys_finalstatus = "Pending";
                        $opb->save();

                        $this->fromEOforSupplierOrders($opb, "opb");
                    
                    }else{ 
                        //detach spo to SO, recompute SO
                        //detach spo to EO, recompute EO
                        //recompute for needs ordering the spo and save as Pending
                        $opb->supplierorder_id = "0";
                        $opb->sys_finalstatus = "Pending";
                        $opb->save();

                        $theSuppOrd->openbox = intval($theSuppOrd->openbox) - intval($opb->qty);
                        $theSuppOrd->save();

                        $this->fromEOforSupplierOrders($opb, "opb");
                    }

                    if (intval($exOrd->sys_total) == intval($opb->qty)) {
                        $exOrd->sys_finalstatus = "Pending";
                        $exOrd->sys_isactive = "0";
                        $exOrd->save();

                        $theSuppOrd->existingorder_id = "0";
                        $theSuppOrd->save();
                        
                    }else{
                        $exOrd->openbox =  intval($exOrd->openbox) - intval($opb->qty);
                        $exOrd->save();
                        $this->recomputeExistingOrderTotals($exOrd);    
                        
                    }

                    
                    $this->openboxesordersGenerateJson();
                    $this->supplierordersGenerateJson();
                    $this->existingordersGenerateJson();
                    
                    break;

                case 'Needs Ordering':

                    $stk = Stock::find($value->id);
                    $theSuppOrd = Supplierorder::find($stk->supplierorder_id);

                    $thereExistsSuppOrd = Supplierorder::where("partsku", $stk->partsku)
                            ->where("suppcode", $theSuppOrd->suppcode)
                            ->where("parentcode", $theSuppOrd->parentcode)
                            ->where("sys_finalstatus", "Pending")
                            ->first();


                    
                    if ( intval($theSuppOrd->sys_total) == intval($stk->reorderqty) && intval($theSuppOrd->reorderqty == intval($stk->reorderqty)) ) { //this means that there is only one ordertype in SO

                        
                        
                        if (!is_null($thereExistsSuppOrd)) { //there exist a supplier order with the same details
                            

                            $THEOTHERSTOCKS = Stock::where("supplierorder_id", $thereExistsSuppOrd->id)
                                ->where("sys_finalstatus", "Pending")
                                ->where("sys_iscurrent", "1")
                                ->where("sys_isactive", "1")
                                ->first();

                            //dd($THEOTHERSTOCKS);


                            if (!is_null($THEOTHERSTOCKS)) {
                                if ($THEOTHERSTOCKS->id != $stk->id) {
                                        $THEOTHERSTOCKS->reorderqty += intval($stk->reorderqty);
                                        $THEOTHERSTOCKS->save();   

                                        $stk->supplierorder_id = 0;
                                        $stk->sys_iscurrent = 0;
                                        $stk->sys_isactive = 0;
                                        $stk->sys_finalstatus = "Cancelled";
                                        $stk->save();

                                    }else{

                                        $stk->supplierorder_id = $thereExistsSuppOrd->id;
                                        $stk->sys_finalstatus = "Pending";
                                        $stk->save();  
                                    }
                            } //end if null



                                
                            

                            $thereExistsSuppOrd->reorderqty += intval($stk->reorderqty);
                            $thereExistsSuppOrd->save();

                            
                                
                            $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

                        }else { //There are no partskus in Supplier Order
                           
                            $newSuppOrd = new Supplierorder;
                            $newSuppOrd->partsku = $stk->partsku;
                            $newSuppOrd->descr = $theSuppOrd->descr;
                            $newSuppOrd->parentcode = $theSuppOrd->parentcode;
                            $newSuppOrd->suppcode = $theSuppOrd->suppcode;
                            $newSuppOrd->reorderqty = $theSuppOrd->reorderqty;
                            $newSuppOrd->sys_finalstatus = "Pending";

                            $newSuppOrd->sys_addedby = Auth::user()->id;
                            $newSuppOrd->sys_isactive = 1;
                            $newSuppOrd->photo = $theSuppOrd->photo;
        
                            $newSuppOrd->save();

                            $this->recomputeSupplierOrderTotals($newSuppOrd);


                            $stk->sys_finalstatus = "Pending";
                            $stk->supplierorder_id = $newSuppOrd->id;
                            $stk->save();
                        }
                    
                    }else{ 
                        //dd("BOTTOM", $stk, $theSuppOrd, $thereExistsSuppOrd);
                        //dd("MORE BOTTOM!", $stk, $theSuppOrd, $thereExistsSuppOrd);
                        //detach spo to SO, recompute SO
                        //detach spo to EO, recompute EO
                        //recompute for needs ordering the spo and save as Pending

                        $stk->supplierorder_id = "0";
                        $stk->sys_finalstatus = "Pending";
                        $stk->save();

                        $theSuppOrd->reorderqty = intval($theSuppOrd->reorderqty) - intval($stk->reorderqty);
                        $theSuppOrd->save();

                        $this->recomputeSupplierOrderTotals($theSuppOrd);
                        

                        if (!is_null($thereExistsSuppOrd)) {
                            
                            $thereExistsSuppOrd->reorderqty += intval($stk->reorderqty);
                            $thereExistsSuppOrd->save();

                            $stk->supplierorder_id = $thereExistsSuppOrd->id;
                            $stk->save();
                                
                            $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

                        }else { //There are no partskus in Supplier Order

                            $newSuppOrd = new Supplierorder;
                            $newSuppOrd->partsku = $stk->partsku;
                            $newSuppOrd->descr = $theSuppOrd->descr;
                            $newSuppOrd->parentcode = $theSuppOrd->parentcode;
                            $newSuppOrd->suppcode = $theSuppOrd->suppcode;
                            $newSuppOrd->reorderqty = $stk->reorderqty;
                            $newSuppOrd->sys_finalstatus = "Pending";

                            $newSuppOrd->sys_addedby = Auth::user()->id;
                            $newSuppOrd->sys_isactive = 1;
                            $newSuppOrd->photo = $theSuppOrd->photo;
        
                            $newSuppOrd->save();

                            $this->recomputeSupplierOrderTotals($newSuppOrd);


                            $stk->sys_finalstatus = "Pending";
                            $stk->supplierorder_id = $newSuppOrd->id;
                            $stk->sys_iscurrent = "1";
                            $stk->save();
                        }
                        
                    }

                    if (intval($exOrd->sys_total) == intval($stk->reorderqty)) {
                        $exOrd->sys_finalstatus = "Pending";
                        $exOrd->sys_isactive = "0";
                        $exOrd->save();

                        $theSuppOrd->existingorder_id = "0";
                        $theSuppOrd->save();
                        
                    }else{
                        $exOrd->reorderqty =  intval($exOrd->reorderqty) - intval($stk->reorderqty);
                        $exOrd->save();
                        $this->recomputeExistingOrderTotals($exOrd);    
                        
                    }

                    
                    $this->partsGenerateJson();
                    $this->supplierordersGenerateJson();
                    $this->existingordersGenerateJson();
                    
                    break;

                
                default:
                    # code...
                    break;
            }
        }
        

        if($exOrd->sys_isactive == "0"){
            return redirect('/existingorders');    
        }else{
            return redirect('/existingorders/'.$exOrd->id.'/edit');    
        }
        //
        
    }





    /**
     * Gets the post values for existing orders
     * 
     * @param Supplierorder $suppo the Supplier Order instance
     * @return String the HTML markup for the rest of the table
     */
    public function additionalOrders (Request $request, Existingorder $exOrd){
        $addlOrder = json_decode($request->to_additionalExOrd);
        
        

        foreach ($addlOrder as $ord) {
            $theOrder = "";

            switch ($ord->type) {
                case 'BKO':
                    $this->bkoToBeAdded($exOrd, $ord);
                    break;

                case 'SPO':
                    $this->spoToBeAdded($exOrd, $ord);
                    break;

                case 'OPB':
                    $this->opbToBeAdded($exOrd, $ord);
                    break;

                case 'RAP':
                    $this->rapToBeAdded($exOrd, $ord);
                    break;

                case 'PI':
                    $this->stkToBeAdded($exOrd, $ord);
                    break;

                default:
                    # code...
                    break;
            }

        }
        
        return redirect('/existingorders/'.$exOrd->id.'/edit');


    } //end additionalOrders function



    /**
     * 
     *
     * @return list of Parents, list of Suppliers
     */
    public function bkoToBeAdded(Existingorder $exOrd, $ord){
        
        $theOrder = Backorder::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);

        //checking totals, if equal
        if (intval($theOrder->qty) == intval($theSuppOrd->sys_total)) {
            $theSuppOrd->existingorder_id = $exOrd->id;
            $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
            $theSuppOrd->save();

            $exOrd->backorder = intval($exOrd->backorder) + intval($theOrder->qty);
            $exOrd->save();

            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();

            $this->recomputeExistingOrderTotals($exOrd);
        }else{
            $theSuppOrd->backorder = intval($theSuppOrd->backorder) - intval($theOrder->qty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $newSupplierOrder = new Supplierorder;
            $newSupplierOrder->partsku = $theSuppOrd->partsku;
            $newSupplierOrder->descr = $theSuppOrd->descr;
            $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
            $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
            $newSupplierOrder->sys_isactive = 1;
            $newSupplierOrder->sys_addedby = Auth::user()->id;
            $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $newSupplierOrder->photo = $theSuppOrd->photo;
            $newSupplierOrder->backorder = $theOrder->qty;
            $newSupplierOrder->existingorder_id = $exOrd->id;
            $newSupplierOrder->save();
            $this->recomputeSupplierOrderTotals($newSupplierOrder);

            $exOrd->backorder = intval($exOrd->backorder) + intval($theOrder->qty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);

            $theOrder->supplierorder_id = $newSupplierOrder->id;
            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();
            
        }
        $this->backordersGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }

    /**
     * 
     *
     * @return list of Parents, list of Suppliers
     */
    public function spoToBeAdded(Existingorder $exOrd, $ord){

        $theOrder = Specialorder::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);

        //checking totals, if equal
        if (intval($theOrder->qty) == intval($theSuppOrd->sys_total)) {
            $theSuppOrd->existingorder_id = $exOrd->id;
            $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
            $theSuppOrd->specialorder = intval($ord->orderedqty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $exOrd->specialorder = intval($exOrd->specialorder) + intval($theSuppOrd->specialorder);
            $exOrd->save();

            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->qty = intval($ord->orderedqty);
            $theOrder->save();

            $this->recomputeExistingOrderTotals($exOrd);
        }else{
            $theSuppOrd->specialorder = intval($theSuppOrd->specialorder) - intval($theOrder->qty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $newSupplierOrder = new Supplierorder;
            $newSupplierOrder->partsku = $theSuppOrd->partsku;
            $newSupplierOrder->descr = $theSuppOrd->descr;
            $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
            $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
            $newSupplierOrder->sys_isactive = 1;
            $newSupplierOrder->sys_addedby = Auth::user()->id;
            $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $newSupplierOrder->photo = $theSuppOrd->photo;
            $newSupplierOrder->specialorder = intval($ord->orderedqty);
            $newSupplierOrder->existingorder_id = $exOrd->id;
            $newSupplierOrder->save();
            $this->recomputeSupplierOrderTotals($newSupplierOrder);

            $exOrd->specialorder = intval($exOrd->specialorder) + intval($ord->orderedqty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);

            $theOrder->supplierorder_id = $newSupplierOrder->id;
            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->qty = intval($ord->orderedqty);
            $theOrder->save();
            
        }
        $this->specialordersGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }



    /**
     * 
     *
     * @return list of Parents, list of Suppliers
     */
    public function opbToBeAdded(Existingorder $exOrd, $ord){
        $theOrder = Openbox::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);

        //checking totals, if equal
        if (intval($theOrder->qty) == intval($theSuppOrd->sys_total)) {
            $theSuppOrd->existingorder_id = $exOrd->id;
            $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
            $theSuppOrd->save();

            $exOrd->openbox = intval($exOrd->openbox) + intval($theOrder->qty);
            $exOrd->save();

            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();

            $this->recomputeExistingOrderTotals($exOrd);
        }else{
            $theSuppOrd->openbox = intval($theSuppOrd->openbox) - intval($theOrder->qty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $newSupplierOrder = new Supplierorder;
            $newSupplierOrder->partsku = $theSuppOrd->partsku;
            $newSupplierOrder->descr = $theSuppOrd->descr;
            $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
            $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
            $newSupplierOrder->sys_isactive = 1;
            $newSupplierOrder->sys_addedby = Auth::user()->id;
            $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $newSupplierOrder->photo = $theSuppOrd->photo;
            $newSupplierOrder->openbox = $theOrder->qty;
            $newSupplierOrder->existingorder_id = $exOrd->id;
            $newSupplierOrder->save();
            $this->recomputeSupplierOrderTotals($newSupplierOrder);

            $exOrd->openbox = intval($exOrd->openbox) + intval($theOrder->qty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);

            $theOrder->supplierorder_id = $newSupplierOrder->id;
            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();
            
        }
        $this->openboxesordersGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }



    /**
     * 
     *
     * @return list of Parents, list of Suppliers
     */
    public function rapToBeAdded(Existingorder $exOrd, $ord){
        $theOrder = Rap::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);


        //dd($theOrder, $theSuppOrd);
        //checking totals, if equal
        if (intval($theOrder->qty) == intval($theSuppOrd->sys_total)) {
            $theSuppOrd->existingorder_id = $exOrd->id;
            $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
            $theSuppOrd->save();

            $exOrd->rap = intval($exOrd->rap) + intval($theOrder->qty);
            $exOrd->save();

            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();

            $this->recomputeExistingOrderTotals($exOrd);
        }else{

            $theSuppOrd->rap = intval($theSuppOrd->rap) - intval($theOrder->qty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $newSupplierOrder = new Supplierorder;
            $newSupplierOrder->partsku = $theSuppOrd->partsku;
            $newSupplierOrder->descr = $theSuppOrd->descr;
            $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
            $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
            $newSupplierOrder->sys_isactive = 1;
            $newSupplierOrder->sys_addedby = Auth::user()->id;
            $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $newSupplierOrder->photo = $theSuppOrd->photo;
            $newSupplierOrder->rap = $theOrder->qty;
            $newSupplierOrder->existingorder_id = $exOrd->id;
            $newSupplierOrder->save();
            $this->recomputeSupplierOrderTotals($newSupplierOrder);

            $exOrd->rap = intval($exOrd->rap) + intval($theOrder->qty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);

            $theOrder->supplierorder_id = $newSupplierOrder->id;
            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();
            
        }
        $this->rapsordersGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }



    /**
     * 
     *
     * 
     */
    public function stkToBeAdded(Existingorder $exOrd, $ord){
        $theOrder = Stock::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);

        //checking totals, if equal
        if (intval($theSuppOrd->sys_total) == intval($theOrder->reorderqty)) {

            if (intval($ord->orderedqty) < intval($theSuppOrd->sys_total)) {
                $newSupplierOrder = new Supplierorder;
                $newSupplierOrder->partsku = $theSuppOrd->partsku;
                $newSupplierOrder->descr = $theSuppOrd->descr;
                $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
                $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
                $newSupplierOrder->sys_isactive = 1;
                $newSupplierOrder->sys_addedby = Auth::user()->id;
                $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
                $newSupplierOrder->photo = $theSuppOrd->photo;
                $newSupplierOrder->existingorder_id = $exOrd->id;
                $newSupplierOrder->reorderqty =  intval($ord->orderedqty); //$theOrder->reorderqty;
                $newSupplierOrder->save();
                $this->recomputeSupplierOrderTotals($newSupplierOrder);

                $newStock = new Stock;
                $newStock->partsku = $theOrder->partsku;
                $newStock->soh = $theOrder->soh;
                $newStock->restockqty = $theOrder->restockqty;
                $newStock->reorderqty = $ord->orderedqty;
                $newStock->onorderqty = $theOrder->onorderqty;
                $newStock->salespast150d = $theOrder->salespast150d;
                $newStock->sys_iscurrent = "0";
                $newStock->sys_status = $theOrder->sys_status;
                $newStock->sys_finalstatus = $exOrd->sys_finalstatus;
                $newStock->sys_addedby = $theOrder->sys_addedby;
                $newStock->sys_lasttouch = Auth::user()->id;
                $newStock->sys_isactive = "1";
                $newStock->partsku_id = $theOrder->partsku_id;
                $newStock->supplierorder_id = $newSupplierOrder->id;
                $newStock->save();

                $theSuppOrd->reorderqty = intval($theSuppOrd->reorderqty) - intval($ord->orderedqty);
                $theSuppOrd->save();
                $this->recomputeSupplierOrderTotals($theSuppOrd);

                $theOrder->reorderqty = intval($theOrder->reorderqty) - intval($ord->orderedqty);
                $theOrder->save();

                $exOrd->reorderqty = intval($exOrd->reorderqty) + intval($ord->orderedqty);
                $exOrd->save();
                
            }else{
                $theSuppOrd->existingorder_id = $exOrd->id;
                $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
                $theSuppOrd->reorderqty = intval($ord->orderedqty);
                $theSuppOrd->save();
                $this->recomputeSupplierOrderTotals($theSuppOrd);

                $exOrd->reorderqty = intval($exOrd->reorderqty) + intval($theSuppOrd->reorderqty);
                $exOrd->save();

                $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
                $theOrder->reorderqty = intval($ord->orderedqty);
                $theOrder->save();
            }
            

            $this->recomputeExistingOrderTotals($exOrd);
            
        }else{
            if (intval($ord->orderedqty) < intval($theSuppOrd->sys_total)) {
                $newSupplierOrder = new Supplierorder;
                $newSupplierOrder->partsku = $theSuppOrd->partsku;
                $newSupplierOrder->descr = $theSuppOrd->descr;
                $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
                $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
                $newSupplierOrder->sys_isactive = 1;
                $newSupplierOrder->sys_addedby = Auth::user()->id;
                $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
                $newSupplierOrder->photo = $theSuppOrd->photo;
                $newSupplierOrder->existingorder_id = $exOrd->id;
                $newSupplierOrder->reorderqty =  intval($ord->orderedqty); //$theOrder->reorderqty;
                $newSupplierOrder->save();
                $this->recomputeSupplierOrderTotals($newSupplierOrder);

                $newStock = new Stock;
                $newStock->partsku = $theOrder->partsku;
                $newStock->soh = $theOrder->soh;
                $newStock->restockqty = $theOrder->restockqty;
                $newStock->reorderqty = $ord->orderedqty;
                $newStock->onorderqty = $theOrder->onorderqty;
                $newStock->salespast150d = $theOrder->salespast150d;
                $newStock->sys_iscurrent = "0";
                $newStock->sys_status = $theOrder->sys_status;
                $newStock->sys_finalstatus = $exOrd->sys_finalstatus;
                $newStock->sys_addedby = $theOrder->sys_addedby;
                $newStock->sys_lasttouch = Auth::user()->id;
                $newStock->sys_isactive = "1";
                $newStock->partsku_id = $theOrder->partsku_id;
                $newStock->supplierorder_id = $newSupplierOrder->id;
                $newStock->save();

                $theSuppOrd->reorderqty = intval($theSuppOrd->reorderqty) - intval($ord->orderedqty);
                $theSuppOrd->save();
                $this->recomputeSupplierOrderTotals($theSuppOrd);

                $exOrd->reorderqty = intval($exOrd->reorderqty) + intval($ord->orderedqty);
                $exOrd->save();
                
            }else{
                $theSuppOrd->existingorder_id = $exOrd->id;
                $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
                $theSuppOrd->reorderqty = intval($ord->orderedqty);
                $theSuppOrd->save();
                $this->recomputeSupplierOrderTotals($theSuppOrd);

                $exOrd->reorderqty = intval($exOrd->reorderqty) + intval($theSuppOrd->reorderqty);
                $exOrd->save();

                $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
                $theOrder->reorderqty = intval($ord->orderedqty);
                $theOrder->save();
            }
            $this->recomputeExistingOrderTotals($exOrd);
            
        }
        $this->partsGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }



}


