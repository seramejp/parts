<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Partsku;
use App\Parentsku;
use App\Supplier;
use App\Stock;
use App\Supplierorder;
use App\Specialorder;
use App\Existingorder;
use App\Openbox;
use App\Rap;
use App\Backorder;

use Auth;
use DB;

class ExistingordersController extends RootController
{
    /**
     * Controller for Parts
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Generate JSON File
     *
     * @return void
     */
    public function generate(){
        return $this->existingordersGenerateJson();
    }


    /**
     * Re-Generate ALL JSON File
     *
     * @return void
     */
    public function generateAllJson(){
        $this->partsGenerateJson();
        $this->supplierordersGenerateJson();
        $this->specialordersGenerateJson();
        $this->existingordersGenerateJson();
        $this->backordersGenerateJson();
        $this->openboxesordersGenerateJson();
        $this->rapsordersGenerateJson();

        return "All Data Generated.";
    }


    /**
     * Load View for Supplierorders Main List
     *
     * @return void
     */
    public function index (){
        return view("existingorders.index");
    }


    public function getOrderKabits (Existingorder $exOrd){
        $tempVal = "";
        if ($exOrd->supplierorders->count() > 0) {
            foreach ($exOrd->supplierorders as $spoTmp) {
                if ($spoTmp->specialorders->count() > 0) {
                    foreach ($spoTmp->specialorders as $spo) {
                        $tempVal .= "<tr>".
                            "<td><a href='/parts/public/specialorders/".$spo->id."/edit'>" . $spo->partsku . "</a></td>" .
                            "<td>" . $spo->descr . "</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td>". $spo->qty."</td>".
                            "</tr>";
                    }
                }

                if ($spoTmp->backorders->count() > 0) {
                    foreach ($spoTmp->backorders as $bko) {
                        $tempVal .= "<tr>".
                            "<td><a href='/parts/public/backorders/".$bko->id."/edit'>" . $bko->partsku . "</a></td>" .
                            "<td>" . $bko->descr . "</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td>". $bko->qty."</td>".
                            "<td> - </td>" .
                            "</tr>";
                    }
                }
                
                if ($spoTmp->raps->count() > 0) {
                    foreach ($spoTmp->raps as $rap) {
                        $tempVal .= "<tr>".
                            "<td><a href='/parts/public/raps/".$rap->id."/edit'>" . $rap->partsku . "</a></td>" .
                            "<td>" . $rap->descr . "</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td>". $rap->qty."</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "</tr>";
                    }
                }

                if ($spoTmp->openboxes->count() > 0) {
                    foreach ($spoTmp->openboxes as $obp) {
                        $tempVal .= "<tr>".
                            "<td><a href='/parts/public/openboxes/".$obp->id."/edit'>" . $obp->partsku . "</a></td>" .
                            "<td>" . $obp->descr . "</td>".
                            "<td> - </td>" .
                            "<td>". $obp->qty."</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "</tr>";
                    }
                }

                if ($spoTmp->stocks->count() > 0) {
                    
                    foreach ($spoTmp->stocks as $psku) {
                        $tempVal .= "<tr>".
                            "<td><a href='/parts/public/partspms/".$psku->partsku_id."/edit'>" . $psku->partsku . "</a></td>" .
                            "<td>" . $psku->descr . "</td>".
                            "<td>". $psku->reorderqty."</td>".
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "<td> - </td>" .
                            "</tr>";
                    }
                }

                
            }
        }
        return $tempVal;
    }


    public function edit(Existingorder $exOrd){

        return view('existingorders.edit', compact('exOrd'));
    }

    /**
     * Load View for Special Orders Main List
     *
     * @return void
     */
    public function getMaxReferenceNumber (){
        $maxrefnum = Openbox::where("sys_isactive", 1)->max("referencenum");

        
        if (empty($maxrefnum)) {
            $maxrefnum = "OBP00001";
        }else{
            $arr = explode("OBP", $maxrefnum);
            $maxrefnum = "OBP" . str_pad(intval($arr[1]) + 1, 5, "0", STR_PAD_LEFT);
        }
        return $maxrefnum;
    }


    /**
     * Load View for Creating a New Special Order
     *
     * @return list of Parents, list of Suppliers
     */
    public function additionalSpoFrom(Existingorder $exOrd){
        $parents = Parentsku::select("parentcode")->orderBy("parentcode")->get();
        return view("existingorders.addspo", compact('exOrd', 'parents'));
    }


    public function additionalSPO(Existingorder $exOrd, Request $request){
        $newSpo = new Specialorder;

        $newSpo->ordernum = $request->ordernum;
        $newSpo->parentcode = $request->parentcode;
        $newSpo->suppcode = $request->suppcode;
        $newSpo->partsku = $request->partsku;

        $newSpo->descr = $request->descr;
        $newSpo->rma = $request->rma;
        $newSpo->fault = $request->fault;
        $newSpo->qty = $request->qty;

        $newSpo->sys_orderstatus = $request->sys_orderstatus;
        $newSpo->sys_finalstatus = $request->sys_finalstatus;
        $newSpo->sys_addedby = Auth::user()->id;
        $newSpo->sys_lasttouch = Auth::user()->id;        
        $newSpo->sys_isactive = 1; //Active always
    
        $newSpo->photo = $request->hasFile('photo') ? $request->photo->getClientOriginalName() : "qmbyjpserame.png";
        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/images'), $request->photo->getClientOriginalName());
        }

        $newSpo->save();

        $theSuppOrds = Supplierorder::where("existingorder_id", $exOrd->id)
            ->where("sys_finalstatus", $exOrd->sys_finalstatus)
            ->first();
        $theSuppOrds->specialorder = intval($theSuppOrds->specialorder) + intval($newSpo->qty);
        $theSuppOrds->save();
        $this->recomputeSupplierOrderTotals($theSuppOrds);

        $exOrd->specialorder = intval($exOrd->specialorder) + intval($newSpo->qty);
        $this->recomputeExistingOrderTotals($exOrd);

        $newSpo->supplierorder_id = $theSuppOrds->id;
        $newSpo->save();

        $this->supplierordersGenerateJson();
        $this->specialordersGenerateJson();
        $this->existingordersGenerateJson();

        return redirect('/existingorders/'.$exOrd->id.'/edit');
    }


    /**
     * Load View for Creating a New Special Order
     *
     * @return list of Parents, list of Suppliers
     */
    public function additionalBkoFrom(Existingorder $exOrd){
        $parents = Parentsku::select("parentcode")->orderBy("parentcode")->get();
        return view("existingorders.addbko", compact('exOrd', 'parents'));
    }



    public function additionalBKO(Existingorder $exOrd, Request $request){
        $newBko = new Backorder;

        $newBko->dateinvoiced = date('Y-m-d', strtotime($request->dateinvoiced));
        $newBko->ordernum = $request->ordernum;
        $newBko->partsku = $request->partsku;
        $newBko->descr = $request->descr;
        $newBko->ordertype = $request->ordertype;

        $newBko->parentcode = $request->parentcode;
        $newBko->suppcode = $request->suppcode;
        
        $newBko->eta = date('Y-m-d', strtotime($request->eta));
        $newBko->qty = $request->qty;
        $newBko->remarks = $request->remarks;
        
        $newBko->sys_orderstatus = $request->sys_orderstatus;
        $newBko->sys_finalstatus = $request->sys_finalstatus;
        $newBko->sys_addedby = Auth::user()->id;
        $newBko->sys_lasttouch = Auth::user()->id;        
        $newBko->sys_isactive = 1; //Active always
    
        $newBko->photo = $request->hasFile('photo') ? $request->photo->getClientOriginalName() : "qmbyjpserame.png";
        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/images'), $request->photo->getClientOriginalName());
        }

        $newBko->save();

        $theSuppOrds = Supplierorder::where("existingorder_id", $exOrd->id)
            ->where("sys_finalstatus", $exOrd->sys_finalstatus)
            ->first();
        $theSuppOrds->backorder = intval($theSuppOrds->backorder) + intval($newBko->qty);
        $theSuppOrds->save();
        $this->recomputeSupplierOrderTotals($theSuppOrds);

        $exOrd->backorder = intval($exOrd->backorder) + intval($newBko->qty);
        $this->recomputeExistingOrderTotals($exOrd);

        $newBko->supplierorder_id = $theSuppOrds->id;
        $newBko->save();

        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
        $this->backordersGenerateJson();

        return redirect('/existingorders/'.$exOrd->id.'/edit');
    }




    /**
     * Load View for Creating a New Special Order
     *
     * @return list of Parents, list of Suppliers
     */
    public function additionalOpbFrom(Existingorder $exOrd){
        $parents = Parentsku::select("parentcode")->orderBy("parentcode")->get();
        return view("existingorders.addopb", compact('exOrd', 'parents'))->with("maxrefnum", $this->getMaxReferenceNumber());
    }



    public function additionalOPB(Existingorder $exOrd, Request $request){
        $newOpb = new Openbox;

        $newOpb->referencenum = $this->getMaxReferenceNumber();
        $newOpb->parentcode = $request->parentcode;
        $newOpb->suppcode = $request->suppcode;
        $newOpb->partsku = $request->partsku;

        $newOpb->descr = $request->descr;
        $newOpb->kayakoid = $request->kayakoid;
        $newOpb->spid = $request->spid;
        $newOpb->fault = $request->fault;
        $newOpb->qty = $request->qty;
        $newOpb->whloc = $request->whloc;

        $newOpb->sys_orderstatus = $request->sys_orderstatus;
        $newOpb->sys_finalstatus = $request->sys_finalstatus;
        $newOpb->sys_addedby = Auth::user()->id;
        $newOpb->sys_lasttouch = Auth::user()->id;        
        $newOpb->sys_isactive = 1; //Active always
    
        $newOpb->photo = $request->hasFile('photo') ? $request->photo->getClientOriginalName() : "qmbyjpserame.png";
        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/images'), $request->photo->getClientOriginalName());
        }

        $newOpb->save();


        $theSuppOrds = Supplierorder::where("existingorder_id", $exOrd->id)
            ->where("sys_finalstatus", $exOrd->sys_finalstatus)
            ->first();
        $theSuppOrds->openbox = intval($theSuppOrds->openbox) + intval($newOpb->qty);
        $theSuppOrds->save();
        $this->recomputeSupplierOrderTotals($theSuppOrds);

        $exOrd->openbox = intval($exOrd->openbox) + intval($newOpb->qty);
        $this->recomputeExistingOrderTotals($exOrd);

        $newOpb->supplierorder_id = $theSuppOrds->id;
        $newOpb->save();

        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
        $this->openboxesordersGenerateJson();

        return redirect('/existingorders/'.$exOrd->id.'/edit');
    }



    /**
     * Load View for Creating a New Special Order
     *
     * @return list of Parents, list of Suppliers
     */
    public function additionalRapFrom(Existingorder $exOrd){
        $parents = Parentsku::select("parentcode")->orderBy("parentcode")->get();
        return view("existingorders.addrap", compact('exOrd', 'parents'));
    }



    public function additionalRAP(Existingorder $exOrd, Request $request){
        $newRap = new Rap;

        $newRap->rmanum = $request->rmanum;
        $newRap->parentcode = $request->parentcode;
        $newRap->suppcode = $request->suppcode;
        $newRap->partsku = $request->partsku;

        $newRap->descr = $request->descr;
        $newRap->kayakoid = $request->kayakoid;
        $newRap->notes = $request->notes;
        $newRap->spid = $request->spid;
        $newRap->whloc = $request->whloc;
        $newRap->qty = $request->qty;
        $newRap->outcome = $request->outcome;

        $newRap->sys_orderstatus = $request->sys_orderstatus;
        $newRap->sys_finalstatus = $request->sys_finalstatus;
        $newRap->sys_addedby = Auth::user()->id;
        $newRap->sys_lasttouch = Auth::user()->id;        
        $newRap->sys_isactive = 1; //Active always
    
        $newRap->photo = $request->hasFile('photo') ? $request->photo->getClientOriginalName() : "qmbyjpserame.png";
        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/images'), $request->photo->getClientOriginalName());
        }

        $newRap->save();



        $theSuppOrds = Supplierorder::where("existingorder_id", $exOrd->id)
            ->where("sys_finalstatus", $exOrd->sys_finalstatus)
            ->first();
        $theSuppOrds->rap = intval($theSuppOrds->rap) + intval($newRap->qty);
        $theSuppOrds->save();
        $this->recomputeSupplierOrderTotals($theSuppOrds);

        $exOrd->rap = intval($exOrd->rap) + intval($newRap->qty);
        $this->recomputeExistingOrderTotals($exOrd);

        $newRap->supplierorder_id = $theSuppOrds->id;
        $newRap->save();


        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
        $this->rapsordersGenerateJson();

        return redirect('/existingorders/'.$exOrd->id.'/edit');
    }




    /**
     * Save modification by user
     * @param Request $request, Partsku $part the Partsku instance
     * @return void
     */
    public function saveUpdateMain(Request $request, Existingorder $exOrd){
        

        $theSuppOrds = Supplierorder::where("existingorder_id", $exOrd->id)->get();
        if (empty($theSuppOrds)) {
            abort(500, "Something is wrong. Please contact JP and tell him to check Existing Order ID: " . $exOrd->id . ". Thank you!");
        }else{
            foreach ($theSuppOrds as $currentSuppOrd) {
                $currentSuppOrd->sys_finalstatus = $request->sys_finalstatus;
                $currentSuppOrd->save();


                if (count($currentSuppOrd->openboxes) > 0) {
                    foreach ($currentSuppOrd->openboxes as $opb) {
                        $opb->sys_finalstatus = $request->sys_finalstatus;
                        $opb->sys_lasttouch = Auth::user()->id;
                        $opb->save();
                    }
                }

                if (count($currentSuppOrd->stocks) > 0) {
                    foreach ($currentSuppOrd->stocks as $stk) {
                        $stk->sys_finalstatus = $request->sys_finalstatus;
                        $stk->sys_lasttouch = Auth::user()->id;
                        $stk->save();
                    }
                }

                if (count($currentSuppOrd->backorders) > 0) {
                    foreach ($currentSuppOrd->backorders as $bko) {
                        $bko->sys_finalstatus = $request->sys_finalstatus;
                        $bko->sys_lasttouch = Auth::user()->id;
                        $bko->save();
                    }
                }

                if (count($currentSuppOrd->specialorders) > 0) {
                    foreach ($currentSuppOrd->specialorders as $spo) {
                        $spo->sys_finalstatus = $request->sys_finalstatus;
                        $spo->sys_lasttouch = Auth::user()->id;
                        $spo->save();
                    }
                }

                if (count($currentSuppOrd->raps) > 0) {
                    foreach ($currentSuppOrd->raps as $rap) {
                        $rap->sys_finalstatus = $request->sys_finalstatus;
                        $rap->sys_lasttouch = Auth::user()->id;
                        $rap->save();
                    }
                }
            }
        }

        $exOrd->update($request->except(
            'created_at', 'photo', 'packlistreceived'
            ));
        $exOrd->sys_lasttouch = Auth::user()->id;
        $exOrd->save();

        if (!empty($request->packlistreceived)) {
            $exOrd->packlistreceived = 1;
            $exOrd->save();
        }else{
            $exOrd->packlistreceived = 0;
            $exOrd->save();
        }

        $this->generateAllJson();
       
        return redirect('/existingorders');
    }





    /**
     * Load View for Creating a New Special Order
     *
     * @return list of Parents, list of Suppliers
     */
    public function additionalSOFrom(Existingorder $exOrd){
        $supplierorders = Supplierorder::select(DB::raw("id, created_at, parentcode, suppcode, partsku, sys_finalstatus, descr"))
            ->with("specialordersdesc", "backordersdesc", "openboxesdesc", "rapsdesc", "stocksdesc")
            ->where("sys_finalstatus", "Pending")
            ->where("suppcode", $exOrd->suppcode)
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        return view("existingorders.addso", compact('exOrd', 'supplierorders'));
    }




    /**
     * Gets the post values for existing orders
     * 
     * @param Supplierorder $suppo the Supplier Order instance
     * @return String the HTML markup for the rest of the table
     */
    public function additionalOrders (Request $request, Existingorder $exOrd){
        $addlOrder = json_decode($request->to_additionalExOrd);

        foreach ($addlOrder as $ord) {
            $theOrder = "";
            
            switch ($ord->type) {
                case 'BKO':
                    $this->bkoToBeAdded($exOrd, $ord);
                    break;

                case 'SPO':
                    $this->spoToBeAdded($exOrd, $ord);
                    break;

                case 'OPB':
                    $this->opbToBeAdded($exOrd, $ord);
                    break;

                case 'RAP':
                    $this->rapToBeAdded($exOrd, $ord);
                    break;

                case 'PI':
                    $this->stkToBeAdded($exOrd, $ord);
                    break;

                default:
                    # code...
                    break;
            }

        }
        
        return back();


    } //end additionalOrders function



    /**
     * 
     *
     * @return list of Parents, list of Suppliers
     */
    public function bkoToBeAdded(Existingorder $exOrd, $ord){
        
        $theOrder = Backorder::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);

        //checking totals, if equal
        if (intval($theOrder->qty) == intval($theSuppOrd->sys_total)) {
            $theSuppOrd->existingorder_id = $exOrd->id;
            $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
            $theSuppOrd->save();

            $exOrd->backorder = intval($exOrd->backorder) + intval($theSuppOrd->backorder);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);
            $this->supplierordersGenerateJson();
            $this->existingordersGenerateJson();
        }else{
            $theSuppOrd->backorder = intval($theSuppOrd->backorder) - intval($theOrder->qty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $newSupplierOrder = new Supplierorder;
            $newSupplierOrder->partsku = $theSuppOrd->partsku;
            $newSupplierOrder->descr = $theSuppOrd->descr;
            $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
            $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
            $newSupplierOrder->sys_isactive = 1;
            $newSupplierOrder->sys_addedby = Auth::user()->id;
            $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $newSupplierOrder->photo = $theSuppOrd->photo;
            $newSupplierOrder->backorder = $theOrder->qty;
            $newSupplierOrder->existingorder_id = $exOrd->id;
            $newSupplierOrder->save();
            $this->recomputeSupplierOrderTotals($newSupplierOrder);

            $exOrd->backorder = intval($exOrd->backorder) + intval($theOrder->qty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);

            $theOrder->supplierorder_id = $newSupplierOrder->id;
            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();
            
        }
        $this->backordersGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }

    /**
     * 
     *
     * @return list of Parents, list of Suppliers
     */
    public function spoToBeAdded(Existingorder $exOrd, $ord){
        $theOrder = Specialorder::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);

        //checking totals, if equal
        if (intval($theOrder->qty) == intval($theSuppOrd->sys_total)) {
            $theSuppOrd->existingorder_id = $exOrd->id;
            $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
            $theSuppOrd->save();

            $exOrd->specialorder = intval($exOrd->specialorder) + intval($theSuppOrd->specialorder);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);
            $this->supplierordersGenerateJson();
            $this->existingordersGenerateJson();
        }else{
            $theSuppOrd->specialorder = intval($theSuppOrd->specialorder) - intval($theOrder->qty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $newSupplierOrder = new Supplierorder;
            $newSupplierOrder->partsku = $theSuppOrd->partsku;
            $newSupplierOrder->descr = $theSuppOrd->descr;
            $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
            $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
            $newSupplierOrder->sys_isactive = 1;
            $newSupplierOrder->sys_addedby = Auth::user()->id;
            $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $newSupplierOrder->photo = $theSuppOrd->photo;
            $newSupplierOrder->specialorder = $theOrder->qty;
            $newSupplierOrder->existingorder_id = $exOrd->id;
            $newSupplierOrder->save();
            $this->recomputeSupplierOrderTotals($newSupplierOrder);

            $exOrd->specialorder = intval($exOrd->specialorder) + intval($theOrder->qty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);

            $theOrder->supplierorder_id = $newSupplierOrder->id;
            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();
            
        }
        $this->specialordersGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }



    /**
     * 
     *
     * @return list of Parents, list of Suppliers
     */
    public function opbToBeAdded(Existingorder $exOrd, $ord){
        $theOrder = Openbox::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);

        //checking totals, if equal
        if (intval($theOrder->qty) == intval($theSuppOrd->sys_total)) {
            $theSuppOrd->existingorder_id = $exOrd->id;
            $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
            $theSuppOrd->save();

            $exOrd->openbox = intval($exOrd->openbox) + intval($theSuppOrd->openbox);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);
            $this->supplierordersGenerateJson();
            $this->existingordersGenerateJson();
        }else{
            $theSuppOrd->openbox = intval($theSuppOrd->openbox) - intval($theOrder->qty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $newSupplierOrder = new Supplierorder;
            $newSupplierOrder->partsku = $theSuppOrd->partsku;
            $newSupplierOrder->descr = $theSuppOrd->descr;
            $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
            $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
            $newSupplierOrder->sys_isactive = 1;
            $newSupplierOrder->sys_addedby = Auth::user()->id;
            $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $newSupplierOrder->photo = $theSuppOrd->photo;
            $newSupplierOrder->openbox = $theOrder->qty;
            $newSupplierOrder->existingorder_id = $exOrd->id;
            $newSupplierOrder->save();
            $this->recomputeSupplierOrderTotals($newSupplierOrder);

            $exOrd->openbox = intval($exOrd->openbox) + intval($theOrder->qty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);

            $theOrder->supplierorder_id = $newSupplierOrder->id;
            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();
            
        }
        $this->openboxesordersGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }



    /**
     * 
     *
     * @return list of Parents, list of Suppliers
     */
    public function rapToBeAdded(Existingorder $exOrd, $ord){
        $theOrder = Rap::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);

        //checking totals, if equal
        if (intval($theOrder->qty) == intval($theSuppOrd->sys_total)) {
            $theSuppOrd->existingorder_id = $exOrd->id;
            $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
            $theSuppOrd->save();

            $exOrd->rap = intval($exOrd->rap) + intval($theSuppOrd->rap);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);
            $this->supplierordersGenerateJson();
            $this->existingordersGenerateJson();
        }else{
            $theSuppOrd->rap = intval($theSuppOrd->rap) - intval($theOrder->qty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $newSupplierOrder = new Supplierorder;
            $newSupplierOrder->partsku = $theSuppOrd->partsku;
            $newSupplierOrder->descr = $theSuppOrd->descr;
            $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
            $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
            $newSupplierOrder->sys_isactive = 1;
            $newSupplierOrder->sys_addedby = Auth::user()->id;
            $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $newSupplierOrder->photo = $theSuppOrd->photo;
            $newSupplierOrder->rap = $theOrder->qty;
            $newSupplierOrder->existingorder_id = $exOrd->id;
            $newSupplierOrder->save();
            $this->recomputeSupplierOrderTotals($newSupplierOrder);

            $exOrd->rap = intval($exOrd->rap) + intval($theOrder->qty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);

            $theOrder->supplierorder_id = $newSupplierOrder->id;
            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();
            
        }
        $this->rapsordersGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }



    /**
     * 
     *
     * 
     */
    public function stkToBeAdded(Existingorder $exOrd, $ord){
        $theOrder = Stock::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);

        //checking totals, if equal
        if (intval($theOrder->qty) == intval($theSuppOrd->sys_total)) {
            $theSuppOrd->existingorder_id = $exOrd->id;
            $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
            $theSuppOrd->save();

            $exOrd->reorderqty = intval($exOrd->reorderqty) + intval($theSuppOrd->reorderqty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);
            $this->supplierordersGenerateJson();
            $this->existingordersGenerateJson();
        }else{
            $theSuppOrd->reorderqty = intval($theSuppOrd->reorderqty) - intval($theOrder->reorderqty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $newSupplierOrder = new Supplierorder;
            $newSupplierOrder->partsku = $theSuppOrd->partsku;
            $newSupplierOrder->descr = $theSuppOrd->descr;
            $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
            $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
            $newSupplierOrder->sys_isactive = 1;
            $newSupplierOrder->sys_addedby = Auth::user()->id;
            $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $newSupplierOrder->photo = $theSuppOrd->photo;
            $newSupplierOrder->existingorder_id = $exOrd->id;
            $newSupplierOrder->reorderqty = $theOrder->reorderqty;
            $newSupplierOrder->save();
            $this->recomputeSupplierOrderTotals($newSupplierOrder);

            $exOrd->reorderqty = intval($exOrd->reorderqty) + intval($theOrder->reorderqty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);

            $theOrder->supplierorder_id = $newSupplierOrder->id;
            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->save();
            
        }
        $this->partsGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }



    // 26/05/2017
    public function stkToBeAdded(Existingorder $exOrd, $ord){
        $theOrder = Stock::find($ord->id);
        $theSuppOrd = Supplierorder::find($theOrder->supplierorder_id);

        //checking totals, if equal
        if (intval($theSuppOrd->sys_total) == intval($theOrder->reorderqty)) {
            $theSuppOrd->existingorder_id = $exOrd->id;
            $theSuppOrd->sys_finalstatus = $exOrd->sys_finalstatus;
            $theSuppOrd->reorderqty = intval($ord->orderedqty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);

            $exOrd->reorderqty = intval($exOrd->reorderqty) + intval($theSuppOrd->reorderqty);
            $exOrd->save();

            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->reorderqty = intval($ord->orderedqty);
            $theOrder->onorderqty = intval($ord->orderedqty);
            $theOrder->save();

            $this->recomputeExistingOrderTotals($exOrd);
            
        }else{
            $theSuppOrd->reorderqty = intval($theSuppOrd->reorderqty) - intval($ord->orderedqty);
            $theSuppOrd->save();
            $this->recomputeSupplierOrderTotals($theSuppOrd);


                //check first if there are Supplier Orders that are open to merge
            $newSupplierOrder = new Supplierorder;
            $newSupplierOrder->partsku = $theSuppOrd->partsku;
            $newSupplierOrder->descr = $theSuppOrd->descr;
            $newSupplierOrder->suppcode = $theSuppOrd->suppcode;
            $newSupplierOrder->parentcode = $theSuppOrd->parentcode;
            $newSupplierOrder->sys_isactive = 1;
            $newSupplierOrder->sys_addedby = Auth::user()->id;
            $newSupplierOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $newSupplierOrder->photo = $theSuppOrd->photo;
            $newSupplierOrder->existingorder_id = $exOrd->id;
            $newSupplierOrder->reorderqty =  intval($ord->orderedqty); //$theOrder->reorderqty;
            $newSupplierOrder->save();
            $this->recomputeSupplierOrderTotals($newSupplierOrder);
            

            $exOrd->reorderqty = intval($exOrd->reorderqty) + intval($ord->orderedqty);
            $exOrd->save();
            $this->recomputeExistingOrderTotals($exOrd);

            $theOrder->supplierorder_id = $newSupplierOrder->id;
            $theOrder->sys_finalstatus = $exOrd->sys_finalstatus;
            $theOrder->reorderqty = intval($ord->orderedqty);
            $theOrder->onorderqty = intval($ord->orderedqty);
            $theOrder->save();
            
        }
        $this->partsGenerateJson();
        $this->supplierordersGenerateJson();
        $this->existingordersGenerateJson();
    }



}


