@extends('layout')

@section('content')
	
		<h2 class="thin">For Additional to Existing Order
			<br><strong><em>{{$exOrd->partsku}}</em></strong>
		</h2>
		<p class="muted">Check the checkbox for orders to be added. These are all Pending.</p>
		

		<ol class="breadcrumb text-left">
			<li><a href="{{url('/existingorders')}}">Existing Orders</a></li>
	        <li><a href="{{url('/existingorders/'. $exOrd->id .'/edit')}}">Return to Edit</a></li>
	        <li class="active">Create Additional</li>
	    </ol>
		<hr>
	
		<div>
			<div id="loadersoaddtable" class="text-center">
				<img src="{{ url('/images/app/rolling.gif') }}">
			</div>
			<table class="table table-striped" id="soaddtable" cellspacing="0" width="100%">
				<thead>
					
					<th >Type</th>
					<th >Parent</th>
                    <th >Supplier</th>
                    <th >Part SKU</th>
                    <th >Description</th>
                    <th >Order Status</th>
                    <th >Qty</th>

                    <th class="hidden">ID</th>
                    <th>Proceed?</th>
				</thead>
				
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<tbody class="text-left hidden">
						
			            @foreach ($collector as $so) 

			            	@if ($so->stocksdesc->count() > 0)  
			                    @foreach ($so->stocksdesc as $stk) 
			                        <tr>
			                        	<td>PI</td>
			                        	<td>{{$so->parentcode}}</td>
			                        	<td>{{$so->suppcode}}</td>
			                            <td>
			                            	<a href='{{ url('/partspms/'.$stk->partsku_id.'/edit')}}'>{{$stk->partsku}}</a>
		                            	</td>
			                            <td>{{$so->descr}}</td>
			                        	<td>{{$so->sys_orderstatus}}</td>    
			                        	<td>{{$stk->reorderqty}}</td>
			                        	<td class="hidden">{{$stk->id}}</td>
			                        	<td></td>
		                            </tr>
			                    @endforeach
			                @endif

			                @if ($so->specialordersdesc->count() > 0)  
			                    @foreach ($so->specialordersdesc as $spo) 
			                        <tr>
			                        	<td>SPO</td>
			                        	<td>{{$so->parentcode}}</td>
			                        	<td>{{$so->suppcode}}</td>
			                            <td>
			                            	<a href='{{ url('/specialorders/'.$spo->id.'/edit')}}'>{{$spo->partsku}}</a>
		                            	</td>
			                            <td>{{$spo->descr}}</td>
			                        	<td>{{$spo->sys_orderstatus}}</td>    
			                        	<td>{{$spo->qty}}</td>
			                        	<td class="hidden">{{$spo->id}}</td>
			                        	<td></td>
		                            </tr>
			                    @endforeach
			                @endif

			                @if ($so->backordersdesc->count() > 0) 
			                    @foreach ($so->backordersdesc as $bko) 
			                        <tr>
			                        	<td>BKO</td>
			                        	<td>{{$so->parentcode}}</td>
			                        	<td>{{$so->suppcode}}</td>
			                            <td>
			                            	<a href='{{ url('/backorders/'.$bko->id.'/edit')}}'>{{$bko->partsku}}</a>
		                            	</td>
			                            <td>{{$bko->descr}}</td>
			                        	<td>{{$bko->sys_orderstatus}}</td>    
			                        	<td>{{$bko->qty}}</td>
			                        	<td class="hidden">{{$bko->id}}</td>
			                        	<td></td>
		                            </tr>
			                    @endforeach
			                @endif

							@if ($so->openboxesdesc->count() > 0) 
			                    @foreach ($so->openboxesdesc as $opb) 
			                        <tr>
			                        	<td>OPB</td>
			                        	<td>{{$so->parentcode}}</td>
			                        	<td>{{$so->suppcode}}</td>
			                            <td>
			                            	<a href='{{ url('/openboxes/'.$opb->id.'/edit')}}'>{{$opb->partsku}}</a>
		                            	</td>
			                            <td>{{$opb->descr}}</td>
			                        	<td>{{$opb->sys_orderstatus}}</td>    
			                        	<td>{{$opb->qty}}</td>
			                        	<td class="hidden">{{$opb->id}}</td>
			                        	<td></td>
		                            </tr>
			                    @endforeach
			                @endif

			                @if ($so->rapsdesc->count() > 0) 
			                    @foreach ($so->rapsdesc as $rap) 
			                        <tr>
			                        	<td>RAP</td>
			                        	<td>{{$so->parentcode}}</td>
			                        	<td>{{$so->suppcode}}</td>
			                            <td>
			                            	<a href='{{ url('/raps/'.$rap->id.'/edit')}}'>{{$rap->partsku}}</a>
		                            	</td>
			                            <td>{{$rap->descr}}</td>
			                        	<td>{{$rap->sys_orderstatus}}</td>    
			                        	<td>{{$rap->qty}}</td>
			                        	<td class="hidden">{{$rap->id}}</td>
			                        	<td></td>
		                            </tr>
			                    @endforeach
			                @endif


			            @endforeach
				</tbody>
			</table>

			<form id="additionalordersform" action=" {{ url('/existingorders/additionalorders/'.$exOrd->id) }}" method="POST" enctype="multipart/form-data">
				<input id="additionalExOrd" type="text" name="to_additionalExOrd" class="form-control">
				{{ csrf_field() }}
			</form>
			
	
		</div>
	
@stop

@section('userdefjs')
	<script>
		

		

		$(function(){
			var table = $('#soaddtable')
				.on( 'init.dt', function () {
			        $("#soaddtable tbody").removeClass('hidden');
			        $("#loadersoaddtable").addClass('hidden');
			    })
				.DataTable({
					"columns": [
				            { "data": "type" },//1
				            { "data": "parentcode" },//2
				            { "data": "suppcode" },//3
				            { "data": "partsku" },//4

				            { "data": "descr" },//5
				            { "data": "sys_orderstatus" },//6
				            { "data": "qty"},//7
				          	{ "data": "id" },//8
				          	{ 
				            	"className": 'select-checkbox',
				                "orderable":      false,
				                "data":           null,
				                "defaultContent": ''
				            } //last
			         
			            ],
			        "columnDefs": [
			            {
			                "targets": [ 7 ],
			                "visible": false,
			                "searchable": false
			            }
			        ],
		       		"select": {
				        	style: 'multi',
				        	selector: 'td:last-child'
				        },
			       	"pageLength": 50,
			       	dom: 'Bfrtip',
		       		"buttons": [
				            {
					            extend: 'selected',
					            text: 'Add to Existing Order',
					            action: function ( e, dt, button, config ) {
					                //alert( dt.rows( { selected: true } ).indexes().length +' row(s) selected' );
					                
					                $("#additionalExOrd").val(JSON.stringify(dt.rows({selected: true}).data().toArray()));
					                $("#additionalordersform").submit();
					                
					            }
					        }	
				        ],
		            "order": [[1, 'asc']]
			});

			$("#soaddtable_filter").closest("div").addClass('text-right');


			$('#soaddtable tbody').on('click', 'td.details-control', function () {
			    var tr = $(this).closest('tr');
			    var row = table.row( tr );
			 
			    if ( row.child.isShown() ) {
			        // This row is already open - close it
			        row.child.hide();
			        tr.removeClass('shown');
			    }
			    else {
			        if ( table.row( '.shown' ).length ) {
						$('.details-control', table.row( '.shown' ).node()).click();
					}

					row.child( format(row.data()) ).show();
					tr.addClass('shown');
					//getKabits(row.data().id);
			    }
			});

			$("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');

			$(".navmenuitemlist li").removeClass('active').eq(1).addClass('active');
			$(".navsubmenuitemlist li").removeClass('active').eq(5).addClass('active');
		});



	</script>
@stop