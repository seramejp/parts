<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialorder extends Model
{
    protected $guarded = array();
    public function supplierorders (){
        return $this->belongsTo(Supplierorder::class);
    }
    public function lasttouch (){
        return $this->belongsTo(User::class, "sys_lasttouch", "id");
    }
}
