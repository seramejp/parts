<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Existingorder extends Model
{
    //
    protected $guarded = array();
    public function supplierorders (){
    	return $this->hasMany(Supplierorder::class);
    }

    public function supplierordersid (){
    	return $this->hasMany(Supplierorder::class)->whereNotIn("sys_finalstatus", ["Pending"]);
    }

    public function lasttouch (){
        return $this->belongsTo(User::class, "sys_lasttouch", "id");
    }
}
