<?php

namespace App\Http\Middleware;

use Closure;

class RolesAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $loggedIn = $request->user();

        //If Admin or Super, proceed
        if ($loggedIn->accesslevel == "99" || 
                $loggedIn->accesslevel == "1") {
            return $next($request);    

        //If  Parts Team
        } elseif($loggedIn->accesslevel == "2"){
            return redirect('existingorders');

        //If  Purchasing Team
        }elseif($loggedIn->accesslevel == "3"){
            return redirect('existingorders');

        //If  CS Team
        }elseif($loggedIn->accesslevel == "4"){
            return redirect('existingorders');
        }

        abort(404, "Admin, No way.");
    }
}
