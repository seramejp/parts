<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Partsku;
use App\Supplier;
use App\Supplierorder;
use App\Specialorder;
use App\Existingorder;
use App\Backorder;
use App\Openbox;
use App\Rap;

use DB;
use Auth;
use Excel;

class DataRoutesController extends Controller
{
    //
    /**
     * Controller for Parts
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Load View for Supplierorders Main List
     *
     * @return void
     */
    public function import (){
        return view("csv.import");
    }


    /**
     * Load View for Supplierorders Main List
     *
     * @return void
     */
    public function export (){
        return view("csv.export");
    }


    /**
     * Load Export for PI
     *
     * @return void
     */
    public function exportparts (){
        $parts = Partsku::join("stocks", "stocks.partsku_id", "partskus.id")
            ->where("stocks.sys_iscurrent", "1")
            ->where("stocks.sys_finalstatus", "Pending")
            ->select("partskus.id", 
                "partskus.partsku", "partskus.descr",
                "partskus.suppcode", "partskus.parentcode", 
                "partskus.oldsku", "partskus.relatedsku",  
                "partskus.whloc", "partskus.bulkloc", 
                "partskus.barcode", "partskus.wiseitemid", 
                "partskus.sys_isactive", "partskus.descr", 
                "stocks.soh",  "stocks.restockqty", 
                "stocks.onorderqty", "stocks.salespast150d", 
                "stocks.sys_status", "stocks.sys_finalstatus")
            ->orderBy("partskus.updated_at", "partskus.created_at", "partskus.partsku")
            ->get();


        Excel::create('PartSKUs_basic', function($excel) use ($parts) {
            $excel->sheet('Sheet1', function($sheet) use ($parts) {
                $sheet->loadView('reports.pixlsx')->with("partskus", $parts);
            });
        })->export('xlsx');

    }


    /**
     * Load Export for Backorders
     *
     * @return void
     */
    public function exportbackorders (){
        $backorders = Backorder::select("id", "dateinvoiced", "ordernum", "parentcode", "suppcode", "partsku", 
            "descr", "ordertype", "category", "eta", "qty", "remarks", "spid", "sys_orderstatus", "sys_finalstatus")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();


        Excel::create('BackOrders_basic', function($excel) use ($backorders) {
            $excel->sheet('Sheet1', function($sheet) use ($backorders) {
                $sheet->loadView('reports.bkoxlsx')->with("backorders", $backorders);
            });
        })->export('xlsx');

    }



    /**
     * Load Export for Special Orders
     *
     * @return void
     */
    public function exportspecialorders (){
        $specialorders = Specialorder::select("id", "created_at", "ordernum", 
                "parentcode", "suppcode", 
                "partsku", "qty", 
                "sys_orderstatus", "sys_finalstatus", 
                "descr", "fault", "rma")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();


        Excel::create('SpecialOrders_basic', function($excel) use ($specialorders) {
            $excel->sheet('Sheet1', function($sheet) use ($specialorders) {
                $sheet->loadView('reports.speoxlsx')->with("specialorders", $specialorders);
            });
        })->export('xlsx');

    }



    /**
     * Load Export for Openboxes Orders
     *
     * @return void
     */
    public function exportopenboxes (){
        $openboxes = Openbox::select("id", "created_at", "referencenum", 
            "parentcode", "suppcode", 
            "partsku", "qty", 
            "whloc", "descr", "fault", "spid", "kayakoid", "sys_addedby",
            "sys_orderstatus", "sys_finalstatus" )
            ->where("is_archived", 0)
            ->with("user")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();


        Excel::create('Openboxes_basic', function($excel) use ($openboxes) {
            $excel->sheet('Sheet1', function($sheet) use ($openboxes) {
                $sheet->loadView('reports.opbxlsx')->with("openboxes", $openboxes);
            });
        })->export('xlsx');

    }



    /**
     * Load Export for Openboxes Orders
     *
     * @return void
     */
    public function exportraps (){
         $raps = Rap::select("created_at", "rmanum", 
                "parentcode", "suppcode", 
                "partsku", "descr", 
                "kayakoid", "spid", 
                "notes", "whloc", 
                "qty",  "outcome", 
                "sys_orderstatus", "sys_finalstatus")
            ->where("is_archived", 0)
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();


        Excel::create('Raps_basic', function($excel) use ($raps) {
            $excel->sheet('Sheet1', function($sheet) use ($raps) {
                $sheet->loadView('reports.rapxlsx')->with("raps", $raps);
            });
        })->export('xlsx');

    }


    /**
     * Load Export for Suppleir Orders from Existing ORders
     *
     * @return void
     */
    public function exportsupplierorders (Existingorder $exOrd){
         $supplierorders = Supplierorder::where("existingorder_id", $exOrd->id)
            ->where("sys_finalstatus", $exOrd->sys_finalstatus)
            ->with("specialordersnonp", "backordersnonp", "stocksnonp", "openboxesnonp", "rapsnonp")
            ->get();

        Excel::create('Supplier_Order', function($excel) use ($supplierorders) {
            foreach ($supplierorders as $supps) {
                $excel->sheet($supps->suppcode, function($sheet) use ($supps) {
                    $sheet->loadView('reports.exoxlsx')->with("supps", $supps);
                });   
            }
        })->export('xlsx');

    }



    /**
     * Load Export for Suppleir Orders from Existing ORders
     *
     * @return void
     */
    public function exportexistingorders (){
         $exords = Existingorder::select("created_at", "spnum",
                "parentcode", "suppcode", 
                "partsku", "descr", 
                "orderavenue", "eta", 
                "notes", "reorderqty", 
                "backorder",  "openbox", 
                "specialorder",  "rap", 
                "sys_total", "sys_finalstatus")
            ->orderBy("created_at", "partsku")
            ->get();


        Excel::create('Existing_Orders', function($excel) use ($exords) {
            $excel->sheet('Sheet1', function($sheet) use ($exords) {
                $sheet->loadView('reports.exoxlsx')->with("exords", $exords);
            });
        })->export('xlsx');

    }



}
