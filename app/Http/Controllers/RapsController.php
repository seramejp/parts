<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Storage;

use App\Parentsku;
use App\Supplier;
use App\Openbox;
use App\Supplierorder;
use App\Rap;

class RapsController extends RootController
{

    /**
     * Controller for Parts
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Load View for Special Orders Main List
     *
     * @return void
     */
    public function index (){
        return view("raps.index");
    }


    /**
     * Load View for Creating a New Special Order
     *
     * @return list of Parents, list of Suppliers
     */
    public function create(){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();
        return view("raps.create", compact('parents', 'suppliers'));
    }



   /**
     * Save the Newly created Special Order
     * @todo Function for those that needs Ordering - how to make new Partsku go to Supplier Orders
     * @param Request $request the Part created by the user
     * @return void
     */
    public function saveNewRap (Request $request){
        $newRap = new Rap;

        $newRap->rmanum = $request->rmanum;
        $newRap->parentcode = $request->parentcode;
        $newRap->suppcode = $request->suppcode;
        $newRap->partsku = $request->partsku;

        $newRap->descr = $request->descr;
        $newRap->kayakoid = $request->kayakoid;
        $newRap->notes = $request->notes;
        $newRap->spid = $request->spid;
        $newRap->whloc = $request->whloc;
        $newRap->qty = $request->qty;
        $newRap->outcome = $request->outcome;

        $newRap->sys_orderstatus = $request->sys_orderstatus;
        $newRap->sys_finalstatus = $request->sys_finalstatus;
        $newRap->sys_addedby = Auth::user()->id;
        $newRap->sys_lasttouch = Auth::user()->id;        
        $newRap->sys_isactive = 1; //Active always
    
        $newRap->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));
        }

        $newRap->save();

        $this->rapsordersGenerateJson();
        $this->forSupplierOrders($request, $newRap);
        $this->supplierordersGenerateJson();

        return redirect("/raps");
    }



    /**
     * Save New opbs to Supplier Orders
     * @param Request $request The variables sent by the user for creation
     * @return void
     */
    public function forSupplierOrders(Request $request, Rap $rma){
            $thereExistsSuppOrd = Supplierorder::where("partsku", $request->partsku)
                ->where("suppcode", $request->suppcode)
                ->where("parentcode", $request->parentcode)
                ->where("sys_isactive", "1")
                ->where("sys_finalstatus", "Pending")
                ->first();

            if (!is_null($thereExistsSuppOrd)) { // if there exist same partsku in Supplier Order
                // There should only be ONE Pending at a time
                if ($rma->supplierorder_id != $thereExistsSuppOrd->id) {
                    $thereExistsSuppOrd->rap += $request->qty;
                    $thereExistsSuppOrd->save();
                }else{
                    $thereExistsSuppOrd->rap = $request->qty;
                    $thereExistsSuppOrd->save();
                }
                
                $rma->supplierorder_id = $thereExistsSuppOrd->id;
                $rma->sys_finalstatus = "Pending";
                $rma->save();
                

                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

            }else { //There are no partskus in Supplier Order

                $newSuppOrd = new Supplierorder;
                $newSuppOrd->partsku = $request->partsku;
                $newSuppOrd->descr = $request->descr;
                $newSuppOrd->parentcode = $request->parentcode;
                $newSuppOrd->suppcode = $request->suppcode;
                $newSuppOrd->rap = $request->qty;
                $newSuppOrd->sys_finalstatus = "Pending";

                $newSuppOrd->sys_addedby = Auth::user()->id;
                $newSuppOrd->sys_isactive = 1;
                $newSuppOrd->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
                
                $newSuppOrd->save();

                $rma->sys_finalstatus = "Pending";
                $rma->supplierorder_id = $newSuppOrd->id;
                $rma->save();

                $this->recomputeSupplierOrderTotals($newSuppOrd);
            }
    }



    /**
     * Load View for Editing Special Orders
     * @param openbox $special the opb instance
     * @return List of Parents, Suppliers, the Part SKU instance, Stocks related to the Part SKU and the Current Stock
     */
    public function edit(Rap $rma){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();

        return view("raps.edit", compact('parents', 'suppliers', 'rma'));
    }




    /**
     * Save modification by user
     * @param Request $request, Partsku $part the Partsku instance
     * @return void
     */
    public function saveUpdateMain(Request $request, Rap $rma){
        

        if ($request->sys_finalstatus == "Cancelled" or $request->sys_finalstatus == "Unresolved") {
                if ( !empty($rma->supplierorder_id )) {
                    $thereExistsSuppOrd = Supplierorder::where("id", $rma->supplierorder_id)
                        ->first();

                    if (!empty($thereExistsSuppOrd)) { //just a precaution
                        $thereExistsSuppOrd->rap = (intval($thereExistsSuppOrd->rap) - intval($rma->qty));
                        $thereExistsSuppOrd->save();

                        $rma->supplierorder_id = 0;
                        $rma->sys_lasttouch = Auth::user()->id;
                        $rma->save();
                        $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                    }

                    if ($thereExistsSuppOrd->sys_total == 0) {
                        $thereExistsSuppOrd->sys_finalstatus = ($request->sys_finalstatus == "Cancelled" ? "Cancelled" : "Unresolved");
                        $thereExistsSuppOrd->sys_isactive = 0;
                        $thereExistsSuppOrd->save();
                    }

                }
        }else if($request->sys_finalstatus == "Pending"){ //Pending pa rin
                $thereExistsSuppOrd = Supplierorder::where("id", $rma->supplierorder_id)
                        ->first();

                $anotherSuppOrd = Supplierorder::where("partsku", $request->partsku)
                    ->where("suppcode", $request->suppcode)
                    ->where("parentcode", $request->parentcode)
                    ->where("sys_isactive", "1")
                    ->where("sys_finalstatus", "Pending")
                    ->first();


                if ($request->parentcode != "0" && $request->suppcode != "0") {
                    if (!empty($thereExistsSuppOrd)) { //just a precaution
                        //dd("TOP", $thereExistsSuppOrd);

                        if ($request->parentcode == $thereExistsSuppOrd->parentcode && $request->suppcode == $thereExistsSuppOrd->suppcode) {
                            
                            $thereExistsSuppOrd->rap = (intval($thereExistsSuppOrd->rap) - intval($rma->qty)) + intval($request->qty);
                            $thereExistsSuppOrd->save();

                        }else{
                             // there has been a change in either supplier or parentcode
                            // if anotherSuppOrd is not empty, attach to that suppord
                            // else, create a new supplierorder


                            if (!empty($anotherSuppOrd)) {
                                $anotherSuppOrd->rap += intval($request->qty);
                                $anotherSuppOrd->save();

                                $this->recomputeSupplierOrderTotals($anotherSuppOrd);

                                $rma->supplierorder_id = $anotherSuppOrd->id;
                                $rma->sys_finalstatus = "Pending";
                                $rma->save();

                                $thereExistsSuppOrd->rap = intval($thereExistsSuppOrd->rap) - intval($rma->qty);
                                $thereExistsSuppOrd->save();

                                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

                                if (intval($thereExistsSuppOrd->sys_total) == 0) {
                                    $thereExistsSuppOrd->sys_finalstatus = "Cancelled";
                                    $thereExistsSuppOrd->save();    
                                }

                            }else{

                                $newSuppOrd = new Supplierorder;
                                $newSuppOrd->partsku = $request->partsku;
                                $newSuppOrd->descr = $request->descr;
                                $newSuppOrd->parentcode = $request->parentcode;
                                $newSuppOrd->suppcode = $request->suppcode;
                                $newSuppOrd->rap = $request->qty;
                                $newSuppOrd->sys_finalstatus = "Pending";

                                $newSuppOrd->sys_addedby = Auth::user()->id;
                                $newSuppOrd->sys_isactive = 1;
                                $newSuppOrd->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
                                
                                $newSuppOrd->save();

                                $rma->sys_finalstatus = "Pending";
                                $rma->supplierorder_id = $newSuppOrd->id;
                                $rma->save();

                                $this->recomputeSupplierOrderTotals($newSuppOrd);

                            }
                        }

                        //dd($thereExistsSuppOrd->rap . "; " . $rma->qty . "; ". $request->qty . ";");

                        $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                    }else{ // no supplierorder pa, meaning, tie in if there is a anotherSuppOrd
                        //dd("BOTTOM", $thereExistsSuppOrd);

                        if (!empty($anotherSuppOrd)) {
                            $anotherSuppOrd->rap += intval($request->qty);
                            $anotherSuppOrd->save();

                            $this->recomputeSupplierOrderTotals($anotherSuppOrd);

                            $rma->supplierorder_id = $anotherSuppOrd->id;
                            $rma->sys_finalstatus = "Pending";
                            $rma->save();
                        }else{
                            $this->forSupplierOrders($request, $rma);
                        }
                    }

                }else{
                    $rma->sys_finalstatus = "Unresolved";
                    $rma->save();
                }
        }


        $rma->update($request->except(
            'created_at', 'photo'
            ));
        $rma->sys_lasttouch = Auth::user()->id;
        $rma->save();

        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));

            $rma->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
        }
                
        $rma->save();


        if ($request->parentcode == "0" or $request->suppcode == "0") {
            $rma->sys_finalstatus = "Unresolved";
            $rma->save();
        }

        $this->rapsordersGenerateJson();
        $this->supplierordersGenerateJson();
       
        return redirect('/raps');
    }



    /**
     * Save modification by user
     * @param Request $request, Partsku $part the Partsku instance
     * @return void
     */
    public function cloneview (Rap $rma){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();
        return view('raps.clone',compact('parents', 'suppliers', 'rma'));
    }








}
