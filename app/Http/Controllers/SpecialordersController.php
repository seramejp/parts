<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Storage;

use App\Parentsku;
use App\Supplier;
use App\Specialorder;
use App\Supplierorder;


class SpecialordersController extends RootController
{
    //
    /**
     * Controller for Parts
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Generate JSON File
     *
     * @return void
     */
    public function generate(){
        return $this->specialordersGenerateJson();
	}


	/**
     * Load View for Special Orders Main List
     *
     * @return void
     */
    public function index (){
        return view("specialorders.index");
    }



    /**
     * Load View for Creating a New Special Order
     *
     * @return list of Parents, list of Suppliers
     */
    public function create(){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();
        return view("specialorders.create", compact('parents', 'suppliers'));
    }



    /**
     * Save New SpOs to Supplier Orders
     * @param Request $request The variables sent by the user for creation
     * @return void
     */
    public function forSupplierOrders(Request $request, Specialorder $spo){
            $thereExistsSuppOrd = Supplierorder::where("partsku", $request->partsku)
                ->where("suppcode", $request->suppcode)
                ->where("parentcode", $request->parentcode)
                ->where("sys_isactive", "1")
                ->where("sys_finalstatus", "Pending")
                ->first();

            if (!is_null($thereExistsSuppOrd)) { // if there exist same partsku in Supplier Order
                // There should only be ONE Pending at a time
            	if ($spo->supplierorder_id != $thereExistsSuppOrd->id) {
            		$thereExistsSuppOrd->specialorder += $request->qty;
	                $thereExistsSuppOrd->save();
            	}else{
            		$thereExistsSuppOrd->specialorder = $request->qty;
	                $thereExistsSuppOrd->save();
            	}
                
                $spo->supplierorder_id = $thereExistsSuppOrd->id;
                $spo->sys_finalstatus = "Pending";
                $spo->save();
                

                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

            }else { //There are no partskus in Supplier Order

                $newSuppOrd = new Supplierorder;
                $newSuppOrd->partsku = $request->partsku;
                $newSuppOrd->descr = $request->descr;
                $newSuppOrd->parentcode = $request->parentcode;
                $newSuppOrd->suppcode = $request->suppcode;
                $newSuppOrd->specialorder = $request->qty;
                $newSuppOrd->sys_finalstatus = "Pending";

                $newSuppOrd->sys_addedby = Auth::user()->id;
                $newSuppOrd->sys_isactive = 1;
                $newSuppOrd->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
                
                $newSuppOrd->save();

                $spo->sys_finalstatus = "Pending";
                $spo->supplierorder_id = $newSuppOrd->id;
                $spo->save();

                $this->recomputeSupplierOrderTotals($newSuppOrd);
            }
    }




    /**
     * Save the Newly created Special Order
     * @todo Function for those that needs Ordering - how to make new Partsku go to Supplier Orders
     * @param Request $request the Part created by the user
     * @return void
     */
    public function saveNewSpO (Request $request){
        $newSpo = new Specialorder;

        $newSpo->ordernum = $request->ordernum;
        $newSpo->parentcode = $request->parentcode;
        $newSpo->suppcode = $request->suppcode;
        $newSpo->partsku = $request->partsku;

        $newSpo->descr = $request->descr;
        $newSpo->rma = $request->rma;
        $newSpo->fault = $request->fault;
        $newSpo->qty = $request->qty;

        $newSpo->sys_orderstatus = $request->sys_orderstatus;
        $newSpo->sys_finalstatus = $request->sys_finalstatus;
        $newSpo->sys_addedby = Auth::user()->id;
        $newSpo->sys_lasttouch = Auth::user()->id;        
        $newSpo->sys_isactive = 1; //Active always
    
        $newSpo->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));
        }

        $newSpo->save();

        

        if ($request->parentcode != "0" && $request->suppcode != "0") {
            $this->forSupplierOrders($request, $newSpo);
            $this->supplierordersGenerateJson();
        }else{
            $newSpo->sys_finalstatus = "Unresolved";
            $newSpo->save();
        }
            
        $this->generate();

        return redirect("/specialorders");
        
    }


    /**
     * Ajax to get image from storage
     * @param Specialorder $spo the Partsku instance
     * @return base 64 encoded image
     */
    public function getImage (Specialorder $spo){
        return base64_encode(Storage::get('/public/' . $spo->photo));
    }




    /**
     * Load View for Editing Special Orders
     * @param Specialorder $special the SPO instance
     * @return List of Parents, Suppliers, the Part SKU instance, Stocks related to the Part SKU and the Current Stock
     */
    public function edit(Specialorder $spo){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();

        return view("specialorders.edit", compact('parents', 'suppliers', 'spo'));
    }



    /**
     * Save modification by user
     * @param Request $request, Partsku $part the Partsku instance
     * @return void
     */
    public function saveUpdateMain(Request $request, Specialorder $spo){
        

        if ($request->sys_finalstatus == "Cancelled" or $request->sys_finalstatus == "Unresolved") {
                if ( !empty($spo->supplierorder_id )) {
                    $thereExistsSuppOrd = Supplierorder::where("id", $spo->supplierorder_id)
                        ->first();

                    if (!empty($thereExistsSuppOrd)) { //just a precaution
                        $thereExistsSuppOrd->specialorder = (intval($thereExistsSuppOrd->specialorder) - intval($spo->qty));
                        $thereExistsSuppOrd->save();

                        $spo->supplierorder_id = 0;
                        $spo->sys_lasttouch = Auth::user()->id;
                        $spo->save();
                        $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                    }

                    if ($thereExistsSuppOrd->sys_total == 0) {
                        $thereExistsSuppOrd->sys_finalstatus = ($request->sys_finalstatus == "Cancelled" ? "Cancelled" : "Unresolved");
                        $thereExistsSuppOrd->sys_isactive = 0;
                        $thereExistsSuppOrd->save();
                    }

                }
        }else if($request->sys_finalstatus == "Pending"){ //Pending pa rin
                $thereExistsSuppOrd = Supplierorder::where("id", $spo->supplierorder_id)
                        ->first();

                $anotherSuppOrd = Supplierorder::where("partsku", $request->partsku)
                ->where("suppcode", $request->suppcode)
                ->where("parentcode", $request->parentcode)
                ->where("sys_isactive", "1")
                ->where("sys_finalstatus", "Pending")
                ->first();


                if ($request->parentcode != "0" && $request->suppcode != "0") {
                    if (!empty($thereExistsSuppOrd)) { //just a precaution
                        //dd("TOP", $thereExistsSuppOrd);

                        if ($request->parentcode == $thereExistsSuppOrd->parentcode && $request->suppcode == $thereExistsSuppOrd->suppcode) {
                            
                            $thereExistsSuppOrd->specialorder = (intval($thereExistsSuppOrd->specialorder) - intval($spo->qty)) + intval($request->qty);
                            $thereExistsSuppOrd->save();

                        }else{
                             // there has been a change in either supplier or parentcode
                            // if anotherSuppOrd is not empty, attach to that suppord
                            // else, create a new supplierorder


                            if (!empty($anotherSuppOrd)) {
                                $anotherSuppOrd->specialorder += intval($request->qty);
                                $anotherSuppOrd->save();

                                $this->recomputeSupplierOrderTotals($anotherSuppOrd);

                                $spo->supplierorder_id = $anotherSuppOrd->id;
                                $spo->sys_finalstatus = "Pending";
                                $spo->save();

                                $thereExistsSuppOrd->specialorder = intval($thereExistsSuppOrd->specialorder) - intval($spo->qty);
                                $thereExistsSuppOrd->save();

                                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

                                if (intval($thereExistsSuppOrd->sys_total) == 0) {
                                    $thereExistsSuppOrd->sys_finalstatus = "Cancelled";
                                    $thereExistsSuppOrd->save();    
                                }

                            }else{

                                $newSuppOrd = new Supplierorder;
                                $newSuppOrd->partsku = $request->partsku;
                                $newSuppOrd->descr = $request->descr;
                                $newSuppOrd->parentcode = $request->parentcode;
                                $newSuppOrd->suppcode = $request->suppcode;
                                $newSuppOrd->specialorder = $request->qty;
                                $newSuppOrd->sys_finalstatus = "Pending";

                                $newSuppOrd->sys_addedby = Auth::user()->id;
                                $newSuppOrd->sys_isactive = 1;
                                $newSuppOrd->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
                                
                                $newSuppOrd->save();

                                $spo->sys_finalstatus = "Pending";
                                $spo->supplierorder_id = $newSuppOrd->id;
                                $spo->save();

                                $this->recomputeSupplierOrderTotals($newSuppOrd);

                            }
                        }

                        //dd($thereExistsSuppOrd->specialorder . "; " . $spo->qty . "; ". $request->qty . ";");

                        $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                    }else{ // no supplierorder pa, meaning, tie in if there is a anotherSuppOrd
                        //dd("BOTTOM", $thereExistsSuppOrd);

                        if (!empty($anotherSuppOrd)) {
                            $anotherSuppOrd->specialorder += intval($request->qty);
                            $anotherSuppOrd->save();

                            $this->recomputeSupplierOrderTotals($anotherSuppOrd);

                            $spo->supplierorder_id = $anotherSuppOrd->id;
                            $spo->sys_finalstatus = "Pending";
                            $spo->save();
                        }else{
                            $this->forSupplierOrders($request, $spo);
                        }
                    }

                }else{
                    $spo->sys_finalstatus = "Unresolved";
                    $spo->save();
                }
        }

        $spo->update($request->except(
            'created_at', 'photo'
            ));
        $spo->sys_lasttouch = Auth::user()->id;
        $spo->save();


        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));
            $spo->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
            $spo->save();
        }

        if ($request->parentcode == "0" or $request->suppcode == "0") {
            $spo->sys_finalstatus = "Unresolved";
            $spo->save();
        }
        

        $this->generate();
        $this->supplierordersGenerateJson();
       
        return redirect('/specialorders');
    }



}

