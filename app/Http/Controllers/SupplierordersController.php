<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use App\Partsku;
use App\Parentsku;
use App\Supplier;
use App\Stock;
use App\Supplierorder;
use App\Specialorder;
use App\Existingorder;
use App\Backorder;
use App\Openbox;
use App\Rap;

use Auth;
use Excel;
use PHPExcel_Worksheet_Drawing;

class SupplierordersController extends RootController
{
    //
    /**
     * Controller for Parts
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Re-Generate ALL JSON File
     *
     * @return void
     */
    public function generateAllJson(){
        $this->partsGenerateJson();
        $this->supplierordersGenerateJson();
        $this->specialordersGenerateJson();
        $this->existingordersGenerateJson();
        $this->backordersGenerateJson();
        $this->openboxesordersGenerateJson();
        $this->rapsordersGenerateJson();

        return "All Data Generated.";
    }


    /**
     * Generate JSON File
     *
     * @return void
     */
    public function generate(){
        return $this->supplierordersGenerateJson();
    }


    /**
     * Load View for Supplierorders Main List
     *
     * @return void
     */
    public function index (){
        return view("supplierorders.index2");
    }




    /**
     * Convert json decoded array to Collection and group by Supplier
     *
     * @return void
     */
    public function convertJSONArrayToCollection(Array $forExistingOrders){
        $draftListOfSO = new Collection;
        foreach ($forExistingOrders as $item) {
            if ($draftListOfSO->count() == 0) {
                
                //$arr = [$item->id];
                $arr = [$item];
                $draftListOfSO->push(["suppcode" => $item->suppcode, "parents" => $arr]);
            }else{
                
                if (!$draftListOfSO->contains("suppcode", $item->suppcode)) {
                    //$arr = [$item->id];
                    $arr = [$item];
                    $draftListOfSO->push(["suppcode" => $item->suppcode, "parents" => $arr]);
                }else{

                    $draftListOfSO = $draftListOfSO->map(function($soItem, $key) use ($item){
                        if ($soItem["suppcode"] == $item->suppcode) {
                            //dd($soItem["parents"]);
                            //array_push($soItem["parents"], $item->id);
                            array_push($soItem["parents"], $item);
                        }
                        return $soItem;
                    });
                }
            }

        }
        return $draftListOfSO;
    }




    /**
     * Gets the post values for existing orders
     * 
     * @param Supplierorder $suppo the Supplier Order instance
     * @return String the HTML markup for the rest of the table
     */
    public function toexisting (Request $request){
        $forExistingOrders = json_decode($request->toex_supplierorders);
        
        $draftListOfSO = $this->convertJSONArrayToCollection($forExistingOrders);


        //dd($forExistingOrders, $draftListOfSO);
        
        //dd($forExistingOrders);
        
        Excel::create('Supplier_Order', function($excel) use ($draftListOfSO) {
            
            foreach ($draftListOfSO as $item) {
                
                $newExistingOrder = new Existingorder;

                $newExistingOrder->suppcode = $item["suppcode"];
                $newExistingOrder->sys_isactive = 1;
                $newExistingOrder->sys_addedby = Auth::user()->id;
                
                foreach ($item["parents"] as $thisSuppCode) {
                    $theSupplierorder = Supplierorder::find($thisSuppCode->id);

                    //dd($theSupplierorder->stockphoto);
                    if (count($item["parents"]) == 1) {
                        $newExistingOrder->partsku .= $thisSuppCode->partsku;
                        $newExistingOrder->descr = $thisSuppCode->descr;
                        $newExistingOrder->parentcode = $thisSuppCode->parentcode;
                    }else{
                        $newExistingOrder->partsku .= $thisSuppCode->partsku . ", ";
                        $newExistingOrder->descr = $thisSuppCode->descr . ", ";
                        $newExistingOrder->parentcode = $thisSuppCode->parentcode . ", ";
                    }

                    $newExistingOrder->save();

                    $theSupplierorder = Supplierorder::find($thisSuppCode->id);

                    if ($thisSuppCode->sys_total == $thisSuppCode->totalorders) {
                            
                            $theSupplierorder->sys_finalstatus = "Pending with Purchasing";
                            $theSupplierorder->existingorder_id = $newExistingOrder->id;
                            $theSupplierorder->save();

                            $newExistingOrder->sys_finalstatus = "Pending with Purchasing";
                            
                        //PI:
                            if ($thisSuppCode->pigo == "1") {
                                $theStock = Stock::find($thisSuppCode->stocksid[0]->id);
                                
                                $theStock->sys_finalstatus = "Pending with Purchasing";
                                $theStock->save();

                                $newExistingOrder->reorderqty = $thisSuppCode->reorderqty;
                                $newExistingOrder->save();
                                $this->recomputeExistingOrderTotals($newExistingOrder);
                                $this->partsGenerateJson();
                            }

                        //Special Order
                            if ($thisSuppCode->spogo != "" || $thisSuppCode->spogo != "0") {
                                //dd($thisSuppCode->specialordersid);
                                foreach ($thisSuppCode->specialordersid as $spo) {
                                    $theSpOrder = Specialorder::find($spo->id);
                                    $theSpOrder->sys_finalstatus = "Pending with Purchasing";
                                    $theSpOrder->save();

                                    $newExistingOrder->specialorder = intval($newExistingOrder->specialorder) + intval($spo->qty);
                                    $newExistingOrder->save();
                                    $this->recomputeExistingOrderTotals($newExistingOrder);
                                }
                                $this->specialordersGenerateJson();
                            }

                        //Back Order
                            if ($thisSuppCode->bkogo != "" || $thisSuppCode->bkogo != "0") {
                                //dd($thisSuppCode->specialordersid);
                                foreach ($thisSuppCode->backordersid as $bko) {
                                    $theBkOrder = Backorder::find($bko->id);
                                    $theBkOrder->sys_finalstatus = "Pending with Purchasing";
                                    $theBkOrder->save();

                                    $newExistingOrder->backorder = intval($newExistingOrder->backorder) + intval($bko->qty);
                                    $newExistingOrder->save();
                                    $this->recomputeExistingOrderTotals($newExistingOrder);
                                }
                                $this->backordersGenerateJson();
                            }

                        //Openbox
                            if ($thisSuppCode->opbgo != "" || $thisSuppCode->opbgo != "0") {
                                //dd($thisSuppCode->specialordersid);
                                foreach ($thisSuppCode->openboxesid as $opb) {
                                    $theOpbOrder = Openbox::find($opb->id);
                                    $theOpbOrder->sys_finalstatus = "Pending with Purchasing";
                                    $theOpbOrder->save();

                                    $newExistingOrder->openbox = intval($newExistingOrder->openbox) + intval($opb->qty);
                                    $newExistingOrder->save();
                                    $this->recomputeExistingOrderTotals($newExistingOrder);
                                }
                                $this->openboxesordersGenerateJson();
                            }


                        //RAPs
                            if ($thisSuppCode->rapgo != "" || $thisSuppCode->rapgo != "0") {
                                //dd($thisSuppCode->specialordersid);
                                foreach ($thisSuppCode->rapsid as $rap) {
                                    $theRapOrder = Rap::find($rap->id);
                                    $theRapOrder->sys_finalstatus = "Pending with Purchasing";
                                    $theRapOrder->save();

                                    $newExistingOrder->rap = intval($newExistingOrder->rap) + intval($rap->qty);
                                    $newExistingOrder->save();
                                    $this->recomputeExistingOrderTotals($newExistingOrder);
                                }
                                $this->rapsordersGenerateJson();
                            }


                            $this->existingordersGenerateJson();
                            $this->supplierordersGenerateJson();

                    } //end if systotal == totalorders
                    else{
                            
                            //Create a New Supplier Order, accepting all the data that is to be ordered. The difference will remain in Supplier Orders as Pending
                            $newSupplierOrder = new Supplierorder;
                            $newSupplierOrder->partsku = $theSupplierorder->partsku;
                            $newSupplierOrder->descr = $theSupplierorder->descr;
                            $newSupplierOrder->suppcode = $theSupplierorder->suppcode;
                            $newSupplierOrder->parentcode = $theSupplierorder->parentcode;
                            $newSupplierOrder->sys_isactive = 1;
                            $newSupplierOrder->sys_addedby = Auth::user()->id;
                            $newSupplierOrder->sys_finalstatus = "Pending with Purchasing";
                            $newSupplierOrder->photo = $theSupplierorder->photo;
                            $newSupplierOrder->existingorder_id = $newExistingOrder->id;
                            $newSupplierOrder->save();

                            $newExistingOrder->sys_finalstatus = "Pending with Purchasing";
                            $newExistingOrder->save();

                        //not equal the total, pigo can be 1 or 0
                            if ($thisSuppCode->pigo == "1") {
                                $theOldStock = Stock::find($thisSuppCode->stocksid[0]->id);
                                $theOldStock->reorderqty = intval($theOldStock->reorderqty) - intval($thisSuppCode->orderqty);
                                $theOldStock->save();
                                
                                $newStock = new Stock;
                                $newStock->partsku = $thisSuppCode->partsku;
                                $newStock->reorderqty = $thisSuppCode->orderqty;
                                $newStock->soh = $theOldStock->soh;
                                $newStock->restockqty = $theOldStock->restockqty;
                                $newStock->salespast150d = $theOldStock->salespast150d;
                                $newStock->sys_iscurrent = "0";
                                $newStock->sys_status = $theOldStock->sys_status;
                                $newStock->sys_finalstatus = "Pending with Purchasing";
                                $newStock->sys_addedby = $theOldStock->sys_addedby;
                                $newStock->sys_lasttouch = Auth::user()->id;
                                $newStock->sys_isactive = "1";
                                $newStock->partsku_id = $theOldStock->partsku_id;
                                $newStock->supplierorder_id = $newSupplierOrder->id;
                                $newStock->save();

                                $theSupplierorder->reorderqty = intval($theSupplierorder->reorderqty) - intval($thisSuppCode->orderqty);
                                $theSupplierorder->save();
                                $this->recomputeSupplierOrderTotals($theSupplierorder);

                                $newExistingOrder->reorderqty = $thisSuppCode->orderqty;
                                $newExistingOrder->save();
                                $this->recomputeExistingOrderTotals($newExistingOrder);

                                $newSupplierOrder->reorderqty = $thisSuppCode->orderqty;
                                $newSupplierOrder->save();
                                $this->recomputeSupplierOrderTotals($newSupplierOrder);

                                $this->recomputeExistingOrderTotals($newExistingOrder);
                                $this->partsGenerateJson();
                            }


                        //SPo
                            if ($thisSuppCode->spogo != "" && $thisSuppCode->spogo != "0" && $thisSuppCode->spogo != 0) {
                                $spoids = explode(";", $thisSuppCode->spogo);

                                foreach ($spoids as $spo) {
                                    if (trim($spo) != "") {
                                        $theSpOrder = Specialorder::find($spo);
                                        $theSpOrder->supplierorder_id = $newSupplierOrder->id;
                                        $theSpOrder->sys_finalstatus = "Pending with Purchasing";
                                        $theSpOrder->save();

                                        $newSupplierOrder->specialorder = intval($newSupplierOrder->specialorder) + intval($theSpOrder->qty);
                                        $newSupplierOrder->save();
                                        $this->recomputeSupplierOrderTotals($newSupplierOrder);

                                        $theSupplierorder->specialorder = intval($theSupplierorder->specialorder) - intval($theSpOrder->qty);
                                        $theSupplierorder->save();
                                        $this->recomputeSupplierOrderTotals($theSupplierorder);

                                        $newExistingOrder->specialorder = intval($newExistingOrder->specialorder) + intval($theSpOrder->qty);
                                        $newExistingOrder->save();
                                        $this->recomputeExistingOrderTotals($newExistingOrder);
                                    }
                                }
                                $this->specialordersGenerateJson();
                            }


                        //Bko
                            if ($thisSuppCode->bkogo != "" && $thisSuppCode->bkogo != "0" && $thisSuppCode->bkogo != 0) {
                                $bkoids = explode(";", $thisSuppCode->bkogo);
                                
                                foreach ($bkoids as $bko) {
                                    if (trim($bko) != "") {
                                        $theBkOrder = Backorder::find($bko);
                                        $theBkOrder->supplierorder_id = $newSupplierOrder->id;
                                        $theBkOrder->sys_finalstatus = "Pending with Purchasing";
                                        $theBkOrder->save();

                                        $newSupplierOrder->backorder = intval($newSupplierOrder->backorder) + intval($theBkOrder->qty);
                                        $newSupplierOrder->save();
                                        $this->recomputeSupplierOrderTotals($newSupplierOrder);

                                        $theSupplierorder->backorder = intval($theSupplierorder->backorder) - intval($theBkOrder->qty);
                                        $theSupplierorder->save();
                                        $this->recomputeSupplierOrderTotals($theSupplierorder);

                                        $newExistingOrder->backorder = intval($newExistingOrder->backorder) + intval($theBkOrder->qty);
                                        $newExistingOrder->save();
                                        $this->recomputeExistingOrderTotals($newExistingOrder);
                                    }   
                                }
                                $this->backordersGenerateJson();
                            }


                        //Openbox
                            if ($thisSuppCode->opbgo != "" && $thisSuppCode->opbgo != "0" && $thisSuppCode->opbgo != 0) {
                                $opbids = explode(";", $thisSuppCode->opbgo);
                                
                                foreach ($opbids as $opb) {
                                    if (trim($opb) != "") {
                                        $theOpbOrder = Openbox::find($opb);
                                        $theOpbOrder->supplierorder_id = $newSupplierOrder->id;
                                        $theOpbOrder->sys_finalstatus = "Pending with Purchasing";
                                        $theOpbOrder->save();

                                        $newSupplierOrder->openbox = intval($newSupplierOrder->openbox) + intval($theOpbOrder->qty);
                                        $newSupplierOrder->save();
                                        $this->recomputeSupplierOrderTotals($newSupplierOrder);

                                        $theSupplierorder->openbox = intval($theSupplierorder->openbox) - intval($theOpbOrder->qty);
                                        $theSupplierorder->save();
                                        $this->recomputeSupplierOrderTotals($theSupplierorder);

                                        $newExistingOrder->openbox = intval($newExistingOrder->openbox) + intval($theOpbOrder->qty);
                                        $newExistingOrder->save();
                                        $this->recomputeExistingOrderTotals($newExistingOrder);
                                    }   
                                }
                                $this->openboxesordersGenerateJson();
                            }


                        //Rap
                            if ($thisSuppCode->rapgo != "" && $thisSuppCode->rapgo != "0" && $thisSuppCode->rapgo != 0) {
                                $rapids = explode(";", $thisSuppCode->rapgo);
                                
                                foreach ($rapids as $rap) {
                                    if (trim($rap) != "") {
                                        $theRapOrder = Rap::find($rap);
                                        $theRapOrder->supplierorder_id = $newSupplierOrder->id;
                                        $theRapOrder->sys_finalstatus = "Pending with Purchasing";
                                        $theRapOrder->save();

                                        $newSupplierOrder->rap = intval($newSupplierOrder->rap) + intval($theRapOrder->qty);
                                        $newSupplierOrder->save();
                                        $this->recomputeSupplierOrderTotals($newSupplierOrder);

                                        $theSupplierorder->rap = intval($theSupplierorder->rap) - intval($theRapOrder->qty);
                                        $theSupplierorder->save();
                                        $this->recomputeSupplierOrderTotals($theSupplierorder);

                                        $newExistingOrder->rap = intval($newExistingOrder->rap) + intval($theRapOrder->qty);
                                        $newExistingOrder->save();
                                        $this->recomputeExistingOrderTotals($newExistingOrder);
                                    }   
                                }
                                $this->rapsordersGenerateJson();
                            }




                            $this->existingordersGenerateJson();
                            $this->supplierordersGenerateJson();

                    } //end else sys_total != totalorders

                } //end foreach item-parents as thisSuppCode


                
                    $excel->sheet($item["suppcode"], function($sheet) use ($item) {
                        $sheet->loadView('reports.soxlsx')->with("suppliers", $item);
                    });
                
            } //end foreach ($draftListOfSO as $item)

        })->export('xlsx');
        

        return redirect('/supplierorders');
    } // end function toexisting



    /**
     * Load View for Changing Supplier
     *
     * @return void
     */
    public function viewchangesupplier (Supplierorder $suppo){
        //dd($suppo->stocksdesc, $suppo->backordersdesc, $suppo->specialordersdesc, $suppo->openboxesdesc, $suppo->rapsdesc );
        $partskus = Partsku::join("stocks", "stocks.partsku_id", "partskus.id")
                ->whereNotIn("stocks.sys_finalstatus", ["Cancelled", "Received", "In Transit"])
                ->where("partskus.parentcode", $suppo->parentcode)
                ->select("partskus.partsku", "stocks.reorderqty", "stocks.sys_finalstatus")
                ->get();


        $backorders = Backorder::where("parentcode", $suppo->parentcode)
            ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
            ->get();

        $openboxes = Openbox::where("parentcode", $suppo->parentcode)
                ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
                ->get();

        $raps = Rap::where("parentcode", $suppo->parentcode)
                ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
                ->get();

        $specialorders = Specialorder::where("parentcode", $suppo->parentcode)
                ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
                ->get();

        $supplierorders = Supplierorder::where("parentcode", $suppo->parentcode)
                ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
                ->get();



        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();

        return view("supplierorders.changesupplier", compact('suppo', 'suppliers', 'partskus', 'backorders', 'openboxes', 'raps', 'specialorders', 'supplierorders'));
    }
    //


    /**
     * Load View for Changing Supplier
     *
     * @return void
     */
    public function updateSupplier (Request $request, Supplierorder $suppo){
        
        $toUpdate = Partsku::join("stocks", "stocks.partsku_id", "partskus.id")
            ->whereNotIn("stocks.sys_finalstatus", ["Cancelled", "Received", "In Transit"])
            ->where("partskus.parentcode", $suppo->parentcode)
            ->get();
            /*->update(["partskus.parentcode" => $request->parentcode, "partskus.suppcode" => $request->suppcode, "partskus.updated_at"=>\Carbon\Carbon::now(), "stocks.updated_at"=>\Carbon\Carbon::now()]);*/

        foreach ($toUpdate as $value) {
            $value->suppcode = $request->suppcode;
            $value->save();
        }

        Backorder::where("parentcode", $suppo->parentcode)
            ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
            ->update(["suppcode" => $request->suppcode]);

        Openbox::where("parentcode", $suppo->parentcode)
            ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
            ->update(["suppcode" => $request->suppcode]);

        Rap::where("parentcode", $suppo->parentcode)
            ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
            ->update(["suppcode" => $request->suppcode]);

        Specialorder::where("parentcode", $suppo->parentcode)
            ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
            ->update(["suppcode" => $request->suppcode]);

        Supplierorder::where("parentcode", $suppo->parentcode)
            ->whereNotIn("sys_finalstatus", ["Cancelled", "Received", "In Transit"])
            ->update(["suppcode" => $request->suppcode]);

        Parentsku::where("parentcode", $suppo->parentcode)
            ->update(["suppcode" => $request->suppcode]);

        $this->generateAllJson();

        return redirect('/supplierorders');
    }

}
