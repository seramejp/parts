<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Storage;

use App\Partsku;
use App\Parentsku;
use App\Supplier;
use App\Stock;
use App\Supplierorder;

class PartsController extends RootController
{
    /**
     * Controller for Parts
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Generate JSON File
     *
     * @return void
     */
    public function generate(){
        return $this->partsGenerateJson();
    }


    /**
     * Load View for Parts Main List
     *
     * @return void
     */
    public function index (){
        return view("parts.index");
    }


    /**
     * Load View for Creating a New Part
     *
     * @return list of Parents, list of Suppliers
     */
    public function create(){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();
        return view("parts.create", compact('parents', 'suppliers'));
    }



    /**
     * Create a New Supplier Order Entry
     * @param Request $request the information required and saved by the user
     * @return Integer id The ID of the newly created Supplier Order Instance
     */
    public function createSupplierOrderEntry(Request $request){
        $newSuppOrd = new Supplierorder;
        $newSuppOrd->partsku = $request->partsku;
        $newSuppOrd->descr = $request->descr;
        $newSuppOrd->parentcode = $request->parentcode;
        $newSuppOrd->suppcode = $request->suppcode;

        if (is_null($request->reorderqty)) {
            $newSuppOrd->reorderqty = $request->onorderqty;    
        }else{
            $newSuppOrd->reorderqty = $request->reorderqty;
        }
            
        
        $newSuppOrd->sys_finalstatus = "Pending";

        $newSuppOrd->sys_addedby = Auth::user()->id;
        $newSuppOrd->sys_isactive = 1;
        $newSuppOrd->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
        
        $newSuppOrd->save();

        $this->recomputeSupplierOrderTotals($newSuppOrd);

        return $newSuppOrd->id;
        
    }



    /**
     * Save New Needs Ordering to Supplier Orders
     * @param Request $request The variables sent by the user for updating/creation
     * @return void
     */
    public function forNeedsOrdering(Request $request, Partsku $part){
        if (  intval($request->soh) <= (intval($request->restockqty)*1.25)  ) {

            $thereExistsSuppOrd = Supplierorder::where("partsku", $request->partsku)
                ->where("suppcode", $request->suppcode)
                ->where("parentcode", $request->parentcode)
                ->where("sys_isactive", "1")
                ->where("sys_finalstatus", "Pending")
                ->first();

            $thisPartsStock = Stock::where("partsku_id", $part->id)
                ->where("sys_iscurrent", "1")
                ->first();

            if (!is_null($thereExistsSuppOrd)) {
                
                $thereExistsSuppOrd->reorderqty = $request->reorderqty;
                $thereExistsSuppOrd->save();

                $thisPartsStock->supplierorder_id = $thereExistsSuppOrd->id;
                $thisPartsStock->sys_finalstatus = "Pending";
                $thisPartsStock->save();
                    
                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

            }else { //There are no partskus in Supplier Order

                $thisPartsStock->supplierorder_id = $this->createSupplierOrderEntry($request);
                $thisPartsStock->sys_finalstatus = "Pending";
                
                $thisPartsStock->save();
            }
        }


    }


    /**
     * Save the Newly created Part SKU
     * @todo Function for those that needs Ordering - how to make new Partsku go to Supplier Orders
     * @param Request $request the Part created by the user
     * @return void
     */
    public function saveNewPart (Request $request){
        $newpart = new Partsku;

        $newpart->partsku = $request->partsku;
        $newpart->descr = $request->descr;
        $newpart->wiseitemid = $request->wiseitemid;
        $newpart->parentcode = $request->parentcode;
        $newpart->suppcode = $request->suppcode;
        $newpart->oldsku = $request->oldsku;
        $newpart->whloc = $request->whloc;
        $newpart->bulkloc = $request->bulkloc;
        $newpart->qtyinbulkloc = $request->qtyinbulkloc;
        
        $newpart->barcode = $request->barcode;

        $newpart->partlen = $request->partlen;
        $newpart->partwidth = $request->partwidth;
        $newpart->partht = $request->partht;
        $newpart->partwt = $request->partwt;
        $newpart->shipmethod = $request->shipmethod;
        $newpart->price = $request->price;

        $newpart->relatedsku = $request->relatedsku;
        $newpart->notes = $request->notes;

        $newpart->sys_addedby = Auth::user()->id;
        $newpart->sys_lasttouch = Auth::user()->id;        

        if (  $request->soh <= (intval($request->restockqty)*1.25)  ) {
            $newpart->needsordering = 1;
        }else{
            $newpart->needsordering = 0;
        }

    
        $newpart->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
        $newpart->sys_isactive = 1; //Active always

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));
        }

        $newpart->save();


        $newStock = new Stock;
        $newStock->partsku = $request->partsku;
        $newStock->soh = $request->soh;
        $newStock->restockqty = $request->restockqty;
        $newStock->reorderqty = $request->reorderqty;
        $newStock->onorderqty = $request->reorderqty;
        $newStock->salespast150d = $request->salespast150d;

        $newStock->sys_iscurrent = "1";
        $newStock->sys_status = "new on order";
        $newStock->sys_finalstatus = "Unresolved";

        $newStock->sys_addedby = Auth::user()->id;
        $newStock->sys_lasttouch = Auth::user()->id;
        $newStock->sys_isactive = "1";
        $newStock->partsku_id = $newpart->id;
        $newStock->save();

        if ($request->forcedPending) {
            //will never merge to anything if Forced to Pending
            $newStock->supplierorder_id = $this->createSupplierOrderEntry($request);
            $newStock->sys_finalstatus = "Pending";
            $newStock->save();

        }else{
            //All Imports require Suppliers and Parent SKUs
            //Manual entries for parts will not compute for Needs Ordering if there is no Parent or Supplier
            //unless forced to be Pending
            if ($request->parentcode != "0" && $request->suppcode != "0") {
                $this->forNeedsOrdering($request, $newpart);
            }       
        }
        $this->generate();
        $this->supplierordersGenerateJson();
        return redirect("/parts");
    }


    /**
     * Ajax to get image from storage
     * @param the Partsku instance
     * @return base 64 encoded image
     */
    public function getImage (Partsku $part){
        return base64_encode(Storage::get('/public/' . $part->photo));
    }


    /**
     * Load View for Editing Part
     * @param the Partsku instance
     * @return List of Parents, Suppliers, the Part SKU instance, Stocks related to the Part SKU and the Current Stock
     */
    public function edit(Partsku $part){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();
        /*$stocks = Stock::select("soh", "restockqty", "reorderqty", "salespast150d", "sys_iscurrent")
            ->where("partsku_id", $part->id)
            ->get();*/

        $currentStock = Stock::select("soh", "restockqty", "reorderqty", "salespast150d", "sys_iscurrent", "sys_finalstatus", "onorderqty", "sys_status", "sys_lasttouch")
            ->where("partsku_id", $part->id)
            ->where("partsku", $part->partsku)
            ->where("sys_iscurrent", "1")
            ->first();

        $partsOrdered = Supplierorder::join("stocks", "stocks.supplierorder_id", "supplierorders.id")
            ->where("stocks.partsku_id", $part->id)
            ->select("supplierorders.suppcode", "supplierorders.created_at", "supplierorders.reorderqty", "supplierorders.sys_total", "supplierorders.sys_finalstatus")
            ->get();


        return view("parts.edit", compact('parents', 'suppliers', 'part', 'currentStock', 'partsOrdered'));
    }



    /**
     * Function to Save Updated Fields (checkbox by user)
     * @param Request, the Partsku instance
     * @return void
     */
    public function updateCheckedFields(Request $request, Partsku $part){
        //Reset has checked
        $part->haschdescr = 0; $part->haschwiseitemid = 0; $part->haschparent = 0;
        $part->haschsuppcode = 0; $part->hascholdsku = 0; $part->haschwhloc = 0;
        $part->haschbulkloc = 0; $part->haschqtyinbulkloc = 0; $part->haschbarcode = 0;
        $part->haschnotes = 0; $part->haschstatus = 0; $part->haschpartlen = 0;
        $part->haschpartwd = 0; $part->haschpartht = 0; $part->haschpartwt = 0;
        $part->haschshipmethod = 0; $part->haschpartprice = 0; $part->haschsoh = 0;
        $part->haschrestockqty = 0; $part->haschreorderqty = 0; $part->haschsaleslast150d = 0;
        $part->haschorderstatus = 0; $part->haschrelatedsku = 0; $part->haschisactive = 0;
        $part->save();

        if (count($request->isfieldupdated)>0) {

            foreach ($request->isfieldupdated as $haschanged) {
                if(strcasecmp("haschdescr", $haschanged)==0) $part->haschdescr = 1;
                if(strcasecmp("haschwiseitemid", $haschanged)==0) $part->haschwiseitemid = 1;
                if(strcasecmp("haschparent", $haschanged)==0) $part->haschparent = 1;
                if(strcasecmp("haschsuppcode", $haschanged)==0) $part->haschsuppcode = 1;
                if(strcasecmp("hascholdsku", $haschanged)==0) $part->hascholdsku = 1;
                if(strcasecmp("haschwhloc", $haschanged)==0) $part->haschwhloc = 1;
                if(strcasecmp("haschbulkloc", $haschanged)==0) $part->haschbulkloc = 1;
                if(strcasecmp("haschqtyinbulkloc", $haschanged)==0) $part->haschqtyinbulkloc = 1;
                if(strcasecmp("haschbarcode", $haschanged)==0) $part->haschbarcode = 1;
                if(strcasecmp("haschnotes", $haschanged)==0) $part->haschnotes = 1;
                if(strcasecmp("haschstatus", $haschanged)==0) $part->haschstatus = 1;
                if(strcasecmp("haschpartlen", $haschanged)==0) $part->haschpartlen = 1;
                if(strcasecmp("haschpartwd", $haschanged)==0) $part->haschpartwd = 1;
                if(strcasecmp("haschpartht", $haschanged)==0) $part->haschpartht = 1;
                if(strcasecmp("haschpartwt", $haschanged)==0) $part->haschpartwt = 1;
                if(strcasecmp("haschshipmethod", $haschanged)==0) $part->haschshipmethod = 1;
                if(strcasecmp("haschpartprice", $haschanged)==0) $part->haschpartprice = 1;
                if(strcasecmp("haschsoh", $haschanged)==0) $part->haschsoh = 1;
                if(strcasecmp("haschrestockqty", $haschanged)==0) $part->haschrestockqty = 1;
                if(strcasecmp("haschreorderqty", $haschanged)==0) $part->haschreorderqty = 1;
                if(strcasecmp("haschsaleslast150d", $haschanged)==0) $part->haschsaleslast150d = 1;
                if(strcasecmp("haschorderstatus", $haschanged)==0) $part->haschorderstatus = 1;
                if(strcasecmp("haschrelatedsku", $haschanged)==0) $part->haschrelatedsku = 1;
                if(strcasecmp("haschisactive", $haschanged)==0) $part->haschisactive = 1;

                $part->save();
            }
        }

    }


    /**
     * Save modification by user
     * @param Request, the Partsku instance
     * @return void
     */
    public function saveUpdateMain(Request $request, Partsku $part){
        $thePartsStock = Stock::where("sys_iscurrent", "1")
            ->where("partsku_id", $part->id)
            ->first();

        $this->forSupplierOrdersProcessing($request, $thePartsStock);
        $this->updateCheckedFields($request, $part);

        
        $part->update($request->except(
            'photo', 'isfieldupdated', 'sys_status', 'soh', 'restockqty', 'reorderqty', 'onorderqty', 'salespast150d', 'sys_finalstatus'
            ));
        $part->sys_lasttouch = Auth::user()->id;
        $part->save();
        
        $thePartsStock->soh = $request->soh;
        $thePartsStock->restockqty = $request->restockqty;
        $thePartsStock->reorderqty = $request->onorderqty;
        $thePartsStock->onorderqty = $request->onorderqty;
        $thePartsStock->salespast150d = $request->salespast150d;
        $thePartsStock->sys_status = $request->sys_status;
        $thePartsStock->sys_lasttouch = Auth::user()->id;

        if (  $request->soh <= (intval($request->restockqty)*1.25)  ) {
            if ($request->suppcode != "0" && $request->parentcode != "0") {
                $thePartsStock->sys_finalstatus = $request->sys_finalstatus;
            }
        }

        if ($request->sys_finalstatus == "Cancelled" or $request->sys_finalstatus == "Unresolved") {
            $thePartsStock->sys_finalstatus = $request->sys_finalstatus;
        }

        $thePartsStock->save();

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));
            $part->photo = preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName());
            $part->save();
        }

        
        $this->generate();
        $this->supplierordersGenerateJson();
        
        return redirect('/parts');
    }




    /**
     * Processing of the user update to those for Supplier Orders
     * @param Partsku $part the PartSKU instance
     * @return void
     */
    public function forSupplierOrdersProcessing(Request $request, Stock $part){
        $rStatus = $request->sys_finalstatus;

        $thereExistsSuppOrd = Supplierorder::where("partsku", $request->partsku)
            ->where("suppcode", $request->suppcode)
            ->where("parentcode", $request->parentcode)
            ->where("sys_finalstatus", "Pending")
            ->first();

        $theCurrentSuppord = Supplierorder::find($part->supplierorder_id);
        $theRealPart = Partsku::find($part->partsku_id);

        if ($rStatus == "Pending") {

            //dd($part->supplierorder_id, $thereExistsSuppOrd);

            if (empty($part->supplierorder_id) or is_null($thereExistsSuppOrd)) { // No linked Supplier Orders
                
                if ($request->parentcode == $theRealPart->parentcode && $request->suppcode == $theRealPart->suppcode) {
                    

                    if (  $request->soh <= (intval($request->restockqty)*1.25)  ) {

                        /*dd($request->suppcode != "0" or $request->parentcode != "0");*/
                        if ($request->suppcode != "0" && $request->parentcode != "0") { //changed Jun 15 from OR to AND
                        
                            if (!is_null($thereExistsSuppOrd)) {
                                $part->supplierorder_id = $thereExistsSuppOrd->id;
                                $part->save();

                                $thereExistsSuppOrd->reorderqty = $request->onorderqty;
                                $thereExistsSuppOrd->save();                        
                            }else{
                                $part->supplierorder_id = $this->createSupplierOrderEntry($request);    
                                $part->save();    
                            }
                            
                            $this->recomputeSupplierOrderTotals(Supplierorder::find($part->supplierorder_id));
                        }
                    }
                }else{ //nag change ng parent adn supplier
                    

                    if (!is_null($thereExistsSuppOrd)) {
                        $theCurrentSuppord->reorderqty = intval($theCurrentSuppord->reorderqty) - intval($part->reorderqty);
                        $theCurrentSuppord->save();

                        $this->recomputeSupplierOrderTotals($theCurrentSuppord);

                        if (intval($theCurrentSuppord->sys_total) == 0) {
                            $theCurrentSuppord->sys_finalstatus = "Cancelled";
                            $theCurrentSuppord->save();
                        }


                        $part->supplierorder_id = $this->createSupplierOrderEntry($request);    
                        $part->save();   
                    }else{
                        
                        $theCurrentSuppord->reorderqty = intval($theCurrentSuppord->reorderqty) - intval($part->reorderqty);
                        $theCurrentSuppord->save();
                        $this->recomputeSupplierOrderTotals($theCurrentSuppord);


                        $part->supplierorder_id = $this->createSupplierOrderEntry($request);    
                        $part->save();    
                        $this->recomputeSupplierOrderTotals(Supplierorder::find($part->supplierorder_id));

                        if (intval($theCurrentSuppord->sys_total) == 0) {
                            $theCurrentSuppord->sys_finalstatus = "Cancelled";
                            $theCurrentSuppord->save();
                        }
                    }

                }

            }else{
                //dd("BOTTOM", $thereExistsSuppOrd, $part);
                //there exist a Supplier Order
                if ($thereExistsSuppOrd->id == $part->supplierorder_id) {
                    
                    $thereExistsSuppOrd->reorderqty = $request->onorderqty;
                    //$thereExistsSuppOrd->onorderqty = $request->onorderqty;
                    $thereExistsSuppOrd->save();

                    $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                }else{
                    //There is a Supplier Order but is different from the current Supplier Order

                    //subtract vals in old SO
                    $theCurrentSuppord->reorderqty = intval($theCurrentSuppord->reorderqty) - intval($part->reorderqty);
                    $theCurrentSuppord->save();
                    $this->recomputeSupplierOrderTotals($theCurrentSuppord);

                    if (intval($theCurrentSuppord->sys_total) == 0) {
                        $theCurrentSuppord->sys_finalstatus = "Cancelled";
                        $theCurrentSuppord->save();
                    }

                    $thereExistsSuppOrd->reorderqty += intval($request->onorderqty);
                    //$thereExistsSuppOrd->onorderqty += $request->onorderqty;
                    $thereExistsSuppOrd->save();
                    $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                    
                    $part->supplierorder_id = $thereExistsSuppOrd->id;
                    $part->save();
                }
            }

        // ********** End IF Pending ********** //
        }elseif($rStatus == "Cancelled" or $rStatus == "Unresolved"){
            if ( !is_null($thereExistsSuppOrd) and !empty($part->supplierorder_id) ) {
                if ($thereExistsSuppOrd->id == $part->supplierorder_id) {
                    $thereExistsSuppOrd->reorderqty = intval($thereExistsSuppOrd->reorderqty) - intval($part->reorderqty);
                    $thereExistsSuppOrd->save();

                    $part->supplierorder_id = 0;
                    $part->save();

                    $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

                    if ($thereExistsSuppOrd->sys_total == 0) {
                        $thereExistsSuppOrd->sys_finalstatus = ($request->sys_finalstatus == "Cancelled" ? "Cancelled" : "Unresolved");
                        $thereExistsSuppOrd->save();
                    }
                }

            }
        }

    }















    /**
     * Save modification by user - BACKUP
     * @param Request, the Partsku instance
     * @return void
     */
    public function saveUpdateMainBackUp(Request $request, Partsku $part){
        $this->forSupplierOrdersProcessing($request, $part);
        $this->updateCheckedFields($request, $part);

        $thePartsStock = Stock::where("sys_iscurrent", "1")
            ->where("partsku_id", $part->id)
            ->first();

        $part->update($request->except(
            'photo', 'isfieldupdated', 'sys_status', 'soh', 'restockqty', 'reorderqty', 'salespast150d', 'sys_finalstatus'
            ));
        
        $thePartsStock->soh = $request->soh;
        $thePartsStock->restockqty = $request->restockqty;
        $thePartsStock->reorderqty = $request->reorderqty;
        $thePartsStock->salespast150d = $request->salespast150d;
        $thePartsStock->sys_finalstatus = $request->sys_finalstatus;
        $thePartsStock->save();

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/images'), $request->photo->getClientOriginalName());
            $part->photo = $request->photo->getClientOriginalName();
            $part->save();
        }

        
        $this->generate();
        $this->supplierordersGenerateJson();
        
        return redirect('/parts');
    }



    /**
     * AJAX to update inneto
     * @param Request, the Partsku instance
     * @return void
     */
    public function neto (Request $request, Partsku $part){

        $part->inneto = $request->checked;
        $part->save();

        $this->generate();
    }
    /**
     * AJAX to update inmage
     * @param Request, the Partsku instance
     * @return void
     */
    public function mage (Request $request, Partsku $part){
        
        $part->inmagento = $request->checked;
        $part->save();

        $this->generate();
    }
    /**
     * AJAX to update inwise
     * @param Request, the Partsku instance
     * @return void
     */
    public function wise (Request $request, Partsku $part){
        
        $part->inwise = $request->checked;
        $part->save();

        $this->generate();
    }


    


} // ./Controller


