<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Partsku;
use App\Supplier;
use App\Supplierorder;
use App\Specialorder;
use App\Existingorder;
use App\Backorder;
use App\Openbox;
use App\Rap;

use DB;
use Auth;

abstract class RootController extends Controller
{

    


    /**
     * Generates a Json file for Parts.index
     *
     * @return json
     */
    public function partsGenerateJson(){

        $parts = Partsku::join("stocks", "stocks.partsku_id", "partskus.id")
            ->where("stocks.sys_iscurrent", "1")
            ->where("stocks.partsku", "partskus.partsku")
            ->select("partskus.id", "partskus.partsku", "partskus.suppcode", "partskus.parentcode", "partskus.oldsku", "partskus.whloc", "partskus.sys_isactive", "partskus.descr", "stocks.soh", "partskus.barcode", "stocks.restockqty", "stocks.reorderqty", "stocks.sys_status","partskus.inneto","partskus.inmagento","partskus.inwise", "stocks.sys_finalstatus")
            ->orderBy("partskus.updated_at", "partskus.created_at", "partskus.partsku")
            ->get();

        $da = array("data" => $parts->toArray());
       
        file_put_contents("json/partsjson.json", json_encode($da));

        return "Data Generated.";
    }



    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function supplierordersGenerateJson(){

        $supplierorders = Supplierorder::select(DB::raw("id, created_at, parentcode, suppcode, partsku, reorderqty, backorder, openbox, specialorder, rap, sys_total, sys_finalstatus, descr, reorderqty as orderqty, false as pigo, false as bkogo, false as opbgo, false as spogo, false as rapgo, 0 as totalorders, updated_at"))
            ->with("specialordersid", "backordersid", "stocksid", "openboxesid", "rapsid")
            ->where("sys_finalstatus", "Pending")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $supps = $supplierorders->filter(function ($item, $key){
            $ctr = 0;

            $ctr = (count($item->specialordersid)>0 ? $ctr + 1 : $ctr + 0);
            $ctr = (count($item->backordersid)>0 ? $ctr + 1 : $ctr + 0);
            $ctr = (count($item->stocksid)>0 ? $ctr + 1 : $ctr + 0);
            $ctr = (count($item->openboxesid)>0 ? $ctr + 1 : $ctr + 0);
            $ctr = (count($item->rapsid)>0 ? $ctr + 1 : $ctr + 0);
            
            return $ctr>0;
        })->values();
        
        //dd($supplierorders->toArray(), $supps->toArray());

        //dd(count($supplierorders), count($singles), $singles);
        
        $da = array("data" => $supps->toArray());
        
        file_put_contents("json/supplierordersjson.json", json_encode($da));

        return "Supplier Orders Data Generated.";
    }



    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function specialordersGenerateJson(){

        $specialorders = Specialorder::select("id", "created_at", "ordernum", "parentcode", "suppcode", "partsku", "qty", "sys_orderstatus", "sys_finalstatus", "descr", "fault", "rma")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $da = array("data" => $specialorders->toArray());
               
        file_put_contents("json/specialordersjson.json", json_encode($da));

        return "Special Orders Data Generated.";
    }



    /**
     * Will re-compute Supplier Order Totals
     * @param Supplierorder $suppord The instance to which the totals will be re-computed
     * @return void
     */
    public function recomputeSupplierOrderTotals(Supplierorder $suppord){
        $suppord->sys_total = intval($suppord->reorderqty) + 
            intval($suppord->backorder) + intval($suppord->openbox) + 
            intval($suppord->specialorder) + intval($suppord->rap); 
        $suppord->save();
    }


    /**
     * Generates a Json file for Existingorders.index
     *
     * @return json
     */
    public function existingordersGenerateJson(){

        $supplierorders = Existingorder::select("id", "created_at", "parentcode", "suppcode", "partsku", "sys_total", "sys_finalstatus", "spnum", "eta", "descr")
            ->with("supplierordersid")
            ->whereNotIn("sys_finalstatus", ["Pending", "Cancelled", "Unresolved"])
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        

        $da = array("data" => $supplierorders->toArray());
        
        file_put_contents("json/existingordersjson.json", json_encode($da));

        return "Existing Orders Data Generated.";
    }


    /**
     * Will re-compute Supplier Order Totals
     * @param Supplierorder $suppord The instance to which the totals will be re-computed
     * @return void
     */
    public function recomputeExistingOrderTotals(Existingorder $exord){
        $exord->sys_total = intval($exord->reorderqty) + 
            intval($exord->backorder) + intval($exord->openbox) + 
            intval($exord->specialorder) + intval($exord->rap); 
        $exord->save();
    }



    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function backordersGenerateJson(){

        $backorders = Backorder::select("id", 'dateinvoiced', "ordernum", "parentcode", "suppcode", "partsku", 
            "descr", "ordertype", "category", "eta", "qty", "remarks", "spid", "sys_orderstatus", "sys_finalstatus")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $da = array("data" => $backorders->toArray());
               
        file_put_contents("json/backordersjson.json", json_encode($da));

        return "Back Orders Data Generated.";
    }



    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function openboxesordersGenerateJson(){

        $openboxes = Openbox::select("id", "created_at", "referencenum", "parentcode", "suppcode", "partsku", "qty", "whloc", "sys_orderstatus", "sys_finalstatus", "descr", "fault", "spid", "sys_addedby")
            ->where("is_archived", 0)
            ->with("user")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();


        $da = array("data" => $openboxes->toArray());
        //dd($da);
               
        file_put_contents("json/openboxesordersjson.json", json_encode($da));

        return "Openboxes Data Generated.";
    }



    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function rapsordersGenerateJson(){

        $raps = Rap::select("id", "created_at", "rmanum", "parentcode", "suppcode", "partsku", "descr", "kayakoid", "notes","spid", "whloc", "qty",  "outcome", "sys_orderstatus", "sys_finalstatus",  "sys_addedby")
            ->where("is_archived", 0)
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $da = array("data" => $raps->toArray());
               
        file_put_contents("json/rapsordersjson.json", json_encode($da));

        return "Raps Data Generated.";
    }


     /**
     * Save New SpOs to Supplier Orders
     * @param Request $request The variables sent by the user for creation
     * @return void
     */
    public function fromEOforSupplierOrders($ord, $type){
            $thereExistsSuppOrd = Supplierorder::where("partsku", $ord->partsku)
                ->where("suppcode", $ord->suppcode)
                ->where("parentcode", $ord->parentcode)
                ->where("sys_isactive", "1")
                ->where("sys_finalstatus", "Pending")
                ->first();

            if (!is_null($thereExistsSuppOrd)) { // if there exist same partsku in Supplier Order
                // There should only be ONE Pending at a time
                if ($ord->supplierorder_id != $thereExistsSuppOrd->id) {
                    if ($type == "spo") {
                        $thereExistsSuppOrd->specialorder += $ord->qty;    
                    }elseif($type == "bko"){
                        $thereExistsSuppOrd->backorder += $ord->qty;    
                    }elseif($type == "opb"){
                        $thereExistsSuppOrd->openbox += $ord->qty;    
                    }elseif($type == "rap"){
                        $thereExistsSuppOrd->rap += $ord->qty;    
                    }
                    
                    $thereExistsSuppOrd->save();
                }else{
                    
                    if ($type == "spo") {
                        $thereExistsSuppOrd->specialorder = $ord->qty;    
                    }elseif($type == "bko"){
                        $thereExistsSuppOrd->backorder = $ord->qty;    
                    }elseif($type == "opb"){
                        $thereExistsSuppOrd->openbox = $ord->qty;    
                    }elseif($type == "rap"){
                        $thereExistsSuppOrd->rap = $ord->qty;    
                    }
                    $thereExistsSuppOrd->save();
                }
                
                $ord->supplierorder_id = $thereExistsSuppOrd->id;
                $ord->sys_finalstatus = "Pending";
                $ord->save();
                

                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

            }else { //There are no partskus in Supplier Order

                $newSuppOrd = new Supplierorder;
                $newSuppOrd->partsku = $ord->partsku;
                $newSuppOrd->descr = $ord->descr;
                $newSuppOrd->parentcode = $ord->parentcode;
                $newSuppOrd->suppcode = $ord->suppcode;
                
                if ($type == "spo") {
                    $newSuppOrd->specialorder = $ord->qty;    
                }elseif($type == "bko"){
                    $newSuppOrd->backorder = $ord->qty;    
                }elseif($type == "opb"){
                    $newSuppOrd->openbox = $ord->qty;    
                }elseif($type == "rap"){
                    $newSuppOrd->rap = $ord->qty;    
                }
                
                $newSuppOrd->sys_finalstatus = "Pending";

                $newSuppOrd->sys_addedby = Auth::user()->id;
                $newSuppOrd->sys_isactive = 1;
                $newSuppOrd->photo = $ord->photo;
                
                $newSuppOrd->save();

                $ord->sys_finalstatus = "Pending";
                $ord->supplierorder_id = $newSuppOrd->id;
                $ord->save();

                $this->recomputeSupplierOrderTotals($newSuppOrd);
            }
    }




    
}
