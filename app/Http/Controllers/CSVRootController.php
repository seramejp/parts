<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\t_partsku;
use App\t_order;
use App\Partsku;
use App\Stock;
use App\Supplierorder;
use App\Existingorder;
use App\Backorder;

use Schema;
use Carbon\Carbon;
use DB;
use Auth;

abstract class CSVRootController extends Controller
{
    //
    public function getCsvHeaders($filename){
	    $csv = array();
	    try {
	                
	        $row = 1;
	        if (($handle = fopen($filename, "r")) !== FALSE) {
	          while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	            $num = count($data);

	            if ($num > 0 ) {
	              $csv = $data;
	              break;
	            }else{
	              $row++;
	            }
	          }
	          fclose($handle);
	        }
	        //echo "\n Getting CSV Headers...";

	    } catch (Exception $e) {
	      echo "Error in opening file." . $e->getMessage();
	    }

	    return $csv;
	}


  	public function whatTableToUse($ordertyp){
  		$useThisTable = "";
  		switch ($ordertyp) {
			case '1': case '7': case '8':
				$useThisTable = "t_partskus";
				break;
			case '2': case '3': case '4': case '5': case '6':
				$useThisTable = "t_orders";
				break;
			
			default:
				$useThisTable = "";
				break;
		}
		return $useThisTable;
  	}


	public function updateIsActiveForTempPartsku($liveHeaders){
		//Update sys_isactive depending on what the user defined - updateStatusSkuinfo
		if (in_array("isactive", $liveHeaders)) {
			DB::update('UPDATE t_partskus set sys_isactive = isactive');

		}else{
			DB::update('UPDATE t_partskus set sys_isactive = 1');
		}
    }


    public function updateInDbForTempPartsku (){
      	DB::update('UPDATE t_partskus as t inner join partskus as p on p.partsku = t.partsku set t.sys_indb = 1');
    }

    public function updateAuthorForTempPartsku(){
      	t_partsku::where("sys_indb", 0)->update(["sys_addedby"=> Auth::user()->id, "sys_lasttouch"=>Auth::user()->id]);
        t_partsku::where("sys_indb", 1)->update(["sys_lasttouch"=> Auth::user()->id]);
    }

    public function truncateTemp($orderTyp){
      switch ($orderTyp) {
          case '1': case '7': case '8':
            t_partsku::truncate();
            break;
          case '2': case '3': case '4': case '5':

            t_order::truncate();
            break;

          default:
            # code...
            break;
        }
    }


    public function processSysInDb1Partsku(){
        //upload those that are in the db. those that are not in db will still be processed as new entries
        //if already in db, replace all values
        //if already Pending, remove from SO and re-process

		foreach (t_partsku::where("sys_indb", "1")->cursor() as $tempPart) {
			$origPart = Partsku::where("partsku", $tempPart->partsku)
				->first();

            $stocksInSO = Stock::where("partsku_id", $origPart->id)
                ->where("sys_finalstatus", "Pending")
                ->get();

            if (count($stocksInSO)>0) {
                foreach ($stocksInSO as $stk) {
                    //remove in Supplier Order if sys_total is not equal to reorderqty
                    $theSo = Supplierorder::find($stk->supplierorder_id);
                    if (intval($theSo->sys_total) != intval($stk->reorderqty)) {
                        $theSo->reorderqty = intval($theSo->reorderqty) - intval($stk->reorderqty);
                        $theSo->save();
                        $this->recomputeSupplierOrderTotals($theSo);    

                        $suppPending = Supplierorder::where("partsku", $stk->partsku)
                            ->where("parentcode", $tempPart->parentcode)
                            ->where("suppcode", $tempPart->suppcode)
                            ->where("sys_finalstatus", "Pending")
                            ->first();

                        if (is_null($suppPending)) {
                            $stk->supplierorder_id = $this->createSupplierOrderEntry($stk, $origPart);
                            $stk->save();
                        }else { //if hindi sya null and may instance of a Supplier Order si current stock
                            $suppPending->reorderqty = intval($suppPending->reorderqty) + intval($stk->reorderqty);
                            $suppPending->save();
                            $this->recomputeSupplierOrderTotals($suppPending);

                            $stk->supplierorder_id = $suppPending->id;
                            $stk->save();
                        }
                    } // if sys_total and reorderqty are the same, no need to do anything
                    
                    $this->findAndReplaceValuesPartsku($origPart, $tempPart);

                    // there should be only one instance of this at a given time   
                }
            }

			

	    } //end foreach cursor, indb 1
    }




    public function processSysInDb1Stock(){

		foreach (t_partsku::where("sys_indb", "1")->cursor() as $tempPart) {

            $theStock = Stock::where("partsku", $tempPart->partsku)
                ->where("sys_iscurrent", "1")
                ->first();

            

            if (is_null($theStock)) {

                    $theNotCurrentStock = Stock::where("partsku", $tempPart->partsku)
                        ->where("sys_iscurrent", "0")
                        ->orderBy("created_at", 'desc')
                        ->first();
                    /*if (!is_null($theNotCurrentStock)) {*/
                        $tryingToFindPartsku = Partsku::where("partsku", $tempPart->partsku)
                            ->first();

                        $newStock = new Stock;
                        $newStock->partsku = $tempPart->partsku;
                        $newStock->soh = $tempPart->soh;
                        $newStock->restockqty = $tempPart->restockqty;
                        $newStock->reorderqty = $tempPart->reorderqty;
                        $newStock->salespast150d = $tempPart->salespast150d;
                        $newStock->onorderqty = $tempPart->reorderqty;

                        $newStock->sys_iscurrent = "1";
                        if (is_null($theNotCurrentStock)) {
                          $newStock->sys_status = "new on order";  // THIS COULD BE THE CULPRIT!
                        }else{
                          $newStock->sys_status = $theNotCurrentStock->sys_status;
                        }
                        
                        
                        if ($tempPart->needsordering == "1") {
                            $newStock->sys_finalstatus = "Pending";
                        }else{
                            $newStock->sys_finalstatus = "Unresolved";
                        }

                        $newStock->sys_addedby = $tryingToFindPartsku->sys_addedby;
                        $newStock->sys_lasttouch = Auth::user()->id;
                        $newStock->sys_isactive = "1";
                        $newStock->partsku_id = $tryingToFindPartsku->id;
                        $newStock->save();

                         //check if there exists in SO same Partsku, Supp, Parent and Pending
                        $thereExistsSuppOrd2 = Supplierorder::where("partsku", $tryingToFindPartsku->partsku)
                            ->where("suppcode", $tryingToFindPartsku->suppcode)
                            ->where("parentcode", $tryingToFindPartsku->parentcode)
                            ->where("sys_isactive", "1")
                            ->where("sys_finalstatus", "Pending")
                            ->first();

                        if (is_null($thereExistsSuppOrd2)) {
                            if ($tempPart->needsordering == "1") {
                                $newStock->supplierorder_id = $this->createSupplierOrderEntry($newStock, $tryingToFindPartsku);
                                $newStock->save();
                            }
                        }else{
                            if ($tempPart->needsordering == "1") {
                                $newStock->supplierorder_id = $thereExistsSuppOrd2->id;
                                $newStock->save();

                                $thereExistsSuppOrd2->reorderqty = intval($thereExistsSuppOrd2->reorderqty) + intval($newStock->reorderqty);
                                $thereExistsSuppOrd2->save();
                                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd2);
                            }
                        }
                    /*}*/
                    

                   



            }else{ //there is a stock na is_current = 1

                $thePartsku = Partsku::find($theStock->partsku_id);

                if ($tempPart->whlocation != "" ) {
                    $thePartsku->whloc = $tempPart->whlocation;
                    $thePartsku->save();
                }
                //dd($thePartsku, $theStock);

                if ($theStock->sys_finalstatus == "Pending") {
                    //Change the existing Stock details
                    $theStock->reorderqty = $tempPart->reorderqty;
                    $theStock->soh = $tempPart->soh;
                    $theStock->restockqty = $tempPart->restockqty;
                    $theStock->salespast150d = $tempPart->salespast150d;
                    $theStock->onorderqty = $tempPart->reorderqty;
                    $theStock->save();

                    //Change the existing supplier order's details
                    $theSuppOrd = Supplierorder::find($theStock->supplierorder_id);
                    if ($theSuppOrd->sys_finalstatus == "Pending") {
                        $theSuppOrd->reorderqty = $tempPart->reorderqty;
                        $theSuppOrd->save();
                        $this->recomputeSupplierOrderTotals($theSuppOrd);    
                    }else{ //fail-safe mechanism
                        $theStock->supplierorder_id = $this->createSupplierOrderEntry($theStock,$thePartsku);
                        $theStock->save();
                    } // end fail safe mechanism
                
                } else { //else Hindi Pending ang Stock

                    //change status ng stock
                    $theStock->sys_iscurrent = "0";
                    $theStock->save();

                    $newStock = new Stock;
                    $newStock->partsku = $thePartsku->partsku;
                    $newStock->soh = $tempPart->soh;
                    $newStock->restockqty = $tempPart->restockqty;
                    $newStock->reorderqty = $tempPart->reorderqty;
                    $newStock->salespast150d = $tempPart->salespast150d;
                    $newStock->onorderqty = $tempPart->reorderqty;

                    $newStock->sys_iscurrent = "1";
                    $newStock->sys_status = $theStock->sys_status;
                    $newStock->sys_finalstatus = "Unresolved";

                    $newStock->sys_addedby = $thePartsku->sys_addedby;
                    $newStock->sys_lasttouch = Auth::user()->id;
                    $newStock->sys_isactive = "1";
                    $newStock->partsku_id = $thePartsku->id;
                    $newStock->save();

                    //check if there exists in SO same Partsku, Supp, Parent and Pending
                    $thereExistsSuppOrd = Supplierorder::where("partsku", $thePartsku->partsku)
                        ->where("suppcode", $thePartsku->suppcode)
                        ->where("parentcode", $thePartsku->parentcode)
                        ->where("sys_isactive", "1")
                        ->where("sys_finalstatus", "Pending")
                        ->first();

                    if (is_null($thereExistsSuppOrd)) {
                        $newStock->supplierorder_id = $this->createSupplierOrderEntry($newStock, $thePartsku);
                        $newStock->sys_finalstatus = "Pending";
                        $newStock->save();
                    }else{
                        $newStock->supplierorder_id = $thereExistsSuppOrd->id;
                        $newStock->sys_finalstatus = $thereExistsSuppOrd->sys_finalstatus;
                        $newStock->save();

                        $thereExistsSuppOrd->reorderqty = intval($thereExistsSuppOrd->reorderqty) + intval($newStock->reorderqty);
                        $thereExistsSuppOrd->save();
                        $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                    }

                } // end else ng Hindi Pending ang Stocks

            } // else ng there is a stock na is_current = 1
            // else ito ng may stock



		} //end foreach cursor, indb 1
    }




    private function findAndReplaceValuesPartsku (Partsku $part, t_partsku $temp){
          //oldsku
          if (empty($part->oldsku) && !empty($temp->oldsku)) {
              $part->oldsku = $temp->oldsku;  
          }elseif(!empty($part->oldsku) && !empty($temp->oldsku)){
              $part->oldsku = $temp->oldsku;  
          }

          if (empty($part->relatedsku) && !empty($temp->relatedsku)) {
              $part->relatedsku = $temp->relatedsku;  
          }elseif(!empty($part->relatedsku) && !empty($temp->relatedsku)){
              $part->relatedsku = $temp->relatedsku;  
          }


          //supplierid
          if (empty($part->suppcode) && !empty($temp->supplier)) {
              $part->suppcode = $temp->supplier;  
          }elseif(!empty($part->suppcode) && !empty($temp->supplier)){
              $part->suppcode = $temp->supplier;  
          }

          //xsell -> parent
          if (empty($part->parentcode) && !empty($temp->parent)) {
              $part->parentcode = $temp->parent;  
          }elseif(!empty($part->parentcode) && !empty($temp->parent)){
              $part->parentcode = $temp->parent;  
          }

          //xsell
          if (empty($part->xsell) && !empty($temp->xsell)) {
              $part->xsell = $temp->xsell;  
          }elseif(!empty($part->xsell) && !empty($temp->xsell)){
              $part->xsell = $temp->xsell;  
          }

          //description
          if (empty($part->descr) && !empty($temp->description)) {
              $part->descr = $temp->description;  
          }elseif(!empty($part->descr) && !empty($temp->description)){
              $part->descr = $temp->description;  
          }

          //whloc
          if (empty($part->whloc) && !empty($temp->whlocation)) {
              $part->whloc = $temp->whlocation;  
          }elseif(!empty($part->whloc) && !empty($temp->whlocation)){
              $part->whloc = $temp->whlocation;  
          }

          //bulkloc
          if (empty($part->bulkloc) && !empty($temp->bulklocation)) {
              $part->bulkloc = $temp->bulklocation;  
          }elseif(!empty($part->bulkloc) && !empty($temp->bulklocation)){
              $part->bulkloc = $temp->bulklocation;  
          }

          //qtyinbulkloc
          if (empty($part->qtyinbulkloc) && !empty($temp->qtyinbulkloc)) {
              $part->qtyinbulkloc = $temp->qtyinbulkloc;  
          }elseif(!empty($part->qtyinbulkloc) && !empty($temp->qtyinbulkloc)){
              $part->qtyinbulkloc = $temp->qtyinbulkloc;  
          }

          //partwt
          if (empty($part->partwt) && !empty($temp->partweight)) {
              $part->partwt = $temp->partweight;  
          }elseif(!empty($part->partwt) && !empty($temp->partweight)){
              $part->partwt = $temp->partweight;  
          }

          //partlen
          if (empty($part->partlen) && !empty($temp->partlength)) {
              $part->partlen = $temp->partlength;  
          }elseif(!empty($part->partlen) && !empty($temp->partlength)){
              $part->partlen = $temp->partlength;  
          }

          //
          if (empty($part->partht) && !empty($temp->partheight)) {
              $part->partht = $temp->partheight;  
          }elseif(!empty($part->partht) && !empty($temp->partheight)){
              $part->partht = $temp->partheight;  
          }

          if (empty($part->partwidth) && !empty($temp->partwidth)) {
              $part->partwidth = $temp->partwidth;  
          }elseif(!empty($part->partwidth) && !empty($temp->partwidth)){
              $part->partwidth = $temp->partwidth;  
          }

          if (empty($part->shipmethod) && !empty($temp->shippingmethod)) {
              $part->shipmethod = $temp->shippingmethod;  
          }elseif(!empty($part->shipmethod) && !empty($temp->shippingmethod)){
              $part->shipmethod = $temp->shippingmethod;  
          }


          if (empty($part->price) && !empty($temp->price)) {
              $part->price = $temp->price;  
          }elseif(!empty($part->price) && !empty($temp->price)){
              $part->price = $temp->price;  
          }

          if (empty($part->remarks) && !empty($temp->remarks)) {
              $part->remarks = $temp->remarks;  
          }elseif(!empty($part->remarks) && !empty($temp->remarks)){
              $part->remarks = $temp->remarks;  
          }

          if (empty($part->notes) && !empty($temp->notes)) {
              $part->notes = $temp->notes;  
          }elseif(!empty($part->notes) && !empty($temp->notes)){
              $part->notes = $temp->notes;  
          }

          if (empty($part->barcode) && !empty($temp->barcode)) {
              $part->barcode = $temp->barcode;  
          }elseif(!empty($part->barcode) && !empty($temp->barcode)){
              $part->barcode = $temp->barcode;  
          }

          if (empty($part->wiseitemid) && !empty($temp->wiseitemid)) {
              $part->wiseitemid = $temp->wiseitemid;  
          }elseif(!empty($part->wiseitemid) && !empty($temp->wiseitemid)){
              $part->wiseitemid = $temp->wiseitemid;  
          }

          if (empty($part->ispurchasing) && !empty($temp->partsorpurchasing)) {
              $part->ispurchasing = $temp->partsorpurchasing;  
          }elseif(!empty($part->ispurchasing) && !empty($temp->partsorpurchasing)){
              $part->ispurchasing = $temp->partsorpurchasing;  
          }
          if (empty($part->isdhl) && !empty($temp->dhlorcontainer)) {
              $part->isdhl = $temp->dhlorcontainer;  
          }elseif(!empty($part->isdhl) && !empty($temp->dhlorcontainer)){
              $part->isdhl = $temp->dhlorcontainer;  
          }


          $part->sys_lasttouch = Auth::user()->id;

          $part->save();

    }


    private function findAndReplaceValuesStock (Stock $part, t_partsku $temp){
    	if (empty($part->soh) && !empty($temp->soh)) {
			$part->soh = $temp->soh;  
		}elseif(!empty($part->soh) && !empty($temp->soh)){
			$part->soh = $temp->soh;  
		}

		if (empty($part->restockqty) && !empty($temp->restockqty)) {
			$part->restockqty = $temp->restockqty;  
		}elseif(!empty($part->restockqty) && !empty($temp->restockqty)){
			$part->restockqty = $temp->restockqty;  
		}

		if (empty($part->reorderqty) && !empty($temp->reorderqty)) {
			$part->reorderqty = $temp->reorderqty;  
		}elseif(!empty($part->reorderqty) && !empty($temp->reorderqty)){
			$part->reorderqty = $temp->reorderqty;  
		}

		if (empty($part->salespast150d) && !empty($temp->salespast150d)) {
			$part->salespast150d = $temp->salespast150d;  
		}elseif(!empty($part->salespast150d) && !empty($temp->salespast150d)){
			$part->salespast150d = $temp->salespast150d;  
		}
          

		//Set status NEW ON ORDER by default
		if (empty($part->sys_status)) {
		  	$part->sys_status = "New on Order"; // IF IT STILL FAILS, This is the culprit!
		}
		//Set orderstatus UNRESOLVED by default
		if (empty($part->sys_finalstatus)) {
		  	$part->sys_finalstatus = "Unresolved";
		}



    }


    /**
     * Create a New Supplier Order Entry
     * @param Request $request the information required and saved by the user
     * @return Integer id The ID of the newly created Supplier Order Instance
     */
    private function createSupplierOrderEntry(Stock $stk, Partsku $part){
        $newSuppOrd = new Supplierorder;
        $newSuppOrd->partsku = $stk->partsku;
        $newSuppOrd->descr = $part->descr;
        $newSuppOrd->parentcode = $part->parentcode;
        $newSuppOrd->suppcode = $part->suppcode;
        $newSuppOrd->reorderqty = $stk->reorderqty;
        $newSuppOrd->sys_finalstatus = "Pending";

        $newSuppOrd->sys_addedby = Auth::user()->id;
        $newSuppOrd->sys_isactive = 1;
        $newSuppOrd->photo = $part->photo;
        
        $newSuppOrd->save();

        $this->recomputeSupplierOrderTotals($newSuppOrd);

        return $newSuppOrd->id;
    }


    /**
     * Will re-compute Supplier Order Totals
     * @param Supplierorder $suppord The instance to which the totals will be re-computed
     * @return void
     */
    public function recomputeSupplierOrderTotals(Supplierorder $suppord){
        $suppord->sys_total = intval($suppord->reorderqty) + 
            intval($suppord->backorder) + intval($suppord->openbox) + 
            intval($suppord->specialorder) + intval($suppord->rap); 
        $suppord->save();
    }





    /* ---------------------------------------------------------------------
    ------------------------------     ---------------------------------------
    ----------------------------    -----------------------------------------
    ------------------------------    ---------------------------------------
    ---------------------------------------------------------------------
    */
        
    public function updateIsActiveForTempOrders(){

          $temps = t_order::all();

          foreach ($temps as $value) {
              $value->sys_isactive = "1";
              $value->save();
          }

    }


    public function updateAuthorForTempOrders(){

        $temps = t_order::all();

        foreach ($temps as $value) {
            $value->sys_addedby = Auth::user()->id;
            $value->sys_lasttouch = Auth::user()->id;
            $value->save();
        }

    }

    public function processBackOrder(){

        foreach (t_order::cursor() as $temp) {
            $newBko = new Backorder;

            //$newBko = date("d-m-Y", strtotime($temp->dateinvoiced)); //for display to view

            $newBko->dateinvoiced = date("Y-m-d", strtotime($temp->dateinvoiced));
            $newBko->ordernum = $temp->ordernumber;
            $newBko->partsku = $temp->partsku;
            $newBko->descr = $temp->description;
            $newBko->ordertype = $temp->ordertype;
            $newBko->category = $temp->category;
            $newBko->spid = $temp->spid;

            $newBko->eta = date('Y-m-d', strtotime($temp->eta));
            $newBko->qty = $temp->qty;
            $newBko->remarks = $temp->remarks;

            $newBko->suppcode = $temp->supplier;
            $newBko->parentcode = $temp->parent;
            
            $newBko->sys_orderstatus = $temp->orderstatus;
            $newBko->sys_finalstatus = "Unresolved";
            $newBko->sys_addedby = Auth::user()->id;
            $newBko->sys_lasttouch = Auth::user()->id;        
            $newBko->sys_isactive = 1; //Active always
            $newBko->save();
            
        } //end foreach cursor, indb 1
        
    } //end Process Backorder

}
