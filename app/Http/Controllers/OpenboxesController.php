<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Storage;

use App\Parentsku;
use App\Supplier;
use App\Openbox;
use App\Supplierorder;

class OpenboxesController extends RootController
{
    //
    //
    /**
     * Controller for Parts
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Load View for Special Orders Main List
     *
     * @return void
     */
    public function index (){
        return view("openboxes.index");
    }



    /**
     * Load View for Special Orders Main List
     *
     * @return void
     */
    public function getMaxReferenceNumber (){
        $maxrefnum = Openbox::where("sys_isactive", 1)->max("referencenum");
        if (empty($maxrefnum)) {
            $maxrefnum = "OBP00001";
        }else{
            $arr = explode("OBP", $maxrefnum);
            $maxrefnum = "OBP" . str_pad(intval($arr[1]) + 1, 5, "0", STR_PAD_LEFT);
        }
        return $maxrefnum;
    }


    /**
     * Load View for Creating a New Special Order
     *
     * @return list of Parents, list of Suppliers
     */
    public function create(){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();
        return view("openboxes.create", compact('parents', 'suppliers'))->with("maxrefnum", $this->getMaxReferenceNumber());
    }



    /**
     * Save the Newly created Special Order
     * @todo Function for those that needs Ordering - how to make new Partsku go to Supplier Orders
     * @param Request $request the Part created by the user
     * @return void
     */
    public function saveNewOPB (Request $request){
        $newOpb = new Openbox;

        $newOpb->referencenum = $this->getMaxReferenceNumber();
        $newOpb->parentcode = $request->parentcode;
        $newOpb->suppcode = $request->suppcode;
        $newOpb->partsku = $request->partsku;

        $newOpb->descr = $request->descr;
        $newOpb->kayakoid = $request->kayakoid;
        $newOpb->spid = $request->spid;
        $newOpb->fault = $request->fault;
        $newOpb->qty = $request->qty;
        $newOpb->whloc = $request->whloc;

        $newOpb->sys_orderstatus = $request->sys_orderstatus;
        $newOpb->sys_finalstatus = $request->sys_finalstatus;
        $newOpb->sys_addedby = Auth::user()->id;
        $newOpb->sys_lasttouch = Auth::user()->id;        
        $newOpb->sys_isactive = 1; //Active always
    
        $newOpb->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));
        }

        $newOpb->save();

        $this->openboxesordersGenerateJson();
        /*$this->forSupplierOrders($request, $newOpb);
        $this->supplierordersGenerateJson();*/

        return redirect("/openboxes");
    }




    /**
     * Save New opbs to Supplier Orders
     * @param Request $request The variables sent by the user for creation
     * @return void
     */
    public function forSupplierOrders(Request $request, Openbox $opb){
            $thereExistsSuppOrd = Supplierorder::where("partsku", $request->partsku)
                ->where("suppcode", $request->suppcode)
                ->where("parentcode", $request->parentcode)
                ->where("sys_isactive", "1")
                ->where("sys_finalstatus", "Pending")
                ->first();

            if (!is_null($thereExistsSuppOrd)) { // if there exist same partsku in Supplier Order
                // There should only be ONE Pending at a time
                if ($opb->supplierorder_id != $thereExistsSuppOrd->id) {
                    $thereExistsSuppOrd->openbox += $request->qty;
                    $thereExistsSuppOrd->save();
                }else{
                    $thereExistsSuppOrd->openbox = $request->qty;
                    $thereExistsSuppOrd->save();
                }
                
                $opb->supplierorder_id = $thereExistsSuppOrd->id;
                $opb->sys_finalstatus = "Pending";
                $opb->save();
                

                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

            }else { //There are no partskus in Supplier Order

                $newSuppOrd = new Supplierorder;
                $newSuppOrd->partsku = $request->partsku;
                $newSuppOrd->descr = $request->descr;
                $newSuppOrd->parentcode = $request->parentcode;
                $newSuppOrd->suppcode = $request->suppcode;
                $newSuppOrd->openbox = $request->qty;
                $newSuppOrd->sys_finalstatus = "Pending";

                $newSuppOrd->sys_addedby = Auth::user()->id;
                $newSuppOrd->sys_isactive = 1;
                $newSuppOrd->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
                
                $newSuppOrd->save();

                $opb->sys_finalstatus = "Pending";
                $opb->supplierorder_id = $newSuppOrd->id;
                $opb->save();

                $this->recomputeSupplierOrderTotals($newSuppOrd);
            }
    }




    /**
     * Load View for Editing Special Orders
     * @param openbox $special the opb instance
     * @return List of Parents, Suppliers, the Part SKU instance, Stocks related to the Part SKU and the Current Stock
     */
    public function edit(Openbox $opb){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();

        return view("openboxes.edit", compact('parents', 'suppliers', 'opb'));
    }



    /**
     * Save modification by user
     * @param Request $request, Partsku $part the Partsku instance
     * @return void
     */
    public function saveUpdateMain(Request $request, Openbox $opb){
        

        if ($request->sys_finalstatus == "Cancelled" or $request->sys_finalstatus == "Unresolved") {
                if ( !empty($opb->supplierorder_id )) {
                    $thereExistsSuppOrd = Supplierorder::where("id", $opb->supplierorder_id)
                        ->first();

                    if (!empty($thereExistsSuppOrd)) { //just a precaution
                        $thereExistsSuppOrd->openbox = (intval($thereExistsSuppOrd->openbox) - intval($opb->qty));
                        $thereExistsSuppOrd->save();

                        $opb->supplierorder_id = 0;
                        $opb->sys_lasttouch = Auth::user()->id;
                        $opb->save();
                        $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                    }

                    if ($thereExistsSuppOrd->sys_total == 0) {
                        $thereExistsSuppOrd->sys_finalstatus = ($request->sys_finalstatus == "Cancelled" ? "Cancelled" : "Unresolved");
                        $thereExistsSuppOrd->sys_isactive = 0;
                        $thereExistsSuppOrd->save();
                    }

                }
        }else if($request->sys_finalstatus == "Pending"){ //Pending pa rin
                $thereExistsSuppOrd = Supplierorder::where("id", $opb->supplierorder_id)
                        ->first();

                $anotherSuppOrd = Supplierorder::where("partsku", $request->partsku)
                    ->where("suppcode", $request->suppcode)
                    ->where("parentcode", $request->parentcode)
                    ->where("sys_isactive", "1")
                    ->where("sys_finalstatus", "Pending")
                    ->first();


                if ($request->parentcode != "0" && $request->suppcode != "0") {
                    if (!empty($thereExistsSuppOrd)) { //just a precaution
                        //dd("TOP", $thereExistsSuppOrd);

                        if ($request->parentcode == $thereExistsSuppOrd->parentcode && $request->suppcode == $thereExistsSuppOrd->suppcode) {
                            
                            $thereExistsSuppOrd->openbox = (intval($thereExistsSuppOrd->openbox) - intval($opb->qty)) + intval($request->qty);
                            $thereExistsSuppOrd->save();

                        }else{
                             // there has been a change in either supplier or parentcode
                            // if anotherSuppOrd is not empty, attach to that suppord
                            // else, create a new supplierorder


                            if (!empty($anotherSuppOrd)) {
                                $anotherSuppOrd->openbox += intval($request->qty);
                                $anotherSuppOrd->save();

                                $this->recomputeSupplierOrderTotals($anotherSuppOrd);

                                $opb->supplierorder_id = $anotherSuppOrd->id;
                                $opb->sys_finalstatus = "Pending";
                                $opb->save();

                                $thereExistsSuppOrd->openbox = intval($thereExistsSuppOrd->openbox) - intval($opb->qty);
                                $thereExistsSuppOrd->save();

                                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

                                if (intval($thereExistsSuppOrd->sys_total) == 0) {
                                    $thereExistsSuppOrd->sys_finalstatus = "Cancelled";
                                    $thereExistsSuppOrd->save();    
                                }

                            }else{

                                $newSuppOrd = new Supplierorder;
                                $newSuppOrd->partsku = $request->partsku;
                                $newSuppOrd->descr = $request->descr;
                                $newSuppOrd->parentcode = $request->parentcode;
                                $newSuppOrd->suppcode = $request->suppcode;
                                $newSuppOrd->specialorder = $request->qty;
                                $newSuppOrd->sys_finalstatus = "Pending";

                                $newSuppOrd->sys_addedby = Auth::user()->id;
                                $newSuppOrd->sys_isactive = 1;
                                $newSuppOrd->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
                                
                                $newSuppOrd->save();

                                $opb->sys_finalstatus = "Pending";
                                $opb->supplierorder_id = $newSuppOrd->id;
                                $opb->save();

                                $this->recomputeSupplierOrderTotals($newSuppOrd);

                            }
                        }

                        //dd($thereExistsSuppOrd->specialorder . "; " . $spo->qty . "; ". $request->qty . ";");

                        $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);
                    }else{ // no supplierorder pa, meaning, tie in if there is a anotherSuppOrd
                        //dd("BOTTOM", $thereExistsSuppOrd);

                        if (!empty($anotherSuppOrd)) {
                            $anotherSuppOrd->openbox += intval($request->qty);
                            $anotherSuppOrd->save();

                            $this->recomputeSupplierOrderTotals($anotherSuppOrd);

                            $opb->supplierorder_id = $anotherSuppOrd->id;
                            $opb->sys_finalstatus = "Pending";
                            $opb->save();
                        }else{
                            $this->forSupplierOrders($request, $opb);
                        }
                    }

                }else{
                    $opb->sys_finalstatus = "Unresolved";
                    $opb->save();
                }
        }

        $opb->update($request->except(
            'created_at', 'photo'
            ));
        $opb->sys_lasttouch = Auth::user()->id;
        $opb->save();

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));
            $opb->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
            
        }
        $opb->save();


        if ($request->parentcode == "0" or $request->suppcode == "0") {
            $opb->sys_finalstatus = "Unresolved";
            $opb->save();
        }


        $this->openboxesordersGenerateJson();
        $this->supplierordersGenerateJson();
       
        return redirect('/openboxes');
    }



    /**
     * Save modification by user
     * @param Request $request, Partsku $part the Partsku instance
     * @return void
     */
    public function cloneview (Openbox $opb){
        $parents = Parentsku::select("parentcode", "suppcode")->orderBy("parentcode")->get();
        $suppliers = Supplier::select("suppcode")->orderBy("suppcode")->get();
        return view('openboxes.clone',compact('parents', 'suppliers','opb'))->with("maxrefnum", $this->getMaxReferenceNumber());
    }



    /**
     * Save modification by user
     * @param Request $request, Partsku $part the Partsku instance
     * @return void
     */
    public function createclone (Request $request){
        
        $newOpb = new Openbox;

        $newOpb->referencenum = $request->referencenum;
        $newOpb->parentcode = $request->parentcode;
        $newOpb->suppcode = $request->suppcode;
        $newOpb->partsku = $request->partsku;

        $newOpb->descr = $request->descr;
        $newOpb->kayakoid = $request->kayakoid;
        $newOpb->spid = $request->spid;
        $newOpb->fault = $request->fault;
        $newOpb->qty = $request->qty;
        $newOpb->whloc = $request->whloc;

        $newOpb->sys_orderstatus = $request->sys_orderstatus;
        $newOpb->sys_finalstatus = $request->sys_finalstatus;
        $newOpb->sys_addedby = Auth::user()->id;
        $newOpb->sys_lasttouch = Auth::user()->id;        
        $newOpb->sys_isactive = 1; //Active always
    
        $newOpb->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";
        

        if ($request->hasFile('photo')) {
            $request->photo->move(storage_path('app/public'), preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()));
        }

        $newOpb->save();

        $this->openboxesordersGenerateJson();
        $this->forSupplierOrders($request, $newOpb);
        $this->supplierordersGenerateJson();

        return redirect("/openboxes");
    }
}

/*

    



*/