<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Partsku;
use App\Parentsku;
use App\Supplier;
use App\Stock;
use App\Supplierorder;
use App\Specialorder;
use App\Existingorder;
use App\Backorder;
use App\Openbox;
use App\Rap;
use App\t_order;
use App\t_partsku;

use Schema;
use Carbon\Carbon;
use DB;
use Auth;

class AutomationsController extends CSVRootController
{
    //---------------------------------------
	//---------------------------------------
	// 				AUTOMATIONS --
	//---------------------------------------
	//---------------------------------------
	/**
     * Load View for Supplierorders Main List
     *
     * @return void
     */
    public function automationimportview (){
        return view("csv.automationimport");
    }


     public function uploadfile (Request $request) {

	    $orderTyp = $request->importtype;
      	$redirecthere = "parts";

		$useThisTable = $this->whatTableToUse($orderTyp);
		
        $file = $request->image;
        $filepath = storage_path('app/csv/'.$file->getClientOriginalName());
        $extension = $file->getClientOriginalExtension();

        $request->image->move(storage_path('app/csv'), $request->image->getClientOriginalName());
	        
        //1. Get Header of CSV File
      	$csvHeaders = $this->getCsvHeaders($filepath);

	      //dd(count($csvHeaders));
		if (count($csvHeaders) === 0) {
			abort(500,"CSV Headers not found. Rectify the issue and please try again.");
		}

		//2. Get list of columns in table tempPartsku
		$colsTestTable = Schema::getColumnListing($useThisTable); 

		//dd($colsTestTable);
		//3. Check what tables are present  against the headers from the CSV File
		//$liveHeaders = $this->getMatchingHeaders($colsTestTable, $csvHeaders);
		$liveHeaders = array_intersect($colsTestTable, $csvHeaders);

		if (count($liveHeaders)===0) {
			abort (500, "CSV Headers did not match any of the Column Tables. Download the template just to be sure and try again.");
		}
		
		//4. Check the difference between CSV headers and those that match from the database
		$errorHeaders = array_udiff($csvHeaders, $liveHeaders, 'strcasecmp');

        

		//5. If there are error in headers, ask user to rectify CSV file. Exit.
		if (count($errorHeaders) > 0) {

			$erroneousHeaders = implode(", ", $errorHeaders);
			abort (500, "Some CSV headers did not match any of the Column Tables. <br>\n# of Columns: " . count($errorHeaders) . "\nErroneous Headers: " . $erroneousHeaders);
		}

		$sqlTables = "";
		$sqlTables .= "(" . implode(", ", $csvHeaders) . ")";

		/* ----------------------------
		  SQL FUNCTIONS
		-------------------------------*/
		//1. Delete all data from TEMP first
		$this->truncateTemp($orderTyp);


		//2. Load Data From CSV to MySQL Table; PartSKU is a required field

		//These are the required fields
		//List here the required fields for each table
		$stockHeaders = array("partsku", "soh", "restockqty", "reorderqty", "needsordering");
		$piHeaders = array("partsku", "parent", "supplier");

		$bkoHeaders = array("partsku", "ordernumber");

		$proceed = true;
		$proceedArr = $stockHeaders;
		if ($orderTyp != "6") {
			
			

			if ($proceed) {
		      	$query = "LOAD DATA LOCAL INFILE '" . addslashes($filepath) . "' INTO TABLE ". $useThisTable ." COLUMNS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES " . $sqlTables;
		        
		      	\DB::connection()->getpdo()->exec($query);
		      
		  	}else{
		    	abort (500, "CSV Headers Error, [".implode(", ", $proceedArr)."]  are all required fields. Please provide the required columns.");
		  	}
			  	

		}else{
			  $query = "LOAD DATA LOCAL INFILE '" . addslashes($filepath) . "' INTO TABLE ". $useThisTable ." COLUMNS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\\r\\n' IGNORE 1 LINES " . $sqlTables;
			        
			  \DB::connection()->getpdo()->exec($query);
		}
		
		//3. Determine what to import:
		switch ($orderTyp) {
			case '1': 
				dd("NOT HERE! - STOCKS");
				break;

			case '7':
				dd("NOT HERE! - PI");
			break;


			case '2':
				dd("NOT HERE! - BKO");
			break;

			case '3': //Special Order
				$finalstatus = $request->sys_finalstatus;
				$this->processSPO($finalstatus);
				$this->specialordersGenerateJson();
				$this->supplierordersGenerateJson();
				$redirecthere = "/specialorders";
				
				break;

			case '4': //Openboxes
				// set sys_isactive, sys_lasttouch
				//sys_addedby will be set via csv upload
				$finalstatus = $request->sys_finalstatus;
				//$this->updateAuthorForAutoOrders();
				$this->processOpenbox($finalstatus);

				$this->openboxesordersGenerateJson();
				$this->supplierordersGenerateJson();
				$redirecthere = "/openboxes";
				break;


			case '5':
				$finalstatus = $request->sys_finalstatus;

				$this->processRMA($finalstatus);
				
				$this->rapsordersGenerateJson();
				$this->supplierordersGenerateJson();
				$redirecthere = "/raps";
				break;

            case '8':
                //$this->processPIUpdate();
                //$this->updatephotos();
                //$this->updatephotosForRAPs();
                //$this->updatephotosForOpenbox();
                //$this->updateStockStatus();

                $redirecthere = "/home";
                break;

			default: 
				$redirecthere = "/home";
				break;
		}

		
        return Redirect::to($redirecthere);

	} //end import file







	/* ------------------------------------------------------
	/* ------------------------------------------------------
	/* ------------------------------------------------------
	
	OPENBOX

	/* ------------------------------------------------------
	/* ------------------------------------------------------
	/* ---------------------------------------------------- */


	public function updateAuthorForAutoOrders(){
	        $temps = t_order::all();

	        foreach ($temps as $value) {
	            $value->sys_lasttouch = Auth::user()->id;
	            $value->save();
	        }
	}




	public function processOpenbox($finstatus){

        foreach (t_order::cursor() as $temp) {

			$newOpb = new Openbox;

	        $newOpb->referencenum = $temp->referencenum;
	        $newOpb->parentcode = $temp->parent;
	        $newOpb->suppcode = $temp->supplier;
	        $newOpb->partsku = $temp->partsku;

	        $newOpb->descr = $temp->description;
	        $newOpb->kayakoid = $temp->kayakoid;
	        $newOpb->spid = $temp->spid;
	        $newOpb->fault = $temp->fault;
	        $newOpb->qty = $temp->qty;
	        $newOpb->whloc = $temp->whloc;

            $newOpb->photo = preg_replace("/[^a-zA-Z.0-9]+/", "", $temp->photo);
            $newOpb->created_at = date('Y-m-d', strtotime($temp->created_at));
	        $newOpb->sys_orderstatus = $temp->sys_orderstatus;
	        $newOpb->sys_finalstatus = $finstatus;
	        $newOpb->sys_addedby = $temp->sys_addedby;
	        $newOpb->sys_lasttouch = Auth::user()->id;        
	        $newOpb->sys_isactive = $temp->sys_isactive; //Active always
	    
	        $newOpb->save();

	        if ($finstatus == "Pending") {
	        	$this->forSupplierOrders($newOpb);
	        }
	        	
            
        } //end foreach cursor, indb 1

    } //end Process Backorder




    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function openboxesordersGenerateJson(){

        $openboxes = Openbox::select("id", "created_at", "referencenum", "parentcode", "suppcode", "partsku", "qty", "whloc", "sys_orderstatus", "sys_finalstatus", "descr", "fault", "spid", "sys_addedby")
            ->with("user")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        

        $da = array("data" => $openboxes->toArray());
        //dd($da);
               
        file_put_contents("json/openboxesordersjson.json", json_encode($da));

        return "Openboxes Data Generated.";
    }


    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function supplierordersGenerateJson(){

        $supplierorders = Supplierorder::select(DB::raw("id, created_at, parentcode, suppcode, partsku, reorderqty, backorder, openbox, specialorder, rap, sys_total, sys_finalstatus, descr, reorderqty as orderqty, false as pigo, false as bkogo, false as opbgo, false as spogo, false as rapgo, 0 as totalorders, updated_at"))
            ->with("specialordersid", "backordersid", "stocksid", "openboxesid", "rapsid")
            ->where("sys_finalstatus", "Pending")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $supps = $supplierorders->filter(function ($item, $key){
            $ctr = 0;

            $ctr = (count($item->specialordersid)>0 ? $ctr + 1 : $ctr + 0);
            $ctr = (count($item->backordersid)>0 ? $ctr + 1 : $ctr + 0);
            $ctr = (count($item->stocksid)>0 ? $ctr + 1 : $ctr + 0);
            $ctr = (count($item->openboxesid)>0 ? $ctr + 1 : $ctr + 0);
            $ctr = (count($item->rapsid)>0 ? $ctr + 1 : $ctr + 0);
            
            return $ctr>0;
        })->values();
        
        //dd($supplierorders->toArray(), $supps->toArray());

        //dd(count($supplierorders), count($singles), $singles);
        
        $da = array("data" => $supps->toArray());
        
        file_put_contents("json/supplierordersjson.json", json_encode($da));

        return "Supplier Orders Data Generated.";
    }




    /**
     * Save New opbs to Supplier Orders
     * @param Request $request The variables sent by the user for creation
     * @return void
     */
    public function forSupplierOrders(Openbox $opb){
            $thereExistsSuppOrd = Supplierorder::where("partsku", $opb->partsku)
                ->where("suppcode", $opb->suppcode)
                ->where("parentcode", $opb->parentcode)
                ->where("sys_isactive", "1")
                ->where("sys_finalstatus", "Pending")
                ->first();

            if (!is_null($thereExistsSuppOrd)) { // if there exist same partsku in Supplier Order
                // There should only be ONE Pending at a time
                if ($opb->supplierorder_id != $thereExistsSuppOrd->id) {
                    $thereExistsSuppOrd->openbox += $opb->qty;
                    $thereExistsSuppOrd->save();
                }else{
                    $thereExistsSuppOrd->openbox = $opb->qty;
                    $thereExistsSuppOrd->save();
                }
                
                $opb->supplierorder_id = $thereExistsSuppOrd->id;
                $opb->sys_finalstatus = "Pending";
                $opb->save();
                

                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

            }else { //There are no partskus in Supplier Order

                $newSuppOrd = new Supplierorder;
                $newSuppOrd->partsku = $opb->partsku;
                $newSuppOrd->descr = $opb->descr;
                $newSuppOrd->parentcode = $opb->parentcode;
                $newSuppOrd->suppcode = $opb->suppcode;
                $newSuppOrd->openbox = $opb->qty;
                $newSuppOrd->sys_finalstatus = "Pending";

                $newSuppOrd->sys_addedby = Auth::user()->id;
                $newSuppOrd->sys_isactive = 1;
                
                $newSuppOrd->save();

                $opb->sys_finalstatus = "Pending";
                $opb->supplierorder_id = $newSuppOrd->id;
                $opb->save();

                $this->recomputeSupplierOrderTotals($newSuppOrd);
            }
    }



    /**
     * Will re-compute Supplier Order Totals
     * @param Supplierorder $suppord The instance to which the totals will be re-computed
     * @return void
     */
    public function recomputeSupplierOrderTotals(Supplierorder $suppord){
        $suppord->sys_total = intval($suppord->reorderqty) + 
            intval($suppord->backorder) + intval($suppord->openbox) + 
            intval($suppord->specialorder) + intval($suppord->rap); 
        $suppord->save();
    }






    /* ---------------------------------------------------------------------
    /* ---------------------------------------------------------------------
    /* ---------------------------------------------------------------------

	RMA

    /* ---------------------------------------------------------------------
    /* ---------------------------------------------------------------------
    /* ------------------------------------------------------------------- */
    public function processRMA($finstatus){

        foreach (t_order::cursor() as $temp) {

			$newRap = new Rap;

	        $newRap->rmanum = $temp->rmanum;
	        $newRap->parentcode = $temp->parent;
	        $newRap->suppcode = $temp->supplier;
	        $newRap->partsku = $temp->partsku;

	        $newRap->descr = $temp->description;
	        $newRap->kayakoid = $temp->kayakoid;
	        $newRap->notes = $temp->notes;
	        $newRap->spid = $temp->spid;
	        $newRap->whloc = $temp->whloc;
	        $newRap->qty = $temp->qty;
	        $newRap->outcome = $temp->outcome;

            $newRap->photo = preg_replace("/[^a-zA-Z.0-9]+/", "", $temp->photo);
            $newRap->created_at = date('Y-m-d', strtotime($temp->created_at));

	        $newRap->sys_orderstatus = $temp->sys_orderstatus;
	        $newRap->sys_finalstatus = $finstatus;
	        $newRap->sys_addedby = $temp->sys_addedby;
	        $newRap->sys_lasttouch = Auth::user()->id;        
	        $newRap->sys_isactive = $temp->sys_isactive; //Active always
	    
	        $newRap->save();

	        if ($finstatus == "Pending") {
	        	$this->forSupplierOrdersRAP($newRap);
	        }
	        	
            
        } //end foreach cursor, indb 1

    } 


    /**
     * Save New opbs to Supplier Orders
     * @param Request $request The variables sent by the user for creation
     * @return void
     */
    public function forSupplierOrdersRAP(Rap $rma){
            $thereExistsSuppOrd = Supplierorder::where("partsku", $rma->partsku)
                ->where("suppcode", $rma->suppcode)
                ->where("parentcode", $rma->parentcode)
                ->where("sys_isactive", "1")
                ->where("sys_finalstatus", "Pending")
                ->first();

            if (!is_null($thereExistsSuppOrd)) { // if there exist same partsku in Supplier Order
                // There should only be ONE Pending at a time
                if ($rma->supplierorder_id != $thereExistsSuppOrd->id) {
                    $thereExistsSuppOrd->rap += $rma->qty;
                    $thereExistsSuppOrd->save();
                }else{
                    $thereExistsSuppOrd->rap = $rma->qty;
                    $thereExistsSuppOrd->save();
                }
                
                $rma->supplierorder_id = $thereExistsSuppOrd->id;
                $rma->sys_finalstatus = "Pending";
                $rma->save();
                

                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

            }else { //There are no partskus in Supplier Order

                $newSuppOrd = new Supplierorder;
                $newSuppOrd->partsku = $rma->partsku;
                $newSuppOrd->descr = $rma->descr;
                $newSuppOrd->parentcode = $rma->parentcode;
                $newSuppOrd->suppcode = $rma->suppcode;
                $newSuppOrd->rap = $rma->qty;
                $newSuppOrd->sys_finalstatus = "Pending";

                $newSuppOrd->sys_addedby = $rma->sys_addedby;
                $newSuppOrd->sys_isactive = 1;
                
                $newSuppOrd->save();

                $rma->sys_finalstatus = "Pending";
                $rma->supplierorder_id = $newSuppOrd->id;
                $rma->save();

                $this->recomputeSupplierOrderTotals($newSuppOrd);
            }
    }




    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function rapsordersGenerateJson(){

        $raps = Rap::select("id", "created_at", "rmanum", "parentcode", "suppcode", "partsku", "descr", "kayakoid", "notes","spid", "whloc", "qty",  "outcome", "sys_orderstatus", "sys_finalstatus",  "sys_addedby")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $da = array("data" => $raps->toArray());
               
        file_put_contents("json/rapsordersjson.json", json_encode($da));

        return "Raps Data Generated.";
    }





    /* ---------------------------------------------------------------------
    /* ---------------------------------------------------------------------
    /* ------------------------------------------------------------------- */

    //Special Orders

    /* ---------------------------------------------------------------------
    /* ---------------------------------------------------------------------
    /* ------------------------------------------------------------------- */
    public function processSPO($finstatus){

        foreach (t_order::cursor() as $temp) {

			$newSpo = new Specialorder;
			$newSpo->created_at = date('Y-m-d', strtotime($temp->dateinvoiced));
	        $newSpo->ordernum = $temp->ordernumber;
	        $newSpo->parentcode = $temp->parent;
	        $newSpo->suppcode = $temp->supplier;
	        $newSpo->partsku = $temp->partsku;

	        $newSpo->descr = $temp->description;
	        $newSpo->rma = $temp->rma;
	        $newSpo->fault = $temp->fault;
	        $newSpo->qty = $temp->qty;

	        $newSpo->sys_orderstatus = $temp->sys_orderstatus;
	        $newSpo->sys_finalstatus = $finstatus;
	        $newSpo->sys_addedby = $temp->sys_addedby;
	        $newSpo->sys_lasttouch = Auth::user()->id;        
	        $newSpo->sys_isactive = 1; //Active always
	    
	        $newSpo->save();

	        if ($finstatus == "Pending") {
	        	$this->forSupplierOrdersSPO($newSpo);
	        }
	        	
            
        } //end foreach cursor, indb 1

    } 



    /**
     * Save New SpOs to Supplier Orders
     * @param Request $request The variables sent by the user for creation
     * @return void
     */
    public function forSupplierOrdersSPO(Specialorder $spo){
            $thereExistsSuppOrd = Supplierorder::where("partsku", $spo->partsku)
                ->where("suppcode", $spo->suppcode)
                ->where("parentcode", $spo->parentcode)
                ->where("sys_isactive", "1")
                ->where("sys_finalstatus", "Pending")
                ->first();

            if (!is_null($thereExistsSuppOrd)) { // if there exist same partsku in Supplier Order
                // There should only be ONE Pending at a time
            	if ($spo->supplierorder_id != $thereExistsSuppOrd->id) {
            		$thereExistsSuppOrd->specialorder += $spo->qty;
	                $thereExistsSuppOrd->save();
            	}else{
            		$thereExistsSuppOrd->specialorder = $spo->qty;
	                $thereExistsSuppOrd->save();
            	}
                
                $spo->supplierorder_id = $thereExistsSuppOrd->id;
                $spo->sys_finalstatus = "Pending";
                $spo->save();
                

                $this->recomputeSupplierOrderTotals($thereExistsSuppOrd);

            }else { //There are no partskus in Supplier Order

                $newSuppOrd = new Supplierorder;
                $newSuppOrd->partsku = $spo->partsku;
                $newSuppOrd->descr = $spo->descr;
                $newSuppOrd->parentcode = $spo->parentcode;
                $newSuppOrd->suppcode = $spo->suppcode;
                $newSuppOrd->specialorder = $spo->qty;
                $newSuppOrd->sys_finalstatus = "Pending";

                $newSuppOrd->sys_addedby = $spo->sys_addedby;
                $newSuppOrd->sys_isactive = 1;
                
                $newSuppOrd->save();

                $spo->sys_finalstatus = "Pending";
                $spo->supplierorder_id = $newSuppOrd->id;
                $spo->save();

                $this->recomputeSupplierOrderTotals($newSuppOrd);
            }
    }


    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function specialordersGenerateJson(){

        $specialorders = Specialorder::select("id", "created_at", "ordernum", "parentcode", "suppcode", "partsku", "qty", "sys_orderstatus", "sys_finalstatus", "descr", "fault", "rma")
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();

        $da = array("data" => $specialorders->toArray());
               
        file_put_contents("json/specialordersjson.json", json_encode($da));

        return "Special Orders Data Generated.";
    }



    /**
     * Generates a Json file for Supplierorders.index
     *
     * @return json
     */
    public function processPIUpdate(){

        $tp = t_partsku::all();

        foreach ($tp as $temp) {
            $theRealPSku = Partsku::where("partsku", $temp->partsku)
                ->first();

            $theStock = Stock::where("partsku_id", $theRealPSku->id)
                ->first();


            /*$theRealPSku->barcode = $temp->barcode;
            $theRealPSku->relatedsku = $temp->relatedsku;
            $theRealPSku->sys_isactive = $temp->sys_isactive;
            $theRealPSku->save();*/

            $theStock->sys_status = $temp->sys_status;
            $theStock->save();

            

            $supplierOrder = Supplierorder::find($theStock->supplierorder_id);
            $otherSupplierOrder = Supplierorder::where("partsku", $theRealPSku->partsku)
                ->where("parentcode", $temp->parent)
                ->where("suppcode", $temp->supplier)
                ->where("sys_finalstatus", "Pending")
                ->first();


            

            if (!is_null($supplierOrder)) { // if 1
                if (intval($theStock->reorderqty) == intval($supplierOrder->sys_total)) { // if 2
                    /*if ($supplierOrder->sys_finalstatus == "Pending") {
                        dd("TOP", $supplierOrder, $theStock);
                    }*/

                    if (!is_null($otherSupplierOrder)) { //if 3
                        
                        //detach the order from Supplier Order
                        $supplierOrder->reorderqty = intval($supplierOrder->reorderqty) - intval($theStock->reorderqty);
                        $supplierOrder->save();
                        $this->recomputeSupplierOrderTotals($supplierOrder);

                        $otherSupplierOrder->reorderqty = intval($otherSupplierOrder->reorderqty) + intval($theStock->reorderqty);
                        $otherSupplierOrder->save();
                        $this->recomputeSupplierOrderTotals($otherSupplierOrder);

                        $theStock->supplierorder_id = $otherSupplierOrder->id;
                        $theStock->save();

                    }else{ // end if 3 

                        $theRealPSku->parentcode = $temp->parent;
                        $theRealPSku->suppcode = $temp->supplier;
                        $theRealPSku->save();

                        $supplierOrder->parentcode = $temp->parent;
                        $supplierOrder->suppcode = $temp->supplier;
                        $supplierOrder->save();
                    } //end else 3
                } else {// end if 2 
                    /*if ($supplierOrder->sys_finalstatus == "Pending") {
                        dd("BOTTOm", $supplierOrder, $theStock);
                    }*/
                    if (!is_null($otherSupplierOrder)) {
                        $supplierOrder->reorderqty = intval($supplierOrder->reorderqty) - intval($theStock->reorderqty);
                        $supplierOrder->save();
                        $this->recomputeSupplierOrderTotals($supplierOrder);

                        $otherSupplierOrder->reorderqty = intval($otherSupplierOrder->reorderqty) + intval($theStock->reorderqty);
                        $otherSupplierOrder->save();
                        $this->recomputeSupplierOrderTotals($otherSupplierOrder);

                        $theStock->supplierorder_id = $otherSupplierOrder->id;
                        $theStock->save();
                    }else{
                        $theRealPSku->parentcode = $temp->parent;
                        $theRealPSku->suppcode = $temp->supplier;
                        $theRealPSku->save();

                        $supplierOrder->parentcode = $temp->parent;
                        $supplierOrder->suppcode = $temp->supplier;
                        $supplierOrder->save();
                    }

                }// end if 2
            } else {// end if 1
                $theRealPSku->parentcode = $temp->parent;
                $theRealPSku->suppcode = $temp->supplier;
                $theRealPSku->save();
            }


        }// end foreach
    } // end processPI Update



    //renamephotos
    public function renamephotos(){

        /*$newSuppOrd->photo = $request->hasFile('photo') ? preg_replace("/[^a-zA-Z.0-9]+/", "", $request->photo->getClientOriginalName()) : "qmbyjpserame.png";*/

        $path = storage_path('app/public');
        $files = scandir($path);
        
        foreach ($files as $currentFile) {
             
            if ($currentFile != "." and $currentFile != ".." and $currentFile != ".gitignore") {
                       
                rename($path . "/" . $currentFile,  $path . "/" . preg_replace("/[^a-zA-Z.0-9]+/", "", $currentFile));
                echo ("DONE WITH " . $currentFile . "<br>");

            } // end if

        }// end foreach

    } // end processPI Update



    //update photos
    public function updatephotos(){

        $temporaries = t_partsku::all();

        foreach ($temporaries as $temp) {
            $thePartSKU = Partsku::where("partsku", $temp->partsku)
                ->first();

            if ($temp->photo != "") {

                $thePartSKU->photo = preg_replace("/[^a-zA-Z.0-9]+/", "", $temp->photo);
                $thePartSKU->save();

                $theStock = Stock::where("partsku_id", $thePartSKU->id)
                    ->first();

                if (!is_null($theStock)) {
                    $suppOrder = Supplierorder::find($theStock->supplierorder_id);

                    if (!is_null($suppOrder)) {
                        $suppOrder->photo = $thePartSKU->photo;
                        $suppOrder->save();
                    }
                }

                   

            }else{
                $thePartSKU->photo = "qmbyjpserame.png";
                $thePartSKU->save();
            }
            
        }

    } // end updatephotos




     //update photos
    public function updatephotosForOpenbox(){

        $temporaries = t_partsku::all();
        //will load only partsku and photo so having it in t_partsku is okay. really.

        foreach ($temporaries as $temp) {
            
            $theOpb = Openbox::where("partsku", $temp->partsku)
                ->where("referencenum", $temp->referencenum)
                ->where("spid", $temp->spid)
                ->where("kayakoid", $temp->kayakoid)
                ->get();

            if (!is_null($theOpb)) {
                foreach ($theOpb as $oneOPB) {
                    if ($oneOPB->photo == "qmbyjpserame.png") {
                        if ($oneOPB->sys_finalstatus == "Pending" or $oneOPB->sys_finalstatus == "Unresolved") {
                            $oneOPB->photo = preg_replace("/[^a-zA-Z.0-9]+/", "", $temp->photo);
                            $oneOPB->save();

                            $suppOrder = Supplierorder::find($oneOPB->supplierorder_id);

                            if (!is_null($suppOrder)) {
                                if ($suppOrder->photo == "qmbyjpserame.png") {
                                    $suppOrder->photo = $oneOPB->photo;
                                    $suppOrder->save();    
                                }
                                
                            } // end if Suporder
                        }
                    } // end if
                     
                } 
                
            }
            
        }
        dd("PROCESSING DONE - OPB");

    } // end updatephotos


    //update photos
    public function updatephotosForRAPs(){

        $temporaries = t_partsku::all();
        //will load only partsku and photo so having it in t_partsku is okay. really.

        foreach ($temporaries as $temp) {
            
            $theOrders = Rap::where("partsku", $temp->partsku)
                ->where("rmanum", $temp->rmanum)
                ->where("spid", $temp->spid)
                ->where("kayakoid", $temp->kayakoid)
                ->where("descr", $temp->description)
                ->get();

            

            if (!is_null($theOrders)) {
                foreach ($theOrders as $oneRAP) {
                    if ($oneRAP->photo == "qmbyjpserame.png") {
                        if ($oneRAP->sys_finalstatus == "Pending" or $oneRAP->sys_finalstatus == "Unresolved") {
                                $oneRAP->photo = preg_replace("/[^a-zA-Z.0-9]+/", "", $temp->photo);
                                $oneRAP->save();

                                $suppOrder = Supplierorder::find($oneRAP->supplierorder_id);

                                if (!is_null($suppOrder)) {

                                    if ($suppOrder->photo == "qmbyjpserame.png") {
                                        $suppOrder->photo = $oneRAP->photo;
                                        $suppOrder->save();    
                                    }
                                    

                                }


                        } // end if pending
                        


                    }
                }
            }
            
        }
        dd("PROCESS DONE!");

    } // end updatephotos



    public function updateStockStatus(){

        foreach (t_partsku::cursor() as $temp) {
                $theStock = Stock::where("partsku", $temp->partsku)
                    ->first();

                //dd($theStock, $temp);
                $theStock->sys_status = $temp->sys_status;
                $theStock->save();
            
            
                
            
        } //end foreach cursor, indb 1

    } //end Process Backorder



    public function listAllExistingOrdersWithOutSupplierOrders(){

        $supplierorders = Existingorder::select("id", "created_at", "parentcode", "suppcode", "partsku", "sys_total", "sys_finalstatus", "spnum", "eta", "descr")
            ->with("supplierordersid")
            ->whereNotIn("sys_finalstatus", ["Pending", "Cancelled", "Unresolved"])
            ->orderBy("updated_at", "created_at", "partsku")
            ->get();
        $none = collect();
        
        foreach ($supplierorders as $exOrd) {
            $suppOrders = Supplierorder::where("existingorder_id", $exOrd->id)->get();
            $items = 0;

            if (count($suppOrders) > 0) {
                
            
                foreach ($suppOrders as $suppord) {
                    $spo = Specialorder::where("supplierorder_id", $suppord->id)
                        ->where("sys_finalstatus", $exOrd->sys_finalstatus)
                        ->get();

                    if (count($spo) > 0) {
                        $items += 1;
                    }else {
                        $bko = Backorder::where("supplierorder_id", $suppord->id)
                            ->where("sys_finalstatus", $exOrd->sys_finalstatus)
                            ->get();    

                        if (count($bko) > 0) {
                            $items += 1;
                        }else{
                            $opb = Openbox::where("supplierorder_id", $suppord->id)
                                ->where("sys_finalstatus", $exOrd->sys_finalstatus)
                                ->get();

                            if (count($opb) > 0) {
                                $items += 1;
                            }else{
                                $rap = Rap::where("supplierorder_id", $suppord->id)
                                    ->where("sys_finalstatus", $exOrd->sys_finalstatus)
                                    ->get();
                                if (count($rap) > 0) {
                                    $items += 1;
                                }else{

                                    $stk = Stock::where("supplierorder_id", $suppord->id)
                                        ->where("sys_finalstatus", $exOrd->sys_finalstatus)
                                        ->get();

                                    if (count($stk) > 0) {
                                        $items += 1;
                                    }
                                }
                                                                
                            }
                        }
                    }

                    if ($items <= 0) {
                        $supplierorders->forget($exOrd->id);
                        $none->push($exOrd->id);
                    }
                    
                }  // end inner foreach
            }else{
                $supplierorders->forget($exOrd->id);
                $none->push($exOrd->id);
            }  // end if
            
        }// end foreach

        
        dd($supplierorders, $none);
    } 


}
