<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplierorder extends Model
{
    //
   public function specialordersid (){
        return $this->hasMany(Specialorder::class)->select(array('id', 'supplierorder_id', 'qty', 'sys_finalstatus', 'photo'))->where("sys_finalstatus", "Pending");
    }

    public function stocksid (){
        return $this->hasMany(Stock::class)
            ->with('partskuIbelong')
            ->where("sys_finalstatus", "Pending");
    }

    public function backordersid (){
        return $this->hasMany(Backorder::class)->select(array('id', 'supplierorder_id', 'qty', 'sys_finalstatus', 'photo'))->where("sys_finalstatus", "Pending");
    }

    public function openboxesid (){
        return $this->hasMany(Openbox::class)->select(array('id', 'supplierorder_id', 'qty', 'sys_finalstatus', 'photo'))->where("sys_finalstatus", "Pending")->where("is_archived", 0);
    }

    public function rapsid (){
        return $this->hasMany(Rap::class)->select(array('id', 'supplierorder_id', 'qty', 'sys_finalstatus', 'photo'))->where("sys_finalstatus", "Pending")->where("is_archived", 0);
    }




    public function specialorders (){
        return $this->hasMany(Specialorder::class)->select(array('id','supplierorder_id', 'partsku', 'descr', 'qty', 'sys_finalstatus'))->where("sys_finalstatus", "!=", "Pending");
    }

    public function stocks (){
        return $this->hasMany(Stock::class)
            ->select(array('id', 'supplierorder_id','partsku_id', 'partsku',  'reorderqty', 'sys_finalstatus'))
            ->with('partskuIbelong')
            ->where("sys_finalstatus", "!=", "Pending");
    }

    public function existingorder (){
        return $this->belongsTo(Existingorder::class);
    }

    public function backorders (){
        return $this->hasMany(Backorder::class)->select(array('id','supplierorder_id', 'partsku', 'descr', 'qty', 'sys_finalstatus'))->where("sys_finalstatus", "!=", "Pending");
    }

    public function openboxes (){
        return $this->hasMany(Openbox::class)->select(array('id','supplierorder_id', 'partsku', 'descr', 'qty', 'sys_finalstatus'))->where("sys_finalstatus", "!=", "Pending");
    }

    public function raps (){
        return $this->hasMany(Rap::class)->select(array('id','supplierorder_id', 'partsku', 'descr', 'qty', 'sys_finalstatus'))->where("sys_finalstatus", "!=", "Pending");
    }






    public function specialordersdesc (){
        return $this->hasMany(Specialorder::class)->select(array('id', 'supplierorder_id','partsku', 'descr', 'qty', 'sys_finalstatus', 'sys_orderstatus'))->where("sys_finalstatus", "Pending");
    }

    public function stocksdesc (){
        return $this->hasMany(Stock::class)/*->select(array('id', 'supplierorder_id', 'partsku_id','partsku', 'reorderqty as qty', 'sys_finalstatus'))*/->where("sys_finalstatus", "Pending");
    }

    public function backordersdesc (){
        return $this->hasMany(Backorder::class)->select(array('id', 'supplierorder_id','partsku', 'descr', 'qty', 'sys_finalstatus', 'sys_orderstatus'))->where("sys_finalstatus", "Pending");
    }

    public function openboxesdesc (){
        return $this->hasMany(Openbox::class)->select(array('id', 'supplierorder_id','partsku', 'descr', 'qty', 'sys_finalstatus', 'sys_orderstatus'))->where("sys_finalstatus", "Pending");
    }

    public function rapsdesc (){
        return $this->hasMany(Rap::class)->select(array('id', 'supplierorder_id','partsku', 'descr', 'qty', 'sys_finalstatus', 'sys_orderstatus'))->where("sys_finalstatus", "Pending");
    }




    public function specialordersnonp (){
        return $this->hasMany(Specialorder::class)->select(array('id', 'supplierorder_id', 'qty', 'sys_finalstatus', 'photo'))->whereNotIn("sys_finalstatus", ["Pending", "Cancelled", "Unresolved"]);
    }

    public function stocksnonp (){
        return $this->hasMany(Stock::class)
            ->with('partskuIbelong')
            ->whereNotIn("sys_finalstatus", ["Pending", "Cancelled", "Unresolved"]);
    }

    public function backordersnonp (){
        return $this->hasMany(Backorder::class)->select(array('id', 'supplierorder_id', 'qty', 'sys_finalstatus', 'photo'))->whereNotIn("sys_finalstatus", ["Pending", "Cancelled", "Unresolved"]);
    }

    public function openboxesnonp (){
        return $this->hasMany(Openbox::class)->select(array('id', 'supplierorder_id', 'qty', 'sys_finalstatus', 'photo'))->whereNotIn("sys_finalstatus", ["Pending", "Cancelled", "Unresolved"]);
    }

    public function rapsnonp (){
        return $this->hasMany(Rap::class)->select(array('id', 'supplierorder_id', 'qty', 'sys_finalstatus', 'photo'))->whereNotIn("sys_finalstatus", ["Pending", "Cancelled", "Unresolved"]);
    }


   

}
