<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Openbox extends Model
{
    //
    protected $guarded = array();
    
    public function supplierorders (){
        return $this->belongsTo(Supplierorder::class);
    }

    public function user (){
        return $this->belongsTo(User::class, "sys_addedby", "id");
    }

    public function lasttouch (){
        return $this->belongsTo(User::class, "sys_lasttouch", "id");
    }
}
