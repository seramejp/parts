<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    /*protected $primaryKey = 'supplierid';*/
    protected $guarded = array();

    public function partskus (){
    	return $this->belongsTo(Partsku::class);
    }

    public function parentskus (){
        return $this->hasMany(Parentsku::class);
    }

    /*public function temppartskus (){
    	return $this->belongsTo(temppartskus::class, "supplierid");
    }

    public function specialorders (){
    	return $this->belongsTo(Specialorder::class, "supplierid");	
    }

    public function openboxes (){
    	return $this->belongsTo(Openbox::class, "supplierid");	
    }

    public function supplierorders (){
        return $this->belongsTo(Supplierorder::class, "supplierid");  
    }
    
    public function backorders (){
        return $this->belongsTo(Supplierorder::class, "supplierid");  
    }*/
}
