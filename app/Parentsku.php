<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parentsku extends Model
{
    //
    protected $guarded = array();
	public function supplier (){
    	return $this->belongsTo(Supplier::class);
    }
    
}
