@extends('layout')

@section('content')
	
		<h2 class="thin"><span style="color:blue;"><strong>Archive</strong> Open Boxes</span></h3>
		<p class="muted">List of Openboxes that is available for archiving. Check items that you want to archive, then click Archive button.</p>
		
		<ol class="breadcrumb text-left">
			<li class="active">Openboxes</li>
			<li><a href="{{url('/settings/archive/unarchive/opb')}}">Un-Archive</a></li>
		</ol>

		<hr>

		@if(Session::has('there-has-been-an-archived-openbox-message'))
            <div class="alert alert-success text-left">
                {{ Session::get('there-has-been-an-archived-openbox-message') }}
            </div>
        @endif

		<div class="text-left">
			<div id="loaderopen" class="text-center">
				<img src="{{url('/images/app/rolling.gif')}}">
			</div>
			<table class="table table-hover" id="archive-optable" cellspacing="0" width="100%">
				<thead>
					<th >Date Created</th>
                    <th >Reference Number</th>
                    <th >Parent SKU</th>
                    <th >Supplier</th>
                    <th >Part Removed</th>
                    <th >Qty</th>
                    <th >Location</th>
                    <th >Opb Status</th>
                    <th >Order Status</th>
                    <th >Descr</th>
                    <th >SP Id</th>
                    <th>Archive?</th>
				</thead>
			
				<tbody class="hidden">
					
				</tbody>

				<tfoot>
					<th colspan="12" rowspan="1" class="text-right" id="buttoncontainer">
					</th>
				</tfoot>
			</table>

			<form id="form_archiveopb" action=" {{ url('/settings/archive/archiveselected') }}" method="POST" enctype="multipart/form-data">
				<input type="text" class="hidden" id="selected_archiveopb" name="selected_archiveopb">
				{{ csrf_field() }}
			</form>	

		</div>

		

	</div>
@stop


@section('userdefjs')
	<script>

		function formatThisDate(thisDate){
			return thisDate.getFullYear() + "-" + (  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "-" + (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   );
		}

		$(document).ready(function() {
			$(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
			$(".navsubmenuitemlist li").removeClass('active').eq(3).addClass('active');
			
			var table = $('#archive-optable')
				.on( 'init.dt', function () {
			        $("#archive-optable tbody").removeClass('hidden');
			        $("#loaderopen").addClass('hidden');
			    })
				.DataTable({
					"processing": true,
					"ajax": '{{ url('/json/forarchiveopb.json') }}',
					"columns": [
			            {
			            	data: 'created_at',
			            	"render": function ( data, type, full, meta ) {
			            		var d = new Date(data);

								return formatThisDate(d);
						    }
			        	},
			            { "data": "referencenum" },
			            { "data": "parentcode" },
			            { "data": "suppcode" },
			            { "data": "partsku" },
			            { "data": "qty" },
			            { "data": "whloc" },
			            { "data": "sys_orderstatus" },
			            { "data": "sys_finalstatus" },

			            { "data": "descr" },
			            { "data": "spid" },


			            { 
			            	"className": 'select-checkbox',
			                "orderable":      false,
			                "data":           null,
			                "defaultContent": ''
			            },
			        ],
			        "select": {
			        	style: 'multi',
			        	selector: 'td:last-child'
			        },
			        dom: 'Bfrtip',
		       		"buttons": [
				            { 
					            extend: 'selected',
					            text: '<i class="fa fa-sign-in"></i> Archive selected',
					            className: 'btn-success',
					            action: function ( e, dt, button, config ) {
					                //alert( dt.rows( { selected: true } ).indexes().length +' row(s) selected' );
					                $("#selected_archiveopb").val(JSON.stringify(dt.rows({selected: true}).data().toArray()));
				                	$("#form_archiveopb").submit();
					            }
					        }	
			        ],
			    });


			table.buttons().container().appendTo( $('#buttoncontainer') );



			$("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');
			$("th.clone-control").removeClass('sorting_asc').addClass('sorting_disabled');



		}); //End Document Ready

		
	</script>
@stop