@extends('layout')

@section('content')
	<div class="importViewPage text-left">
		<h3 class="text-center">Automated Uploading of Existing Data</h3>
		<h5 class="text-center"><small><em>
			Bla Bla Bla.
		<br>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit voluptate error, ducimus perspiciatis! Rerum, saepe explicabo molestiae labore perspiciatis architecto natus obcaecati accusantium! Cumque ipsum eius voluptates. Voluptates deleniti esse magni, ab, consequatur aspernatur vel corporis illo cum molestias ea quidem, nisi. Temporibus alias sequi repudiandae doloremque minima, atque molestias.
		</em></small><h5>
		
		<hr>
		<h5>
			<small> 
			</small>
		</h5>
	
			<div class="row dataImportSelect">

				<div class="col-lg-4">
					<h4 class="text-primary">Data Type</h4>
				</div>
				<div class="col-lg-8">
					<h4>Select Data to Import</h4>
					<h4><small>The type of data selected determines your import options</small></h4>

					<div class="form-group">
						
						<div class="radio">
							<label>
								<input type="radio" name="optDataImport" value="4" checked>Open Boxes
							</label>
						</div> 

						<div class="radio">
							<label>
								<input type="radio" name="optDataImport" value="5">RAP
							</label>
						</div> 
						
						<div class="radio">
							<label>
								<input type="radio" name="optDataImport" value="3">Special Orders
							</label>
						</div>

						<div class="radio">
							<label>
								<input type="radio" name="optDataImport" value="8">Parts Inventory Update
							</label>
						</div>

						
						 
					</div> 

				</div><br>

			</div>

		<form action="{{url('/automation/import/uploadcsv')}}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}

			<div class="row">
				<div class="col-lg-4">
					<h4 class="text-primary">Import Data</h4>
				</div>
				<div class="col-lg-8">
					<h4>Final Status</h4>
					<select name="sys_finalstatus" class="form-control">
						<option value="Unresolved">Unresolved</option>
						<option value="Cancelled">Cancelled</option>
						<option value="Pending">Pending</option>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-8 col-lg-offset-4">
					<br>
					<h4>Upload</h4>
					<h4><small>Import your CSV data following the format specified by the template.</small></h4>
					<input id="ivFile" type="file" accept=".csv" class="form-control" name="image"><br>
					<button class="btn btn-primary col-lg-4" type="submit" id="btnIVUpload" disabled="true"><i id="faIconUpload" class="fa fa-upload"></i> Import CSV</button>
					<input type="text" class="hidden form-control input-sm" name="importtype" value="4">
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col-lg-12">
				&nbsp;
			</div>
		</div>
	</div>
@stop

@section('userdefjs')
	<script>
		$(function(){
			$(".navmenuitemlist li.dropdown").removeClass('active').eq(2).addClass('active');
        	$(".navsubmenuitemlist li").removeClass('active').eq(7).addClass('active');

			$("input[name='optDataImport']").on('change', function(event) {
				event.preventDefault();
				/* Act on the event */
				$("#spanDataImport").text($(this).closest("label").text());
				selectedOptRadioVal = $("input:radio[name='optDataImport']:checked").val().trim();
				$("input[name='importtype']").val(selectedOptRadioVal);

			});

			$("#btnIVGenerateTemplate").on('click', function(event) {
				event.preventDefault();
				/* Act on the event */

				var arrColsImp = [];
				var csvD1 = {};
				var csvD2 = {};
				var csvD3 = {};
				var csvD4 = {};
				var csvData = [];
				

				selectedOptRadioText = $("input:radio[name='optDataImport']:checked").parent().text().trim();
				selectedOptRadioVal = $("input:radio[name='optDataImport']:checked").val().trim();

			

				switch(selectedOptRadioVal){
					case "1": //parts inventory - CHECKED
							arrColsImp = ["partsku", "oldsku", "parent", "description", "whlocation", "bulklocation", "qtyinbulkloc", "partweight",  "partlength", "partwidth", "partheight", "shippingmethod", "price", "supplier", "barcode", "wiseitemid", "partsorpurchasing",  "dhlorcontainer", "remarks"];

							csvD1 = {"partsku": "P-PartName-901", "oldsku": "MyOldSKU", "parent": "MyParentSKU", "description": "This is a description", "whlocation": "Location in Warehouse", "bulklocation": "Bulk Location", "qtyinbulkloc": "2", "partweight": "10", "partlength": "2", "partwidth": "1.2", "partheight": "0.1", "shippingmethod": "Sample Shipping Method", "price": "12.50", "supplier": "MIT-SUP-0001", "barcode": "0100101001010000", "wiseitemid": "11011", "partsorpurchasing": "Purchasing", "dhlorcontainer": "DHL", "remarks": "Any Remark"};
							csvD2 = {"partsku": "P-PartName-901", "oldsku": "MyOldSKU", "parent": "MyParentSKU", "description": "This is a description", "whlocation": "Location in Warehouse", "bulklocation": "Bulk Location", "qtyinbulkloc": "2", "partweight": "10", "partlength": "2", "partwidth": "1.2", "partheight": "0.1", "shippingmethod": "Sample Shipping Method", "price": "12.50", "supplier": "MIT-SUP-0001", "barcode": "0100101001010000", "wiseitemid": "11011", "partsorpurchasing": "Purchasing", "dhlorcontainer": "DHL", "remarks": "Any Remark"};

						break;

					case "2": //back orders- will not require Supplier and Parent
							arrColsImp = ["dateinvoiced", "ordernumber", "partsku", "description", "remarks", "ordertype", "category", "qty", "orderstatus", "spid" , "eta"];

							csvD1 = {"dateinvoiced": "2017-01-31", "ordernumber": "M2016519", "partsku": "P-Mousethatworks-801", "description": "Mouse that works well with a mousepad.", "remarks": "remarks", "ordertype": "Website", "category": "Backorder Approved", "qty": "2", "orderstatus": "new", "spid": "11013", "eta": "2017-12-31" };

							csvD2 = {"dateinvoiced": "yyyy-mm-dd", "ordernumber": "JP100121", "partsku": "P-IDSling-801", "description": "ID Holder.", "remarks": "remarks", "ordertype": "Rma", "category": "Backorder In Transit", "qty": "4", "orderstatus": "in transit", "spid": "11013", "eta": "yyyy-mm-dd" };

						break;

					case "3": //Special Orders - CHEKCED
							arrColsImp = ["datecreated", "ordernumber", "supplier", "parent", "partsku", "description", "rmanumber", "fault", "qty", "orderstatus" ];

							csvD1 = {"datecreated": "2016-01-31", "ordernumber": "JJ14716", "supplier": "MIT-SUP-0214" , "parent": "CONSAWBMRA08X", "partsku": "Special order", "description": "Saw Blade", "rmanumber": "LYR-543-59914", "fault": "Not my fault", "qty": "23", "orderstatus": "received" };

							csvD2 = {"datecreated": "2016-01-31", "ordernumber": "JJ14716", "supplier": "MIT-SUP-0214" , "parent": "CONSAWBMRA09X", "partsku": "Special Order", "description": "Blade", "rmanumber": "LYR-543-12345", "fault": "Not my fault", "qty": "44", "orderstatus": "cancelled" };
						break;

					case "4": //Open Boxes
							arrColsImp = ["datecreated", "referencenum", "parentcode", "partsku", "location", "spID", "partdesc", "fault", "opbqty", "rma" ];
							csvD1 = {"datecreated": "30/08/2016", "referencenum": "SP 14716", "parentcode": "CONSAWBMRA08X", "partsku": "Special order only", "location": "Basement", "spID": "SP 4321", "partdesc": "Saw Blade", "fault": "Missing part", "opbqty": "23", "rma": "LYR-543-59914" };
							csvD2 = {"datecreated": "02/08/2016", "referencenum": "", "parentcode": "FURINDOVDARBL", "partsku": "Special order only", "location": "Front Lawn", "spID": "SP 1234", "partdesc": "Chair Base", "fault": "Broken Part", "opbqty": "1", "rma": "SXT-543-69916" };
						break;

					case "5": // Existing Orders
							arrColsImp = ["datecreated", "ordernumber", "partsku", "supplier", "description", "orderavenue", "orderqty", "eta", "orderstatus", "trackingnum" ];
							csvD1 = {"datecreated": "07/06/2016", "ordernum": "161239", "partsku": "P-BBQ-8BNR-1-901", "supplier": "MIT-SUP-0141", "partdesc": "BBQ Cover 8 Burner", "orderavenue": "Parts", "orderqty": "23", "eta": "23/08/2016", "orderstatus": "1-New, 3-In Transit, 4-Received, 5-Canceled, 6-Dispatched", "trackingnum": "123456" };
							csvD2 = {"datecreated": "18/02/2016", "ordernum": "161271", "partsku": "P-BBQCOV-903", "supplier": "MIT-SUP-0159", "partdesc": "BBQ cover for 8 burner double hood BBQ", "orderavenue": "Purchasing", "orderqty": "33", "eta": "23/08/2016", "orderstatus": "1-New, 3-In Transit, 4-Received, 5-Canceled, 6-Dispatched", "trackingnum": "987654" };

						break;

					case "6":
							arrColsImp = ["parent", "supplier", "description"];

							csvD1 = {"parent": "EXCLBR", "supplier": "King Arthur", "description": "A +9 Sword that glows brighter for every blood spilled"};
							csvD2 = {"parent": "SMSNGMNTR", "supplier": "Samsung Electronics", "description": "A viewing device that will either entertain or harm you"};
						break;

					case "7": //STOCKS
							arrColsImp = ["partsku", "soh", "restockqty", "reorderqty", "salespast150d", "needsordering"];

							csvD1 = {"partsku": "P-PartName-901", "soh": "31", "restockqty": "30",
									"reorderqty": "30", "salespast150d": "10", "needsordering": "1"};
							csvD2 = {"partsku": "P-PartName-901", "soh": "31", "restockqty": "30",
									"reorderqty": "30", "salespast150d": "10", "needsordering": "0"};
						break;


					default:
							arrColsImp = ["nothing shown"];
							csvD1 = {"nothing shown": "0"};
							csvD2 = {"nothing shown": "0"};
						break;
				}

				

				csvData = [csvD1, csvD2];
				JSONToCSVConvertor(csvData, arrColsImp, "PMS_Template - " + selectedOptRadioText, true);
			});

			$(".importViewPage").on('change', '#ivFile', function(event) {
				event.preventDefault();
				
				if ($("#ivFile").val()!="") {
					$("#btnIVUpload").attr("disabled", false);
				}else{
					$("#btnIVUpload").attr("disabled", true);
				}
			});

			$(".importViewPage").on('click', '#btnIVUpload', function() {
				
				$("#faIconUpload").removeClass('fa fa-upload').addClass('fa fa-spinner fa-pulse fa-fw text-danger');
				
			});
		});
	</script>
@stop
