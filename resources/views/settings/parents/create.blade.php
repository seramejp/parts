@extends("layout")

@section("content")
    
    <h2 class="thin">Add New Parent SKU</h2>
    <p class="text-muted">This is the way to add a new Parent SKU. Make sure to select the proper Supplier.</p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/settings/parents/create')}}">Parent SKU</a></li>
        <li class="active">Add New</li>
    </ol>
    <hr>
	
			<form method="POST" action="{{url('/settings/parents/create/new')}}" class="text-left" enctype="multipart/form-data">
				{{ csrf_field() }}
				
				<div class="panel panel-primary">
					<div class="panel-heading">
						General Information
						<small><em>will contain the required information of this Parent SKU.</em></small>
					</div>
					
					<div class="panel-body">

						<div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Date Created
                                    	<span class="text-success"><small><em>
                                    		DD-MM-YYYY format
                                    	</em></small></span>
                                	</label>
                                    <input type="text" class="form-control input-sm" name="created_at" readonly="" value="{{ Carbon\Carbon::now()->format("d-m-Y") }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Parent SKU
                                    	<span class="text-success"><small><em>
                                    		name this Parent SKU
                                    	</em></small></span>
                                    </label>
                                    <input type="text" class="form-control input-sm" name="parentcode" autofocus="">
                                    
                                </div>
                            </div>
                            

                        </div>

                         <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Supplier
                                        <span class="text-success"><small><em>
                                            select from a list of suppliers
                                        </em></small></span>
                                    </label>
                                    <select name="suppcode" id="suppcode" class="input-sm form-control selectpicker" data-size="8" data-live-search="true">
                                        @foreach($suppliers as $supp)
                                            <option value="{{$supp->suppcode}}">{{$supp->suppcode}}</option>        
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
						
					</div> <!-- ./Panel-body -->
						
				</div> <!-- ./Panel -->

				<div>
					<div class="row">
						<div class="col-md-4 pull-right">
				        	<button class="btn btn-primary btn-block paddtop paddbot" type="submit">Save</button>
				        </div>
                        <div class="col-md-4 pull-right">
                            <a class="btn btn-default btn-block" href="{{url('/settings/parents')}}">Cancel</a>
                        </div>
					</div>
				</div>
			</form>

@stop

@section("userdefjs")
	<script>
 	

		$(function(){

           $(".navmenuitemlist li.dropdown").removeClass('active').eq(3).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(12).addClass('active');
        
		});
	</script>
@stop