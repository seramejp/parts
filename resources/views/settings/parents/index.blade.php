@extends('layout')

@section('content')
    
        <h2 class="thin">Parent SKU's</h3>
        <p class="muted">The list of all Parent SKUs integrated in this system. Parent SKUs is an alias for Cross-Sell SKUs in Neto.</p>
        
        <ol class="breadcrumb text-left">
            <li class="active">Parent SKUs</li>
        </ol>

        <hr>

        <div class="text-left">
            <div id="loaderopen" class="text-center">
                <img src="{{url('/images/app/rolling.gif')}}">
            </div>

            <div class="col-lg-10 col-lg-offset-1">
                <table class="table table-hover" id="pskutable" cellspacing="0" width="100%">
                    <thead>
                        <th width="5%"></th>
                        <th >Parent SKU</th>
                        <th >Supplier</th>
                    </thead>
                    <tfoot>
                        <th colspan="3" rowspan="1"><a href="{{url('/settings/parents/create')}}" data-toggle='tooltip' data-placement="top" title="Add Parent SKU"><i class="fa fa-plus"></i> Add Parent SKU</a></th>

                    </tfoot>
                
                    <tbody class="hidden">
                        @foreach($parentskus as $psku)
                            <tr>
                                <td style="text-align: center; vertical-align: middle;"><a href="{{url('/settings/parents/'.$psku->id.'/edit') }}" data-toggle="tooltip" data-placement="top" title="Click to modify {{$psku->parentcode}}."><img src="{{url('/images/app/details_edit.png')}}"></a></td>
                                <td>{{ $psku->parentcode }}</td>
                                <td>{{ $psku->suppcode }}</td>
                            </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>


        </div>

        

    </div>
@stop


@section('userdefjs')
    <script>
   
        $(document).ready(function() {

            $(".navmenuitemlist li.dropdown").removeClass('active').eq(3).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(12).addClass('active');
            
            var table = $('#pskutable')
                .on( 'init.dt', function () {
                    $("#pskutable tbody").removeClass('hidden');
                    $("#loaderopen").addClass('hidden');
                })
                .DataTable({
                "columns": [
                    {
                        "className":'edit-control',
                        "orderable":      false,
                        "searchable": false
                        
                    },
                    { "data": "parentcode" },
                    { "data": "suppcode" },
                    ],
                    select: 'single',
                    "iDisplayLength": 10,
                    "order": [[1, 'asc']]
            });

            $("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');

        }); //End Document Ready

        
    </script>
@stop