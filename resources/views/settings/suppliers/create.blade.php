@extends("layout")

@section("content")
    
    <h2 class="thin">Add New Supplier</h2>
    <p class="text-muted">This is the way to add a new Supplier. </p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/settings/suppliers')}}">Suppliers</a></li>
        <li class="active">Add New</li>
    </ol>
    <hr>
	
			<form method="POST" action="{{url('/settings/suppliers/create/new')}}" class="text-left" enctype="multipart/form-data">
				{{ csrf_field() }}
                
                
           
				<div class="panel panel-primary">
					<div class="panel-heading">
						General Information
						<small><em>will contain the required information of this Supplier.</em></small>
					</div>
					
					<div class="panel-body">

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Supplier Code
                                    	<span class="text-success"><small><em>
                                    		assign this supplier a code.
                                    	</em></small></span>
                                    </label>
                                    <input type="text" class="form-control input-sm" name="suppcode" autofocus="">
                                    
                                </div>
                            </div>

                        </div>
						
					</div> <!-- ./Panel-body -->
						
				</div> <!-- ./Panel -->
                
				<div>
					<div class="row">
						<div class="col-md-4 pull-right">
				        	<button class="btn btn-primary btn-block paddtop paddbot" type="submit">Save</button>
				        </div>
                        <div class="col-md-4 pull-right">
                            <a class="btn btn-default btn-block" href="{{url('/settings/suppliers')}}">Cancel</a>
                        </div>
					</div>
				</div>
			</form>

@stop

@section("userdefjs")
	<script>
		$(function(){

            $(".navmenuitemlist li.dropdown").removeClass('active').eq(3).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(11).addClass('active');
        
		});
	</script>
@stop