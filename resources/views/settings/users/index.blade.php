@extends('layout')

@section('content')
    
        <h2 class="thin">User List</h3>
        <p class="muted">The list of all Users in the system.</p>
        
        <ol class="breadcrumb text-left">
            <li class="active">Users</li>
        </ol>

        <hr>

        <div class="text-left">
            <div id="loaderopen" class="text-center">
                <img src="{{url('/images/app/rolling.gif')}}">
            </div>
            <table class="table table-hover" id="userstable" cellspacing="0" width="100%">
                <thead>
                    <th width="7%"></th>
                    <th >Fullname</th>
                    <th >Email Address</th>
                    <th >Access Level</th>
                </thead>
                <tfoot>
                    <th colspan="4" rowspan="1"><a href="{{url('/settings/users/create')}}" data-toggle='tooltip' data-placement="top" title="Add New User"><i class="fa fa-plus"></i> Add User</a></th>

                </tfoot>
            
                <tbody class="hidden">
                    @foreach($users as $user)
                        <tr>
                            <td style="text-align: center; vertical-align: middle;"><a href="{{url('/settings/users/'. $user->id.'/edit') }}" data-toggle="tooltip" data-placement="top" title="Click to modify {{$user->name}}."><img src="{{url('/images/app/details_edit.png')}}"></a></td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ ($user->accesslevel == 1 ? "Admin" : ($user->accesslevel == 2 ? "Parts" : ($user->accesslevel == 3 ? "Purchasing" : ($user->accesslevel == 4 ? "CS" : ($user->accesslevel == 99 ? "Super" : "Undefined"))))) }}</td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>


        </div>

        

    </div>
@stop


@section('userdefjs')
    <script>
   
        $(document).ready(function() {
            $(".navmenuitemlist li.dropdown").removeClass('active').eq(3).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(9).addClass('active');
            
            var table = $('#userstable')
                .on( 'init.dt', function () {
                    $("#userstable tbody").removeClass('hidden');
                    $("#loaderopen").addClass('hidden');
                })
                .DataTable({
                "columns": [
                    {
                        "className":'edit-control',
                        "orderable":      false,
                        "searchable": false
                        
                    },
                    { "data": "name" },
                    { "data": "email" },
                    { "data": "accesslevel" }
                    ],
                    select: 'single',
                    "order": [[1, 'asc']]
            });

            $("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');

        }); //End Document Ready

        
    </script>
@stop