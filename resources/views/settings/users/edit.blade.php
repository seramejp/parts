@extends("layout")

@section("content")

    <h2 class="thin">Modify User</h2>
    <p class="text-muted">This is the way to modify a User. Make sure that the entries are correct. <br>
        Password Resets can be achieved using the Forgot Password link at login and will be sent to your email address.
    </p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/settings/users')}}">Users</a></li>
        <li class="active">Edit</li>
    </ol>
    <hr>

    <form method="POST" action="{{url('/settings/users/modify/'.$user->id) }}" class="text-left" enctype="multipart/form-data" id="userform">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}

            <div class="row">
                <div class="col-lg-6">
                    <h3>{{ $user->name }}</h3>
                    <h5><small><em>{{ ($user->accesslevel == "1" ? "Admin" : ($user->accesslevel == "2") ? "Parts" : ($user->accesslevel == "3" ? "Purchasing" : ($user->accesslevel == "4" ? "CS" : ($user->accesslevel == "99" ? "Super" : "Undefined")))) }}</em></small></h5>
                </div>
            </div>
            <hr>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    General Information
                    <small><em>will contain the basic information of this Back Order.</em></small>
                </div>
                
                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Date Created
                                    <span class="text-success"><small><em>
                                        DD-MM-YYYY format, today.
                                    </em></small></span>
                                </label>
                                <input type="text" class="form-control input-sm" name="created_at" readonly="" value="{{ date('d-m-Y', strtotime($user->created_at)) }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Name
                                    <span class="text-success"><small><em>
                                        fullname of this user
                                    </em></small></span>

                                </label>
                                <input type="text" class="form-control input-sm" name="name" autofocus="" value="{{$user->name}}">
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Email Address
                                    <span class="text-success"><small><em>
                                        will be used to login.
                                    </em></small></span>
                                </label>
                                <input type="email"  class="form-control input-sm" name="email" value="{{ $user->email }}">
                            </div>
                        </div>                        

                    </div>


                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Change Password
                                    <span class="text-success"><small><em>
                                        min of 6 characters
                                    </em></small></span>
                                </label>
                                <input type="password"  pattern=".{6,}" class="form-control input-sm" name="newpassword" id="newpassword" >
                            </div>
                        </div>

                        <div class="col-lg-4 hidden" id="confirmpw">
                            <div class="form-group">
                                <label for="">Confirm Password
                                    <span class="text-success" id="passwspan"><small><em>
                                        passwords should match
                                    </em></small></span>
                                </label>
                                <input type="password" pattern=".{6,}"  class="form-control input-sm" name="confirmpassword" id="confirmpassword">
                            </div>
                        </div>
                    </div>

                     <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Access Level
                                    <span class="text-success"><small><em>
                                        the level of access of this user.
                                    </em></small></span>
                                </label>
                                <select name="accesslevel" class="form-control input-sm">
                                    <option value="2" {{ ($user->accesslevel == "2" ? "selected" : "") }}>Parts</option>
                                    <option value="3" {{ ($user->accesslevel == "3" ? "selected" : "") }}>Purchasing</option>
                                    <option value="4" {{ ($user->accesslevel == "4" ? "selected" : "") }}>CS</option>
                                    <option value="1" {{ ($user->accesslevel == "1" ? "selected" : "") }}>Admin</option>
                                    <option value="99" {{ ($user->accesslevel == "99" ? "selected" : "") }}>Super</option>;
                                </select>
                            </div>
                        </div>

                        
                    </div>
                    
                </div> <!-- ./Panel-body -->
                    
            </div> <!-- ./Panel -->

            <div>
                <div class="row">
                    <div class="col-md-4 pull-right">
                        <button class="btn btn-primary btn-block" type="submit" id="submitUserForm">Save</button>
                    </div>
                    <div class="col-md-4 pull-right">
                        <a class="btn btn-default btn-block" href="{{url('/settings/users')}}">Cancel</a>
                    </div>
                </div>
            </div>
    </form>
@stop


@section('userdefjs')
    <script>
        
        $(function(){

            $(".navmenuitemlist li.dropdown").removeClass('active').eq(3).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(9).addClass('active');

            $("#userform").on('change', '#newpassword', function(event) {
                event.preventDefault();
                //alert("changed");

                newphandler = $(this);

                if (newphandler.val().length > 0) {
                    $("#confirmpw").removeClass('hidden');
                }else{
                    $("#confirmpw").addClass('hidden');
                }
            });


            $("#userform").on('click', '#submitUserForm', function(event) {
                event.preventDefault();
                /* Act on the event */

                newp = $("#newpassword").val();
                conf = $("#confirmpassword").val();

                if(newp === conf){
                    $("#passwspan").removeClass('text-danger').addClass('text-success');
                    $("#userform").submit();
                }else{
                    $("#passwspan").removeClass('text-success').addClass('text-danger');
                }
            });

        });
    </script>
@stop