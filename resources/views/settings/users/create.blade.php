@extends("layout")

@section("content")
    
    <h2 class="thin">Add New User</h2>
    <p class="text-muted">This is the way to add a new User. </p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/settings/users')}}">Users</a></li>
        <li class="active">Adding New</li>
    </ol>
    <hr>
	
			<form method="POST" action="{{url('/settings/users/create/new')}}" class="text-left" enctype="multipart/form-data">
				{{ csrf_field() }}
                
                
           
				<div class="panel panel-primary">
					<div class="panel-heading">
						General Information
						<small><em>will contain the required information of this User.</em></small>
					</div>
					
					<div class="panel-body">

						<div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Date Created
                                    	<span class="text-success"><small><em>
                                    		DD-MM-YYYY format
                                    	</em></small></span>
                                	</label>
                                    <input type="text" class="form-control input-sm" name="created_at" readonly="" value="{{ Carbon\Carbon::now()->format("d-m-Y") }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Name
                                    	<span class="text-success"><small><em>
                                    		user's full name
                                    	</em></small></span>
                                    </label>
                                    <input type="text" class="form-control input-sm" name="name" autofocus="" required="">
                                    
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Email Address
                                        <span class="text-success"><small><em>
                                            will be used to login.
                                        </em></small></span>
                                    </label>
                                    <input type="email" class="form-control input-sm" name="email" required="">
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Password
                                        <span class="text-success"><small><em>
                                            minimum of 6 characters.
                                        </em></small></span>
                                    </label>
                                    <input type="password" pattern=".{6,}" class="form-control input-sm" name="password" required="">
                                </div>
                            </div>

                        </div>

                         <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Access Level
                                    <span class="text-success"><small><em>
                                        the level of access of this user.
                                    </em></small></span>
                                </label>
                                <select name="accesslevel" class="form-control input-sm">
                                    <option value="1">Admin</option>
                                    <option value="2">Parts</option>
                                    <option value="3">Purchasing</option>
                                    <option value="4">CS</option>
                                </select>
                            </div>
                        </div>

                        
                    </div>
						
					</div> <!-- ./Panel-body -->
						
				</div> <!-- ./Panel -->
                
				<div>
					<div class="row">
						<div class="col-md-4 pull-right">
				        	<button class="btn btn-primary btn-block paddtop paddbot" type="submit">Save</button>
				        </div>
                        <div class="col-md-4 pull-right">
                            <a class="btn btn-default btn-block" href="{{url('/settings/users')}}">Cancel</a>
                        </div>
					</div>
				</div>
			</form>

@stop

@section("userdefjs")
	<script>
 		

		$(function(){
			
			

          $(".navmenuitemlist li.dropdown").removeClass('active').eq(3).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(9).addClass('active');
        
		});
	</script>
@stop