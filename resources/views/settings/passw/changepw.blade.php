@extends("layout")

@section("content")

    <h2 class="thin">Change Password</h2>
    <p class="text-muted">I wanted to change my password!<br>
        "Passwords should be at least 6 characters long." - Anonymous <br>
    </p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/settings/users')}}">Users</a></li>
        <li><a href="{{url('/home')}}">Parts</a></li>
    </ol>
    <hr>

    <form method="POST" action="{{url('/settings/users/changecredentials/'.$userid) }}" class="text-left" enctype="multipart/form-data" id="userform">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}

            <hr>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Change Password
                    <small><em>nothing to see here!</em></small>
                </div>
                
                <div class="panel-body">


                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Change Password
                                    <span class="text-success"><small><em>
                                        min of 6 characters
                                    </em></small></span>
                                </label>
                                <input type="password"  pattern=".{6,}" class="form-control input-sm" name="newpassword" id="newpassword" >
                            </div>
                        </div>

                        <div class="col-lg-4" id="confirmpw">
                            <div class="form-group">
                                <label for="">Confirm Password
                                    <span class="text-success" id="passwspan"><small><em>
                                        passwords should match
                                    </em></small></span>
                                </label>
                                <input type="password" pattern=".{6,}"  class="form-control input-sm" name="confirmpassword" id="confirmpassword">
                            </div>
                        </div>
                    </div>

                </div> <!-- ./Panel-body -->
                    
            </div> <!-- ./Panel -->

            <div>
                <div class="row">
                    <div class="col-md-4 pull-right">
                        <button class="btn btn-primary btn-block" type="submit" id="submitUserForm">Save</button>
                    </div>
                    <div class="col-md-4 pull-right">
                        <a class="btn btn-default btn-block" href="{{url('/settings/users')}}">Cancel</a>
                    </div>
                </div>
            </div>
    </form>
@stop


@section('userdefjs')
    <script>
        
        $(function(){

            $(".navmenuitemlist li.dropdown").removeClass('active').eq(4).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(12).addClass('active');

            $("#userform").on('change', '#newpassword', function(event) {
                event.preventDefault();
                //alert("changed");

                newphandler = $(this);

                if (newphandler.val().length > 0) {
                    $("#confirmpw").removeClass('hidden');
                }else{
                    $("#confirmpw").addClass('hidden');
                }
            });


            $("#userform").on('click', '#submitUserForm', function(event) {
                event.preventDefault();
                /* Act on the event */

                newp = $("#newpassword").val();
                conf = $("#confirmpassword").val();

                if(newp === conf){
                    $("#passwspan").removeClass('text-danger').addClass('text-success');
                    $("#userform").submit();
                }else{
                    $("#passwspan").removeClass('text-success').addClass('text-danger');
                }
            });

        });
    </script>
@stop