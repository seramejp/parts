@extends('layout')

@section('content')
    <h2 class="thin">Modify Open Box</h2>
    <p class="muted">If you need to modify this open box, do as you will.</p>
    
    <ol class="breadcrumb text-left">
        <li><a href=" {{ url('/openboxes') }}">Openboxes</a></li>
        <li class="active">Edit</li>
    </ol>

    <hr>
    <form id="opbFormEdit" method="POST" action="{{ url('/openboxes/modify/'.$opb->id) }}" class="text-left" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-6">
                <h3>{{ $opb->partsku }}</h3>
                <h5><small><em>{{ $opb->descr or "This text is supposed to be a description." }}</em></small></h5>
            </div>
            <div class="col-lg-6 text-right">
                <label for="photo" style="cursor: pointer;">
                    <img src="{{ "data:image/jpeg;base64," . base64_encode(Storage::get('public/' . $opb->photo)) }}" alt="{{$opb->partsku }}" class='img-thumbnail' height='200px' width='200px' id="photoimg">
                </label>
                <input type="file" class="form-control input-sm hidden" id="photo" name="photo" accept="image/*" value=""/>
            </div>
        </div>
        <h6><small><em>Last Modified by: </em>{{ $opb->lasttouch->name or "Somebody that I used to know" }}</small></h6>
        <hr>
        <div class="panel panel-primary">
            <div class="panel-heading">
                General Information
                <small><em>will contain the basic information of this Openbox.</em></small>
            </div>
            
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Date Created
                                <span class="text-success"><small><em>
                                    DD/MM/YYYY format
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="created_at" readonly="" value="{{ date('d/m/Y', strtotime($opb->created_at)) }}">
                        </div>
                    </div>

                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="form-group">
                            <label for="">Reference Number
                                <span class="text-success"><small><em>
                                    automatically filled in.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="referencenum" autofocus="" value="{{$opb->referencenum}}">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Parent
                                <span class="text-success"><small><em>
                                    select from Parent SKU
                                </em></small></span>

                            </label>
                            <select name="parentcode" id="parentcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true">
                                @foreach($parents as $parent)
                                    <option value="{{$parent->parentcode}}" {{ ($opb->parentcode == $parent->parentcode ? "selected" : "") }}  data-suppcode="{{$parent->suppcode}}">{{$parent->parentcode}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Supplier
                                <span class="text-success"><small><em>
                                    filled in automatically
                                </em></small></span>
                            </label>
                            <select id="suppcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true" disabled>
                                @foreach($suppliers as $supp)
                                    <option value="{{$supp->suppcode}}" {{ ($opb->suppcode == $supp->suppcode ? "selected" : "") }}>{{$supp->suppcode}}</option>
                                @endforeach
                            </select>

                            <input type="text" name="suppcode" id="suppcodehidden" value="{{$opb->suppcode}}" class="hidden">
                        </div>
                    </div>


                    

                </div>

                 <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Removed Part <span class="text-danger"><small><em>required.</em></small></span>
                                <span class="text-success"><small><em>
                                    name the Part SKU removed.
                                </em></small></span>

                            </label>
                            <input type="text" class="form-control input-sm" name="partsku" value="{{$opb->partsku}}" required>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Description
                                <span class="text-success"><small><em>
                                    a little description does help.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="descr" value="{{$opb->descr}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">RMA
                                <span class="text-success"><small><em>
                                    or Kayako Ticket ID.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="kayakoid" value="{{$opb->kayakoid}}">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Fault
                                <span class="text-success"><small><em>
                                    reasons why this needs ordered.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="fault" value="{{$opb->fault}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">SP Order ID
                                <span class="text-success"><small><em>
                                    Sharepoint ID.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="spid" value="{{$opb->spid}}">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Location
                                <span class="text-success"><small><em>
                                    Where is waldo?
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="whloc" value="{{$opb->whloc}}">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Quantity <span class="text-danger"><small><em>required.</em></small></span></label>
                            <input type="number" class="form-control input-sm" name="qty" value="{{$opb->qty or 0}}" required="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Status <span class="text-success"><small><em>of this openbox.</em></small></span></label>
                            <select name="sys_orderstatus" class="form-control input-sm">
                                 <option value="New" {{ strcasecmp($opb->sys_orderstatus, "New" ) == 0 ? "selected='true'" : "" }}>New </option>
                                 <option value="Requested" {{ strcasecmp($opb->sys_orderstatus, "Requested" ) == 0 ? "selected='true'" : "" }}>Requested</option>
                                 <option value="Waiting" {{ strcasecmp($opb->sys_orderstatus, "Waiting" ) == 0 ? "selected='true'" : "" }}>Waiting</option>
                                 <option value="Waiting, Order Created" {{ strcasecmp($opb->sys_orderstatus, "Waiting, Order Created" ) == 0 ? "selected='true'" : "" }}>Waiting, Order Created</option> ,
                                 <option value="To Pilfer Parts" {{ strcasecmp($opb->sys_orderstatus, "To Pilfer Parts" ) == 0 ? "selected='true'" : "" }}>To Pilfer Parts</option>
                                 <option value="Replaced" {{ strcasecmp($opb->sys_orderstatus, "Replaced" ) == 0 ? "selected='true'" : "" }}>Replaced</option>
                                 <option value="Completed BTS" {{ strcasecmp($opb->sys_orderstatus, "Completed BTS" ) == 0 ? "selected='true'" : "" }}>Completed BTS</option>
                                 <option value="Completed ADC" {{ strcasecmp($opb->sys_orderstatus, "Completed ADC" ) == 0 ? "selected='true'" : "" }}>Completed ADC</option>
                                 <option value="Cancelled" {{ strcasecmp($opb->sys_orderstatus, "Cancelled" ) == 0 ? "selected='true'" : "" }}>Cancelled</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Order Status
                                <span class="text-success"><small><em>
                                    Openbox's current order status
                                </em></small></span>
                            </label>
                                @if($opb->sys_finalstatus == "Pending with Daisy" or $opb->sys_finalstatus == "Pending with Purchasing" or $opb->sys_finalstatus == "Confirmed with Purchasing" or $opb->sys_finalstatus == "Confirmed with Daisy" or $opb->sys_finalstatus == "Confirmed with Purchasing" or $opb->sys_finalstatus == "In Transit" or $opb->sys_finalstatus == "Received")

                                        <select name="sys_finalstatus" class="form-control input-sm">
                                            <option value="{{ $opb->sys_finalstatus }}">{{ $opb->sys_finalstatus }}</option>
                                        </select>
                                @else

                                    <select name="sys_finalstatus" class="form-control input-sm" >
                                        @if($opb->sys_finalstatus == "Unresolved" or $opb->sys_finalstatus == "Cancelled")
                                                <option value="Pending" {{ $opb->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Cancelled" {{ strcasecmp($opb->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($opb->sys_finalstatus,"Unresolved") == 0) || empty($opb->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @elseif($opb->sys_finalstatus == "Pending")
                                                <option value="Pending" {{ $opb->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Cancelled" {{ strcasecmp($opb->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($opb->sys_finalstatus,"Unresolved") == 0) || empty($opb->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @else
                                                <option value="Pending" {{ $opb->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Pending with Purchasing" {{ strcasecmp($opb->sys_finalstatus,"Pending with Purchasing")==0 ? "selected='true'" : ""  }}>Pending with Purchasing</option>
                                                <option value="Pending with Daisy" {{ strcasecmp($opb->sys_finalstatus,"Pending with Daisy") == 0 ? "selected='true'" : ""  }}>Pending with Daisy</option>
                                                <option value="Confirmed with Purchasing" {{ strcasecmp($opb->sys_finalstatus,"Confirmed with Purchasing") == 0 ? "selected='true'" : ""  }}>Confirmed with Purchasing</option>
                                                <option value="Confirmed with Daisy" {{ strcasecmp($opb->sys_finalstatus,"Confirmed with Daisy") == 0 ? "selected='true'" : ""  }}>Confirmed with Daisy</option>
                                                <option value="In Transit" {{ strcasecmp($opb->sys_finalstatus,"In Transit") == 0 ? "selected='true'" : ""  }}>In Transit</option>
                                                <option value="Received" {{ strcasecmp($opb->sys_finalstatus,"Received") == 0 ? "selected='true'" : ""  }}>Received</option>
                                                <option value="Cancelled" {{ strcasecmp($opb->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($opb->sys_finalstatus,"Unresolved") == 0) || empty($opb->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @endif

                                    </select>
                                @endif
                        </div>
                    </div>
                </div>
                

                
            </div> <!-- ./Panel-body -->
                
        </div> <!-- ./Panel -->

        <div>
            <div class="row">
                <div class="col-md-4 pull-right">
                    <button class="btn btn-primary btn-block" type="submit">Save</button>
                </div>
                <div class="col-md-4 pull-right">
                    <a class="btn btn-default btn-block" href="{{ url('/openboxes') }}">Cancel</a>
                </div>
            </div>
        </div>
    </form>
@stop

@section('userdefjs')

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#photoimg').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function(){
            $(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(3).addClass('active');

            $("#photo").on('change', function() {
                //$("#photoimg").prop("src", $("#photo").val().substring(12));              
                readURL(this);
            });

            $("#opbFormEdit").on('change', '#parentcode', function(event) {
                event.preventDefault();
                var suppcode = $("#parentcode option:selected").data("suppcode");
                
                $("#suppcode.selectpicker").selectpicker('val', suppcode);
                $("#suppcode.selectpicker").selectpicker('refresh');

                $("#suppcodehidden").val(suppcode);
            });
        });
    </script>
@stop