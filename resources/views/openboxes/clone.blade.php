@extends('layout')

@section('content')
    <h2 class="thin">Cloning Open Box</h2>
    <p class="muted">Cloned Open boxes are Pending by default. If you need to modify this open box, do as you will.</p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/openboxes')}}">Openboxes</a></li>
        <li class="active">Clone</li>
    </ol>

    <hr>
    <form id="opbFormClone" method="POST" action="{{url('/openboxes/create/clone')}}" class="text-left" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-6">
                <h3>{{ $opb->partsku }}</h3>
                <h5><small><em>{{ $opb->descr or "This text is supposed to be a description." }}</em></small></h5>
            </div>
            <div class="col-lg-6 text-right">
                <label for="photo" style="cursor: pointer;">
                    <img src="{{ "data:image/jpeg;base64," . base64_encode(Storage::get('public/qmbyjpserame.png')) }}" alt="{{$opb->partsku }}" class='img-thumbnail' height='200px' width='200px' id="photoimg">
                </label>
                <input type="file" class="form-control input-sm hidden" id="photo" name="photo" accept="image/*" value=""/>
            </div>
        </div>
        
        <hr>
        <div class="panel panel-primary">
            <div class="panel-heading">
                General Information
                <small><em>will contain the basic information of this Openbox.</em></small>
            </div>
            
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Date Created
                                <span class="text-success"><small><em>
                                    DD/MM/YYYY format
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="sys_datecreated" readonly="" value="{{ Carbon\Carbon::now()->format("d/m/Y") }}">
                        </div>
                    </div>

                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="form-group">
                            <label for="">Reference Number
                                <span class="text-success"><small><em>
                                    automatically filled in.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="referencenum" autofocus="" value="{{$opb->referencenum}}">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Parent
                                <span class="text-success"><small><em>
                                    select from Parent SKU
                                </em></small></span>

                            </label>

                            <input type="text" name="parentcode" class="hidden" value="{{$opb->parentcode}}">
                            <select id="parentcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true" disabled>
                                    <option value="{{ $opb->parentcode}}" >{{ $opb->parentcode }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Supplier
                                <span class="text-success"><small><em>
                                    filled in automatically
                                </em></small></span>
                            </label>
                            
                            <select id="suppcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true" disabled>
                                @foreach($suppliers as $supp)
                                    <option value="{{$supp->suppcode}}" {{ ($opb->suppcode == $supp->suppcode ? "selected" : "") }}>{{$supp->suppcode}}</option>
                                @endforeach
                            </select>
                            <input type="text" name="suppcode" id="suppcodehidden" value="{{$opb->suppcode}}" class="hidden">
                        </div>
                    </div>


                    

                </div>

                 <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Removed Part <span class="text-danger"><small><em>required.</em></small></span>
                                <span class="text-success"><small><em>
                                    name the Part SKU removed.
                                </em></small></span>

                            </label>
                            <input type="text" class="form-control input-sm" name="partsku" value="{{$opb->partsku}}" required>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Description
                                <span class="text-success"><small><em>
                                    a little description does help.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="descr" value="{{$opb->descr}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">RMA
                                <span class="text-success"><small><em>
                                    or Kayako Ticket ID.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="kayakoid" value="{{$opb->kayakoid}}">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Fault
                                <span class="text-success"><small><em>
                                    reasons why this needs ordered.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="fault" value="{{$opb->fault}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">SP Order ID
                                <span class="text-success"><small><em>
                                    Sharepoint ID.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="spid" value="{{$opb->spid}}">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Location
                                <span class="text-success"><small><em>
                                    Where is waldo?
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="whloc" value="{{$opb->whloc}}">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Quantity <span class="text-danger"><small><em>required.</em></small></span></label>
                            <input type="number" class="form-control input-sm" name="qty" value="{{$opb->qty or 0}}" required="">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Status <span class="text-success"><small><em>of this cloned openbox.</em></small></span></label>
                            <select name="sys_orderstatus" class="form-control input-sm" >
                                <!-- <option value="new" {{-- $opb->sys_orderstatus == "new" ? "selected='true'" : ""  --}}>New</option>
                                <option value="in transit" {{-- strcasecmp($opb->sys_orderstatus,"in transit")==0 ? "selected='true'" : ""  --}}>In Transit</option>
                                <option value="dispatched" {{-- strcasecmp($opb->sys_orderstatus,"dispatched") == 0 ? "selected='true'" : ""  --}}>Dispatched</option>
                                <option value="received" {{-- strcasecmp($opb->sys_orderstatus,"received") == 0 ? "selected='true'" : ""  --}}>Received</option>
                                <option value="cancelled" {{-- strcasecmp($opb->sys_orderstatus,"cancelled") == 0 ? "selected='true'" : ""  --}}>Cancelled</option> -->

                                <option value="New" >New </option>
                                 <option value="Requested">Requested</option>
                                 <option value="Waiting" selected="true">Waiting</option>
                                 <option value="Waiting, Order Created">Waiting, Order Created</option> ,
                                 <option value="To Pilfer Parts" >To Pilfer Parts</option>
                                 <option value="Replaced" >Replaced</option>
                                 <option value="Completed BTS">Completed BTS</option>
                                 <option value="Completed ADC" >Completed ADC</option>
                                 <option value="Cancelled" >Cancelled</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4 pull-right">
                        <div class="form-group">
                            <label for="">Order Status
                                <span class="text-success"><small><em>
                                    Openbox's current order status
                                </em></small></span>
                            </label>
                            <select name="sys_finalstatus" class="form-control input-sm" >
                                 <option value="Pending">Pending</option>
                            </select>
                        </div>
                    </div>
                </div>
                
            </div> <!-- ./Panel-body -->
                
        </div> <!-- ./Panel -->

        <div>
            <div class="row">
                <div class="col-md-4 pull-right">
                    <button class="btn btn-primary btn-block" type="submit">Save</button>
                </div>
                <div class="col-md-4 pull-right">
                    <a class="btn btn-default btn-block" href="{{url('/openboxes')}}">Cancel</a>
                </div>
            </div>
        </div>
    </form>
@stop

@section('userdefjs')

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#photoimg').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function(){
            $(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(3).addClass('active');

            $("#photo").on('change', function() {
                //$("#photoimg").prop("src", $("#photo").val().substring(12));              
                readURL(this);
            });


            $("#opbFormClone").on('change', '#parentcode', function(event) {
                event.preventDefault();
                var suppcode = $("#parentcode option:selected").data("suppcode");
                
                $("#suppcode.selectpicker").selectpicker('val', suppcode);
                $("#suppcode.selectpicker").selectpicker('refresh');
                $("#suppcodehidden").val(suppcode);
            });
        });
    </script>
@stop