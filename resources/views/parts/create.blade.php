

@extends("layout")

@section("content")
  	<h2 class="thin">Create New Part</h2>
    <p class="text-muted">So, you wanted to create a new part manually huh.</p>

    <ol class="breadcrumb text-left">
		<li><a href="/parts/public">Parts</a></li>
		<li class="active">Create</li>
	</ol>

	<hr>

			<form id="partsFormCreate" method="POST" action="{{url('/partspms/create/new')}}" class="text-left" enctype="multipart/form-data">
				{{ csrf_field() }}

				<div class="row">
					<div class="col-lg-6">
						
					</div>
					<div class="col-lg-6 text-right">
						<label for="photo" style="cursor: pointer;">
							<img src="{{url('/images/qmbyjpserame.png')}}" alt="Photo" class='img-thumbnail' height='200px' width='200px' id="photoimg">
						</label>
						<input type="file" class="form-control input-sm hidden" id="photo" name="photo" accept="image/*" value=""/>
					</div>
				</div>
				
				<hr>
				<div class="row">
		          	<div class="col-lg-12">
			            <div class="checkbox">
			                <label>
			                  <input type="checkbox" value="1" name="forcedPending"><em>Force to be Pending</em>
			                </label>
			            </div>
		          	</div>
		        </div>
				<div class="panel panel-primary">
					<div class="panel-heading">
						General Information
						<small><em>will contain the basic information of this Part.</em></small>
					</div>
					
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="">Part SKU</label>
									<input name="partsku" type="text" class="form-control input-sm" value="P-" autofocus="" onfocus="this.value=this.value" />
								</div>
			                </div>

			                <div class="col-md-4">
								<div class="form-group">
									<label for="">Description</label>
									<input name="descr" type="text" class="form-control input-sm" value="" />
								</div>
			                </div>

			                <div class="col-md-4">
								<div class="form-group">
									<label for="">WISE ID</label>
									<input name="wiseitemid" type="text" class="form-control input-sm" value="" />
								</div>
			                </div>

						</div>

						<div class="row">

							<div class="col-md-4">
								<div class="form-group">
									<label for="">Parent SKU</label>
									<!-- <input name="xsell" type="text" class="form-control input-sm" value="" /> -->
									<select name="parentcode" id="parentcode" class="input-sm form-control selectpicker" data-size="8" data-live-search="true">
										@foreach($parents as $parent)
                                            <option value="{{$parent->parentcode}}"  data-suppcode="{{$parent->suppcode}}">{{$parent->parentcode}}</option>
                                        @endforeach;
									</select>
								</div>
							</div>
							
				
							<div class="col-md-4">
								<div class="form-group">
									<label for="">Old SKU</label>
									<input name="oldsku" type="text" class="form-control input-sm" value="" />
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label>Related SKUs</label>
									<input name="relatedsku" type="text" class="form-control input-sm" />
								</div>
							</div>

						</div>

						<div class="row">
							<div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Supplier
                                    	<span class="text-success"><small><em>
                                    		select supplier
                                    	</em></small></span>
                                    </label>
                                    <select name="suppcode" id="suppcode" class="input-sm form-control selectpicker" data-size="8" data-live-search="true">
										@foreach($suppliers as $supp)
											<option value="{{$supp->suppcode}}">{{$supp->suppcode}}</option>		
										@endforeach;
									</select>
                                </div>
                            </div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="">Warehouse Location</label>
									<input name="whloc" type="text" class="form-control input-sm" value="" />
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label for="">Bulk Location</label>
									<input name="bulkloc" type="text" class="form-control input-sm" value="" />
								</div>
							</div>

						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
			                        <label for="">Qty in Bulk Location</label>
			                        <input name="qtyinbulkloc" type="number" class="form-control input-sm" value="0" placeholder="0" />
			                    </div>
			                </div>

							<div class="col-md-4">
								<div class="form-group">
			                        <label for="">Barcode</label>
			                        <input name="barcode" type="text" class="form-control input-sm" value="" />
			                    </div>
			                </div>

			                <div class="col-lg-4">
				                <div class="form-group">
									<label>Status<span class="text-success"><small><em>
			                            part's current status
			                        </em></small></span>
									</label>
				                    <select name="sys_status" class="form-control input-sm" >
				                        <option value="new on order">New on Order</option>
				                    </select>
				                </div>
				            </div>
						</div>

						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label>Notes</label>
			                        <input name="notes" type="text" class="form-control input-sm" />
			                    </div>
			                </div>
						</div>

						

						
					</div> <!-- ./Panel-body -->
						
				</div> <!-- ./Panel -->


				<!--========================-->
				<!-- DIMENSIONS				 -->
				<!--========================-->
				<div class="panel panel-primary">
					<div class="panel-heading">
						Shipping Details
						<small><em>
							contains the dimensions of the Part.
						</em></small>
					</div>
					
					<div class="panel-body">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="">Length, m</label>
									<input name="partlen" type="number" step="any" class="form-control input-sm" value="0.0" />
								</div>
			                </div>

			                <div class="col-md-4">
								<div class="form-group">
									<label for="">Width, m</label>
									<input name="partwidth" type="number" step="any" class="form-control input-sm" value="0.0" />
								</div>
			                </div>

			                <div class="col-md-4">
								<div class="form-group">
									<label for="">Height, m</label>
									<input name="partht" type="number" step="any" class="form-control input-sm" value="0.0" />
								</div>
			                </div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="">Weight, kg</label>
									<input name="partwt" type="number" step="any" class="form-control input-sm" value="0.0" />
								</div>
			                </div>

			                <div class="col-md-4">
								<div class="form-group">
									<label for="">Shipping Method</label>
									<input name="shipmethod" type="text" class="form-control input-sm" value="" >
								</div>
			                </div>

			                <div class="col-md-4">
								<div class="form-group">
									<label for="">Part Price</label>
									<input name="price" type="number" step="any" class="form-control input-sm" value="0.00" >
								</div>
			                </div>

						</div>
						
					</div> <!-- ./Panel-body -->
						
				</div> <!-- ./Panel -->

				<!--========================-->
				<!-- PANEL FOR STOCKS -->
				<!--========================-->
				<div class="panel panel-primary">
					<div class="panel-heading">
						Stock Control
						<small><em>
							contains the current quantity of the Part in the inventory.
						</em></small>
					</div>
					
					<div class="panel-body">
						
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="">Stocks on Hand</label>
										<input name="soh" type="number" class="form-control input-sm"  value="0" placeholder="0" />
									</div>
			                    </div>

			                    <div class="col-md-4">
									<div class="form-group">
										<label for="">Re-Stock Qty</label>
										<input name="restockqty" type="number" class="form-control input-sm"  value="0" placeholder="0" />
									</div>
			                    </div>

			                    <div class="col-md-4">
									<div class="form-group">
										<label for="">Re-Order Qty</label>
										<input name="reorderqty" type="number" class="form-control input-sm"  value="0" placeholder="0" />
									</div>
			                    </div>
							</div>
						
						
							<div class="row">
								

								<div class="col-md-4">
									<div class="form-group">
										<label for="">Sales last 150 days</label>
										<input name="salespast150d" type="text" class="form-control input-sm"  value="0" placeholder="0" />
									</div>
								</div>

								<div class="col-lg-4">
		                            <div class="form-group">
		                                <label for="">Order Status
		                                    <span class="text-success"><small><em>
		                                        Part's current order status
		                                    </em></small></span>
		                                </label>
		                                <select name="sys_finalstatus" class="form-control input-sm" >
		                                     <option value="Unresolved" >Unresolved</option>
		                                </select>
		                            </div>
		                        </div>

							</div>
						
					</div> <!-- ./Panel-body -->
				</div> <!-- ./Panel -->


				<div>
					<div class="row">
						<div class="col-md-4 pull-right">
				        	<button class="btn btn-primary btn-block" type="submit">Save</button>
				        </div>

						<div class="col-md-4 pull-right">
							<a class="btn btn-default btn-block" href="{{url('/parts')}}">Cancel</a>
						</div>
					</div>
				</div>
			</form>

@stop


 @section('userdefjs')
 	<script>
 		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        reader.onload = function (e) {
		            $('#photoimg').attr('src', e.target.result);
		        }
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$(function(){
			$("#partsFormCreate").on('change', '#parentcode', function(event) {
                event.preventDefault();
                var suppcode = $("#parentcode option:selected").data("suppcode");
                
                $("#suppcode.selectpicker").selectpicker('val', suppcode);
                $("#suppcode.selectpicker").selectpicker('refresh');
            });

			$("#photo").on('change', function() {
				//$("#photoimg").prop("src", $("#photo").val().substring(12));				
				readURL(this);
			});
			
			$(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
			$(".navsubmenuitemlist li").removeClass('active').eq(0).addClass('active');

		});
	</script>
 @stop