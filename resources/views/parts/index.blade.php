@extends("layout")


@section('content')
	<h2 class="thin">Parts Inventory</h2>
	<p class="text-muted">This is the repository of all Parts that is in the database.</p>
	
	<ol class="breadcrumb text-left">
		<li class="active">Parts</li>
	</ol>

	<hr>

	<div class="text-left">
		<div id="loaderparts" class="text-center">
			<img src="{{url('/images/app/rolling.gif')}}">
		</div>
		<table class="text-left table table-hover table-index-list" id="partstable" cellspacing="0" width="100%">
			<thead>
				<th width="3%"></th>
				<th width="5%"></th>
				<th class="hidden">ID</th>
				<th width="25%">SKU</th>
				<th width="15%">Supplier</th>
				<th width="15%">Parent</th>
				<th class="hidden">Old SKU</th>
				<th class="hidden">Wh Loc</th>
				<th class="hidden">Neto Status</th>
				<th width="20%">Description</th>
				<th class="hidden">SOH</th>
				<th class="hidden">Barcode</th>
				<th class="hidden">Restockqty</th>
				<th class="hidden">Reorderqty</th>
			
				<th width="20%">Status</th>

				<th width="5%">Neto</th>
				<th width="5%">Magento</th>
				<th width="5%">WISE</th>
				<th>Order Status</th>
			</thead>
			<tfoot>
				<th colspan="19" rowspan="1"><a href="{{url("/partspms/create")}}" data-toggle='tooltip' data-placement="top" title="Create Another Part"><i class="fa fa-plus"></i> New Part</a></th>
			
			</tfoot>
			<meta name="csrf-token" content="{{ csrf_token() }}" />
			
			<tbody class="hidden">
			</tbody>
		</table>


	</div>

	


@stop

@section('userdefjs')
	<script>

		function getImage(obj){
			
			mydata = "";
			myModalData = "";
			$.ajax({
				url: '{{url("/partspms/getimage/")}}' + '/' + obj,
				type: 'GET',
				success: function(data){
					mydata  += "<a href='' data-toggle='modal' data-target='#myModal'><img src='data:image/jpeg;base64," + data + "' alt='Photo' class='img-thumbnail' width='100' height='100'></a>";

					myModalData  += "<img src='data:image/jpeg;base64," + data + "' alt='Photo' class='img-responsive'>";
					
					$(".appendhere").empty().append(mydata);
					$(".appendheremodal").empty().append(myModalData);

				}
			});
			

			return mydata;
		}

		function format(d){
		
			html = '<table class="table table-default" width="40%">'+
		        '<tr class="info">'+
		            '<td width="20%"><strong>' + d.partsku +'</strong></td>'+
		            '<td width="40%"><em>' + d.descr +'</em></td>'+
		            '<td width="10%"></td>'+
		            '<td width="30%"></td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Barcode:</td>'+
		            '<td>' + d.barcode +'</td>'+
		            '<td>Parent SKU:</td>'+
		            '<td>' + d.parentcode +'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Stocks on Hand:</td>'+
		            '<td>'+ d.soh +'</td>'+
		            '<td>Old SKU:</td>'+
		            '<td>'+ d.oldsku +'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Re-Stock Qty:</td>'+
		            '<td>'+ d.restockqty +'</td>'+
		            '<td>Warehouse Location:</td>'+
		            '<td>'+ d.whloc +'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Re-Order Qty:</td>'+
		            '<td>'+ d.reorderqty +'</td>'+
		             '<td>NETO Status:</td>'+
		            '<td>'+ (d.sys_isactive == "1" ? "Active" : "Inactive") +'</td>'+
		        '</tr>'+
		        '<tr>'+
		            '<td>Image Thumbnail:</td>'+
		            '<td class="appendhere"></td>'+
		             '<td></td>'+
		            '<td></td>'+
		        '</tr>'+
		    '</table>';

		    return html;

		}

		$(function(){
			var globalUrl = "{{url('/')}}";

			//set active menu item
			$(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
			$(".navsubmenuitemlist li").removeClass('active').eq(0).addClass('active');

			var table = $("#partstable")
				.on( 'init.dt', function () {
			        $("#partstable tbody").removeClass('hidden');
			        $("#loaderparts").addClass('hidden');
			    })
				.DataTable({
			        "ajax": '{{url("/json/partsjson.json")}}',
			        "columns": [
			        	{
			                "className":'details-control',
			                "orderable":      false,
			                "data":           null,
		                	"defaultContent": ""		            
			            },
			            {
			                "className":'edit-control',
			                "orderable":      false,
			                "searchable": false,
			                "data": null,
			                "render": function ( data, type, full, meta ) {
						      	return "<td style='text-align: center; vertical-align: middle;'><a href='{{url("/partspms/")}}/"+data.id+"/edit' data-toggle='tooltip' data-placement='top' title='Click to modify.'><img src='{{url("/images/app/details_edit.png")}}'></a></td>";
						    }
		                	
		            	}, 
						{ "data": "id" },
						{ "data": "partsku" },
						{ "data": "suppcode" },
						{ "data": "parentcode" },
						{ "data": "oldsku" },
						{ "data": "whloc" },
						{ "data": "sys_isactive" },
						{ "data": "descr" }, //9
						{ "data": "soh" },
						{ "data": "barcode" },
						{ "data": "restockqty" },
						{ "data": "reorderqty" },
						{ "data": "sys_status" }, //14
						{ "data": "inneto" },
						{ "data": "inmagento" },
						{ "data": "inwise" },
						{ "data": "sys_finalstatus" },
		        ],
		        "columnDefs": [
			            {
			                "targets": [ 2, 6, 7, 8, 10, 11, 12, 13 ],
			                "visible": false,
			                "searchable": false
			            },
			            {
			            	"targets": [8],
			            	"render": function ( data, type, full, meta ) {
					      		return (data == "1" ? 'Active' : 'Inactive');
						    }
			            },			           
			            
			            /*{
			            	"targets": [14],
			            	"render": function ( data, type, full, meta ) {
					      		return (data == "" ? "" : data.split(' ').map(w => w[0].toUpperCase() + w.substr(1).toLowerCase()).join(' ') );
						    }
			            },*/

			            {
			            	"targets": [15],
			            	"render": function ( data, type, full, meta ) {
					      		return (data == "1" ? "<input class='cneto' type='checkbox' checked>" : "<input  class='cneto' type='checkbox'>");
						    } 
			            },

			            {
			            	"targets": [16],
			            	"render": function ( data, type, full, meta ) {
					      		return (data == "1" ? "<input class='cmage' type='checkbox' checked>" : "<input  class='cmage' type='checkbox'>");
						    } 
			            },

			            {
			            	"targets": [17],
			            	"render": function ( data, type, full, meta ) {
					      		return (data == "1" ? "<input class='cwise' type='checkbox' checked>" : "<input  class='cwise' type='checkbox'>");
						    } 
			            },

		        	],
		        "dom": 'Bfrtip',
		        "buttons": [
			        {
			            extend: 'csv',
			            text: '<i class="fa fa-download"></i> Download CSV',
			            className: 'btn-primary',
			            title: 'Parts_Inventory_Export'
			        }
			    ],
	        	"select": 'single',
		        "deferRender": true
		    } );

			$('#partstable tbody').on('click', 'td.details-control', function () {
			    var tr = $(this).closest('tr');
			    var row = table.row( tr );
			 
			    if ( row.child.isShown() ) {
			        // This row is already open - close it
			        row.child.hide();
			        tr.removeClass('shown');
			    }
			    else {
			        if ( table.row( '.shown' ).length ) {
						$('.details-control', table.row( '.shown' ).node()).click();
					}

					row.child( format(row.data()) ).show();
					tr.addClass('shown');
					getImage(row.data().id);
					
			    }
			});

			$("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');
			$("th.details-control").removeClass('sorting_asc').addClass('sorting_disabled');

			$("#partstable").on('change', '.cneto', function(event) {
			
					event.preventDefault();
					/* Act on the event */

					var tr = $(this).closest('tr');
				    var row = table.row( tr );
					
					var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

					var checkeds =  $(this).is(':checked');
					var checked;
					if (checkeds) {
						checked=1;
					}else{
						checked=0;
					}
					//console.log(row.data().photo);
					$.post("{{url("/partspms/netoupdate/")}}" + "/" + row.data().id, 
						{
							_token: CSRF_TOKEN,
							_method: 'PATCH',
							checked: checked,
						},
						function(data){
							
					});
			});

			$("#partstable").on('change', '.cmage', function(event) {
					event.preventDefault();
					/* Act on the event */
					var tr = $(this).closest('tr');
				    var row = table.row( tr );
					
					var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

					var checkeds =  $(this).is(':checked');
					var checked;
					if (checkeds) {
						checked=1;
					}else{
						checked=0;
					}
					//console.log(row.data().id);
					$.post("{{url("/partspms/mageupdate/")}}" + "/" + row.data().id, 
						{
							_token: CSRF_TOKEN,
							_method: 'PATCH',
							checked: checked,
						},
						function(data){
							
					});
			});

			$("#partstable").on('change', '.cwise', function(event) {
					event.preventDefault();
					/* Act on the event */
					var tr = $(this).closest('tr');
				    var row = table.row( tr );
					
					var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

					var checkeds =  $(this).is(':checked');
					var checked;
					if (checkeds) {
						checked=1;
					}else{
						checked=0;
					}
					//console.log(row.data().id);
					$.post("{{url("/partspms/wiseupdate/")}}" + "/" + row.data().id, 
						{
							_token: CSRF_TOKEN,
							_method: 'PATCH',
							checked: checked,
						},
						function(data){
							
					});
			});

			$("#partstable td:nth-child(7)").css('background-color','aliceblue');
			$("#partstable_filter").addClass('pull-right');
		});
	</script>
@stop

