<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Parts Management System">
    
    <title>TEST v3.0</title>

    <link rel="shortcut icon" href="{{url('/images/app/favicon.ico')}}">
    
    <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <link rel="stylesheet" href="{{ url('/css/app.css')}}">
    <link rel="stylesheet" href="{{ url('/css/font-awesome.css')}}">

    <!-- Custom styles for our template -->
    
    <link rel="stylesheet" href="{{ url('/css/select.datatables.min.css')}}">
    <link rel="stylesheet" href="{{ url('/css/buttons.datatables.min.css')}}">
    

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ url('/css/bootstrap-select.min.css')}}">
    
    <!-- Latest compiled and minified CSS -->
    <!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.css">
    <link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable -->>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->

    <style>
        table.dataTable thead .sorting,table.dataTable thead .sorting_asc,table.dataTable thead .sorting_desc,table.dataTable thead .sorting_asc_disabled,table.dataTable thead .sorting_desc_disabled {
          background-repeat: no-repeat;
          background-position: center right;
        }

        table.dataTable thead .sorting {
          background-image: url("{{url('/')}}/images/app/sort_both.png");
        }

        table.dataTable thead .sorting_asc {
          background-image: url("{{url('/')}}/images/app/sort_asc.png");
        }

        table.dataTable thead .sorting_desc {
          background-image: url("{{url('/')}}/images/app/sort_desc.png");
        }

        table.dataTable thead .sorting_asc_disabled {
          background-image: url("{{url('/')}}/images/app/sort_asc_disabled.png");
        }

        table.dataTable thead .sorting_desc_disabled {
          background-image: url("{{url('/')}}/images/app/sort_desc_disabled.png");
        }

        td.details-control {
            background: url('{{url('/')}}/images/app/zoom-in.png') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('{{url('/')}}/images/app/zoom-out.png') no-repeat center center;
            
        }
    </style>
</head>

<body class="home">
    <!-- Fixed navbar -->
    <div class="navbar  navbar-inverse navbar-fixed-top  headroom" >
        <div class="container">
            <div class="navbar-header">
                <!-- Button for smallest screens -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="{{ url('/home')}}"><small>TESTER</small><br> tester tester</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav pull-right navmenuitemlist">
                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ url('/images/app/cardboard-box-24.png')}}" alt=""> Inventory Control <b class="caret"></b></a>
                        <ul class="dropdown-menu navsubmenuitemlist">
                            @if(Auth::user()->accesslevel == "99" || Auth::user()->accesslevel == "1" || Auth::user()->accesslevel == "2" )
                                <li class=""><a href="{{ url('/home')}}">Parts Inventory</a></li>
                                <li class=""><a href="{{ url('/specialorders')}}">Special Orders</a></li>
                                <li class=""><a href="{{ url('/backorders')}}">Back Orders</a></li>
                            @endif

                            @if(Auth::user()->accesslevel == "99" || Auth::user()->accesslevel == "1" || Auth::user()->accesslevel == "2" )
                                <li class=""><a href="{{ url('/openboxes')}}">Open Boxes</a></li>
                                <li class=""><a href="{{ url('/raps')}}">RAP</a></li>
                            @endif
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ url('/images/app/delivery-24.png')}}" alt=""> Orders <b class="caret"></b></a>
                        <ul class="dropdown-menu navsubmenuitemlist">
                            @if(Auth::user()->accesslevel == "99" || Auth::user()->accesslevel == "1" || Auth::user()->accesslevel == "2" )
                                <li class=""><a href="{{ url('/supplierorders')}}">Supplier Orders</a></li>
                            @endif

                                <li class=""><a href="{{ url('/existingorders') }}">Existing Orders</a></li>

                        </ul>
                    </li>
                    
                    @if(Auth::user()->accesslevel == "99" || Auth::user()->accesslevel == "1" || Auth::user()->accesslevel == "2" || Auth::user()->accesslevel == "3" )
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{url('/images/app/download-24.png')}}" alt=""> Porting <b class="caret"></b></a>
                            <ul class="dropdown-menu navsubmenuitemlist"> 
                                @if(Auth::user()->accesslevel == "99" || Auth::user()->accesslevel == "1" )
                                   <li class=""><a href="{{ url('/import') }}">Import</a></li>
                                @endif

                                @if(Auth::user()->accesslevel == "99" || Auth::user()->accesslevel == "1" || Auth::user()->accesslevel == "2" || Auth::user()->accesslevel == "3" )
                                    <li class=""><a href="{{ url('/export') }}">Export</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif

                    @if(Auth::user()->accesslevel == "99" || Auth::user()->accesslevel == "1" || Auth::user()->accesslevel == "2" || Auth::user()->accesslevel == "3" )
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{url('/images/app/presentation-20.png')}}" alt=""> Reports <b class="caret"></b></a>
                            <ul class="dropdown-menu navsubmenuitemlist">
                                <!-- <li class=""><a href="/parts/public/reports/goodsreceiving">Goods Receiving</a></li> -->
                                <li class=""><a href="{{url('/reports/fourteendayreport')}}">14d Report</a></li>
                            </ul>
                        </li>
                    @endif
                    
                    @if(Auth::user()->accesslevel == "99" || Auth::user()->accesslevel == "1" )
                         <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{url('/images/app/setting-cog-16.png')}}" alt=""> Settings <b class="caret"></b></a>
                            <ul class="dropdown-menu navsubmenuitemlist">
                                <li><a href="{{url('/settings/users')}}">Users</a></li>
                                <li role="separator" class="divider"></li>
                                <li class=""><a href="{{url('/settings/suppliers')}}">Suppliers</a></li>
                                <li class=""><a href="{{url('/settings/parents')}}">Parents</a></li>
                                <li role="separator" class="divider"></li>
                                <li class=""><a href="{{url('/settings/archive/opb')}}">Archive Openboxes</a></li>
                                <li class=""><a href="{{url('/settings/archive/rap')}}">Archive RAPs</a></li>
                            </ul>
                        </li>
                    @endif



                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li class=""><a href="{{url('/settings/user/pw')}}">Change Password</a></li>
                            <li>
                                <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div> 
    <!-- /.navbar -->

    
    <!-- Intro -->
    <div class="container text-center">
        <br> <br> <br><br><br>

        @yield('content')
        
        <div class="container">
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body appendheremodal text-center">
                            
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    


    <footer id="footer" class="top-space">

        <div class="footer1">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-3 widget">
                        <h3 class="widget-title">Contact</h3>
                        <div class="widget-body">
                            <p><i class="fa fa-mobile"></i> +63 921 774 4221<br>
                                <i class="fa fa-user"></i> Joseph Jingco<br>
                                <i class="fa fa-skype"></i> millstrading.joseph.j@gmail.com
                                <br><br>
                                <i class="fa fa-mobile"></i> +61 438 670 981 <br>
                                <i class="fa fa-user"></i> Felicity Smith<br>
                                <i class="fa fa-skype"></i> millstrading.felicity@gmail.com<br>
                                <!-- <br><br>
                                <i class="fa fa-user"></i> JP Serame<br>
                                <i class="fa fa-skype"></i> millstrading.johnpaul@gmail.com<br> -->
                                
                            </p>    
                        </div>
                    </div>


                    <div class="col-md-6 widget">
                        <h3 class="widget-title">Parts Management System</h3>
                        <div class="widget-body">
                            <p is an inventory system that calculates the bla, the bla and the bla.</p>
                            <p>Eius consequatur nihil quibusdam! Laborum, rerum, quis, inventore ipsa autem repellat provident assumenda labore soluta minima alias temporibus facere distinctio quas adipisci nam sunt explicabo officia tenetur at ea quos doloribus dolorum voluptate reprehenderit architecto sint libero illo et hic.</p>
                        </div>
                    </div>

                </div> <!-- /row of widgets -->
            </div>
        </div>

        <div class="footer2">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-6 widget">
                        <div class="widget-body">
                            @if(Auth::user()->accesslevel == "99" || Auth::user()->accesslevel == "1" || Auth::user()->accesslevel == "2" )
                                <p class="simplenav">
                                    <a href="{{url('/settings/archive/opb')}}">Archive Openboxes</a> | 
                                    <a href="{{url('/settings/archive/rap')}}">Archive RAPs</a>
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6 widget">
                        <div class="widget-body">
                            <p class="text-right">
                                Copyright &copy; 2016, MIT
                            </p>
                        </div>
                    </div>

                </div> <!-- /row of widgets -->
            </div>
        </div>

    </footer>   
        




    <!-- JavaScript libs are placed at the end of the document so the pages load faster -->
    <script src="{{url('/js/jQuery.js')}}"></script>
    <script src="{{url('/js/bootstrap.js')}}"></script>

    <script src="{{url('/js/headroom.min.js')}}"></script>
    <script src="{{url('/js/jQuery.headroom.min.js')}}"></script>
    <script src="{{url('/js/template.js')}}"></script>
    <script src="{{url('/js/datatables.min.js')}}"></script>
    <script src="{{url('/js/buttons.dataTables.js')}}"></script>
    <script src="{{url('/js/select.dataTables.js')}}"></script> 
    <script src="{{url('/js/userdefined.js')}}"></script> 

    <!-- Latest compiled and minified JavaScript -->
    <script src="{{url('/js/bootstrap-select.min.js')}}"></script>
    <!-- <script src="/js/dataTables.altEditor.free.js"></script> -->
    
    <!-- Latest compiled and minified JavaScript -->
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.1/bootstrap-table.min.js"></script>
    -->
    
    @yield('userdefjs')

    <script>
        $(function(){
             $('[data-toggle="tooltip"]').tooltip();
        });
    </script>


</body>
</html>