@extends("layout")

@section("content")
    <h2 class="thin">Create RMA Awaiting Parts</h2>
    <p class="text-muted">This is the way to create a RAP. Make sure that entries are correct.</p>

    <ol class="breadcrumb text-left">
        <li><a href="{{url('/raps')}}">RMA Awaiting Parts</a></li>
        <li class="active">Create</li>
    </ol>
    <hr>
    
            <form id="rapFormCreate" method="POST" action="{{url('/raps/create/new')}}" class="text-left" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-6">
                        
                    </div>
                    <div class="col-lg-6 text-right">
                        <label for="photo" style="cursor: pointer;">
                            <img src="{{url('/images/qmbyjpserame.png')}}" alt="Photo" class='img-thumbnail' height='200px' width='200px' id="photoimg">
                        </label>
                        <input type="file" class="form-control input-sm hidden" id="photo" name="photo" accept="image/*" value=""/>
                    </div>
                </div>
                
                <hr>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        General Information
                        <small><em>will contain the basic information of this RAP.</em></small>
                    </div>
                    
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Date Created
                                        <span class="text-success"><small><em>
                                            DD/MM/YYYY format
                                        </em></small></span>
                                    </label>
                                    <input type="text" class="form-control input-sm" name="created_at" readonly="" value="{{ Carbon\Carbon::now()->format("d/m/Y") }}">
                                </div>
                            </div>

                            <div class="col-lg-4 col-lg-offset-4">
                                <div class="form-group">
                                    <label for="">RMA Number
                                        <span class="text-success"><small><em>
                                            
                                        </em></small></span>
                                    </label>
                                    <input type="text" class="form-control input-sm" name="rmanum" autofocus="" >
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Parent
                                        <span class="text-success"><small><em>
                                            select from Parent SKU
                                        </em></small></span>
    
                                    </label>
                                    <select name="parentcode" id="parentcode" class="input-sm form-control selectpicker" data-size="8" data-live-search="true">
                                        @foreach($parents as $parent)
                                            <option value="{{$parent->parentcode}}"  data-suppcode="{{$parent->suppcode}}">{{$parent->parentcode}}</option>
                                        @endforeach;
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Supplier
                                        <span class="text-success"><small><em>
                                            filled in automatically
                                        </em></small></span>
                                    </label>
                                    <select id="suppcode" class="input-sm form-control selectpicker" data-size="8" data-live-search="true" disabled>
                                        @foreach($suppliers as $supp)
                                            <option value="{{$supp->suppcode}}">{{$supp->suppcode}}</option>        
                                        @endforeach;
                                    </select>

                                    <input type="text" name="suppcode" id="suppcodehidden" value="0" class="hidden">
                                </div>
                            </div>


                            

                        </div>

                         <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Part SKU <span class="text-danger"><small><em>required.</em></small></span>
                                        <span class="text-success"><small><em>
                                            name the Part SKU.
                                        </em></small></span>
    
                                    </label>
                                    <input type="text" class="form-control input-sm" name="partsku" required>
                                </div>
                            </div>

                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label for="">Description
                                        <span class="text-success"><small><em>
                                            a little description does help.
                                        </em></small></span>
                                    </label>
                                    <input type="text" class="form-control input-sm" name="descr">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Kayako Ticket ID
                                        <span class="text-success"><small><em>
                                            
                                        </em></small></span>
                                    </label>
                                    <input type="text" class="form-control input-sm" name="kayakoid">
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label for="">Notes
                                        <span class="text-success"><small><em>
                                            some notes for us to ponder.
                                        </em></small></span>
                                    </label>
                                    <input type="text" class="form-control input-sm" name="notes">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">SP Order ID
                                        <span class="text-success"><small><em>
                                            Sharepoint ID.
                                        </em></small></span>
                                    </label>
                                    <input type="text" class="form-control input-sm" name="spid">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Location
                                        <span class="text-success"><small><em>
                                            Where is waldo?
                                        </em></small></span>
                                    </label>
                                    <input type="text" class="form-control input-sm" name="whloc">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Quantity <span class="text-danger"><small><em>required.</em></small></span></label>
                                    <input type="number" class="form-control input-sm" name="qty" value="1" required="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">Outcome <span class="text-success"><small><em>of this RAP.</em></small></span></label>
                                    <select name="outcome" class="form-control input-sm">
                                        <option value="return to customer">Return to Customer</option>
                                        <option value="shop">Shop</option>
                                        <option value="To Parts">To Parts</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="">RAP Status <span class="text-success"><small><em>of this RAP.</em></small></span></label>
                                    <select name="sys_orderstatus" class="form-control input-sm">
                                        <option value="Pending Parts">Pending Parts</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4 pull-right">
                                <div class="form-group">
                                    <label for="">Order Status
                                        <span class="text-success"><small><em>
                                            RAP's current order status
                                        </em></small></span>
                                    </label>
                                    <select name="sys_finalstatus" class="form-control input-sm" >
                                         <option value="Pending">Pending</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
    
                        
                    </div> <!-- ./Panel-body -->
                        
                </div> <!-- ./Panel -->

                <div>
                    <div class="row">
                        <div class="col-md-4 pull-right">
                            <button class="btn btn-primary btn-block" type="submit">Save</button>
                        </div>
                        <div class="col-md-4 pull-right">
                            <a class="btn btn-default btn-block" href="{{url('/raps') }}">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
@stop

@section("userdefjs")
    <script>
        function readURL(input, selector) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    selector.attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function(){
            $("#photo").on('change', function() {           
                readURL(this,$("#photoimg") );
            });


            $(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(4).addClass('active');
            

            $("#rapFormCreate").on('change', '#parentcode', function(event) {
                event.preventDefault();
                var suppcode = $("#parentcode option:selected").data("suppcode");
                
                $("#suppcode.selectpicker").selectpicker('val', suppcode);
                $("#suppcode.selectpicker").selectpicker('refresh');

                $("#suppcodehidden").val(suppcode);
            });

            
        });
    </script>
@stop