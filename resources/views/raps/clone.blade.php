@extends('layout')

@section('content')
    <h2 class="thin">Clone this RAP</h2>
    <p class="muted">If you need to clone this RAP, do as you will.</p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/raps')}}">RMA Awaiting Parts</a></li>
        <li class="active">Clone</li>
    </ol>

    <hr>
    <form id="rapFormClone" method="POST" action="{{url('/raps/create/clone')}}" class="text-left" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-6">
                <h3>{{ $rma->partsku }}</h3>
                <h5><small><em>{{ $rma->descr or "This text is supposed to be a description." }}</em></small></h5>
            </div>
            <div class="col-lg-6 text-right">
                <label for="photo" style="cursor: pointer;">
                    <img src="{{ "data:image/jpeg;base64," . base64_encode(Storage::get('public/' . $rma->photo)) }}" alt="{{$rma->partsku }}" class='img-thumbnail' height='200px' width='200px' id="photoimg">
                </label>
                <input type="file" class="form-control input-sm hidden" id="photo" name="photo" accept="image/*" value=""/>
            </div>
        </div>
        
        <hr>
        <div class="panel panel-primary">
            <div class="panel-heading">
                General Information
                <small><em>will contain the basic information of this RAP.</em></small>
            </div>
            
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Date Created
                                <span class="text-success"><small><em>
                                    DD/MM/YYYY format
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="created_at" readonly="" value="{{ Carbon\Carbon::now()->format("d/m/Y") }}">
                        </div>
                    </div>

                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="form-group">
                            <label for="">RMA Number
                                <span class="text-success"><small><em>
                                    
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="rmanum" autofocus="" value="{{$rma->rmanum}}">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Parent
                                <span class="text-success"><small><em>
                                    select from Parent SKU
                                </em></small></span>

                            </label>
                            <input type="text" name="parentcode" class="hidden" value="{{$rma->parentcode}}">
                            <select id="parentcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true" disabled>
                                     <option value="{{ $rma->parentcode}}" >{{ $rma->parentcode }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Supplier
                                <span class="text-success"><small><em>
                                    filled in automatically
                                </em></small></span>
                            </label>
                            <select id="suppcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true" disabled>
                                @foreach($suppliers as $supp)
                                    <option value="{{$supp->suppcode}}" {{ ($rma->suppcode == $supp->suppcode ? "selected" : "") }}>{{$supp->suppcode}}</option>
                                @endforeach
                            </select>

                            <input type="text" name="suppcode" id="suppcodehidden" value="{{$rma->suppcode}}" class="hidden">
                        </div>
                    </div>


                    

                </div>

                 <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Part SKU <span class="text-danger"><small><em>required.</em></small></span>
                                <span class="text-success"><small><em>
                                    name the Part SKU.
                                </em></small></span>

                            </label>
                            <input type="text" class="form-control input-sm" name="partsku" value="{{$rma->partsku}}" required>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Description
                                <span class="text-success"><small><em>
                                    a little description does help.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="descr" value="{{$rma->descr}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Kayako Ticket ID
                                <span class="text-success"><small><em>
                                    
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="kayakoid" value="{{$rma->kayakoid}}">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Notes
                                <span class="text-success"><small><em>
                                    make it beautiful.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="notes" value="{{$rma->notes}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">SP Order ID
                                <span class="text-success"><small><em>
                                    Sharepoint ID.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="spid" value="{{$rma->spid}}">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Location
                                <span class="text-success"><small><em>
                                    Where is waldo?
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="whloc" value="{{$rma->whloc}}">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Quantity <span class="text-danger"><small><em>required.</em></small></span></label>
                            <input type="number" class="form-control input-sm" name="qty" value="{{$rma->qty or 0}}" required="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Outcome <span class="text-success"><small><em>of this RAP.</em></small></span></label>
                            <select name="outcome" class="form-control input-sm">
                                <option value="return to customer"{{ strcasecmp($rma->outcome, "return to customer") == 0 ? "selected='true'" : ""  }}>Return to Customer</option>
                                <option value="shop"{{ strcasecmp($rma->outcome, "shop") == 0 ? "selected='true'" : ""  }}>Shop</option>
                                <option value="To Parts"{{ strcasecmp($rma->outcome, "To Parts") == 0 ? "selected='true'" : ""  }}>To Parts</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Status <span class="text-success"><small><em>of this openbox.</em></small></span></label>
                            <select name="sys_orderstatus" class="form-control input-sm">
                                <option value="new" {{ strcasecmp($rma->sys_orderstatus, "new") == 0 ? "selected='true'" : ""  }}>New</option>
                                <option value="pending parts" {{ strcasecmp($rma->sys_orderstatus, "pending parts") == 0 ? "selected='true'" : ""  }}>Pending Parts</option>
                                <option value="on order" {{ strcasecmp($rma->sys_orderstatus, "on order") == 0 ? "selected='true'" : ""  }}>On Order</option>
                                <option value="completed adc" {{ strcasecmp($rma->sys_orderstatus, "completed adc") == 0 ? "selected='true'" : ""  }}>Completed ADC</option>
                                <option value="completed customer" {{ strcasecmp($rma->sys_orderstatus, "completed customer") == 0 ? "selected='true'" : ""  }}>Completed Customer</option>
                                <option value="cancelled" {{ strcasecmp($rma->sys_orderstatus, "cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4 pull-right">
                        <div class="form-group">
                            <label for="">Order Status
                                <span class="text-success"><small><em>
                                    RAP's current order status
                                </em></small></span>
                            </label>
                            <select name="sys_finalstatus" class="form-control input-sm" >
                                 <option value="Pending">Pending</option>
                            </select>
                        </div>
                    </div>
                </div>
                

                
            </div> <!-- ./Panel-body -->
                
        </div> <!-- ./Panel -->

        <div>
            <div class="row">
                <div class="col-md-4 pull-right">
                    <button class="btn btn-primary btn-block" type="submit">Save</button>
                </div>
                <div class="col-md-4 pull-right">
                    <a class="btn btn-default btn-block" href="{{url('/raps')}}">Cancel</a>
                </div>
            </div>
        </div>
    </form>
@stop

@section('userdefjs')

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#photoimg').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function(){
            $(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(4).addClass('active');

            $("#photo").on('change', function() {
                //$("#photoimg").prop("src", $("#photo").val().substring(12));              
                readURL(this);
            });

            $("#rapFormClone").on('change', '#parentcode', function(event) {
                event.preventDefault();
                var suppcode = $("#parentcode option:selected").data("suppcode");
                
                $("#suppcode.selectpicker").selectpicker('val', suppcode);
                $("#suppcode.selectpicker").selectpicker('refresh');

                $("#suppcodehidden").val(suppcode);
            });
        });
    </script>
@stop