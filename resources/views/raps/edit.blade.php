@extends('layout')

@section('content')
    <h2 class="thin">Modify this RAP</h2>
    <p class="muted">If you need to modify this RAP, do as you will.</p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/raps')}}">RMA Awaiting Parts</a></li>
        <li class="active">Edit</li>
    </ol>
    
    <hr>
    <form id="rapFormEdit" method="POST" action="{{ url('/raps/modify/'.$rma->id) }}" class="text-left" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-6">
                <h3>{{ $rma->partsku }}</h3>
                <h5><small><em>{{ $rma->descr or "This text is supposed to be a description." }}</em></small></h5>
            </div>
            <div class="col-lg-6 text-right">
                <label for="photo" style="cursor: pointer;">
                    <img src="{{ "data:image/jpeg;base64," . base64_encode(Storage::get('public/' . $rma->photo)) }}" alt="{{$rma->partsku }}" class='img-thumbnail' height='200px' width='200px' id="photoimg">
                </label>
                <input type="file" class="form-control input-sm hidden" id="photo" name="photo" accept="image/*" value=""/>
            </div>
        </div>
        <h6><small><em>Last Modified by: </em>{{ $rma->lasttouch->name or "Somebody that I used to know" }}</small></h6>
        <hr>
        <div class="panel panel-primary">
            <div class="panel-heading">
                General Information
                <small><em>will contain the basic information of this RAP.</em></small>
            </div>
            
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Date Created
                                <span class="text-success"><small><em>
                                    DD/MM/YYYY format
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="created_at" readonly="" value="{{ date('d/m/Y',strtotime($rma->created_at)) }}">
                        </div>
                    </div>

                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="form-group">
                            <label for="">RMA Number
                                <span class="text-success"><small><em>
                                    
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="rmanum" autofocus="" value="{{$rma->rmanum}}">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Parent
                                <span class="text-success"><small><em>
                                    select from Parent SKU
                                </em></small></span>

                            </label>
                            <select name="parentcode" id="parentcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true">
                                @foreach($parents as $parent)
                                    <option value="{{$parent->parentcode}}" {{ ($rma->parentcode == $parent->parentcode ? "selected" : "") }}  data-suppcode="{{$parent->suppcode}}">{{$parent->parentcode}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Supplier
                                <span class="text-success"><small><em>
                                    filled in automatically
                                </em></small></span>
                            </label>
                            <select id="suppcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true" disabled>
                                @foreach($suppliers as $supp)
                                    <option value="{{$supp->suppcode}}" {{ ($rma->suppcode == $supp->suppcode ? "selected" : "") }}>{{$supp->suppcode}}</option>
                                @endforeach
                            </select>

                            <input type="text" name="suppcode" id="suppcodehidden" value="{{$rma->suppcode}}" class="hidden">
                        </div>
                    </div>


                    

                </div>

                 <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Part SKU <span class="text-danger"><small><em>required.</em></small></span>
                                <span class="text-success"><small><em>
                                    name the Part SKU.
                                </em></small></span>

                            </label>
                            <input type="text" class="form-control input-sm" name="partsku" value="{{$rma->partsku}}" required>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Description
                                <span class="text-success"><small><em>
                                    a little description does help.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="descr" value="{{$rma->descr}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Kayako Ticket ID
                                <span class="text-success"><small><em>
                                    
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="kayakoid" value="{{$rma->kayakoid}}">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Notes
                                <span class="text-success"><small><em>
                                    make it beautiful.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="notes" value="{{$rma->notes}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">SP Order ID
                                <span class="text-success"><small><em>
                                    Sharepoint ID.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="spid" value="{{$rma->spid}}">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Location
                                <span class="text-success"><small><em>
                                    Where is waldo?
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="whloc" value="{{$rma->whloc}}">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Quantity <span class="text-danger"><small><em>required.</em></small></span></label>
                            <input type="number" class="form-control input-sm" name="qty" value="{{$rma->qty or 0}}" required="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Outcome <span class="text-success"><small><em>of this RAP.</em></small></span></label>
                            <select name="outcome" class="form-control input-sm">
                                <option value="return to customer"{{ strcasecmp($rma->outcome, "return to customer") == 0 ? "selected='true'" : ""  }}>Return to Customer</option>
                                <option value="shop"{{ strcasecmp($rma->outcome, "shop") == 0 ? "selected='true'" : ""  }}>Shop</option>
                                <option value="To Parts"{{ strcasecmp($rma->outcome, "To Parts") == 0 ? "selected='true'" : ""  }}>To Parts</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">RAP Status <span class="text-success"><small><em>of this rap.</em></small></span></label>
                            <select name="sys_orderstatus" class="form-control input-sm">
                                <option value="new" {{ strcasecmp($rma->sys_orderstatus, "new") == 0 ? "selected='true'" : ""  }}>New</option>
                                <option value="pending parts" {{ strcasecmp($rma->sys_orderstatus, "pending parts") == 0 ? "selected='true'" : ""  }}>Pending Parts</option>
                                <option value="on order" {{ strcasecmp($rma->sys_orderstatus, "on order") == 0 ? "selected='true'" : ""  }}>On Order</option>
                                <option value="completed adc" {{ strcasecmp($rma->sys_orderstatus, "completed adc") == 0 ? "selected='true'" : ""  }}>Completed ADC</option>
                                <option value="completed customer" {{ strcasecmp($rma->sys_orderstatus, "completed customer") == 0 ? "selected='true'" : ""  }}>Completed Customer</option>
                                <option value="cancelled" {{ strcasecmp($rma->sys_orderstatus, "cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Order Status
                                <span class="text-success"><small><em>
                                    RAP's current order status
                                </em></small></span>
                            </label>

                                @if($rma->sys_finalstatus == "Pending with Daisy" or $rma->sys_finalstatus == "Pending with Purchasing" or $rma->sys_finalstatus == "Confirmed with Purchasing" or $rma->sys_finalstatus == "Confirmed with Daisy" or $rma->sys_finalstatus == "Confirmed with Purchasing" or $rma->sys_finalstatus == "In Transit" or $rma->sys_finalstatus == "Received")

                                        <select name="sys_finalstatus" class="form-control input-sm">
                                            <option value="{{ $rma->sys_finalstatus }}">{{ $rma->sys_finalstatus }}</option>
                                        </select>
                                @else
                                    <select name="sys_finalstatus" class="form-control input-sm" >
                                        @if($rma->sys_finalstatus == "Unresolved" or $rma->sys_finalstatus == "Cancelled")
                                                <option value="Pending" {{ $rma->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Cancelled" {{ strcasecmp($rma->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($rma->sys_finalstatus,"Unresolved") == 0) || empty($rma->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @elseif($rma->sys_finalstatus == "Pending")
                                                <option value="Pending" {{ $rma->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Cancelled" {{ strcasecmp($rma->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($rma->sys_finalstatus,"Unresolved") == 0) || empty($rma->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @else
                                                <option value="Pending" {{ $rma->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Pending with Purchasing" {{ strcasecmp($rma->sys_finalstatus,"Pending with Purchasing")==0 ? "selected='true'" : ""  }}>Pending with Purchasing</option>
                                                <option value="Pending with Daisy" {{ strcasecmp($rma->sys_finalstatus,"Pending with Daisy") == 0 ? "selected='true'" : ""  }}>Pending with Daisy</option>
                                                <option value="Confirmed with Purchasing" {{ strcasecmp($rma->sys_finalstatus,"Confirmed with Purchasing") == 0 ? "selected='true'" : ""  }}>Confirmed with Purchasing</option>
                                                <option value="Confirmed with Daisy" {{ strcasecmp($rma->sys_finalstatus,"Confirmed with Daisy") == 0 ? "selected='true'" : ""  }}>Confirmed with Daisy</option>
                                                <option value="In Transit" {{ strcasecmp($rma->sys_finalstatus,"In Transit") == 0 ? "selected='true'" : ""  }}>In Transit</option>
                                                <option value="Received" {{ strcasecmp($rma->sys_finalstatus,"Received") == 0 ? "selected='true'" : ""  }}>Received</option>
                                                <option value="Cancelled" {{ strcasecmp($rma->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($rma->sys_finalstatus,"Unresolved") == 0) || empty($rma->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @endif

                                    </select>
                                @endif
                        </div>
                    </div>
                </div>
                

                
            </div> <!-- ./Panel-body -->
                
        </div> <!-- ./Panel -->

        <div>
            <div class="row">
                <div class="col-md-4 pull-right">
                    <button class="btn btn-primary btn-block" type="submit">Save</button>
                </div>
                <div class="col-md-4 pull-right">
                    <a class="btn btn-default btn-block" href="{{url('/raps')}}">Cancel</a>
                </div>
            </div>
        </div>
    </form>
@stop

@section('userdefjs')

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#photoimg').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function(){
           $(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(4).addClass('active');
            

            $("#photo").on('change', function() {
                //$("#photoimg").prop("src", $("#photo").val().substring(12));              
                readURL(this);
            });


            $("#rapFormEdit").on('change', '#parentcode', function(event) {
                event.preventDefault();
                var suppcode = $("#parentcode option:selected").data("suppcode");
                
                $("#suppcode.selectpicker").selectpicker('val', suppcode);
                $("#suppcode.selectpicker").selectpicker('refresh');

                $("#suppcodehidden").val(suppcode);
            });
        });
    </script>
@stop