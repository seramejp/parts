@extends("layout")

@section("content")

    <h2 class="thin">Modify Back Order</h2>
    <p class="text-muted">This is the way to modify a back order. Back Orders are blah blah blah. Make sure that entries are correct.</p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/backorders')}}">Backorders</a></li>
        <li class="active">Edit</li>
    </ol>
    <hr>

    <form id="bkoFormEdit" method="POST" action="{{url('/backorders/modify/'.$bko->id ) }}" class="text-left" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}

            <div class="row">
                <div class="col-lg-6">
                    <h3>{{ $bko->partsku }}</h3>
                    <h5><small><em>{{ $bko->descr or "This text is supposed to be a description." }}</em></small></h5>
                </div>
                <div class="col-lg-6 text-right">
                    <label for="photo" style="cursor: pointer;">
                        <img src="{{ "data:image/jpeg;base64," . base64_encode(Storage::get('public/' . $bko->photo)) }}" alt="{{$bko->partsku }}" class='img-thumbnail' height='200px' width='200px' id="photoimg">
                    </label>
                    <input type="file" class="form-control input-sm hidden" id="photo" name="photo" accept="image/*" value=""/>
                </div>
            </div>
            <h6><small><em>Last Modified by: </em>{{ $bko->lasttouch->name or "Somebody that I used to know" }}</small></h6>
            <hr>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    General Information
                    <small><em>will contain the basic information of this Back Order.</em></small>
                </div>
                
                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Date Invoiced
                                    <span class="text-success"><small><em>
                                        DD/MM/YYYY format, today.
                                    </em></small></span>
                                </label>
                                <input type="date" class="form-control input-sm" name="dateinvoiced" value="{{ $bko->dateinvoiced }}" readonly="">
                            </div>
                        </div>

                        <div class="col-lg-4 col-lg-offset-4">
                            <div class="form-group">
                                <label for="">Order ID
                                    <span class="text-success"><small><em>
                                        can be filled in later
                                    </em></small></span>
                                </label>
                                <input type="text" class="form-control input-sm" name="ordernum" autofocus="" value="{{$bko->ordernum}}">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Parent
                                    <span class="text-success"><small><em>
                                        select from Parent SKU
                                    </em></small></span>

                                </label>
                                <select name="parentcode" id="parentcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true">
                                    @foreach($parents as $parent)
                                        <option value="{{$parent->parentcode}}" {{ ($bko->parentcode == $parent->parentcode ? "selected" : "") }}  data-suppcode="{{$parent->suppcode}}">{{$parent->parentcode}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Supplier
                                    <span class="text-success"><small><em>
                                        filled in automatically
                                    </em></small></span>
                                </label>
                                <select name="suppcode" id="suppcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true">
                                    @foreach($suppliers as $supp)
                                        <option value="{{$supp->suppcode}}" {{ ($bko->suppcode == $supp->suppcode ? "selected" : "") }}>{{$supp->suppcode}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        

                    </div>

                     <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Part SKU <span class="text-danger"><small><em>required.</em></small></span>
                                    <span class="text-success"><small><em>
                                        name the Back Order.
                                    </em></small></span>

                                </label>
                                <input type="text" class="form-control input-sm" name="partsku" value="{{ $bko->partsku }}">
                            </div>
                        </div>

                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="">Description
                                    <span class="text-success"><small><em>
                                        a little description will help.
                                    </em></small></span>
                                </label>
                                <input type="text" class="form-control input-sm" name="descr" value="{{ $bko->descr }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Order Type
                                    <span class="text-success"><small><em>
                                        Website or RMA?
                                    </em></small></span>
                                </label>
                                <select name="ordertype" class="form-control input-sm">
                                    <option value="Website" {{ strcasecmp($bko->ordertype, "Website") == 0 ? "selected='true'" : ""  }}>Website</option>
                                    <option value="RMA" {{ strcasecmp($bko->ordertype, "RMA") == 0 ? "selected='true'" : ""  }}>RMA</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Category
                                    <span class="text-success"><small><em>
                                        approved?
                                    </em></small></span>
                                </label>
                                <input type="text" class="form-control input-sm" name="category" value="{{ $bko->category }}">
                            </div>
                        </div>

                        

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">ETA
                                    <span class="text-success"><small><em>
                                        when will the order arrive? DD/MM/YYYY format
                                    </em></small></span>
                                </label>
                                <input type="date" class="form-control input-sm" name="eta" value="{{ $bko->eta }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Quantity <span class="text-danger"><small><em>required.</em></small></span></label>
                                <input type="number" class="form-control input-sm" name="qty" required="" value="{{ $bko->qty }}">
                            </div>
                        </div>
                        
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label for="">Remarks
                                    <span class="text-success"><small><em>
                                        some notes, i guess.
                                    </em></small></span>
                                </label>
                                <input type="text" class="form-control input-sm" name="remarks" value="{{ $bko->remarks }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">SP Id
                                    <span class="text-success"><small><em>
                                        Sharepoint ID
                                    </em></small></span>
                                </label>

                                <input type="text" class="form-control input-sm" name="spid" value="{{ $bko->spid }}">
                            </div>
                        </div>

    


                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">BkO Status
                                    <span class="text-success"><small><em>
                                        back order's current status
                                    </em></small></span>
                                </label>
                                <select name="sys_orderstatus" class="form-control input-sm" >
                                    <option value="new" {{ $bko->sys_orderstatus == "new" ? "selected='true'" : ""  }}>New</option>
                                    <option value="Approved" {{ strcasecmp($bko->sys_orderstatus,"Approved")==0 ? "selected='true'" : ""  }}>Approved</option>
                                    <option value="dispatched" {{ strcasecmp($bko->sys_orderstatus,"dispatched") == 0 ? "selected='true'" : ""  }}>Dispatched</option>
                                    <option value="cancelled" {{ strcasecmp($bko->sys_orderstatus,"cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Order Status
                                    <span class="text-success"><small><em>
                                        Backorder's current order status
                                    </em></small></span>
                                </label>

                                    @if($bko->sys_finalstatus == "Pending with Daisy" or $bko->sys_finalstatus == "Pending with Purchasing" or $bko->sys_finalstatus == "Confirmed with Purchasing" or $bko->sys_finalstatus == "Confirmed with Daisy" or $bko->sys_finalstatus == "Confirmed with Purchasing" or $bko->sys_finalstatus == "In Transit" or $bko->sys_finalstatus == "Received")

                                        <select name="sys_finalstatus" class="form-control input-sm">
                                            <option value="{{ $bko->sys_finalstatus }}">{{ $bko->sys_finalstatus }}</option>
                                        </select>
                                    @else
                                        <select name="sys_finalstatus" class="form-control input-sm" >
                                            @if($bko->sys_finalstatus == "Unresolved" or $bko->sys_finalstatus == "Cancelled")
                                                <option value="Pending" {{ $bko->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Cancelled" {{ strcasecmp($bko->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($bko->sys_finalstatus,"Unresolved") == 0) || empty($bko->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @elseif($bko->sys_finalstatus == "Pending")
                                                <option value="Pending" {{ $bko->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Cancelled" {{ strcasecmp($bko->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($bko->sys_finalstatus,"Unresolved") == 0) || empty($bko->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @else
                                                <option value="Pending" {{ $bko->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Pending with Purchasing" {{ strcasecmp($bko->sys_finalstatus,"Pending with Purchasing")==0 ? "selected='true'" : ""  }}>Pending with Purchasing</option>
                                                <option value="Pending with Daisy" {{ strcasecmp($bko->sys_finalstatus,"Pending with Daisy") == 0 ? "selected='true'" : ""  }}>Pending with Daisy</option>
                                                <option value="Confirmed with Purchasing" {{ strcasecmp($bko->sys_finalstatus,"Confirmed with Purchasing") == 0 ? "selected='true'" : ""  }}>Confirmed with Purchasing</option>
                                                <option value="Confirmed with Daisy" {{ strcasecmp($bko->sys_finalstatus,"Confirmed with Daisy") == 0 ? "selected='true'" : ""  }}>Confirmed with Daisy</option>
                                                <option value="In Transit" {{ strcasecmp($bko->sys_finalstatus,"In Transit") == 0 ? "selected='true'" : ""  }}>In Transit</option>
                                                <option value="Received" {{ strcasecmp($bko->sys_finalstatus,"Received") == 0 ? "selected='true'" : ""  }}>Received</option>
                                                <option value="Cancelled" {{ strcasecmp($bko->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($bko->sys_finalstatus,"Unresolved") == 0) || empty($bko->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @endif

                                        </select>
                                    @endif
                            </div>
                        </div>
                    </div>
                    

                    
                </div> <!-- ./Panel-body -->
                    
            </div> <!-- ./Panel -->

            <div>
                <div class="row">
                    <div class="col-md-4 pull-right">
                        <button class="btn btn-primary btn-block" type="submit">Save</button>
                    </div>
                    <div class="col-md-4 pull-right">
                        <a class="btn btn-default btn-block" href="{{url('/backorders')}}">Cancel</a>
                    </div>
                </div>
            </div>
    </form>
@stop


@section('userdefjs')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#photoimg').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function(){
            $("#photo").on('change', function() {
                //$("#photoimg").prop("src", $("#photo").val().substring(12));              
                readURL(this);
            });

            $(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(2).addClass('active');

            $("#bkoFormEdit").on('change', '#parentcode', function(event) {
                event.preventDefault();
                var suppcode = $("#parentcode option:selected").data("suppcode");
                
                $("#suppcode.selectpicker").selectpicker('val', suppcode);
                $("#suppcode.selectpicker").selectpicker('refresh');
            });
        });
    </script>
@stop