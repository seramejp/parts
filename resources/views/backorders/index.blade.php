@extends('layout')


@section('content')
		<h2 class="thin">Back Orders</h2>
		<p class="text-muted">These orders are those that were'nt included in the regular orders.</p>
		
		<ol class="breadcrumb text-left">
			<li class="active">Backorders</li>
		</ol>
		<hr>
		

		<div class="text-left">
			<div id="loaderbacks" class="text-center">
				<img src="{{url('/images/app/rolling.gif') }}">
			</div>
			<table class="table table-hover" id="bktable" cellspacing="0" width="100%">
				<thead>
					<th width="3%"></th>
					<th width="4%"></th>
					<th width="7%">Date Invoiced</th>
					<th >Supplier</th>
					<th >Parent</th>
                    <th >SKU</th>
                    <th >Order ID</th>
                    <th class="hidden">Order Type</th>
                    <th >Qty</th>
                    <th >ETA</th>
                    <th >BkO Status</th>
					
					<th class="hidden">Category</th>
					<th class="hidden">SP ID</th>
                    <th class="hidden">Description</th>
                    <th class="hidden">Remarks</th>
                    <th class="hidden">ID</th>
                    <th >Order Status</th>
				</thead>

				<tfoot>
					<th colspan="17" rowspan="1"><a href="{{url('/backorders/create')}}" data-toggle='tooltip' data-placement="top" title="Create Backorder"><i class="fa fa-plus"></i> New Backorder</a></th>
				</tfoot>
			
				<tbody class="hidden">
					
				</tbody>
			</table>


		</div>

@stop


@section('userdefjs')
	<script>
		function formatThisDate(thisDate){
			/*return thisDate.getFullYear() + "-" + (  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "-" + (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   );*/

			return (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   )  + "/"  +
				(  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "/" + 
				thisDate.getFullYear();
		}

		function getImage(obj){
			
			mydata = "";
			myModalData = "";
			$.ajax({
				url: '/public/backorders/getimage/' + obj,
				type: 'GET',
				success: function(data){
					mydata  += "<a href='' data-toggle='modal' data-target='#myModal'><img src='data:image/jpeg;base64," + data + "' alt='Photo' class='img-thumbnail' width='100' height='100'></a>";

					myModalData  += "<img src='data:image/jpeg;base64," + data + "' alt='Photo' class='img-responsive'>";
					
					$(".appendhere").empty().append(mydata);
					$(".appendheremodal").empty().append(myModalData);

				}
			});
			

			return mydata;
		}

		function format(d){
			return '<table class="table table-default" width="40%">'+
		        '<tr class="info">'+
		            '<td width="10%"><strong>' + d.partsku +'</strong></td>'+
		            '<td width="40%"><em>' + d.descr +'</em></td>'+
		        '</tr>'+

		        '<tr>'+
		            '<td>Category:</td>'+
		            '<td>'+ d.category +'</td>'+
		        '</tr>'+

		        '<tr>'+
		            '<td>SP ID:</td>'+
		            '<td>'+ d.spid +'</td>'+
		        '</tr>'+
		    
		        '<tr>'+
		            '<td>Remarks:</td>'+
		            '<td>'+ d.remarks +'</td>'+
		        '</tr>'+

		        '<tr>'+
		            '<td>Image Thumbnail:</td>'+
		            '<td class="appendhere"></td>'+
		        '</tr>'+
		        
		    '</table>';
		}


		$(document).ready(function() {
			$(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
			$(".navsubmenuitemlist li").removeClass('active').eq(2).addClass('active');

			
			var table = $('#bktable')
				.on( 'init.dt', function () {
			        $("#bktable tbody").removeClass('hidden');
			        $("#loaderbacks").addClass('hidden');
			    })
				.DataTable({
					"ajax": '/parts/public/json/backordersjson.json',
					"columns": [
			            {
			                "className":'details-control',
			                "orderable":      false,
			                "data":           null,
			                "defaultContent": ""
			            }, //0
			            {
			                "className":'edit-control',
			                "orderable":      false,
			                "searchable": false,
			                "data": null,
			                "render": function ( data, type, full, meta ) {
						      	return "<td style='text-align: center; vertical-align: middle;'><a href='/parts/public/backorders/"+data.id+"/edit' data-toggle='tooltip' data-placement='top' title='Click to modify.'><img src='/parts/public/images/app/details_edit.png'></a></td>";
						    }
			            }, //1
								
			            { "data": "dateinvoiced"}, //2
			            { "data": "suppcode" }, //3
			            { "data": "parentcode" }, //4
			            { "data": "partsku" }, //5
			            { "data": "ordernum" }, //6
			            { "data": "ordertype" }, //7
			            { "data": "qty" }, //8
			            { "data": "eta" }, //9
			            { "data": "sys_orderstatus" }, //10

			            { "data": "category" }, //11
			            { "data": "spid" }, //12
			            { "data": "descr" }, //13
			            { "data": "remarks" }, //14
			            { "data": "id" }, //15
			            { "data": "sys_finalstatus" } //16
			            
			            ],
			        "columnDefs": [
			        	{
			                "targets": [ 7, 11, 12, 13, 14, 15 ],
			                "visible": false,
			                "searchable": true
			            },
			            {
			            	"targets": [2, 9],
			            	"render": function ( data, type, full, meta ) {
			            		var d = new Date(data);

								return formatThisDate(d);
						    }
			            },
			        	],
			        	select: 'single',
			        	"deferRender": true,		
			            "order": [[1, 'asc']]
			});

			$('#bktable tbody').on('click', 'td.details-control', function () {

			    var tr = $(this).closest('tr');
			    var row = table.row( tr );
			 
			    if ( row.child.isShown() ) {
			        // This row is already open - close it
			        row.child.hide();
			        tr.removeClass('shown');
			    }
			    else {
			        if ( table.row( '.shown' ).length ) {
						$('.details-control', table.row( '.shown' ).node()).click();
					}

					row.child( format(row.data()) ).show();
					tr.addClass('shown');
					getImage(row.data().id);
			    }
			});

			$("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');



		}); //End Document Ready

		
	</script>
@stop