@extends("layout")

@section("content")
    <h2 class="thin">Modify Special Order</h2>
    <p class="muted">This is the way to modify a special order. Special Orders are Non-SKUd Parts. Make sure that entries are correct.</p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/specialorders')}}">Special Orders</a></li>
        <li class="active">Edit</li>
    </ol>

    <hr>

    <form id="spoFormEdit" method="POST" action="{{url('/specialorders/modify/'.$spo->id)}}" class="text-left" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}

        <div class="row">
            <div class="col-lg-6">
                <h3>{{ $spo->partsku }}</h3>
                <h5><small><em>{{ $spo->descr or "This text is supposed to be a description." }}</em></small></h5>
            </div>

            <div class="col-lg-6 text-right">
                <label for="photo" style="cursor: pointer;">
                    <img src="{{ "data:image/jpeg;base64," . base64_encode(Storage::get('public/' . $spo->photo)) }}" alt="{{$spo->partsku }}" class='img-thumbnail' height='200px' width='200px' id="photoimg">
                </label>
                <input type="file" class="form-control input-sm hidden" id="photo" name="photo" accept="image/*" value=""/>
            </div>
        </div>
        <h6><small><em>Last Modified by: </em>{{ $spo->lasttouch->name or "Somebody that I used to know" }}</small></h6>
        <hr>
        <div class="panel panel-primary">
            <div class="panel-heading">
                General Information
                <small><em>will contain the basic information of this Special Order.</em></small>
            </div>
            
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Date Created
                                <span class="text-success"><small><em>
                                    DD/MM/YYYY format
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="created_at" readonly="" value="{{ date("d/m/Y", strtotime($spo->created_at)) }}">
                        </div>
                    </div>

                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="form-group">
                            <label for="">Order Number
                                <span class="text-success"><small><em>
                                    can be filled in later
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="ordernum" autofocus="" value="{{ $spo->ordernum }}">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Parent
                                <span class="text-success"><small><em>
                                    select from Parent SKU
                                </em></small></span>

                            </label>
                            <select name="parentcode" id="parentcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true">
                                @foreach($parents as $parent)
                                    <option value="{{$parent->parentcode}}" {{ ($spo->parentcode == $parent->parentcode ? "selected" : "") }}  data-suppcode="{{$parent->suppcode}}">{{$parent->parentcode}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Supplier
                                <span class="text-success"><small><em>
                                    filled in automatically
                                </em></small></span>
                            </label>
                            <select name="suppcode" id="suppcode" class="form-control input-sm selectpicker" data-size="8" data-live-search="true">
                                @foreach($suppliers as $supp)
                                    <option value="{{$supp->suppcode}}" {{ ($spo->suppcode == $supp->suppcode ? "selected" : "") }}>{{$supp->suppcode}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    

                </div>

                 <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Part SKU <span class="text-danger"><small><em>required.</em></small></span>
                                <span class="text-success"><small><em>
                                    name the Special Order.
                                </em></small></span>

                            </label>
                            <input type="text" class="form-control input-sm" name="partsku" value="{{ $spo->partsku }}">
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Description
                                <span class="text-success"><small><em>
                                    a little description does help.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="descr" value="{{ $spo->descr }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">RMA
                                <span class="text-success"><small><em>
                                    or Kayako Ticket ID.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="rma" value="{{ $spo->rma }}">
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Fault
                                <span class="text-success"><small><em>
                                    reasons why this needs ordered.
                                </em></small></span>
                            </label>
                            <input type="text" class="form-control input-sm" name="fault" value="{{ $spo->fault }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Quantity <span class="text-danger"><small><em>required.</em></small></span></label>
                            <input type="number" class="form-control input-sm" name="qty" value="{{ $spo->qty }}" required="">
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">SPO Status
                                <span class="text-success"><small><em>
                                    special order's current status
                                </em></small></span>
                            </label>
                            <select name="sys_orderstatus" class="form-control input-sm" >
                                <option value="new" {{ $spo->sys_orderstatus == "new" ? "selected='true'" : ""  }}>New</option>
                                <option value="in transit" {{ strcasecmp($spo->sys_orderstatus,"in transit")==0 ? "selected='true'" : ""  }}>In Transit</option>
                                <option value="dispatched" {{ strcasecmp($spo->sys_orderstatus,"dispatched") == 0 ? "selected='true'" : ""  }}>Dispatched</option>
                                <option value="received" {{ strcasecmp($spo->sys_orderstatus,"received") == 0 ? "selected='true'" : ""  }}>Received</option>
                                <option value="cancelled" {{ strcasecmp($spo->sys_orderstatus,"cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Order Status
                                <span class="text-success"><small><em>
                                    Special order's current order status
                                </em></small></span>
                            </label>

                                @if($spo->sys_finalstatus == "Pending with Daisy" or $spo->sys_finalstatus == "Pending with Purchasing" or $spo->sys_finalstatus == "Confirmed with Purchasing" or $spo->sys_finalstatus == "Confirmed with Daisy" or $spo->sys_finalstatus == "Confirmed with Purchasing" or $spo->sys_finalstatus == "In Transit" or $spo->sys_finalstatus == "Received")

                                        <select name="sys_finalstatus" class="form-control input-sm">
                                            <option value="{{ $spo->sys_finalstatus }}">{{ $spo->sys_finalstatus }}</option>
                                        </select>
                                @else
                                    <select name="sys_finalstatus" class="form-control input-sm" >
                                        @if($spo->sys_finalstatus == "Unresolved" or $spo->sys_finalstatus == "Cancelled")
                                                <option value="Pending" {{ $spo->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Cancelled" {{ strcasecmp($spo->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($spo->sys_finalstatus,"Unresolved") == 0) || empty($spo->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @elseif($spo->sys_finalstatus == "Pending")
                                                <option value="Pending" {{ $spo->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Cancelled" {{ strcasecmp($spo->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($spo->sys_finalstatus,"Unresolved") == 0) || empty($spo->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @else
                                                <option value="Pending" {{ $spo->sys_finalstatus == "Pending" ? "selected='true'" : ""  }}>Pending</option>
                                                <option value="Pending with Purchasing" {{ strcasecmp($spo->sys_finalstatus,"Pending with Purchasing")==0 ? "selected='true'" : ""  }}>Pending with Purchasing</option>
                                                <option value="Pending with Daisy" {{ strcasecmp($spo->sys_finalstatus,"Pending with Daisy") == 0 ? "selected='true'" : ""  }}>Pending with Daisy</option>
                                                <option value="Confirmed with Purchasing" {{ strcasecmp($spo->sys_finalstatus,"Confirmed with Purchasing") == 0 ? "selected='true'" : ""  }}>Confirmed with Purchasing</option>
                                                <option value="Confirmed with Daisy" {{ strcasecmp($spo->sys_finalstatus,"Confirmed with Daisy") == 0 ? "selected='true'" : ""  }}>Confirmed with Daisy</option>
                                                <option value="In Transit" {{ strcasecmp($spo->sys_finalstatus,"In Transit") == 0 ? "selected='true'" : ""  }}>In Transit</option>
                                                <option value="Received" {{ strcasecmp($spo->sys_finalstatus,"Received") == 0 ? "selected='true'" : ""  }}>Received</option>
                                                <option value="Cancelled" {{ strcasecmp($spo->sys_finalstatus,"Cancelled") == 0 ? "selected='true'" : ""  }}>Cancelled</option>
                                                <option value="Unresolved" {{ (strcasecmp($spo->sys_finalstatus,"Unresolved") == 0) || empty($spo->sys_finalstatus) ? "selected='true'" : ""  }}>Unresolved</option>
                                            @endif

                                    </select>
                                @endif
                        </div>
                    </div>
                </div>

                
                

                
            </div> <!-- ./Panel-body -->
                
        </div> <!-- ./Panel -->

        <div>
            <div class="row">
                <div class="col-md-4 pull-right">
                    <button class="btn btn-primary btn-block" type="submit">Save</button>
                </div>
                <div class="col-md-4 pull-right">
                    <a class="btn btn-default btn-block" href="/parts/public/specialorders">Cancel</a>
                </div>
            </div>
        </div>
    </form>

@stop

@section("userdefjs")
    <script>
        function readURL(input, selector) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    selector.attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(function(){
            $("#photo").on('change', function() {           
                readURL(this,$("#photoimg") );
            });

            $("#spoFormEdit").on('change', '#parentcode', function(event) {
                event.preventDefault();
                var suppcode = $("#parentcode option:selected").data("suppcode");
                
                $("#suppcode.selectpicker").selectpicker('val', suppcode);
                $("#suppcode.selectpicker").selectpicker('refresh');
            });

            $(".navmenuitemlist li").removeClass('active').eq(0).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(1).addClass('active');
            
            
            
            
            
        });
    </script>
@stop