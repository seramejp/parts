<html> 
    <body> <h1>RMA Awaiting Parts</h1> 

    @if(!empty($raps)) 

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Date Created</th>
                    <th>RMA Num</th>
                    <th>Part SKU</th>
                    <th>Supplier</th>
                    <th>Parent SKU</th>
                    <th>Description</th>
                    <th>Kayako ID</th>
                    <th>SPID</th>
                    <th>WH Loc</th>
                    <th>Outcome</th>
                    <th>Qty</th>
                    <th>Rap Status</th>
                    <th>Order Status</th>

                </tr>
            </thead>
            
            @foreach ($raps as $row)
                   
                <tr>
                    <td>{{$row->created_at}}</td>
                    <td>{{$row->rmanum}}</td>
                    <td>{{$row->partsku}}</td>
                    <td>{{$row->suppcode}}</td>
                    <td>{{$row->parentcode}}</td>
                    <td>{{$row->descr}}</td>
                    <td>{{$row->kayakoid}}</td>
                    <td>{{$row->spid}}</td>
                    <td>{{$row->whloc}}</td>
                    <td>{{$row->outcome}}</td>
                    <td>{{$row->qty}}</td>
                    <td>{{$row->sys_orderstatus}}</td>
                    <td>{{$row->sys_finalstatus}}</td>
                    
                </tr>
                      
            @endforeach 
        </table> 

    @endif 
    </body> 
</html>