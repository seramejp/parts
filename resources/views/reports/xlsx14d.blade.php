<html> 
    <body> <h1>14 day Report</h1> 

    @if(!empty($ponums)) 

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th >Date Created</th>
                    <th >SP Number</th>
                    <th >Supplier</th>
                    <th >Part SKU</th>
                    <th >Description</th>
                    <th >Qty</th>
                    <th >Notes</th>
                </tr>
            </thead>
            
            @foreach ($ponums as $existingorder)
                @if(intval($existingorder->sys_total)>0)
                    <tr>
                        <tr>
                            <td>{{ $existingorder->created_at->format("Y-m-d") }}</td>
                            <td>{{ $existingorder->spnum }}</td>
                            <td>{{ $existingorder->suppcode or '' }}</td>
                            <td>{{ $existingorder->partsku }}</td>
                            <td>{{ $existingorder->descr or '' }}</td>
                            <td>{{ $existingorder->sys_total or '-' }}</td>
                            <td>{{ $existingorder->notes or '' }}</td>
                        </tr>
                    </tr>
                @endif
                   
            @endforeach 
        </table> 

    @endif 
    </body> 
</html>