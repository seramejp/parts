<html> 
    <body> <h1>Supplier Orders</h1> 

    @if(!empty($suppliers)) 

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td>Order Type</td>
                    <th>Part SKU</th>
                    <th>Description</th>
                    <th>Supplier</th>
                    <th>Parent SKU</th>
                    <th>Total</th>
                    <th>Photo</th>
                </tr>
            </thead>
            
            @foreach ($suppliers["parents"] as $row)
                    @if(count($row->backordersid)>0 && $row->bkogo != "0" && $row->bkogo != 0 &&  $row->bkogo != "")
                        @foreach($row->backordersid as $rowitem)
                            <tr>
                                <td>Back Order</td>
                                <td>{{$row->partsku}}</td>
                                <td>{{$row->descr}}</td>
                                <td>{{$row->suppcode}}</td>
                                <td>{{$row->parentcode}}</td>
                                <td>{{$row->backorder}}</td>
                                <td>
                                   <img src="{{ "storage/".$rowitem->photo }}" class='img-thumbnail' height='200px' width='200px'>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    @if(count($row->specialordersid)>0 && $row->spogo != "0" && $row->spogo != 0 &&  $row->spogo != "")
                        @foreach($row->specialordersid as $rowitem)

                            <tr>
                                <td>Special Order</td>
                                <td>{{$row->partsku}}</td>
                                <td>{{$row->descr}}</td>
                                <td>{{$row->suppcode}}</td>
                                <td>{{$row->parentcode}}</td>
                                <td>{{$row->specialorder}}</td>
                                <td>
                                    <img src="{{ "storage/".$rowitem->photo }}" class='img-thumbnail' height='200px' width='200px'>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    @if(count($row->openboxesid)>0 && $row->opbgo != "0" && $row->opbgo != 0 &&  $row->opbgo != "")
                        @foreach($row->openboxesid as $rowitem)
                            <tr>
                                <td>Open Box</td>
                                <td>{{$row->partsku}}</td>
                                <td>{{$row->descr}}</td>
                                <td>{{$row->suppcode}}</td>
                                <td>{{$row->parentcode}}</td>
                                <td>{{$row->openbox}}</td>
                                <td>
                                    <img src="{{ "storage/".$rowitem->photo }}" class='img-thumbnail' height='200px' width='200px'>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    @if(count($row->rapsid)>0 && $row->rapgo != "0" && $row->rapgo != 0 &&  $row->rapgo != "")
                        @foreach($row->rapsid as $rowitem)
                            <tr>
                                <td>RAP</td>
                                <td>{{$row->partsku}}</td>
                                <td>{{$row->descr}}</td>
                                <td>{{$row->suppcode}}</td>
                                <td>{{$row->parentcode}}</td>
                                <td>{{$row->rap}}</td>
                                <td>
                                    <img src="{{ "storage/".$rowitem->photo }}" class='img-thumbnail' height='200px' width='200px'>
                                </td>
                            </tr>
                        @endforeach
                    @endif

                    @if(count($row->stocksid)>0 && $row->pigo != "0" && $row->pigo != 0 &&  $row->pigo != "")
                        @foreach($row->stocksid as $rowitem)
                        
                            <tr>
                                <td>Parts Inventory</td>
                                <td>{{$row->partsku}}</td>
                                <td>{{$rowitem->partsku_ibelong->descr}}</td>
                                <td>{{$row->suppcode}}</td>
                                <td>{{$row->parentcode}}</td>
                                <td>{{$row->orderqty}}</td>
                                <td>
                                    <img src="{{ "storage/".$rowitem->partsku_ibelong->photo }}" class='img-thumbnail' height='200px' width='200px'>
                                </td>
                            </tr>
                        @endforeach
                    @endif
            @endforeach 
        </table> 

    @endif 
    </body> 
</html>