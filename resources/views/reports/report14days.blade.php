@extends('layout')

@section('content')
	<div class="text-left">
		<h3 class="text-center"><img src="{{url('/images/app/fright-trolley-2-32.png')}}" alt=""> 14-Days Report </h3>
		<h5 class="text-center"><small><em>
			This is a summary of the Orders Pending or Confirmed with Daisy that are more than 14-days old and DHL as Order avenue.
		</em></small><h5>
		
		<hr>
		<h5>
			<small> 
			</small>
		</h5>
	
			

			<div class="row">

				<div class="col-lg-12">
					<h4 class="text-primary">Data View
						<small><a id="exportMe" href="{{url('/reports/fourteendayreport/export')}}" class="text-success pull-right" ><i class="fa fa-download"></i> EXPORT</a></small>
					</h4>
	
					<div class="panel panel-primary">
						
						<div class="panel-body">

							<div class="row">
								<div class="col-lg-12">
									<table class="table table-striped">
										<thead>
											<th width="10%">Date Created</th>
											<th width="10%">SP Number</th>
											<th width="10%">Supplier</th>
											<th width="20%">Part SKU</th>
											<th width="20%">Description</th>
											<th width="5%" class="text-center">Qty</th>
											<th width="25%">Notes</th>
										</thead>


										<tbody id="grBody">
											@foreach($ponums as $existingorder)
													@if(intval($existingorder->sys_total)>0)
														<tr>
															<td>{{ $existingorder->created_at->format("Y-m-d") }}</td>
															<td>{{ $existingorder->spnum }}</td>
															<td>{{ $existingorder->suppcode or '' }}</td>
															<td>{{ $existingorder->partsku }}</td>
															<td>{{ $existingorder->descr or '' }}</td>
															<td>{{ $existingorder->sys_total or '-' }}</td>
															<td>{{ $existingorder->notes or '' }}</td>
														</tr>
													@endif
											@endforeach
										</tbody>
									</table>

								</div>
							</div>

						</div> <!-- \Panel Body -->
					
					</div> <!-- \Panel -->

				</div> <!-- \col-lg-12 -->
			</div> <!-- \row -->

		

		<div class="row">
			<div class="col-lg-12">
				&nbsp;
			</div>
		</div>
	</div>
@stop

@section('userdefjs')
	<script>
		$(function(){
			$(".navmenuitemlist li").removeClass('active').eq(11).addClass('active');
			$(".navsubmenuitemlist li").removeClass('active').eq(8).addClass('active');			
			
			/*if ($("#exportMe").attr("href", "/pms/public/reports/fourteendayreport/export/0")) {
				$("#exportMe").attr("onclick", "return false");
			}*/

		});
	</script>
@stop