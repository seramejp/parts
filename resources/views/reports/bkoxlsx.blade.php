<html> 
    <body> <h1>Back Orders</h1> 

    @if(!empty($backorders)) 

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Date Invoiced</th>
                    <th>Order Num</th>
                    <th>Part SKU</th>
                    <th>Supplier</th>
                    <th>Parent SKU</th>
                    <th>Order Type</th>
                    <th>Category</th>
                    <th>ETA</th>
                    <th>Qty</th>
                    <th>Remarks</th>
                    <th>SPID</th>
                    <th>Back Order Status</th>
                    <th>Order Status</th>

                </tr>
            </thead>
            
            @foreach ($backorders as $row)
                   
                <tr>
                    <td>{{$row->dateinvoiced}}</td>
                    <td>{{$row->ordernum}}</td>
                    <td>{{$row->partsku}}</td>
                    <td>{{$row->suppcode}}</td>
                    <td>{{$row->parentcode}}</td>
                    <td>{{$row->ordertype}}</td>
                    <td>{{$row->category}}</td>
                    <td>{{$row->eta}}</td>
                    <td>{{$row->qty}}</td>
                    <td>{{$row->remarks}}</td>
                    <td>{{$row->spid}}</td>
                    <td>{{$row->sys_orderstatus}}</td>
                    <td>{{$row->sys_finalstatus}}</td>
                    
                </tr>
                      
            @endforeach 
        </table> 

    @endif 
    </body> 
</html>