<html> 
    <body> <h1>Open Boxes Orders</h1> 

    @if(!empty($openboxes)) 

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    
                    <th>Part SKU</th>
                    <th>Reference Num</th>
                    <th>Supplier</th>
                    <th>Parent SKU</th>
                    <th>SPID</th>
                    <th>Fault</th>
                    <th>Description</th>
                    <th>WH Loc</th>
                    <th>Kayako ID</th>
                    <th>Qty</th>
                    <th>Openbox Status</th>
                    <th>Order Status</th>
                    <th>Added By</th>
                    <th>Date Created</th>
                </tr>
            </thead>
            
            @foreach ($openboxes as $row)
                   
                <tr>
                    <td>{{$row->partsku}}</td>
                    <td>{{$row->referencenum}}</td>
                    <td>{{$row->suppcode}}</td>
                    <td>{{$row->parentcode}}</td>
                    <td>{{$row->spid}}</td>
                    <td>{{$row->fault}}</td>
                    <td>{{$row->descr}}</td>
                    <td>{{$row->whloc}}</td>
                    <td>{{$row->kayakoid}}</td>
                    <td>{{$row->qty}}</td>
                    <td>{{$row->sys_orderstatus}}</td>
                    <td>{{$row->sys_finalstatus}}</td>
                    <td>{{$row->user->name}}</td>
                    <td>{{$row->created_at}}</td>
                    
                </tr>
                      
            @endforeach 
        </table> 

    @endif 
    </body> 
</html>