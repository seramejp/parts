<html> 
    <body> <h1>Special Orders</h1> 

    @if(!empty($specialorders)) 

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Date Created</th>
                    <th>Order Num</th>
                    <th>Part SKU</th>
                    <th>Supplier</th>
                    <th>Parent SKU</th>
                    <th>Description</th>
                    <th>Fault</th>
                    <th>RMA</th>
                    <th>Qty</th>
                    <th>Special Order Status</th>
                    <th>Order Status</th>

                </tr>
            </thead>
            
            @foreach ($specialorders as $row)
                   
                <tr>
                    <td>{{$row->created_at}}</td>
                    <td>{{$row->ordernum}}</td>
                    <td>{{$row->partsku}}</td>
                    <td>{{$row->suppcode}}</td>
                    <td>{{$row->parentcode}}</td>
                    <td>{{$row->descr}}</td>
                    <td>{{$row->fault}}</td>
                    <td>{{$row->rma}}</td>
                    <td>{{$row->qty}}</td>
                    <td>{{$row->sys_orderstatus}}</td>
                    <td>{{$row->sys_finalstatus}}</td>
                    
                </tr>
                      
            @endforeach 
        </table> 

    @endif 
    </body> 
</html>