<!DOCTYPE html>
<html>
    <head>
        <title>There occured an error.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{url('/css/app.css')}}">
    
        <style>
            .title {
                font-weight: 100;
                font-family: 'Lato', sans-serif;
                font-size: 72px;
                margin-bottom: 40px;
                color: red;
            }
        </style>
    </head>
    <body>
        <div class="jumbotron">
            <div class="container">
                <div class="row">  
                    <div class="col-lg-12">
                        <p class="home pull-right"><a href="{{url('/')}}"><i class="glyphicon glyphicon-home"></i> home</a></p>    
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="title text-danger"><strong>Error 500!</strong></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <p class="subtitle pull-right">
                            {{ $exception->getMessage() }}
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
