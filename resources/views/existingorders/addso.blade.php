@extends('layout')

@section('content')
	
		<h2 class="thin">For Additional to Existing Order
			<br><strong><em>{{$exOrd->partsku}}</em></strong>
		</h2>
		<p class="muted">Check the checkbox for orders to be added. These are all Pending.</p>
		

		<ol class="breadcrumb text-left">
			<li><a href="{{url('/existingorders')}}">Existing Orders</a></li>
	        <li><a href="{{url('/existingorders/'. $exOrd->id .'/edit')}}">Return to Edit</a></li>
	        <li class="active">Create Additional</li>
	    </ol>
		<hr>
	
		<div>
			<div id="loadersoaddtable" class="text-center">
				<img src="{{ url('/images/app/rolling.gif') }}">
			</div>
			<table class="table table-striped" id="soaddtable" cellspacing="0" width="100%">
				<thead>
					
					<th >Type</th>
					<th >Parent</th>
                    <th >Supplier</th>
                    <th >Part SKU</th>
                    <!-- <th >Description</th> -->
                    <th >Order Status</th>
                    <th >Qty</th>
                    <th class="text-center">Ordered Qty</th>

                    <th class="hidden">ID</th>
                    <th class="text-center">Proceed?</th>
				</thead>
				
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<tbody class="text-left hidden">
						
			            @foreach ($supplierorders as $so) 

			            	@if ($so->stocksdesc->count() > 0)  
			                    @foreach ($so->stocksdesc as $stk) 
			                    	@if($stk->reorderqty > 0)
				                        <tr>
				                        	<td>PI</td>
				                        	<td>{{$so->parentcode}}</td>
				                        	<td>{{$so->suppcode}}</td>
				                            <td>
				                            	<a href='{{ url('/partspms/'.$stk->partsku_id.'/edit')}}'>{{$stk->partsku}}</a>
			                            	</td>
				                            
				                        	<td>{{$stk->sys_status}}</td>    
				                        	<td class="formodify">
				                        		<a href="javascript:void(0);" class="soaddtable-popup" data-toggle='popover' data-origqty="{{$stk->reorderqty}}" data-holderid="{{'pi'.$stk->id}}">{{$stk->reorderqty}}</a>
				                        		</td>
				                        	<td class="text-center" id="pi{{$stk->id}}">{{$stk->reorderqty}}</td>
				                        	<td class="hidden">{{$stk->id}}</td>
				                        	<td></td>
			                            </tr>
		                            @endif
			                    @endforeach
			                @endif

			                @if ($so->specialordersdesc->count() > 0)  
			                    @foreach ($so->specialordersdesc as $spo) 
			                        <tr>
			                        	<td>SPO</td>
			                        	<td>{{$so->parentcode}}</td>
			                        	<td>{{$so->suppcode}}</td>
			                            <td>
			                            	<a href='{{ url('/specialorders/'.$spo->id.'/edit')}}'>{{$spo->partsku}}</a>
		                            	</td>
			                            
			                        	<td>{{$spo->sys_orderstatus}}</td>    
			                        	<td class="formodify">
			                        		<a href="javascript:void(0);" class="soaddtable-popup" data-toggle='popover' data-origqty="{{$spo->qty}}" data-holderid="{{'spo'.$spo->id}}">{{$spo->qty}}</a>
		                        			</td>
			                        	<td class="text-center" id="{{'spo'.$spo->id}}">{{$spo->qty}}</td>
			                        	<td class="hidden">{{$spo->id}}</td>
			                        	<td></td>
		                            </tr>
			                    @endforeach
			                @endif

			                @if ($so->backordersdesc->count() > 0) 
			                    @foreach ($so->backordersdesc as $bko) 
			                        <tr>
			                        	<td>BKO</td>
			                        	<td>{{$so->parentcode}}</td>
			                        	<td>{{$so->suppcode}}</td>
			                            <td>
			                            	<a href='{{ url('/backorders/'.$bko->id.'/edit')}}'>{{$bko->partsku}}</a>
		                            	</td>
			                            
			                        	<td>{{$bko->sys_orderstatus}}</td>    
			                        	<td>{{$bko->qty}}</td>
			                        	<td class="text-center">{{$bko->qty}}</td>
			                        	<td class="hidden">{{$bko->id}}</td>
			                        	<td></td>
		                            </tr>
			                    @endforeach
			                @endif

							@if ($so->openboxesdesc->count() > 0) 
			                    @foreach ($so->openboxesdesc as $opb) 
			                        <tr>
			                        	<td>OPB</td>
			                        	<td>{{$so->parentcode}}</td>
			                        	<td>{{$so->suppcode}}</td>
			                            <td>
			                            	<a href='{{ url('/openboxes/'.$opb->id.'/edit')}}'>{{$opb->partsku}}</a>
		                            	</td>
			                            
			                        	<td>{{$opb->sys_orderstatus}}</td>    
			                        	<td>{{$opb->qty}}</td>
			                        	<td class="text-center">{{$opb->qty}}</td>
			                        	<td class="hidden">{{$opb->id}}</td>
			                        	<td></td>
		                            </tr>
			                    @endforeach
			                @endif

			                @if ($so->rapsdesc->count() > 0) 
			                    @foreach ($so->rapsdesc as $rap) 
			                        <tr>
			                        	<td>RAP</td>
			                        	<td>{{$so->parentcode}}</td>
			                        	<td>{{$so->suppcode}}</td>
			                            <td>
			                            	<a href='{{ url('/raps/'.$rap->id.'/edit')}}'>{{$rap->partsku}}</a>
		                            	</td>
			                            
			                        	<td>{{$rap->sys_orderstatus}}</td>    
			                        	<td>{{$rap->qty}}</td>
			                        	<td class="text-center">{{$rap->qty}}</td>
			                        	<td class="hidden">{{$rap->id}}</td>
			                        	<td></td>
		                            </tr>
			                    @endforeach
			                @endif


			            @endforeach
				</tbody>

				<tfoot>
					<th colspan="9" rowspan="1" class="text-right" id="buttoncontainertfoot">
					</th>
				</tfoot>
			</table>

			<form id="additionalordersform" action=" {{ url('/existingorders/additionalorders/'.$exOrd->id) }}" method="POST" enctype="multipart/form-data">
				<input id="additionalExOrd" type="text" name="to_additionalExOrd" class="form-control hidden">
				{{ csrf_field() }}
			</form>
			
	
		</div>
	
@stop

@section('userdefjs')
	<script>
		

		function destroyPopover(){
			$(".popover").popover("destroy");
		}

		$(function(){
			modifyEdit = function(holderidx, origqty){
				theOrderQty = $("#soaddtable-popup-input").val();
				
				$("#"+holderidx).text(theOrderQty);

				table.rows().every( function () {
				    var d = this.data();
				 
				    d.counter++; // update data source for the row
				 
				    this.invalidate(); // invalidate the data DataTables has cached for this row
				} );
				table.draw();
				//console.log( table.row( this ).data() );
				
				destroyPopover();
			}


			var table = $('#soaddtable')
				.on( 'init.dt', function () {
			        $("#soaddtable tbody").removeClass('hidden');
			        $("#loadersoaddtable").addClass('hidden');
			    })
				.DataTable({
					"columns": [
				            { "data": "type" },//1
				            { "data": "parentcode" },//2
				            { "data": "suppcode" },//3
				            { "data": "partsku" },//4

				            { "data": "sys_orderstatus" },//6
				            { "data": "qty"},//7
				            { "data": "orderedqty"},//8
				          	{ "data": "id" },//9
				          	{ 
				            	"className": 'select-checkbox',
				                "orderable":      false,
				                "data":           null,
				                "defaultContent": ''
				            } //last
			         
			            ],
			        "columnDefs": [
			            {
			                "targets": [ 7 ],
			                "visible": false,
			                "searchable": false
			            }
			        ],
		       		"select": {
				        	style: 'multi',
				        	selector: 'td:last-child'
				        },
			       	"pageLength": 50,
			       	dom: 'Bfrtip',
		       		"buttons": [
				            {
					            extend: 'selected',
					            className: 'btn-primary',
					            text: 'Add to Existing Order',
					            action: function ( e, dt, button, config ) {
					                //alert( dt.rows( { selected: true } ).indexes().length +' row(s) selected' );
					                
					                $("#additionalExOrd").val(JSON.stringify(dt.rows({selected: true}).data().toArray()));
					                $("#additionalordersform").submit();
					                
					            }
					        }	
				        ],
		            "order": [[1, 'asc']]
			});

			$("#soaddtable_filter").closest("div").addClass('text-right');


			table.buttons().container().appendTo( $('#buttoncontainertfoot') );

			/*$('#soaddtable tbody').on( 'click', '.formodify', function () {
				alert(table.row(this)[0]);
			    
			} );*/

			$('#soaddtable').popover({
	            selector: '[class="soaddtable-popup"]',
	            content: 
	            		"<input type='number' min='1' class='form-control input-sm' id='soaddtable-popup-input'>&nbsp;"+
	            		"<button class='btn btn-success btn-sm' id='soaddtable-popup-button-yes' onclick=''><i class='fa fa-check'></i></button>&nbsp;"+
	            		"<button class='btn btn-danger btn-sm' id='soaddtable-popup-button-no' onclick='javascript:destroyPopover()'><i class='fa fa-times'></i></button>",
	            html: true,
	            placement: 'top',
	            trigger: 'click'
	        });

			$('#soaddtable').on('shown.bs.popover', '.soaddtable-popup', function(event) {
				
	        	event.preventDefault();
	        	theSource = $(this);
	        	origqty = theSource.data("origqty");
	        	holderidx = theSource.data("holderid");
	        	
	        	theInputBox = $("#soaddtable-popup-input");
	        	theInputBox.val(origqty);
	        	theInputBox.select();

	        	$("#soaddtable-popup-button-yes").attr("onclick", "javascript:modifyEdit('"+holderidx+"', "+origqty+");");
	        });



			$("th.edit-control").removeClass('sorting_asc').addClass('sorting_disabled');

			$(".navmenuitemlist li.dropdown").removeClass('active').eq(1).addClass('active');
        	$(".navsubmenuitemlist li").removeClass('active').eq(6).addClass('active');
		});



	</script>
@stop