@extends('layout')

@section('content')
	
		<h2 class="thin">Supplier Orders</h3>
		<p class="muted">These orders will be sent to the Suppliers.</p>
		
		<hr>
	
		<div>
			<div id="loadersuppo" class="text-center">
				<img src="{{ url('/images/app/rolling.gif') }}">
			</div>
			<table class="table table-striped" id="supotable" cellspacing="0" width="100%">
				<thead>
					<th></th>
					<th >Date Created</th>
					<th >Parent</th>
                    <th >Supplier</th>
                    <th >Part SKU</th>
                    <th >ReO Qty</th>
                    <th >BkO Qty</th>
                    <th >OBP Qty</th>
                    <th >SpO Qty</th>
                    <th >RAP Qty</th>
                    <th >Total</th>

                    <th class="hidden">Description</th>
                    <th class="hidden">ID</th>
                    <th>Order Status</th>
                    <th class="hidden">Orderqty</th>
                    <th class="hidden">PIGo</th>
                    <th class="hidden">BkoGo</th>
                    <th class="hidden">OBPGo</th>
                    <th class="hidden">SPOGo</th>
                    <th class="hidden">RAPGo</th>
                    
                    <th class="hidden">PIS</th>
                    <th class="hidden">BkOS</th>
                    <th class="hidden">OBPS</th>
                    <th class="hidden">SPOS</th>
                    <th class="hidden">RAPS</th> 
                    <th class="hidden">Total Orders</th>
                    <th>Proceed?</th>
				</thead>
				
				
			
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<tbody class="text-left hidden">
					
				</tbody>

				<tfoot>
					<th colspan="27" rowspan="1" class="text-right" id="buttoncontainer">
						
					</th>
				</tfoot>
			</table>

			<form id="supplierordersform" action=" {{ url('/supplierorders/orderstoexisting') }}" method="POST" enctype="multipart/form-data">
				<input type="text" id="so-checked" name="toex_supplierorders" class="hidden">
				<input type="text" id="pi-checked" name="toex_needsordering" class="hidden">
				<input type="text" id="sp-checked" name="toex_specialorders" class="hidden">
				<input type="number" value="0" min="0" id="totalOrderQty" class="hidden" name="toex_totalorderqty">
				{{ csrf_field() }}
			</form>
	
	
		</div>


		<!-- Modal -->
		<div class="modal fade" id="modalChangeSupplier" tabindex="-1" role="dialog" aria-labelledby="modalChangeSupplierlabel" style="position: fixed; top: 20%;">
		  	<div class="modal-dialog" role="document">
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        		<h4 class="modal-title">Changing Supplier from</h4>
		        		<h4 class="modal-title" id="modalChangeSupplierLabel">MIT-SUP-0001</h4>
		      		</div>
		      	
		      		<div class="modal-body">
			        	<p>My modal body</p>
			      	</div>
		      		
		      		<div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" id="btnModalChangeSupplierSave">Save changes</button>
			      	</div>
		    	</div>
		  	</div>
		</div>
	
@stop

@section('userdefjs')
	<script>
		var globalUrl = "{{ url('/') }}";
		function formatThisDate(thisDate){
			/*return thisDate.getFullYear() + "-" + (  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "-" + (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   );*/

			return (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   )  + "/"  +
				(  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "/" + 
				thisDate.getFullYear();
		}

		function destroyPopover(){
			$(".popover").popover("destroy");
		}

		

	</script>
	<script src="{{ url('/js/supplierorders/so.js') }}">
	</script>
@stop