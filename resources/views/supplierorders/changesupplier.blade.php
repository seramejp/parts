@extends("layout")

@section("content")

    <h2 class="thin">Modify Supplier</h2>
    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit alias amet expedita unde ad facere, minima esse et, nobis quidem corrupti.</p>
    
    <ol class="breadcrumb text-left">
        <li><a href="{{url('/supplierorders')}}">Supplier Orders</a></li>
        <li class="active">Changing Supplier</li>
    </ol>
    <hr>

    <form method="POST" action="{{url('/supplierorders/supplier/update/'.$suppo->id) }}" class="text-left" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}

            <div class="row">
                <div class="col-lg-6">
                    <h3><small>Part SKU: </small>{{ $suppo->partsku }}</h3>
                    <h3><small>Parent SKU: </small>{{ $suppo->parentcode }}</h3>
                </div>
            </div>
            <hr>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    this Supplierorder's details
                </div>
                
                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Date Created
                                    <span class="text-success"><small><em>
                                        DD-MM-YYYY, date this order was saved.
                                    </em></small></span>
                                </label>
                                <input type="text" class="form-control input-sm" readonly="" value="{{ date('d-m-Y', strtotime($suppo->created_at)) }}">
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Order Status
                                    <span class="text-success"><small><em>
                                        current status of this order
                                    </em></small></span>

                                </label>
                                <input type="text" class="form-control input-sm" value="{{$suppo->sys_finalstatus}}" readonly>
                            </div>
                        </div> 
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Parent SKU
                                    <span class="text-success"><small><em>
                                        name of this Parent SKU
                                    </em></small></span>

                                </label>
                                <input type="text" class="form-control input-sm" value="{{$suppo->parentcode}}" readonly>
                            </div>
                        </div>   

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="" class="text-danger"><strong>Supplier</strong>
                                    <span class="text-success"><small><em>
                                        current supplier assigned to this parent
                                    </em></small></span>
                                </label>
                                <select name="suppcode" class="form-control input-sm">
                                    @foreach($suppliers as $supp)
                                        <option value="{{$supp->suppcode}}" {{ ($supp->suppcode == $suppo->suppcode ? "selected" : "") }}>{{$supp->suppcode}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>                     

                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-hover table-striped">
                                <caption><strong>Orders under this Supplier Order</strong></caption>
                                <thead>
                                        <th>Type</th>
                                        <th>Partsku</th>
                                        <th>Description</th>
                                        <th>Qty</th>
                                </thead>

                                <tbody>
                                    @if(count($suppo->stocksdesc) > 0)
                                        
                                            @foreach($suppo->stocksdesc as $ord)
                                                <tr>
                                                    <td>Parts Inventory</td>
                                                    <td>{{$ord->partsku}}</td>
                                                    <td>{{$ord->desc}}</td>
                                                    <td>{{$ord->reorderqty}}</td>
                                                </tr>
                                            @endforeach
                                    @endif

                                    @if(count($suppo->specialordersdesc) > 0)
                                        
                                            @foreach($suppo->specialordersdesc as $ord)
                                                <tr>
                                                    <td>Special Order</td>
                                                    <td>{{$ord->partsku}}</td>
                                                    <td>{{$ord->descr}}</td>
                                                    <td>{{$ord->qty}}</td>
                                                </tr>
                                            @endforeach
                                    @endif

                                    @if(count($suppo->backordersdesc) > 0)
                                        
                                            @foreach($suppo->backordersdesc as $ord)
                                                <tr>
                                                    <td>Back Order</td>
                                                    <td>{{$ord->partsku}}</td>
                                                    <td>{{$ord->descr}}</td>
                                                    <td>{{$ord->qty}}</td>
                                                </tr>
                                            @endforeach
                                    @endif

                                    @if(count($suppo->openboxesdesc) > 0)
                                        
                                            @foreach($suppo->openboxesdesc as $ord)
                                                <tr>
                                                    <td>Openbox</td>
                                                    <td>{{$ord->partsku}}</td>
                                                    <td>{{$ord->descr}}</td>
                                                    <td>{{$ord->qty}}</td>
                                                </tr>
                                            @endforeach
                                    @endif

                                    @if(count($suppo->rapsdesc) > 0)
                                        
                                            @foreach($suppo->rapsdesc as $ord)
                                                <tr>
                                                    <td>RAP</td>
                                                    <td>{{$ord->partsku}}</td>
                                                    <td>{{$ord->descr}}</td>
                                                    <td>{{$ord->qty}}</td>
                                                </tr>
                                            @endforeach
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div> <!-- ./Panel-body -->
                    
            </div> <!-- ./Panel -->


            <div class="panel panel-danger">
                <div class="panel-heading">
                    Orders with the same Parent SKU will also be changed
                </div>
                
                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-hover table-striped">
                                <thead>
                                        <th>Type</th>
                                        <th>Partsku</th>
                                        <th>Qty</th>
                                        <th>Order Status</th>
                                </thead>

                                <tbody>
                                    @if(count($partskus) > 0)
                                        
                                            @foreach($partskus as $ord)
                                                <tr>
                                                    <td>Parts Inventory</td>
                                                    <td>{{$ord->partsku}}</td>
                                                    <td>{{$ord->reorderqty}}</td>
                                                    <td>{{$ord->sys_finalstatus}}</td>
                                                </tr>
                                            @endforeach
                                    @endif

                                    @if(count($specialorders) > 0)
                                        
                                            @foreach($specialorders as $ord)
                                                <tr>
                                                    <td>Special Order</td>
                                                    <td>{{$ord->partsku}}</td>
                                                    <td>{{$ord->qty}}</td>
                                                    <td>{{$ord->sys_finalstatus}}</td>
                                                </tr>
                                            @endforeach
                                    @endif

                                    @if(count($backorders) > 0)
                                        
                                            @foreach($backorders as $ord)
                                                <tr>
                                                    <td>Back Order</td>
                                                    <td>{{$ord->partsku}}</td>
                                                    <td>{{$ord->qty}}</td>
                                                    <td>{{$ord->sys_finalstatus}}</td>
                                                </tr>
                                            @endforeach
                                    @endif

                                    @if(count($openboxes) > 0)
                                        
                                            @foreach($openboxes as $ord)
                                                <tr>
                                                    <td>Openbox</td>
                                                    <td>{{$ord->partsku}}</td>
                                                    <td>{{$ord->qty}}</td>
                                                    <td>{{$ord->sys_finalstatus}}</td>
                                                </tr>
                                            @endforeach
                                    @endif

                                    @if(count($raps) > 0)
                                        
                                            @foreach($raps as $ord)
                                                <tr>
                                                    <td>RAP</td>
                                                    <td>{{$ord->partsku}}</td>
                                                    <td>{{$ord->qty}}</td>
                                                    <td>{{$ord->sys_finalstatus}}</td>
                                                </tr>
                                            @endforeach
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div> <!-- ./Panel-body -->
                    
            </div> <!-- ./Panel -->

            <div>
                <div class="row">
                    <div class="col-md-4 pull-right">
                        <button class="btn btn-primary btn-block" type="submit">Save</button>
                    </div>
                    <div class="col-md-4 pull-right">
                        <a class="btn btn-default btn-block" href="{{url('/supplierorders')}}">Cancel</a>
                    </div>
                </div>
            </div>
    </form>
@stop


@section('userdefjs')
    <script>
        
        $(function(){

            $(".navmenuitemlist li.dropdown").removeClass('active').eq(3).addClass('active');
            $(".navsubmenuitemlist li").removeClass('active').eq(12).addClass('active');
        });
    </script>
@stop