@extends('layout')

@section('content')
	
		<h2 class="thin">Supplier Orders</h3>
		<p class="muted">These orders will be sent to the Suppliers.</p>
		
		<hr>
	
		<div>
			<div id="loadersuppo" class="text-center">
				<img src="/images/app/rolling.gif">
			</div>
			<table class="table table-striped" id="supotable" cellspacing="0" width="100%">
				<thead>
					<th></th>
					<th >Date Created</th>
					<th >Parent</th>
                    <th >Supplier</th>
                    <th >Part SKU</th>
                    <th >ReO Qty</th>
                    <th >BkO Qty</th>
                    <th >OBP Qty</th>
                    <th >SpO Qty</th>
                    <th >RAP Qty</th>
                    <th >Total</th>

                    <th class="hidden">Description</th>
                    <th class="hidden">ID</th>
                    <th>Order Status</th>
                    <th class="hidden">With1</th>
                    <th class="hidden">With2</th>
                    <th>Proceed?</th>
				</thead>
				
				
			
				<meta name="csrf-token" content="{{ csrf_token() }}" />
				<tbody class="text-left hidden">
					
				</tbody>
			</table>
			<input type="text" id="so-checked" class="form-control">
			<input type="text" id="pi-checked" class="form-control">
			<input type="text" id="sp-checked" class="form-control">
	
	
		</div>
	
	
	
	
@stop

@section('userdefjs')
	<script>
		function formatThisDate(thisDate){
			return thisDate.getFullYear() + "-" + (  ((thisDate.getMonth() + 1) < 9) ? ("0"+(thisDate.getMonth() + 1) ) : (thisDate.getMonth() + 1)   ) + "-" + (  (thisDate.getDate() < 9) ? ("0"+thisDate.getDate() ) : thisDate.getDate()   );
		}

		function isSubItemChecked(theItemId, itemType){ //Item Types: 1-PI, 2-SPO, 3-BKO, 4-
			isChecked = "";
			switch(itemType){
				case 1:
					var piids = $("#pi-checked").val();
					isChecked = (piids.indexOf(theItemId) != "-1" ? "checked" : "");
					break;
				
				case 2:
					var spids = $("#sp-checked").val();
					isChecked = (spids.indexOf(theItemId) != "-1" ? "checked" : "");
					break;
				
				default:
					break;
			}
			return isChecked;
		}

		function format(d){
			html = "";
			spo = d.specialordersid;
			no = d.stocksid;
			
			html = "" + '<div class="col-lg-10 col-lg-offset-1">' + '<table class="table table-hover table-striped table-bordered">' + '<thead>' + '<tr class="danger">'+ '<td width="20%"><strong>' + d.partsku +'</strong></td>'+ '<td width="30%" colspan="3"><em>' + d.descr +'</em></td>'+ '</tr>' +
					        	'</thead>'+ '<tbody>';
			

			if (typeof spo !== "undefined") {
	    		for (var i = spo.length - 1; i >= 0; i--) {
	    			html += "<tr><td><a href='/specialorders/"+spo[i].id+"/edit'>" + d.partsku + "</a></td><td>Special Order</td><td>" + spo[i].qty + "</td><td width='3%' class='text-center'><div class='checkbox'><label><input type='checkbox' value='"+spo[i].id+"' class='procb chr"+d.id+"' "+isSubItemChecked(spo[i].id, 2)+"></label></div></tr>";
	    		}
    		}

    		if (typeof no !== "undefined") {
	    		for (var i = no.length - 1; i >= 0; i--) {
	    			html += "<tr><td><a href='/parts/"+no[i].id+"/edit'>" + d.partsku + "</a></td><td>Needs Ordering</td><td>" + no[i].reorderqty + "</td><td width='3%' class='text-center'><div class='checkbox'><label><input type='checkbox' value='"+no[i].id+"' class='procb chr"+d.id+"' "+isSubItemChecked(no[i].id, 1)+"></label></div></tr>";
	    		}
	    	}
    		
    		html += "" + '</tbody>' + '</table>' + '</div>';

			return html;
		}

		function addToArray(theData, theGivenArray){
			current = theGivenArray.val();
			forInput = "";

			if (theData[0].length > 0) {
				for (var i = theData[0].length - 1; i >= 0; i--) {
					forInput += theData[0][i].id + ";";
				}
				theGivenArray.val(current + forInput);
			}
		}

		function removeFromArray(theData, theInputArray){
			if (theData[0].length > 0) {
				for (var i = theData[0].length - 1; i >= 0; i--) {
					$(".procb[value='"+ theData[0][i].id +"']").removeAttr('checked');
					updatedData = theInputArray.val();
					updatedData = updatedData.split(";")
										.filter(item => item != theData[0][i].id)
										.join(";");
			 		theInputArray.val(updatedData);
				}
			}
		}

		$(function(){
			var soids = $("#so-checked");
			var piids = $("#pi-checked");
			var spids = $("#sp-checked");

			$(".navmenuitemlist li").removeClass('active').eq(1).addClass('active');
			$(".navsubmenuitemlist li").removeClass('active').eq(0).addClass('active');

			var table = $('#supotable')
				.on( 'init.dt', function () {
			        $("#supotable tbody").removeClass('hidden');
			        $("#loadersuppo").addClass('hidden');
			    })
				.DataTable({
					"ajax": '../json/supplierordersjson.json',
					"columns": [
				            {
				                "className":'details-control',
				                "orderable":      false,
				                "data":           null,
				                "defaultContent": ''
				            }, //0
				            { "data": "created_at" }, //1
				            { "data": "parentcode" }, //2
				            { "data": "suppcode" }, //3
				            { "data": "partsku" }, //4
				            { "data": "reorderqty" }, //5
				            { "data": "backorder" }, //6
				            { "data": "openbox" }, //7
				            { "data": "specialorder" }, //8
				            { "data": "rap" }, //9
				            { "data": "sys_total" }, //10
				            { "data": "descr" }, //11

				            { "data": "id" }, //12
				            { "data": "sys_finalstatus" }, //13
				            {
				            	"orderable":false,
				            	"data":"specialordersid",
				            }, //14
				            {
				            	"orderable":false,
				            	"data":"stocksid",
				            }, //15
				            { 
				            	"className": 'select-checkbox',
				                "orderable":      false,
				                "data":           null,
				                "defaultContent": ''
				            } //last
			            ],
			        "columnDefs": [
				        	{
				            	"targets": [1],
				            	"render": function ( data, type, full, meta ) {
				            		var d = new Date(data);

									return formatThisDate(d);
							    }
				            },
				            {
				                "targets": [ 11 ],
				                "visible": false,
				                "searchable": true
				            },
				            {
				                "targets": [ 12 ],
				                "visible": false,
				                "searchable": false
				            },
				            {
				                "targets": [ 14 ],
				                "searchable": false,
				                "visible": false,
				                "render": function ( data, type, full, meta ) {
				                	dataVals = "";
				            		for (var i = data.length - 1; i >= 0; i--) {
				            			dataVals+= data[i].id + "," + data[i].qty + "-2;";
				            		}
				            		return dataVals;
							    }
				            },
				            {
				                "targets": [ 15 ],
				                "searchable": false,
				                "visible": false,
				                "render": function ( data, type, full, meta ) {
				                	dataVals = "";
				            		for (var i = data.length - 1; i >= 0; i--) {
				            			dataVals+= data[i].id + "," + data[i].reorderqty + "-1;";
				            		}
				            		return dataVals;
							    }
				            }
			            
			        	],
			        "select": {
			        	style: 'multi',
			        	selector: 'td:last-child'
			        },
			        dom: 'Bfrtip',
		       		"buttons": [
				            {
					            extend: 'selected',
					            text: 'Proceed with Order',
					            action: function ( e, dt, button, config ) {
					                alert( dt.rows( { selected: true } ).indexes().length +' row(s) selected' );
					            }
					        }	
				        ],
		            "order": [[1, 'asc']],
		        	"deferRender": true

			});

			table.on( 'select', function ( e, dt, type, indexes ) {
			    if ( type === 'row' ) {
			        var data = table.rows( indexes ).data().pluck( 'id' );
			 		current = soids.val();
			 		soids.val(current + data[0] + ";");

			 		var px = table.rows( indexes ).data().pluck( 'stocksid' );
			 		addToArray(px, piids);	

			 		var sx = table.rows( indexes ).data().pluck( 'specialordersid' );
			 		addToArray(sx, spids);

			    }
			} );

			table.on( 'deselect', function ( e, dt, type, indexes ) {
			    if ( type === 'row' ) {
			        var data = table.rows( indexes ).data().pluck( 'id' );
			 		splitted = soids.val()
			 						.split(";")
			 						.filter(item => item != data[0])
			 						.join(";");
			 		soids.val(splitted);

			 		var dataPI = table.rows( indexes ).data().pluck( 'stocksid' );
			 		removeFromArray(dataPI, piids);

			 		var dataSp = table.rows( indexes ).data().pluck( 'specialordersid' );
			 		removeFromArray(dataSp, spids);
			    }
			} );

			$("#supotable_filter").closest("div").addClass('text-right');
			$("#supotable_wrapper").find("div.row").first().addClass('text-left');


			$('#supotable tbody').on('click', 'td.details-control', function () {
			    var tr = $(this).closest('tr');
			    var row = table.row( tr );
			 
			    if ( row.child.isShown() ) {
			        // This row is already open - close it
			        row.child.hide();
			        tr.removeClass('shown');
			    }
			    else {
			        if ( table.row( '.shown' ).length ) {
						$('.details-control', table.row( '.shown' ).node()).click();
					}

					row.child( format(row.data()) ).show();
					tr.addClass('shown');
					//getKabits(row.data().id);
			    }
			});


			$("#supotable tbody").on('change', '.procb', function(event) {
				event.preventDefault();

				
				console.log(table.rows({ selected: true }).data());
			});
		});
	</script>
@stop