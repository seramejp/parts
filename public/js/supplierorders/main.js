

	function format(d){
		html = "";
		spo = d.specialordersid;
		no = d.stocksid;
		bko = d.backordersid;
		
		html = "" + 
			'<div class="col-lg-10 col-lg-offset-1">' + 
				'<table class="table table-hover table-striped table-bordered supotable-child-table" style="border-radius:4px;overflow:hidden;background-color:white;">' + 
					'<thead>' + 
						'<tr class="danger">'+ 
							'<td width="40%"><strong>Part SKU</strong></td>'+ 
							'<td width="30%"><strong>Type</strong></td>'+ 
							'<td width="25%"><strong>Qty</strong></td>'+ 
							'<td width="5%"></td>'+ 
						'</tr>' +
		        	'</thead>'+ 
	        	'<tbody>';
		

	    //Special Orders
		if (typeof spo !== "undefined") {
			for (var i = spo.length - 1; i >= 0; i--) {
				html += "" + 
					"<tr>"+
						"<td><a href='/parts/public/specialorders/"+spo[i].id+"/edit'>" + d.partsku + "</a></td>"+
						"<td>Special Order</td>"+
						"<td>" + spo[i].qty + "</td>"+
						"<td width='3%' class='text-center'>"+
							"<label>"+
								"<input type='checkbox' value='spo' data-soid='"+d.id+"' data-spoid='"+spo[i].id+"'  class='procb chr"+d.id+" item"+spo[i].id+"'  "+isSubItemChecked(spo[i].id, 2)+">"+
							"</label>"+
						"</td>"+
					"</tr>";
			}
		}


		//Back Orders
		if (typeof bko !== "undefined") {
			for (var i = bko.length - 1; i >= 0; i--) {
				html += "" + 
					"<tr>"+
						"<td><a href='/parts/public/backorders/"+bko[i].id+"/edit'>" + d.partsku + "</a></td>"+
						"<td>Special Order</td>"+
						"<td>" + bko[i].qty + "</td>"+
						"<td width='3%' class='text-center'>"+
							"<label>"+
								"<input type='checkbox' value='bko' data-soid='"+d.id+"' data-bkoid='"+bko[i].id+"'  class='procb chr"+d.id+" item"+bko[i].id+"'  "+isSubItemChecked(bko[i].id, 2)+">"+
							"</label>"+
						"</td>"+
					"</tr>";
			}
		}



		//Needs Ordering
		if (typeof no !== "undefined") {
			for (var i = no.length - 1; i >= 0; i--) {
				orderqty = isIdInPiids(no[i].id, no[i].reorderqty);
				html += "" +
					"<tr>"+
						"<td><a href='/parts/public/partspms/"+no[i].id+"/edit'>" + d.partsku + "</a></td>"+
						"<td>Needs Ordering</td>"+
						"<td>" + 
							"<a href='javascript:void(0)' class='supotable-popup' id='aid"+d.id+"_"+no[i].id+"' data-toggle='popover' data-soid='"+d.id+"' data-noid='"+no[i].id+"' data-reorderqty='" + no[i].reorderqty + "' data-orderqty='" + orderqty + "'>" + orderqty + "</a>" +
						"</td>"+
						"<td width='3%' class='text-center'>"+
							"<label>"+
								"<input type='checkbox' value='no' data-soid='"+d.id+"' data-noid='"+no[i].id+"' data-orderqty='"+orderqty+"' class='procb chr"+d.id+" item"+no[i].id+"'  "+isSubItemChecked(no[i].id, 1)+" >"+
							"</label>"+
						"</td>"+
					"</tr>";
			}
		}
		
		html += "" + '</tbody>' + '</table>' + '</div>';

		return html;
	}


	function checkOrNotThisCB(soid, id, typ, toCheck){
		if (toCheck == 1) {
			$(".chr"+soid+".item"+id+"[value='"+typ+"']").prop('checked', true);	
		}else{
			$(".chr"+soid+".item"+id+"[value='"+typ+"']").prop('checked', false).removeAttr('checked');
		}
		
		console.log("Checked/Removed Check");
	}

	function transformPIIDS(piids){
		piidsObj = [];
		if (piids != "") {
			//1,5;
			firstSplit = piids.split(";");
			for (var i = firstSplit.length - 1; i >= 0; i--) {
				item = firstSplit[i].split(",");
				piidsObj.push({
					id: item[0],
					qty: item[1],
				});
			}
		}
		return piidsObj;
	}

	function addToPI(piObj){ //piObj is an object containing values of the for adding Needs Ordering. There will only be one NO per Supplier Order
		thePI = $("#pi-checked");
		piids = transformPIIDS(thePI.val());
		console.log(piids);

		if (piids.length > 0) {
			let obj = piids.find(item => item.id == piObj.noid);
			let index = piids.indexOf(obj);
			
			if (index >= 0) {
				piids.fill(obj.qty=piObj.orderqty, index, index++);	
			}else{
				thePI.val(thePI.val() + piObj.noid + "," + piObj.orderqty + ";");
			}
			checkOrNotThisCB(piObj.soid, piObj.noid, "no", 1);
		}else{ //if piids is empty, add the piObj
			thePI.val(piObj.noid + "," + piObj.orderqty + ";");
			checkOrNotThisCB(piObj.soid, piObj.noid, "no", 1);
		}
		
	}

	function removeFromPI(soid, noid){
		thePI = $("#pi-checked");
		piids = transformPIIDS(thePI.val());

		if (piids.length > 0) {
			let obj = piids.find(item => item.id == noid);
			let index = piids.indexOf(obj);
			
			if (index >= 0) {
				piids.fill(obj.qty="", index, index++);	
				piids.fill(obj.id="", index, index++);	
			}
			thePI.val(piids.map(function(elem){
				returnVal = "";
				if (elem.id != "") {
					returnVal = elem.id + "," + elem.qty + ";";
				}
				return returnVal;
			}).join(""));
			checkOrNotThisCB(soid, noid, "no", 2);
		}
		if (thePI.val()==";") {thePI.val("");}
	}

	function addToArray(typ, soid, theData, theGivenArray){
		current = theGivenArray.val();
		forInput = "";

		if (typ == "arr") { //the type of data passed is either an array or a single digit
			if (theData[0].length > 0) {
				for (var i = theData[0].length - 1; i >= 0; i--) {
					
					updatedData = current.split(";");
					if(updatedData.indexOf(theData[0][i].id.toString()) < 0){
						forInput += theData[0][i].id + ";";
						$(".procb[value='"+ theData[0][i].id +"']").prop('checked', true);
						checkOrNotThisCB(soid, theData[0][i].id, "spo", 1);
					}
					
				}
				theGivenArray.val(current + forInput);
			}
		}else{ //if a single digit is passed
			updatedData = current.split(";");
			if(updatedData.indexOf(theData.toString()) < 0){
				theGivenArray.val(current + theData + ";");
				checkOrNotThisCB(soid, theData, "spo", 1);
			}
		}
	}


	function removeFromArray(typ,soid,  theData, theInputArray){
		if (typ == "arr") {
			if (theData[0].length > 0) {
				for (var i = theData[0].length - 1; i >= 0; i--) {

					checkOrNotThisCB(soid, theData[0][i].id, "spo", 2);

					updatedData = theInputArray.val();
					updatedData = updatedData.split(";")
										.filter(item => item != theData[0][i].id)
										.join(";");
			 		theInputArray.val(updatedData);
				}
			}
		}else{
			updatedData = theInputArray.val();
			updatedData = updatedData.split(";")
								.filter(item => item != theData)
								.join(";");
	 		theInputArray.val(updatedData);
		}
	}


	function isIdInPiids(noid, reorderqty){
		thePI = $("#pi-checked");
		piids = transformPIIDS(thePI.val());

		if (piids.length > 0) {
			let obj = piids.find(item => item.id == noid);
			let index = piids.indexOf(obj);
			if (index >= 0) {
				return obj.qty;	
			}
		}

		return reorderqty;
	}

	function isSubItemChecked(theItemId, itemType){ //Item Types: 1-PI, 2-SPO, 3-BKO, 4-
		isChecked = "";
		switch(itemType){
			case 1:
				var piids = $("#pi-checked").val();
				isChecked = (isIdInArrayPIIDS(theItemId.toString()) != "-1" ? "checked='true'" : "");
				break;
			
			case 2:
				var spids = $("#sp-checked").val();
				isChecked = (isIdInArray(theItemId.toString(), spids) != "-1" ? "checked='true'" : "");
				break;

			case 2:
				var bkoids = $("#bk-checked").val();
				isChecked = (isIdInArray(theItemId.toString(), bkoids) != "-1" ? "checked='true'" : "");
				break;
			
			default:
				isChecked = "";
				break;
		}
		return isChecked;
	}

	function isIdInArray(id, array){
		theArray = array.split(";");

		if (theArray.length > 0) {
			let obj = theArray.find(item => item == id);
			let index = theArray.indexOf(obj);
			if (index >= 0) {
				return index;	
			}
		}

		return -1;
	}

	function isIdInArrayPIIDS(noid){
		thePI = $("#pi-checked");
		piids = transformPIIDS(thePI.val());

		if (piids.length > 0) {
			let obj = piids.find(item => item.id == noid);
			let index = piids.indexOf(obj);
			if (index >= 0) {
				return index;	
			}
		}

		return -1;
	}

	function appendSOIDs(){
		/*theSO = $("#so-checked");
		theSO.val("");*/

		thePI = $("#pi-checked");
		split1 = thePI.val().split(";");
		for (var i = split1.length - 1; i >= 0; i--) {
			if (split1[i].toString() != "") {
				noid = split1[i].split(",")[0];	
				theCB = $(".procb.item"+noid+"[value='no']");

				soid = theCB.data("soid");
			}
			
		}
	}

	function modifyEdit(soid, noid, qty){
		theOrderQty = $("#supotable-popup-input").val();
		if (theOrderQty <= qty) {
			$("#aid"+soid+"_"+noid).data("orderqty", theOrderQty).text(theOrderQty);	

			theCheckbox = $(".chr"+soid+".item"+noid+"[value='no']");

			theCheckbox.data("orderqty", theOrderQty);

			if (theCheckbox.prop("checked")) {
				// remove id in piids
				removeFromPI(soid, noid);

				//theCheckbox.prop('checked', false).removeAttr('checked');
				checkOrNotThisCB(soid, noid, "pi", 2);

				/*var d = table.row('.shown').data();
				d.pigo = "0";
				d.orderqty = "0";
				table.row('.shown').data(d).draw();*/
			}
		}
		destroyPopover();
	}


	$(function(){
		

		var piids = $("#pi-checked");
		var spids = $("#sp-checked");

		

		$(".navmenuitemlist li").removeClass('active').eq(1).addClass('active');
		$(".navsubmenuitemlist li").removeClass('active').eq(0).addClass('active');

		$('#supotable').popover({
            selector: '[class="supotable-popup"]',
            content: 
            		"<input type='number' min='1' class='form-control input-sm' id='supotable-popup-input'>&nbsp;"+
            		"<button class='btn btn-success btn-sm' id='supotable-popup-button-yes' onclick=''><i class='fa fa-check'></i></button>&nbsp;"+
            		"<button class='btn btn-danger btn-sm' id='supotable-popup-button-no' onclick='javascript:destroyPopover()'><i class='fa fa-times'></i></button>",
            html: true,
            placement: 'top',
            trigger: 'click'
        });

        $('#supotable').on('shown.bs.popover', '.supotable-popup', function(event) {
        	event.preventDefault();
        	theSource = $(this);
        	reorderqty = theSource.data("reorderqty");
        	orderqty = theSource.data("orderqty");
        	noid = theSource.data("noid");
        	soid = theSource.data("soid");

        	theInputBox = $("#supotable-popup-input");
        	theInputBox.val(orderqty);
        	theInputBox.focus();

        	$("#supotable-popup-button-yes").attr("onclick", "javascript:modifyEdit("+soid+", "+noid+", "+reorderqty+");");
        });

		var table = $('#supotable')
			.on( 'init.dt', function () {
		        $("#supotable tbody").removeClass('hidden');
		        $("#loadersuppo").addClass('hidden');
		    })
			.DataTable({
				"ajax": '/parts/public/json/supplierordersjson.json',
				"columns": [
			            {
			                "className":'details-control',
			                "orderable":      false,
			                "data":           null,
			                "defaultContent": ''
			            }, //0
			            { "data": "created_at" }, //1
			            { "data": "parentcode" }, //2
			            { "data": "suppcode" }, //3
			            { "data": "partsku" }, //4
			            { "data": "reorderqty" }, //5
			            { "data": "backorder" }, //6
			            { "data": "openbox" }, //7
			            { "data": "specialorder" }, //8
			            { "data": "rap" }, //9
			            { "data": "sys_total" }, //10
			            { "data": "descr" }, //11

			            { "data": "id" }, //12
			            { "data": "sys_finalstatus" }, //13

			            { "data": "orderqty" }, //14
			            { "data": "pigo" }, //15
			            { "data": "bkogo" }, //16
			            { "data": "opbgo" }, //17
			            { "data": "spogo" }, //18
			            { "data": "rapgo" }, //19

			            {
			            	"orderable":false,
			            	"visible": false,
			            	"data":"stocksid",
			            }, //20 - PIs
			            {
			            	"orderable":false,
			            	"visible": false,
			            	"data":"backordersid",
			            }, //21 - BKOs
			            {
			            	"orderable":false,
			            	"visible": false,
			            	"data":"stocksid",
			            }, //22 - OBPs
			            {
			            	"orderable":false,
			            	"visible": false,
			            	"data":"specialordersid",
			            }, //23 - SPOs
			            {
			            	"orderable":false,
			            	"visible": false,
			            	"data":"stocksid",
			            }, //24 - RAPs
			            { 
			            	"className": 'select-checkbox',
			                "orderable":      false,
			                "data":           null,
			                "defaultContent": ''
			            } //last
		            ],
		        "columnDefs": [
			        	{
			            	"targets": [1],
			            	"render": function ( data, type, full, meta ) {
			            		var d = new Date(data);

								return formatThisDate(d);
						    }
			            },
			            {
			                "targets": [ 11, 12, 15, 16,17,18,19 ],
			                "visible": false,
			                "searchable": true
			            },
		            
		        	],
		        "select": {
		        	style: 'multi',
		        	selector: 'td:last-child'
		        },
		        dom: 'Bfrtip',
	       		"buttons": [
			            {
				            extend: 'selected',
				            text: 'Proceed with Order',
				            action: function ( e, dt, button, config ) {
				                //alert( dt.rows( { selected: true } ).indexes().length +' row(s) selected' );
				                
				                $("#so-checked").val(JSON.stringify(dt.rows({selected: true}).data().toArray()));
				                $("#supplierordersform").submit();
				            }
				        }	
			        ],
	            "order": [[1, 'asc']],
	        	"deferRender": true

		});

		table.on( 'select', function ( e, dt, type, indexes ) {
		    if ( type === 'row' ) {
		    	var soData = table.rows( indexes ).data().pluck( 'id' );

		    	var pxObj = {};
		 		var px = table.rows( indexes ).data().pluck( 'stocksid' );
		 		for (var i = px[0].length - 1; i >= 0; i--) {
		 			orderqty = px[0][i].reorderqty;
		 			theCB = $(".chr"+soData[0]+".item"+px[0][i].id+"[value='no']");
		 			if (theCB.length > 0) {
		 				orderqty = theCB.data("orderqty");
		 			}
		 			
		 			pxObj = {
		 				soid: soData[0],
		 				noid: px[0][i].id,
		 				orderqty: orderqty,
		 				reorderqty: px[0][i].reorderqty,
		 			};
		 		}

		 		if (px[0].length > 0) {
		 			
			 			addToPI(pxObj);
			 			
			 			var d = table.row(indexes).data();
						d.pigo = "1";
						d.orderqty = pxObj.orderqty;
						table.row(indexes).data(d).draw();
					
		 		}

		 		var sx = table.rows( indexes ).data().pluck( 'specialordersid' );

		 		addToArray("arr",soData[0], sx, spids);

				if (sx[0].length > 0) {
		 			for (var i = sx[0].length - 1; i >= 0; i--) {
		 				
			 				var d = table.row(indexes).data();
							d.spogo == "0" ? d.spogo = sx[0][i].id + ";" : d.spogo += sx[0][i].id + ";";
							table.row(indexes).data(d).draw();
						
		 			}
		 		}


		 		var bx = table.rows( indexes ).data().pluck( 'backordersid' );

		 		addToArray("arr",soData[0], bx, spids);

				if (bx[0].length > 0) {
		 			for (var i = bx[0].length - 1; i >= 0; i--) {
		 				
			 				var d = table.row(indexes).data();
							d.bkogo == "0" ? d.bkogo = bx[0][i].id + ";" : d.bkogo += bx[0][i].id + ";";
							table.row(indexes).data(d).draw();
						
		 			}
		 		}

		    }
		} );

		table.on( 'deselect', function ( e, dt, type, indexes ) {
		    if ( type === 'row' ) {
		    	var soData = table.rows( indexes ).data().pluck( 'id' );

		 		var dataPI = table.rows( indexes ).data().pluck( 'stocksid' );
		 		if (dataPI[0].length > 0) {
		 			removeFromPI(soData[0], dataPI[0][0].id);

		 			var d = table.row(indexes).data();
					d.pigo = "0";
					d.orderqty = "0";

					table.row(indexes).data(d).draw();
		 		}
		 			
		 		var dataSp = table.rows( indexes ).data().pluck( 'specialordersid' );
		 		removeFromArray("arr",soData[0], dataSp, spids);

		 		if (dataSp[0].length > 0) {
		 			for (var i = dataSp[0].length - 1; i >= 0; i--) {
		 				var d = table.row(indexes).data();
						d.spogo = d.spogo.split(";")
								.filter(item => item.toString() != dataSp[0][i].id.toString())
								.join(";");
						if (d.spogo == "") {
							d.spogo = "0";
						}
						table.row(indexes).data(d).draw();
		 			}
		 		}


		 		var dataBko = table.rows( indexes ).data().pluck( 'backordersid' );
		 		removeFromArray("arr",soData[0], dataBko, spids);

		 		if (dataBko[0].length > 0) {
		 			for (var i = dataBko[0].length - 1; i >= 0; i--) {
		 				var d = table.row(indexes).data();
						d.bkogo = d.bkogo.split(";")
								.filter(item => item.toString() != dataBko[0][i].id.toString())
								.join(";");
						if (d.bkogo == "") {
							d.bkogo = "0";
						}
						table.row(indexes).data(d).draw();
		 			}
		 		}
		    }
		} );

		$("#supotable_filter").closest("div").addClass('text-right');
		$("#supotable_wrapper").find("div.row").first().addClass('text-left');


		$('#supotable tbody').on('click', 'td.details-control', function () {
		    var tr = $(this).closest('tr');
		    var row = table.row( tr );
		 
		    if ( row.child.isShown() ) {
		        // This row is already open - close it
		        row.child.hide();
		        tr.removeClass('shown');
		    }
		    else {
		        if ( table.row( '.shown' ).length ) {
					$('.details-control', table.row( '.shown' ).node()).click();
				}

				row.child( format(row.data()) ).show();
				tr.addClass('shown');
		    }
		});


		$("#supotable tbody").on('change', '.procb', function(event) {
			event.preventDefault();

			cname = $("."+this.className.split(" ")[1]);
			len = cname.length;
			countOfChecked = 0;
				
			cname.each(function(index, el) {
				if (el.checked) {
					countOfChecked++;
				}
			});


			if (this.checked) {
				
				if (countOfChecked == len) {
					tdText = this.value;

					if (tdText == "spo") {
						thisSoid = $(this).data("soid");
						thisSpoid = $(this).data("spoid");

						var d = table.row('.shown').data();
						d.spogo == "0" ? d.spogo = thisSpoid + ";" : d.spogo += thisSpoid + ";";
						table.row('.shown').data(d).draw();

					}else if(tdText == "no"){
						thisSoid = $(this).data("soid");
						thisNoid = $(this).data("noid");
						thisOrderqty = $(this).data("orderqty");
						thisReorderqty = $(this).data("reorderqty");

						var d = table.row('.shown').data();
						d.pigo = "1";
						d.orderqty = thisOrderqty;
						table.row('.shown').data(d).draw();
					} else if (tdText == "bko") {
						thisSoid = $(this).data("soid");
						thisBkoid = $(this).data("bkoid");

						var d = table.row('.shown').data();
						d.bkogo == "0" ? d.bkogo = thisBkoid + ";" : d.bkogo += thisBkoid + ";";
						table.row('.shown').data(d).draw();

					}

					table.row('.shown', { page: 'current' }).select();
				}else{
					tdText = this.value;
					
					if (tdText == "spo") {
						thisSoid = $(this).data("soid");
						thisSpoid = $(this).data("spoid");
						addToArray("single", thisSoid, thisSpoid, spids);
						
						var d = table.row('.shown').data();
						d.spogo == "0" ? d.spogo = thisSpoid + ";" : d.spogo += thisSpoid + ";";
						table.row('.shown').data(d).draw();

					}else if(tdText == "no"){
						thisSoid = $(this).data("soid");
						thisNoid = $(this).data("noid");
						thisOrderqty = $(this).data("orderqty");
						thisReorderqty = $(this).data("reorderqty");
						pxObj = {
			 				soid: thisSoid,
			 				noid: thisNoid,
			 				orderqty: thisOrderqty,
			 				reorderqty: thisReorderqty,
			 			};
						addToPI(pxObj);	

						var d = table.row('.shown').data();
						d.pigo = "1";
						d.orderqty = thisOrderqty;
						table.row('.shown').data(d).draw();

					}else if (tdText == "bko") {
						thisSoid = $(this).data("soid");
						thisBkoid = $(this).data("bkoid");
						addToArray("single", thisSoid, thisBkoid, spids);
						
						var d = table.row('.shown').data();
						d.bkogo == "0" ? d.bkogo = thisBkoid + ";" : d.bkogo += thisBkoid + ";";
						table.row('.shown').data(d).draw();

					}
				}

			}else{ //if unchecked
				if (countOfChecked>0) {
					tdText = this.value;
					if (tdText == "spo") {
						thisSoid = $(this).data("soid");
						thisSpoid = $(this).data("spoid");
						removeFromArray("single",thisSoid, thisSpoid, spids);

						var d = table.row('.shown').data();
						
						d.spogo = d.spogo.split(";")
								.filter(item => item.toString() != thisSpoid.toString())
								.join(";");

						if (d.spogo == "") {
							d.spogo = "0";
						}

						table.row('.shown').data(d).draw();
					}else if(tdText == "no"){
						//removeFromPI(this.className.split(" ")[1].split("chr")[1], this.value, piids);		
						thisSoid = $(this).data("soid");
						thisNoid = $(this).data("noid");

						removeFromPI(thisSoid, thisNoid);

						var d = table.row('.shown').data();
						d.pigo = "0";
						d.orderqty = "0";
						table.row('.shown').data(d).draw();
					}else if (tdText == "bko") {
						thisSoid = $(this).data("soid");
						thisSpoid = $(this).data("bkoid");
						removeFromArray("single",thisSoid, thisSpoid, spids);

						var d = table.row('.shown').data();
						
						d.bkogo = d.bkogo.split(";")
								.filter(item => item.toString() != thisSpoid.toString())
								.join(";");

						if (d.bkogo == "") {
							d.bkogo = "0";
						}

						table.row('.shown').data(d).draw();
				}else{
					tdText = this.value;

					if (tdText == "spo") {
						var d = table.row('.shown').data();
						d.spogo = "0";
						table.row('.shown').data(d).draw();

					}else if(tdText == "no"){
						var d = table.row('.shown').data();
						d.pigo = "0";
						d.orderqty = "0";
						table.row('.shown').data(d).draw();
					}else if (tdText == "spo") {
						var d = table.row('.shown').data();
						d.bkogo = "0";
						table.row('.shown').data(d).draw();

					}


					table.row('.shown', { page: 'current' }).deselect();
				}
				}
			}

		});

		



	});